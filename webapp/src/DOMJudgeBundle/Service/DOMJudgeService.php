<?php declare(strict_types=1);

namespace DOMJudgeBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use DOMJudgeBundle\Entity\AuditLog;
use DOMJudgeBundle\Entity\Configuration;
use DOMJudgeBundle\Entity\Contest;
use DOMJudgeBundle\Entity\ContestProblem;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\Team;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\Language;
use DOMJudgeBundle\Entity\Role;
use DOMJudgeBundle\Entity\TestcaseWithContent;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Entity\Semester;
use DOMJudgeBundle\Entity\Topic;
use DOMJudgeBundle\Entity\Userproblem;
use DOMJudgeBundle\Utils\Utils;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use DOMJudgeBundle\Utils\FlsConstant;
use ZipArchive;

class DOMJudgeService
{
    protected $em;
    protected $logger;
    protected $request;
    protected $container;
    protected $hasAllRoles = false;
    /** @var Configuration[] */
    protected $configCache = [];

    const DATA_SOURCE_LOCAL = 0;
    const DATA_SOURCE_CONFIGURATION_EXTERNAL = 1;
    const DATA_SOURCE_CONFIGURATION_AND_LIVE_EXTERNAL = 2;

    const CONFIGURATION_DEFAULT_PENALTY_TIME = 20;

    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        RequestStack $requestStack,
        Container $container
    ) {
        $this->em        = $em;
        $this->logger    = $logger;
        $this->request   = $requestStack->getCurrentRequest();
        $this->container = $container;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Query configuration variable, with optional default value in case
     * the variable does not exist and boolean to indicate if cached
     * values can be used.
     *
     * When $name is null, then all variables will be returned.
     * @param string|null $name
     * @param mixed       $default
     * @param bool        $onlyIfPublic
     * @return Configuration[]|mixed
     * @throws \Exception
     */
    public function dbconfig_get($name, $default = null, bool $onlyIfPublic = false)
    {
        if (empty($this->configCache)) {
            $configs           = $this->em->getRepository('DOMJudgeBundle:Configuration')->findAll();
            $this->configCache = [];
            foreach ($configs as $config) {
                $this->configCache[$config->getName()] = $config;
            }
        }

        if (is_null($name)) {
            $ret = [];
            foreach ($this->configCache as $config) {
                if (!$onlyIfPublic || $config->getPublic()) {
                    $ret[$config->getName()] = $config->getValue();
                }
            }
            return $ret;
        }

        if (!empty($this->configCache[$name]) &&
            (!$onlyIfPublic || $this->configCache[$name]->getPublic())) {
            return $this->configCache[$name]->getValue();
        }

        if ($default === null) {
            throw new \Exception("Configuration variable '$name' not found.");
        }
        $this->logger->warning("Configuration variable '$name' not found, using default.");
        return $default;
    }

    /**
     * Return all the contests that are currently active indexed by contest ID
     * @param int|null $onlyofteam If -1, get only public contests. If > 0 get only contests for the given team
     * @param bool     $alsofuture If true, also get future contests
     * @return Contest[]
     */
    public function getCurrentContests($onlyofteam = null, bool $alsofuture = false)
    {
        $now = Utils::now();
        $qb  = $this->em->createQueryBuilder();
        $qb->select('c')->from('DOMJudgeBundle:Contest', 'c', 'c.cid');
        if ($onlyofteam !== null && $onlyofteam > 0) {
            $qb->leftJoin('c.teams', 'ct')
                ->andWhere('ct.teamid = :teamid OR c.public = 1')
                ->setParameter(':teamid', $onlyofteam);
            // $contests = $DB->q("SELECT * FROM contest
            //                     LEFT JOIN contestteam USING (cid)
            //                     WHERE (contestteam.teamid = %i OR contest.public = 1)
            //                     AND enabled = 1 ${extra}
            //                     AND ( deactivatetime IS NULL OR
            //                           deactivatetime > UNIX_TIMESTAMP() )
            //                     ORDER BY activatetime", $onlyofteam);
        } elseif ($onlyofteam === -1) {
            $qb->andWhere('c.public = 1');
            // $contests = $DB->q("SELECT * FROM contest
            //                     WHERE enabled = 1 AND public = 1 ${extra}
            //                     AND ( deactivatetime IS NULL OR
            //                           deactivatetime > UNIX_TIMESTAMP() )
            //                     ORDER BY activatetime");
        }
        $qb->andWhere('c.enabled = 1')
            ->andWhere($qb->expr()->orX(
                'c.deactivatetime is null',
                $qb->expr()->gt('c.deactivatetime', $now)
            ))
            ->orderBy('c.activatetime');

        if (!$alsofuture) {
            $qb->andWhere($qb->expr()->lte('c.activatetime', $now));
        }

        $contests = $qb->getQuery()->getResult();
        return $contests = "";
    }

    /**
     * Get the currently selected contest
     * @param int|null $onlyofteam If -1, get only public contests. If > 0 get only contests for the given team
     * @param bool     $alsofuture If true, also get future contests
     * @return Contest|null
     */
    public function getCurrentContest($onlyofteam = null, bool $alsofuture = false)
    {
        // $selected_cid = $this->request->cookies->get('domjudge_cid');
        // if ($selected_cid == -1) {
        //     return null;
        // }

        // $contests = $this->getCurrentContests($onlyofteam, $alsofuture);
        // foreach ($contests as $contest) {
        //     if ($contest->getCid() == $selected_cid) {
        //         return $contest;
        //     }
        // }
        // if (count($contests) > 0) {
        //     return reset($contests);
        // }
        return null;
    }

    /**
     * Get the currently selected contest
     * @param int|null $onlyofteam If -1, get only public contests. If > 0 get only contests for the given team
     * @param bool     $alsofuture If true, also get future contests
     * @return Contest|null
     */
    public function getCurrentClass($userId)
    {
        $key = $userId.'classes_id';
        $selected_cid = $this->getCookie($key);
        if ($selected_cid == -1 || $selected_cid == null) {
            return null;
        }
        $classes = $this->em->getRepository(Classes::class)->find($selected_cid);
        if($classes){
            return $classes;
        }
        return null;
    }

    	/**
     * Get the currently selected contest
     * @param int|null $onlyofteam If -1, get only public contests. If > 0 get only contests for the given team
     * @param bool     $alsofuture If true, also get future contests
     * @return Contest|null
     */
    public function getCurrentClassByClassID()
    {
        $selected_cid = $this->getCookie('classes_id');
        if ($selected_cid == -1 || $selected_cid == null) {
            return null;
        }
        $classes = $this->em->getRepository(Classes::class)->find($selected_cid);
        if($classes){
            return $classes;
        }
        return null;
    }

    /**
     * 
     */
    public function getCurrentSemester()
    {
        $currentTime = Utils::now();
        $semester = $this->em->getRepository(Semester::class)
            ->createQueryBuilder('sem')
            ->where('sem.startedTime <= :currentTime')
            ->andwhere('sem.finishedTime >= :currentTime')
            ->setParameter(':currentTime', $currentTime)
            ->select('sem')
            ->getQuery()
            ->getResult();
        if($semester){
            return $semester;
        }else{
            return null;
        }        
    }

    /**
     * Get the contest with the given contest ID
     * @param int $cid
     * @return Contest|null
     */
    public function getContest($cid)
    {
        return $this->em->getRepository(Contest::class)->find($cid);
    }

    /**
     * Get the team with the given team ID
     * @param int $teamid
     * @return Team|null
     */
    public function getTeam($teamid)
    {
        return $this->em->getRepository(Team::class)->find($teamid);
    }

    /**
     * Get the problem with the given team ID
     * @param int $probid
     * @return Problem|null
     */
    public function getProblem($probid)
    {
        return $this->em->getRepository(Problem::class)->find($probid);
    }

    public function checkrole(string $rolename, bool $check_superset = true): bool
    {
        if ($this->hasAllRoles) {
            return true;
        }

        $user = $this->getUser();
        if ($user === null) {
            return false;
        }

        $authchecker = $this->container->get('security.authorization_checker');
        if ($check_superset) {
            // if ($authchecker->isGranted('ROLE_ADMIN') 
            //     && ($rolename == 'team' && $user->getTeam() != null)
            //     ) {
            //     return true;
            // }
        }
        return $authchecker->isGranted('ROLE_' . strtoupper($rolename));
    }

    public function getClientIp()
    {
        $clientIP = $this->container->get('request_stack')->getMasterRequest()->getClientIp();
        return $clientIP;
    }

    /**
     * Get the logged in user
     * @return User|null
     */
    public function getUser()
    {
        $token = $this->container->get('security.token_storage')->getToken();
        if ($token == null) {
            return null;
        }

        $user = $token->getUser();

        // Ignore user objects if they aren't a DOMJudgeBundle user
        // Covers cases where users are not logged in
        if (!is_a($user, 'DOMJudgeBundle\Entity\User')) {
            return null;
        }

        return $user;
    }

    /**
     * Get the value of the cookie with the given name
     * @param string $cookieName
     * @return mixed|null
     */
    public function getCookie(string $cookieName)
    {
        if (!$this->request->cookies) {
            return null;
        }
        return $this->request->cookies->get($cookieName);
    }

    /**
     * Set the given cookie on the response, returning the response again to allow chaining
     * @param string        $cookieName
     * @param string        $value
     * @param int           $expire
     * @param string|null   $path
     * @param string        $domain
     * @param bool          $secure
     * @param bool          $httponly
     * @param Response|null $response
     * @return Response
     */
    public function setCookie(
        string $cookieName,
        string $value = '',
        int $expire = 0,
        string $path = null,
        string $domain = '',
        bool $secure = false,
        bool $httponly = false,
        Response $response = null
    ) {
        if ($response === null) {
            $response = new Response();
        }
        if ($path === null) {
            $path = $this->request->getBasePath();
        }

        $response->headers->setCookie(new Cookie($cookieName, $value, $expire, $path, $domain, $secure, $httponly));

        return $response;
    }

    /**
     * Clear the given cookie on the response, returning the response again to allow chaining
     * @param string        $cookieName
     * @param string|null   $path
     * @param string        $domain
     * @param bool          $secure
     * @param bool          $httponly
     * @param Response|null $response
     * @return Response
     */
    public function clearCookie(
        string $cookieName,
        string $path = null,
        string $domain = '',
        bool $secure = false,
        bool $httponly = false,
        Response $response = null
    ) {
        if ($response === null) {
            $response = new Response();
        }
        if ($path === null) {
            $path = $this->request->getBasePath();
        }

        $response->headers->clearCookie($cookieName, $path, $domain, $secure, $httponly);
        return $response;
    }

    public function getUpdates(): array
    {
        $contest = $this->getCurrentContest();
        $currentUser = $this->getUser();

        $currentSemester = $this->getCurrentSemester();
        $submissions = '';
        if($currentSemester != null ) {
            $classes = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Classes', 'c')
                ->leftJoin('c.semester', 's')
                ->select('c')
                ->andWhere(':userid MEMBER OF c.userid')
                ->andWhere('s.semid = :semid')
                ->setParameter('userid', $currentUser)
                ->setParameter('semid', $currentSemester[0]->getSemid())
                ->getQuery()
                ->getResult();
            if($classes != null) {
                // $submissions = $this->em->createQueryBuilder()
                //     ->select('s.submitid, s.cid, c.cid, c.name')
                //     ->from('DOMJudgeBundle:Submission', 's')
                //     ->leftJoin('s.userproblem', 'up')
                //     ->leftJoin('s.classes', 'c')  
                //     ->where('s.classes IN (:class)')
                //     ->andWhere('up.status = 3')
                //     ->setParameter('class', $classes)
                //     ->getQuery()->getResult();

                $submissions = $this->em->createQueryBuilder()
                    ->select('c.cid, c.name')
                    ->from('DOMJudgeBundle:Userproblem', 'up')
                    ->leftJoin('up.cid', 'c') 
                    // ->where('up.cid = :class')
                    ->where('up.cid IN (:class)')
                    ->andWhere('up.status = 3')
                    ->setParameter('class', $classes)
                    ->getQuery()->getResult();
            }
            
        }        
        
        $clarifications = $this->em->createQueryBuilder()
                ->select('clar.clarid', 'clar.body')
                ->from('DOMJudgeBundle:Clarification', 'clar')
                ->andWhere('clar.receiver = :username')
                ->andWhere('clar.answered = 0')
                ->setParameter('username', $currentUser->getUsername())
                ->getQuery()->getResult();

        $judgehosts = $this->em->createQueryBuilder()
            ->select('j.hostname', 'j.polltime')
            ->from('DOMJudgeBundle:Judgehost', 'j')
            ->andWhere('j.active = 1')
            ->andWhere('j.polltime < :i')
            ->setParameter('i', time() - $this->dbconfig_get('judgehost_critical', 120))
            ->getQuery()->getResult();

        $rejudgings = $this->em->createQueryBuilder()
            ->select('r.rejudgingid, r.starttime, r.endtime')
            ->from('DOMJudgeBundle:Rejudging', 'r')
            ->andWhere('r.endtime is null')
            ->getQuery()->getResult();
        
        // $submissions = $this->em->createQueryBuilder()
        //     ->select('s.submitid, s.userid, u.name, p.shortname')
        //     ->from('DOMJudgeBundle:Submission', 's')
        //     ->leftJoin('s.problem', 'p')
        //     ->leftJoin('s.user', 'u')
        //     ->leftJoin('s.classes', 'c')
        //     ->leftJoin('s.userproblem', 'up')
        //     ->andWhere('up.status = 3')
        //     ->andWhere(':curUser MEMBER OF c.userid')
        //     ->setParameter('curUser', $currentUser)
        //     ->getQuery()->getResult();                          

        $internal_error = $this->em->createQueryBuilder()
            ->select('ie.errorid', 'ie.description')
            ->from('DOMJudgeBundle:InternalError', 'ie')
            ->andWhere('ie.status = :status')
            ->setParameter('status', 'open')
            ->getQuery()->getResult();

        return [
            'clarifications' => $clarifications,
            'submissions' => $submissions,
            'judgehosts' => $judgehosts,
            'rejudgings' => $rejudgings,
            'internal_error' => $internal_error,
        ];
    }

    public function getHttpKernel()
    {
        return $this->container->get('http_kernel');
    }

    /**
     * @return bool
     */
    public function getHasAllRoles(): bool
    {
        return $this->hasAllRoles;
    }

    /**
     * Run the given callable with all roles.
     *
     * This will result in all calls to checkrole() to return true.
     *
     * @param callable $callable
     */
    public function withAllRoles(callable $callable)
    {
        $this->hasAllRoles = true;
        $callable();
        $this->hasAllRoles = false;
    }

    /**
     * Log an action to the auditlog table
     *
     * @param string     $datatype
     * @param mixed      $dataid
     * @param string     $action
     * @param mixed|null $extraInfo
     * @param mixed|null $forceUsername
     * @param int|null   $cid
     */
    public function auditlog(
        string $datatype,
        $dataid,
        string $action,
        $extraInfo = null,
        $forceUsername = null,
        $cid = null
    ) {
        if (!empty($forceUsername)) {
            $user = $forceUsername;
        } else {
            $user = $this->getUser() ? $this->getUser()->getUsername() : null;
        }

        $auditLog = new AuditLog();
        $auditLog
            ->setLogtime(Utils::now())
            ->setCid($cid)
            ->setUser($user)
            ->setDatatype($datatype)
            ->setDataid($dataid)
            ->setAction($action)
            ->setExtrainfo($extraInfo);

        $this->em->persist($auditLog);
        $this->em->flush();
    }

    /**
     * Call alert plugin program to perform user configurable action on
     * important system events. See default alert script for more details.
     *
     * @param string $messageType
     * @param string $description
     */
    public function alert(string $messageType, string $description = '')
    {
        $alert = $this->container->getParameter('domjudge.libdir') . '/alert';
        system(sprintf('%s %s %s &', $alert, escapeshellarg($messageType), escapeshellarg($description)));
    }

    /**
     * Decode a JSON string and handle errors
     * @param string $str
     * @return mixed
     */
    public function jsonDecode(string $str)
    {
        $res = json_decode($str, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpException(500, sprintf("Error decoding JSON data '%s': %s", $str, json_last_error_msg()));
        }
        return $res;
    }

    /**
     * Decode a JSON string and handle errors
     * @param $data
     * @return string
     */
    public function jsonEncode($data): string
    {
        $res = json_encode($data);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpException(500, sprintf("Error encoding data to JSON: %s", json_last_error_msg()));
        }
        return $res;
    }

    /**
     * Dis- or re-enable what caused an internal error
     * @param array        $disabled
     * @param Contest|null $contest
     * @param bool|null    $enabled
     */
    public function setInternalError($disabled, $contest, $enabled)
    {
        switch ($disabled['kind']) {
            case 'problem':
                $this->em->createQueryBuilder()
                    ->update('DOMJudgeBundle:Problem', 'p')
                    ->set('p.allowJudge', ':enabled')
                    ->andWhere('p.contest = :cid')
                    ->andWhere('p.probid = :probid')
                    ->setParameter(':enabled', $enabled)
                    ->setParameter(':cid', $contest)
                    ->setParameter(':probid', $disabled['probid'])
                    ->getQuery()
                    ->execute();
                break;
            case 'judgehost':
                $this->em->createQueryBuilder()
                    ->update('DOMJudgeBundle:Judgehost', 'j')
                    ->set('j.active', ':active')
                    ->andWhere('j.hostname = :hostname')
                    ->setParameter(':active', $enabled)
                    ->setParameter(':hostname', $disabled['hostname'])
                    ->getQuery()
                    ->execute();
                break;
            case 'language':
                $this->em->createQueryBuilder()
                    ->update('DOMJudgeBundle:Language', 'lang')
                    ->set('lang.allowJudge', ':enabled')
                    ->andWhere('lang.langid = :langid')
                    ->setParameter(':enabled', $enabled)
                    ->setParameter(':langid', $disabled['langid'])
                    ->getQuery()
                    ->execute();
                break;
            default:
                throw new HttpException(500, sprintf("unknown internal error kind '%s'", $disabled['kind']));
        }
    }

    /**
     * Perform an internal API request to the given URL with the given data
     *
     * @param string $url
     * @param string $method
     * @param array  $queryData
     * @return mixed|null
     * @throws \Exception
     */
    public function internalApiRequest(string $url, string $method = Request::METHOD_GET, array $queryData = [])
    {
        $request  = Request::create('/api' . $url, $method, $queryData);
        $response = $this->getHttpKernel()->handle($request, HttpKernelInterface::SUB_REQUEST);

        $status = $response->getStatusCode();
        if ($status < 200 || $status >= 300) {
            $this->logger->warning(sprintf("executing internal %s request to url %s: http status code: %d, response: %s",
                                           $method, $url, $status, $response));
            return null;
        }

        return $this->jsonDecode($response->getContent());
    }

    /**
     * Get the etc directory of this DOMjudge installation
     * @return string
     */
    public function getDomjudgeEtcDir(): string
    {
        return $this->container->getParameter('domjudge.etcdir');
    }

    /**
     * Get the tmp directory of this DOMjudge installation
     * @return string
     */
    public function getDomjudgeTmpDir(): string
    {
        return $this->container->getParameter('domjudge.tmpdir');
    }

    /**
     * Get the submit directory of this DOMjudge installation
     * @return string
     */
    public function getDomjudgeSubmitDir(): string
    {
        return $this->container->getParameter('domjudge.submitdir');
    }

    /**
     * Get the webapp directory of this DOMjudge installation
     * @return string
     */
    public function getDomjudgeWebappDir(): string
    {
        return $this->container->getParameter('domjudge.webappdir');
    }

    /**
     * Open the given ZIP file
     * @param string $filename
     * @return ZipArchive
     */
    public function openZipFile(string $filename): ZipArchive
    {
        $zip = new ZipArchive();
        $res = $zip->open($filename, ZIPARCHIVE::CHECKCONS);
        if ($res === ZIPARCHIVE::ER_NOZIP || $res === ZIPARCHIVE::ER_INCONS) {
            throw new ServiceUnavailableHttpException(null, 'No valid zip archive given');
        } elseif ($res === ZIPARCHIVE::ER_MEMORY) {
            throw new ServiceUnavailableHttpException(null, 'Not enough memory to extract zip archive');
        } elseif ($res !== true) {
            throw new ServiceUnavailableHttpException(null, 'Unknown error while extracting zip archive');
        }

        return $zip;
    }

    /**
     * Legacy function to make print send method available outside
     * Symfony. Can be removed if the team interface uses Symfony.
     */
    public function sendPrint(...$args): array
    {
        return \DOMJudgeBundle\Utils\Printing::send(...$args);
    }

    /**
     * Get a ZIP with sample data
     *
     * @param ContestProblem $contestProblem
     * @return string Filename of the location of the temporary ZIP file. Make sure to remove it after use
     */
    public function getSamplesZip(ContestProblem $contestProblem)
    {
        /** @var TestcaseWithContent[] $testcases */
        $testcases = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:TestcaseWithContent', 'tc')
            ->join('tc.problem', 'p')
            ->join('p.contest_problems', 'cp', Join::WITH, 'cp.contest = :contest')
            ->select('tc')
            ->andWhere('tc.probid = :problem')
            ->andWhere('tc.sample = 1')
            ->andWhere('cp.allowSubmit = 1')
            ->setParameter(':problem', $contestProblem->getProbid())
            ->setParameter(':contest', $contestProblem->getCid())
            ->orderBy('tc.testcaseid')
            ->getQuery()
            ->getResult();


        $zip = new ZipArchive();
        if (!($tempFilename = tempnam($this->getDomjudgeTmpDir(), "export-"))) {
            throw new ServiceUnavailableHttpException(null, 'Could not create temporary file.');
        }

        $res = $zip->open($tempFilename, ZipArchive::OVERWRITE);
        if ($res !== true) {
            throw new ServiceUnavailableHttpException(null, 'Could not create temporary zip file.');
        }

        foreach ($testcases as $index => $testcase) {
            foreach (['input', 'output'] as $type) {
                $extension = substr($type, 0, -3);

                $filename = sprintf("%s.%s", $index + 1, $extension);
                $content  = null;

                switch ($type) {
                    case 'input':
                        $content = $testcase->getInput();
                        break;
                    case 'output':
                        $content = $testcase->getOutput();
                        break;
                }

                $zip->addFromString($filename, $content);
            }
        }

        $zip->close();

        return $tempFilename;
    }

    /**
     * check class is active
     */
    public function isActiveCLass($classId)
    {
        $class = $this->em->getRepository(Classes::class)->find($classId);

        
        if($class->getStarttimeEnabled()){
            return true;
        }else{
            $currentTime = Utils::now();
            if($currentTime > $class->getStarttime() && $currentTime < $class->getEndtime() ){
                return true;
            }else{
                return false;
            }
        }

    }

    /**
     * random problem for class
     * 
     * @param interger $classid
     * @return array
     */
    public function genarateProblemForClass($classid)
    {
        return $this->genarateProblem($classid);
    }

    /**
     * random problem for one student
     * 
     * @param interger $classid, $studentId
     * @return array
     */
    public function genarateProblemForStudent($classid, $studentId)
    {
        return $this->genarateProblem($classid, $studentId);
    }

    /**
     * get Require Line of Code of language
     * 
     * @param string $langid
     */
    public function getRequireLoc(string $langid)
    {
        $language  = $this->em->getRepository(Language::class)->createQueryBuilder('l')
                ->select('l')
                ->where('l.langid = :langid')
                ->setParameter(':langid', $langid)
                ->getQuery()
                ->getResult();
        return ( $language[0] != null ) ? $language[0]->getRequireLoc():0;
    }

    /**
     * random problem for student
     * 
     * @param interger $classid
     * 
     */
    protected function genarateProblem($classid, $studentId = null)
    {
        $listProStu = [];

        // get language id of class
        $class = $this->em->getRepository(Classes::class)->find($classid);
        if(!$class){
            throw new NotFoundHttpException(sprintf('Class have id %d not found',$classid));
        }
        $langid = $class->getlangid()->getLangid();
        if(!$langid){
            throw new NotFoundHttpException(sprintf('Language of class %d not found',$class->getName()));
        }
        // get config total log to pass
        $configLoc = $this->getRequireLoc($langid);
        
        // get list Difficulty
        $listDifficult = array_column($this->getListDifficult($langid), 'difficult');
        if(empty($listDifficult)){
            $listDifficult[0] = 0;
        }
        // get list problem by difficult
        foreach ($listDifficult as $difficulty) {
            $listProblem[$difficulty] = $this->getListProByDif($difficulty, $langid);
        }
        if(empty($listProblem) or (isset($listProblem[0]) && empty($listProblem[0])) ){
            throw new NotFoundHttpException(sprintf('Not have problem in class %s!',$class->getName()));
        }
        // get number topic of language
        $listTopic = $this->em->getRepository(Topic::class)->createQueryBuilder('t')
            ->innerjoin('t.probid','p')
            ->where('p.langid = :langid')
            ->setParameter(':langid', $langid)
            ->select('t')
            ->getQuery()
            ->getResult();

        // get list student of class
        if($studentId != null){
            $listStudent[0] = $this->em->getRepository(User::class)->find($studentId);
        }else{
            $listStudent = $class->getUserid();
        }
        
        $tempTopic = [];
        // generate problem for each student
        foreach ($listStudent as $key => $student) {
            // check teacher
            if($this->isTeacher($student)){
                continue;
            }

            $totalLoc = $this->getTotalLoc($student->getUserid(),$class->getCid());
            if($totalLoc > $configLoc){
                continue;
            }
            $listProTemp = $this->verifyProblemUser($listProblem,$student->getUserid(),$class->getCid());;
            
            $tempDif = $listDifficult;
            $out = [];
            $tempResult = [];
            // fillter level of problem for student
            while ($configLoc > $totalLoc){
                // break if not enough problem
                if(count($out) == count($listDifficult)){
                    break;
                }else{
                    // get list check level if it is null
                    // Skip level if the level is not enough problem
                    if(count($tempDif) == 0){
                        $tempDif = array_diff($listDifficult, $out);
                    }
                }
                
                $tempDifIndex = array_rand($tempDif,1);
                if(count($listProTemp[$tempDif[$tempDifIndex]]) == 0){
                    $out[] = $tempDif[$tempDifIndex];
                    unset($tempDif[$tempDifIndex]);
                    continue;
                }
                $tempResult[] = $listProStu[] = $this->ramdomProblem(
                    $listProTemp[$tempDif[$tempDifIndex]],
                    $totalLoc,
                    $tempTopic,
                    $listTopic,
                    $student);
                unset($tempDif[$tempDifIndex]);
            }
            unset($tempTopic);
            $tempTopic = [];
            $noOfRan = ceil(count($tempResult) / 2);
            for ($i=0; $i < $noOfRan; $i++) {
                $indexResult = array_rand($tempResult);
                $proIndex = array_search($tempResult[$indexResult],$listProStu);
                $listProStu[$proIndex]['review'] = 1;
                unset($tempResult[$indexResult]);
            }
            unset($tempResult);
        }
        return $listProStu;        
    }

    /**
     * @param ORM/ entity/user
     * 
     * check role teacher
     */
    public function isTeacher($user)
    {
        $roles = $user->getRoles();
        foreach ($roles as $role) {
            if($role->getRole() == 'ROLE_TEACHER'){
                return true;
            }
        }
        return false;
    }

    /**
     * remove all problem dulicate with user problem
     */
    public function verifyProblemUser($listPro, $studentId, $classId )
    {
        $proOfStudent = $this->getListProOfStudent($studentId, $classId);
        foreach ($listPro as $key => $value) {
            $prob = array();
            foreach($proOfStudent as $p) {
                $prob[] = $p->getProbid();
            }
            // filter array using gathered
            $new = array_filter($value, function($v) use($prob){
                return !in_array($v->getProbid(), $prob);
            });
            $listPro[$key] = $new;
        }
        return $listPro;
    }

    /**
     * validate student to add class
     */
    public function validateStudent($classes, $student)
    {
        $listClass = $student->getCid();
        $semester = $classes->getSemester();
        $language = $classes->getLangid();
        
        foreach ($listClass as $c) {
            // student exists in class
            if($c->getCid() == $classes->getCid()){
                return 'Student '.$student->getName().' were exists in class '.$classes->getName();
            }
            // student exists Language in semester  
            if($c->getSemester()->getSemid() == $semester->getSemid() 
                && $c->getLangid()->getLangid() == $language->getLangid()){
                    return 'Student '.$student->getName().' have '.$language->getName().' class in this semester!';  
            }
        }
        return true;
    }

    /**
     * random problem
     *  
     */
    protected function ramdomProblem(&$temp,&$totalLoc,&$tempTopic,$listTopic,$student)
    {
        // reset list temp topic
        if(count($tempTopic) == 0){
            $tempTopic = $listTopic;
        }
        // random topic for student
        $randTopicIndex = array_rand($tempTopic,1);
        $randTopic = $tempTopic[$randTopicIndex];

        //fillter list problem by topic
        $tempProTop = array_filter($temp, function($v, $k) use($randTopic) {
            $listTopOfPro = $v->getTid();
            if(count($listTopOfPro) > 0){
                if(in_array($randTopic,$listTopOfPro->toarray())){
                    return $v;
                }
            }else{
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH);
        if(count($tempProTop) == 0){
            $tempProTop = $temp;
        }else{
            // Remove topic if at least 1 problem have topic is selected
            unset($tempTopic[$randTopicIndex]);
        }
        // random problem id in list
        $randProbIndex = array_rand($tempProTop,1);
        $listProStu = [
            'student' => $student,
            'problem' => $tempProTop[$randProbIndex],
            'review' => 0
        ];
        $totalLoc += (integer) $tempProTop[$randProbIndex]->getPoints();

        
        // remove problem in list        
        unset($temp[array_search($tempProTop[$randProbIndex], $temp)]);
        unset($tempProTop);

        return $listProStu;
    }

    /**
     * get all problem by difficult and language id 
     * 
     * @param integer $difficult, $langid
     * @return array $listProblem
     */
    protected function getListProByDif($difficult, $langid){
        // get all problem by points and language id 
        $listProblem = $this->em->getRepository(Problem::class)->createQueryBuilder('p')
            ->where('p.langid = :langid')
            ->andwhere('p.difficult = :difficult')
            ->setParameter('langid', $langid)
            ->setParameter('difficult', $difficult)
            ->select('p')
            ->getQuery()
            ->getResult();

        return $listProblem;
    }

    /**
     * get all problem by userid and class id 
     * 
     * @param integer $studentId, $classId
     * @return array $listProblem
     */
    protected function getListProOfStudent($studentId, $classId)
    {
        $listProblem = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Problem','p')
            ->join('DOMJudgeBundle:Userproblem', 'up', Join::WITH, 'up.probid = p.probid')
            ->where('up.cid = :cid')
            ->andwhere('up.userid = :userid')
            ->setParameter('cid', $classId)
            ->setParameter('userid', $studentId)
            ->select('p')
            ->getQuery()
            ->getResult();

        return $listProblem;
    }

    /**
     * get all problem by difficult and language id 
     * 
     * @param integer $langid
     * @return array $listDifficult
     */
    protected function getListDifficult($langid){
        // get all problem by points and language id 
        $listDifficult = $this->em->getRepository(Problem::class)->createQueryBuilder('p')
            ->where('p.langid = :langid')
            ->setParameter(':langid', $langid)
            ->select('DISTINCT p.difficult')
            ->getQuery()
            ->getResult();

        return $listDifficult;
    }

    public function getTotalLoc($studentId, $classId){
        $query = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
            ->innerjoin('up.probid','p')
            ->where('up.userid = :userid')
            ->setParameter('userid', $studentId)
            ->andwhere('up.cid = :cid')
            ->andwhere('up.status != 5')
            ->andwhere('up.changed != 2')
            ->setParameter('cid', $classId)
            ->select('sum(p.points) as loc')
            ->getQuery();
        $loc = $query->getOneOrNullResult();
        return ($loc['loc'] != null) ? $loc['loc'] : 0;
    }

    public function getteacherByClassId($classId) {
        $teacher = $this->em->createQueryBuilder()
            ->select('u.userid','u.username')
            ->from('DOMJudgeBundle:Classes', 'c')
            ->leftJoin('c.userid', 'u')
            ->leftJoin('u.roles', 'r')
            ->andWhere('c.cid = :cid')
            ->andWhere('r.dj_role = :role')
            ->setParameter('cid', $classId)
            ->setParameter('role', 'teacher')
            ->getQuery()->getResult();

        return $teacher;
    }

    //check student position
    public function checkStudentPosition($userId, $classId) {
        $clientIP = $this->getClientIp();
        $uipid = $this->em->createQueryBuilder()
            ->select('rip.pccode', 'rip.ipAddress')
            ->from('DOMJudgeBundle:Userip', 'uip')
            ->leftJoin('uip.roomip', 'rip')
            ->andWhere('uip.cid = :cid')
            ->andWhere('uip.userid = :userid')
            // ->andWhere('rip.ipAddress = :ipAddress')
            ->setParameter('cid', $classId)
            ->setParameter('userid', $userId)
            // ->setParameter('ipAddress', $clientIP)
            ->getQuery()->getResult();
        if($uipid != null) {
            if($uipid[0]['ipAddress'] == $clientIP) {
                return true;
            }else {
                //return pc code
                return $uipid[0]['pccode'];
            }            
        }else {
            //when student's position not set in this class 
            return false;
        }
    }
}
