<?php

declare(strict_types=1);

namespace DOMJudgeBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Role;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Entity\ClassSchedule;
use DOMJudgeBundle\Entity\Language;
use DOMJudgeBundle\Entity\Leaderboard;
use DOMJudgeBundle\Entity\Semester;
use DOMJudgeBundle\Entity\TimeSlot;
use DOMJudgeBundle\Entity\LabRoom;
use DOMJudgeBundle\Form\Type\GeneratePasswordsType;
use DOMJudgeBundle\Form\Type\Flab\ClassType;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Utils\Utils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\classnamePasswordToken;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use ZipArchive;
use \Datetime;

/**
 * @Route("/admin/classes")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ClassController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    private $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em              = $em;
        $this->dj              = $dj;
        $this->eventLogService = $eventLogService;
        $this->tokenStorage    = $tokenStorage;
    }

    /**
     * @Route("/{classId}/edit", name="admin_class_edit", requirements={"classId": "^$|\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param int     $classId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, int $classId)
    {
        /** @var class $class */
        $class = $this->em->getRepository(classes::class)->find($classId);
        if (!$class) {
            throw new NotFoundHttpException(sprintf('class with ID %s not found', $classId));
        }

        $form = $this->createForm(classType::class, $class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveEntity(
                $this->em,
                $this->eventLogService,
                $this->dj,
                $class,
                $class->getclassid(),
                false
            );

            // If we save the currently logged in used, update the login token
            if ($class->getclassid() === $this->dj->getclass()->getclassid()) {
                $token = new classnamePasswordToken(
                    $class,
                    null,
                    'main',
                    $class->getRoles()
                );

                $this->tokenStorage->setToken($token);
            }

            return $this->redirect($this->generateUrl(
                'admin_class',
                ['classId' => $class->getCid()]
            ));
        }

        return $this->render('@DOMJudge/Fls/Admin/Fls/class_edit.html.twig', [
            'class' => $class,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/detail_class/{cid}", name="admin_class_detail", requirements={"cid": "^$|\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param int     $cid
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function detailClass(Request $request, int $cid)
    {
        $data = [];
        /** @var User[] $users */
        $users = $this->em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->select('u')
            ->innerJoin('u.cid', 'uc')
            ->where('uc.cid = :cid')
            ->setParameter('cid', $cid)
            ->getQuery()->getResult();

        $students = $this->em->createQueryBuilder()
            ->select('u', 'r')
            ->from('DOMJudgeBundle:User', 'u')
            ->leftJoin('u.roles', 'r')
            ->where('r.dj_role = :dj_role')
            ->setParameter('dj_role', "student")
            ->getQuery()->getResult();

        $listUsers = [];
        $listStudents = [];
        //Add list users to Dom
        foreach ($users as $c => $user) {
            $temp['cId'] = $cid;
            $temp['uId'] = $user->getUserid();
            $temp['uUsername'] = $user->getUsername();
            $temp['uName'] = $user->getName();
            $temp['uEmail'] = $user->getEmail();
            $roles = $user->getRoles();
            foreach ($roles as $role) {
                if($role->getRole() == "ROLE_STUDENT"){
                    $temp['role'] = $role->getDjRole();
                }
                if($role->getRole() == "ROLE_TEACHER"){
                    $temp['role'] = $role->getDjRole();
                }
            }
            $listUsers[] = $temp;
            unset($temp);
        }
        //Add list students to Dom
        foreach ($students as $c => $student) {
            $temp['cId'] = $cid;
            $temp['uId'] = $student->getUserid();
            $temp['uUsername'] = $student->getUsername();
            $temp['uName'] = $student->getName();
            $temp['uEmail'] = $student->getEmail();
            $listStudents[] = $temp;
            unset($temp);
        }

        $data['cid'] = $cid;
        $data['classMenu'] = $this->em->getRepository(Classes::class)->find($cid);
        $data['moreClass'] = true;
        $data['listUsers'] = json_encode($listUsers);
        $data['listStudents'] = json_encode($listStudents);
        return $this->render('@DOMJudge/Fls/Admin/class_detail.html.twig', $data);
    }

    /**
     * @Route("/search/student", name="search_student")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajaxSearchStudent(Request $request)
    {
        $dataRequest = json_decode($request->getContent());
        if(isset($dataRequest->searchText)){
            $textSearch = $dataRequest->searchText;
            $listStudent = $this->em->getRepository(User::class)->createQueryBuilder('u')
                ->where('u.username LIKE :textSearch or u.name LIKE :textSearch')
                ->setParameter('textSearch','%'.$textSearch.'%')
                ->join('u.roles','r')
                ->andwhere('r.dj_role = :role')
                ->setParameter('role','student')
                ->select('u.name as name, u.username as username, u.userid as id, u.email as email')
                ->getQuery()->getResult();
            return $this->json(["result" => $listStudent]);
        }
        return $this->json(["result" => null ]);
    }

    /**
     * @Route("/search/teacher", name="search_teacher")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajaxSearchTeacher(Request $request)
    {
        $dataRequest = json_decode($request->getContent());
        if(isset($dataRequest->searchText)){
            $textSearch = $dataRequest->searchText;
            $listStudent = $this->em->getRepository(User::class)->createQueryBuilder('u')
                ->where('u.username LIKE :textSearch or u.name LIKE :textSearch')
                ->setParameter('textSearch','%'.$textSearch.'%')
                ->join('u.roles','r')
                ->andwhere('r.dj_role = :role')
                ->setParameter('role','teacher')
                ->select('u.name as name, u.username as username, u.userid as id, u.email as email')
                ->getQuery()->getResult();
            return $this->json(["result" => $listStudent]);
        }
        return $this->json(["result" => null ]);
    }

    /**
     * @Route("/detail_class_del", name="admin_class_detail_del")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function detailClassDel(Request $request)
    {
        $class = new Classes();
        $user = new User();
        $dataUsers  = $request->getContent();
        $arrDataUsers = json_decode($dataUsers);
        $qb = $this->em->createQueryBuilder();
        for ($i = 0; $i < count($arrDataUsers); $i++) {
            //delete student from class

            $class = $this->em->getRepository(Classes::class)->Find($arrDataUsers[$i]->cId);
            $user = $this->em->getRepository(User::class)->Find($arrDataUsers[$i]->uId);
            $class->removeUserid($user); //make sure the removeGroup method is defined in your User model. 
            $this->em->persist($class);
            //delete student from leaderboard
            $qb->delete('DOMJudgeBundle:Leaderboard', 'lb')
                ->where('lb.cid = :cid')
                ->andWhere('lb.userid = :uid')
                ->setParameter('cid', $arrDataUsers[$i]->cId)
                ->setParameter('uid', $arrDataUsers[$i]->uId)
                ->getQuery()->execute();
        }
        $this->em->flush();
        return $this->json(["isDelete" => true]);
    }
    /**
     * @Route("/add/student", name="add_student")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addStudent(Request $request)
    {
        $data = [];
        $dataRquest = json_decode($request->getContent());

        if(isset($dataRquest->cid) && isset($dataRquest->userid)){
            $classes = $this->em->getRepository(Classes::class)->Find($dataRquest->cid);
            $student = $this->em->getRepository(User::class)->Find($dataRquest->userid);
            $validate = $this->dj->validateStudent($classes, $student);
            if($validate === true ){
                $classes->addUserid($student);
                $this->em->persist($classes);
                $leader_board = new Leaderboard();
                $leader_board->setCid($dataRquest->cid);
                $leader_board->setUserid($dataRquest->userid);
                $this->em->persist($leader_board);
                $this->em->flush();
                $data['result'] = ['success' => $student->getName().' has been added to class '.$classes->getName()];
            }else{
                $data['result'] = ['error' => $validate];
            }
        }
        return $this->json($data);
    }

    /**
     * @Route("/teacher/add", name="add_teacher")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addTeacher(Request $request)
    {
        $data = [];
        $msg = '';
        $dataRquest = json_decode($request->getContent());
        $teachers_old = $this->em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->select('u', 'uc', 'r')
            ->innerJoin('u.cid', 'uc')
            ->innerJoin('u.roles', 'r')
            ->where('uc.cid = :cid')
            ->andWhere('r.dj_role = :dj_role')
            ->setParameter('cid', $dataRquest->cid)
            ->setParameter('dj_role', "teacher")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if($dataRquest->mode == 'add'){
            //Find user to add
            $teachers_new = $this->em->getRepository(User::class)->Find($dataRquest->tid);
            //Check duplicate teacher
            $class = $this->em->getRepository(Classes::class)->Find($dataRquest->cid);
            if(!empty($teachers_old)) {
                $class->removeUserid($teachers_old);
                $class->addUserid($teachers_new);
                $this->em->persist($class);
                $msg = $class->getName()." have been changing to ".$teachers_new->getName();
            }else{
                $class->addUserid($teachers_new);
                $this->em->persist($class);
                $msg = $class->getName()." have been assigning to ".$teachers_new->getName();
            }            
            $this->em->transactional(function () {
                $this->em->flush();
            });
            $data['success'] = "Teacher of class ".$msg;
        }else{
            if(!empty($teachers_old)){
                $data['teacher_old'] = $teachers_old->getName();
            }else{
                $data['teacher_old'] = 'Not assign';
            }
        }
        
        return $this->json([ 'result' => $data]);
        //only call this after you've made all your data modifications
        //  } 
    }

    /**
     * @Route("/class/schedule/add", name="add_schedule")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addSchedule(Request $request)
    {
        $data = [];
        $msg = '';
        $dataRquest = json_decode($request->getContent());
        if(isset($dataRquest->cid)){
            $classes = $this->em->getRepository(Classes::class)->Find($dataRquest->cid);
            $data['classes'] = $classes;
        }
        if($dataRquest->mode == 'add' && !empty($classes)){
            if(!isset($dataRquest->date) || !isset($dataRquest->roomid) || !isset($dataRquest->slotid)){
                $data['error'] = 'Data error, Please try again!';
            }else{
                $scheduleDate = $dataRquest->date;
                $room = $this->em->getRepository(LabRoom::class)->Find($dataRquest->roomid);
                $slot = $this->em->getRepository(TimeSlot::class)->Find($dataRquest->slotid);
                $validateDate = $this->validateDate($scheduleDate);
                $scheduleDate = DateTime::createFromFormat('d/m/Y', $scheduleDate);
                $validateSchedule = $this->validateSchedule($scheduleDate, $room, $slot, $classes->getCid());
                if($validateDate != 'valid'){
                    $data['error_date'] = 'Date '.$validateDate;
                }else if($validateSchedule !== true){
                    $data['error'] = $validateSchedule;
                }else{
                    $classSchedule = new ClassSchedule();
                    $classSchedule->setCheDate($scheduleDate);
                    $classSchedule->setCid($classes);
                    $classSchedule->setSlotid($slot);
                    $classSchedule->setRoomid($room);
                    $this->em->persist($classSchedule);
                    $this->em->flush($classSchedule);
                    $data['success'] = "Schedule have added!";
                }
            }
            return $this->json($data);
        }else{
            if(!empty($classes)){
                $schedules = $this->em->getRepository(ClassSchedule::class)
                    ->createQueryBuilder('s')
                    ->select('count(s.cheid)')
                    ->where('s.cid = :cid')
                    ->setParameter("cid", $classes->getCid())
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                if(!empty($schedules)){
                    $data['total'] = $schedules[1];
                }else{
                    $data['total'] = 0;
                }
            }
            $listRooms = $this->em->getRepository(LabRoom::class)->Findall();
            $listSlots = $this->em->getRepository(TimeSlot::class)->Findall();
            if(!empty($listRooms)){
                $data['listRooms'] = $listRooms;
            }
            if(!empty($listSlots)){
                $data['listSlots'] = $listSlots;
            }
            $template = $this->render('@DOMJudge/Fls/Admin/partials/modal_add_schedule.html.twig', $data)->getContent();
            
            $json = json_encode($template);
            $response = new Response($json, 200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        
        return $this->json([ 'result' => $data]);
        //only call this after you've made all your data modifications
        //  } 
    }

    //validate date
    public function validateDate($date) {
        if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/",$date)) {
            $split_date = explode("/", $date);
            if(count($split_date) != 3) {
                return 'wrong format';
            }
            if(checkdate((int) $split_date[1],(int) $split_date[0], (int) $split_date[2])) {
                return 'valid';
            }else {
                return 'invalid';
            }
        } else {
            return 'wrong format';
        }        
    }

    // validate schedule
    public function validateSchedule($date, $roomid, $slotid, $cid) {
        $query = $this->em->getRepository(ClassSchedule::class)->createQueryBuilder('cs')
            ->where('cs.cheDate = :date')
            ->andwhere('cs.roomid = :roomid')
            ->andwhere('cs.slotid = :slotid')
            ->setMaxResults(1)
            ->select('count(cs.cheid)')
            ->setParameter(':date', $date->format('Y-m-d'))
            ->setParameter(':roomid', $roomid)
            ->setParameter(':slotid', $slotid);
        $duplicateSlot = $query->getQuery()
            ->getOneOrNullResult();
        $duplicateInClass = $query->andwhere('cs.cid = :cid')
            ->setParameter(':cid',$cid)            
            ->getQuery()
            ->getOneOrNullResult();
        if($duplicateSlot[1] <= 0 && $duplicateInClass[1] <= 0){
            return true;
        }else{
            if($duplicateSlot[1] > 0){
                $msg =  'Slot have been asign in other class!';
            }
            if($duplicateInClass[1] > 0){
                $msg =  'Slot have been exists!';
            }
        }
        return $msg;
    }

    /**
     * @Route("/schedule/clear/{classId}", name="clear_schedule" , requirements={"classId": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param int     $classId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function clearSchedule(Request $request, int $classId)
    {
        /** @var class $class */
        $class = $this->em->getRepository(classes::class)->find($classId);
        if (!$class) {
            throw new NotFoundHttpException(sprintf('class with ID %s not found', $classId));
        }
        //clear all schedule of class
        $qb = $this->em->createQueryBuilder()
            ->delete('DOMJudgeBundle:ClassSchedule', 'cs')
            ->where('cs.cid = :class')
            ->setParameter('class', $class)
            ->getQuery()->execute();

        return $this->redirectToRoute('class_schedule', ['cid' => $class->getCid()]);
        
    }

    /**
     * @Route("/class_add_semester", name="admin_class_add_semester")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addSemester(Request $request)
    {
        $duplicate = false;
        $semester = $this->em->getRepository(Semester::class)->findAll();
        $jsonData  = $request->getContent();
        $arrData = json_decode($jsonData);
        $newSemester = $arrData->semester . $arrData->year;
        for ($i = 0; $i < count($semester); $i++) {
            $seName =  $semester[$i]->getName();
            if ($newSemester == $seName) {
                $duplicate = true;
                break;
            }
        }
        if ($duplicate) {
            $this->addFlash('danger', "Semester is exist!");
            return $this->json(['isSuccess' => false]);
        } else {
            $obj_semester = new Semester();
            $obj_semester->setName($newSemester);
            $this->em->persist($obj_semester);
            $this->em->flush();
            $this->addFlash('info', "Add semester successful!");
            return $this->json(['isSuccess' => true]);
        }
    }


    /**
     * @Route("/{classId}/delete", name="admin_class_delete", requirements={"classId": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param int     $classId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function deleteAction(Request $request, int $classId)
    {
        /** @var class $class */
        $class = $this->em->getRepository(classes::class)->find($classId);
        if (!$class) {
            throw new NotFoundHttpException(sprintf('class with ID %s not found', $classId));
        }

        return $this->deleteEntity(
            $request,
            $this->em,
            $this->dj,
            $class,
            $class->getName(),
            $this->generateUrl('teacher_index')
        );
    }

    /**
     * @Route("/add", name="admin_class_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addAction(Request $request)
    {
        $class = new Classes();
        // if ($request->query->has('team')) {
        //     $class->setTeam($this->em->getRepository(Team::class)->find($request->query->get('team')));
        // }

        $form = $this->createForm(ClassType::class, $class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $duplicate = false;
            $listClasses = $this->em->getRepository(Classes::class)->findAll();
            $shortname = $form->getData()->getName();
            $class->setShortname($shortname);
            for ($i = 0; $i < count($listClasses); $i++) {
                if ($shortname == $listClasses[$i]->getShortname()) {
                    $duplicate = true;
                    break;
                }
            }
            if ($duplicate) {
                $this->addFlash('danger', "Class is exist!");
                return $this->redirectToRoute('admin_class_add');
            } else {
                $this->em->persist($class);
                $this->saveEntity(
                    $this->em,
                    $this->eventLogService,
                    $this->dj,
                    $class,
                    $class->getcid(),
                    true
                );
                $this->addFlash('info', "Add class: " . $shortname. " successfully!");
            }

            return $this->redirectToRoute('teacher_index');
        }

        return $this->render('@DOMJudge/Fls/Admin/class_add.html.twig', [
            'class' => $class,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/insert/start_time/", name="data-route-starttime")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function InsertStartTime(Request $request)
    {
        $timeFormat  = (string) $this->dj->dbconfig_get('time_format', '%H:%M');
        $data = json_decode($request->getContent());
        $start_time = Utils::now();
        $start_time_string = Utils::printtime($start_time, $timeFormat);
        $class = $this->em->getRepository(Classes::class)->Find($data->cId);
        $class->setStarttime($start_time); //make sure the removeGroup method is defined in your User model. 
        $class->setStarttimeString($start_time_string);
        $class->setStarttimeEnabled(true);
        $this->em->persist($class);
        $this->em->flush(); //only call this after you've made all your data modifications

        $class_schedule = $this->em->getRepository(ClassSchedule::class)
            ->createQueryBuilder('cu')
            ->select('cu', 'c', 'tl')
            ->innerJoin('cu.cid', 'c')
            ->Join('cu.slotid', 'tl')
            ->where('c.cid = :cid')
            ->setParameter("cid", $data->cId)
            ->getQuery()->getResult();
        // $start_time = Utils::printtime($start_time, $timeFormat);
        $free_time = true;
        $end_time_string = "freetime";
        foreach ($class_schedule as $cs) {
            $time_slot_start  = strval($cs->getSlotid()->getStartHour() . ':' . $cs->getSlotid()->getStartMinute());
            $time_slot_end  = strval($cs->getSlotid()->getEndHour() . ':' . $cs->getSlotid()->getEndMinute());
            if (
                date('H:i', strtotime($start_time_string)) >= date('H:i', strtotime($time_slot_start))
                && date('H:i', strtotime($start_time_string)) <= date('H:i', strtotime($time_slot_end))
            ) {
                $now = date("Y-m-d") . " " . $time_slot_end;
                $end_time = date("U.u", strtotime($now));
                $end_time_string = Utils::printtime($end_time, $timeFormat);
                $free_time = false;
                break;
            }
        }


        if ($free_time) {
            $class->setStarttimeString($start_time_string);
            $class->setEndtimeString($end_time_string);
            $this->em->persist($class);
            $this->em->flush(); //only call this after you've made all your data modifications  
            return $this->json([
                'starttime' => $start_time_string,
                'freetime' => $free_time,
                'endtime' => $end_time_string,
                'active' => true
            ]);
        }
        $class->setEndtime($end_time); //make sure the removeGroup method is defined in your User model. 
        $class->setEndtimeString($end_time_string);
        $this->em->persist($class);
        $this->em->flush(); //only call this after you've made all your data modifications     
        return $this->json([
            'starttime' =>  $start_time_string,
            'endtime' => $end_time_string,
            'freetime' => $free_time,
            'active' => false
        ]);
    }
    /**
     * @Route("/insert/stop_time/", name="data-route-stoptime")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function InsertStopTime(Request $request)
    {
        $timeFormat  = (string) $this->dj->dbconfig_get('time_format', '%H:%M');
        $data = json_decode($request->getContent());
        $end_time = Utils::now();
        $end_time_string = Utils::printtime($end_time, $timeFormat);
        $class = $this->em->getRepository(Classes::class)->Find($data->cId);
        $class->setStarttimeEnabled(false);
        $class->setEndtime($end_time); //make sure the removeGroup method is defined in your User model. 
        $class->setEndtimeString($end_time_string);
        $this->em->persist($class);
        $this->em->flush(); //only call this after you've made all your data modifications     
        return $this->json([
            'endtime' => $end_time_string,
            'active' => false
        ]);
    }

    /**
     * @Route("/export", name="admin_class_export")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function ExportExcel(Request $request)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Export Success !');
        $sheet->setTitle("Worksheet");

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = 'demo_export.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    /**
     * @Route("/import", name="admin_class_import")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function ImportExcel(Request $request)
    {
        $uploadStatus = false;
        $message = '';
        $file_data = $_FILES['Filedata']['tmp_name'];
        $file_name = $_FILES['Filedata']['name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file_data);
        $sheet = $spreadsheet->getActiveSheet();
        $Totalrow = $sheet->getHighestRow();
        $LastColumn = $sheet->getHighestColumn();
        $TotalCol = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($LastColumn);
        $arrayData =  explode('_', $file_name);

        //validate excel file name
        if (count($arrayData) != 4) {
            $message .= 'Filename is wrong format, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }
        $txt_Semester = $arrayData[3];
        $arrSemester = explode('.', $txt_Semester);
        $Semester = $arrSemester[0]; //FALL2019
        $file_extension = $arrSemester[1];
        $SubjectCode = $arrayData[1]; //LAB211
        $ShortCode = $SubjectCode;
        $Class  = $arrayData[0]; //SE1407
        $Teacher  = $arrayData[2]; //DuongTB

        //validate excel file extension
        if ($file_extension != 'xlsx' && $file_extension != 'xls' && $file_extension != 'csv') {
            $message .= 'your file is not .xlsx, .xls, .csv, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }
        //validate file column
        $SemesterCol = $sheet->getCellByColumnAndRow(1, 1)->getValue();
        $SubjectCodeCol = $sheet->getCellByColumnAndRow(2, 1)->getValue();
        $ClassCol = $sheet->getCellByColumnAndRow(3, 1)->getValue();
        $RollNumberCol = $sheet->getCellByColumnAndRow(4, 1)->getValue();
        $MemberCodeCol = $sheet->getCellByColumnAndRow(5, 1)->getValue();
        $FullNameCol = $sheet->getCellByColumnAndRow(6, 1)->getValue();
        if (
            strcasecmp($SemesterCol, 'Semester') != 0 || strcasecmp($SubjectCodeCol, 'SubjectCode') != 0 || strcasecmp($ClassCol, 'Class') != 0
            || strcasecmp($RollNumberCol, 'RollNumber') != 0 || strcasecmp($MemberCodeCol, 'MemberCode') != 0 || strcasecmp($FullNameCol, 'FullName') != 0
        ) {
            $message .= 'your file column incorect, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //Check Class is exist
        $existClass = $this->em->getRepository(Classes::class)
            ->createQueryBuilder('c')
            ->select('c')
            ->where('c.name = :name')
            ->setParameter("name", $Class)
            ->getQuery()
            ->getResult();
        if ($existClass != null) {
            $message .= 'Class ' . $existClass[0]->getName() . ' already exist.';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //check semester exist
        $existSemester = $this->em->getRepository(Semester::class)
            ->createQueryBuilder('s')
            ->select('s')
            ->where('s.name = :name')
            ->setParameter("name", $Semester)
            ->getQuery()
            ->getResult();
        if ($existSemester == null) {
            $message .= 'Semester ' . $Semester . ' does not exist, Please create Semester before!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //check teacher exist
        $existTeacher = $this->em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->select('u', 'r')
            ->innerJoin('u.roles', 'r')
            ->where('u.username = :username')
            ->setParameter("username", $Teacher)
            ->getQuery()
            ->getResult();
        if ($existTeacher != null) {
            if (($existTeacher[0]->getRoles())[0]->getDjRole() != 'teacher') {
                $message .= 'User ' . $Teacher . ' already exist, but user is not Teacher, Please check again!';
                return $this->json([
                    "uploadStatus" => $uploadStatus,
                    "message" => $message,
                ]);
            }
        } else { //teacher not exist
            $message .= 'Teacher ' . $Teacher . ' is not exist, Please create Teacher before!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //check exist Language
        $language = $this->em->getRepository(Language::class)->Find($SubjectCode);
        if ($language == null) {
            $message .= 'Language(Subject) ' . $SubjectCode . ' is not exist, Please create Language(Subject) before!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //create class
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            //insert data to DB
            $classes = new Classes();
            $classes->setLangid($language);
            $classes->setName($Class);
            $classes->setSemester($existSemester[0]);
            $classes->setShortname($Class);
            $classes->setStarttimeEnabled(false);
            $this->em->persist($classes);
            $this->em->flush();
            $class =  $this->em->createQueryBuilder()
                ->select('c', 'u')
                ->from('DOMJudgeBundle:Classes', 'c')
                ->leftJoin('c.userid', 'u')
                ->where('c.name=:name')
                ->setParameter('name', $Class)
                ->getQuery()->getResult();

            //Add student into class
            $error = false;
            $exist_student = 0;
            $new_student = 0;
            $add_leaderboard = [];
            $role_student = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'student']);
            for ($i = 2; $i <= $Totalrow; $i++) {
                $MemberCode = $sheet->getCellByColumnAndRow(5, $i)->getValue();
                if ($MemberCode == null || $MemberCode == '') {
                    $message .= 'Membercode in line' . $i . ' is null, Please check again!';
                    return $this->json([
                        "uploadStatus" => $uploadStatus,
                        "message" => $message,
                    ]);
                }
                $FullName = $sheet->getCellByColumnAndRow(6, $i)->getValue();
                $Email = $MemberCode . '@fpt.edu.vn';
                //$user->setUsername($MemberCode);
                //   $temp['uUsername'] =  $MemberCode;
                $user = $this->em->createQueryBuilder()
                    ->select('u')
                    ->from('DOMJudgeBundle:User', 'u')
                    ->where('u.username=:username')
                    ->setParameter('username', $MemberCode)
                    ->getQuery()->getResult();
                if ($user == null) {
                    $newUser = new User();
                    $newUser->setUsername($MemberCode);
                    $newUser->setName($FullName);
                    $newUser->setEmail($Email);
                    //default user password = 1
                    $password  = $this->get('security.password_encoder')->encodePassword($newUser, "1");
                    $newUser->setPassword($password);
                    $newUser->setEnabled(true);
                    $newUser->addRole($role_student[0]);
                    $this->em->persist($newUser);
                    $class[0]->addUserid($newUser);
                    $new_student++;                    
                    $add_leaderboard[] = $newUser->getUserid();
                } else {
                    //check user
                    $validate_user = $this->dj->validateStudent($class[0], $user[0]);
                    if($validate_user == true) {
                        $class[0]->addUserid($user[0]);
                        $add_leaderboard[] = $user[0]->getUserid();
                        $exist_student++;                        
                    }else {
                        return $this->json([
                            "uploadStatus" => $uploadStatus,
                            "message" => $validate_user,
                        ]);
                    }                    
                }                
            }
            //add teacher
            $class[0]->addUserid($existTeacher[0]);

            $this->em->persist($class[0]); 
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $message = 'Something wrong, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
            throw $e;
        }
        //import leaderboard
        $list_class_user = $class[0]->getUserid();
        foreach($list_class_user as $user) {
            //add student to leaderboard
            $leader_board = new Leaderboard();
            $leader_board->setCid($class[0]->getCid());
            $leader_board->setUserid($user->getUserid());
            $this->em->persist($leader_board);
            $this->em->flush();
        }
        $uploadStatus = true;
        $message = 'Added: ' . $exist_student . ' exist student, and: ' . $new_student . ' new student!';
        return $this->json([
            "uploadStatus" => $uploadStatus,
            "message" => $message,
        ]);
    }

    /**
     * @Route("/import-schedule", name="admin_schedule_import")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function ImportExcelSchedule(Request $request)
    {
        $uploadStatus = false;
        $message = '';
        $file_data = $_FILES['Filedata']['tmp_name'];
        $file_name = $_FILES['Filedata']['name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file_data);
        $sheet = $spreadsheet->getActiveSheet();
        $Totalrow = $sheet->getHighestRow();
        $LastColumn = $sheet->getHighestColumn();
        $TotalCol = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($LastColumn);
        $arrayData =  explode('_', $file_name);

        //validate excel file name
        if (count($arrayData) != 2) {
            $message .= 'Filename is wrong format, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }
        $Class = $arrayData[0]; //SE1407
        $arrSchedule = explode('.', $arrayData[1]);
        $ScheduleString = $arrSchedule[0]; //SCHEDULE
        $file_extension = $arrSchedule[1]; //.xls .xlsx .csv

        //validate excel file name
        if (strcasecmp($ScheduleString, 'SCHEDULE') != 0) {
            $message .= 'Filename is wrong format, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //validate excel file extension
        if ($file_extension != 'xlsx' && $file_extension != 'xls' && $file_extension != 'csv') {
            $message .= 'your file is not .xlsx, .xls, .csv, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }
        //validate file column
        $SemesterCol = $sheet->getCellByColumnAndRow(1, 1)->getValue();
        $SubjectCodeCol = $sheet->getCellByColumnAndRow(2, 1)->getValue();
        $ClassCol = $sheet->getCellByColumnAndRow(3, 1)->getValue();
        $DateCol = $sheet->getCellByColumnAndRow(4, 1)->getValue();
        $SlotCol = $sheet->getCellByColumnAndRow(5, 1)->getValue();
        $RoomCol = $sheet->getCellByColumnAndRow(6, 1)->getValue();
        if (
            strcasecmp($SemesterCol, 'Semester') != 0 || strcasecmp($SubjectCodeCol, 'SubjectCode') != 0 || strcasecmp($ClassCol, 'Class') != 0
            || strcasecmp($DateCol, 'Date') != 0 || strcasecmp($SlotCol, 'Slot') != 0 || strcasecmp($RoomCol, 'Room') != 0
        ) {
            $message .= 'your file column incorect, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //Check Class is exist
        $existClass = $this->em->getRepository(Classes::class)
            ->createQueryBuilder('c')
            ->select('c')
            ->where('c.name = :name')
            ->setParameter("name", $Class)
            ->getQuery()
            ->getResult();
        if ($existClass == null) {
            $message .= 'Class: ' . $Class . ' does not exist, please create class before import schedule!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //check class schedule exist

        $classSchedule = $this->em->getRepository(ClassSchedule::class)
            ->createQueryBuilder('cs')
            ->select('cs')
            ->where('cs.cid = :class')
            ->setParameter("class", $existClass[0]->getCid())
            ->getQuery()
            ->getResult();

        if ($classSchedule != null) {
            $message .= 'Schedule of Class: ' . $existClass[0]->getName() . ' is exist, please clear schedule before!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }


        //add class schedule
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            //Add student into class
            $added_slot = 0;
            for ($i = 2; $i <= $Totalrow; $i++) {
                $sche_date = $sheet->getCellByColumnAndRow(4, $i)->getFormattedValue();
                // $sche_date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($sheet->getCellByColumnAndRow(4, $i)->getValue());
                $sche_slot = $sheet->getCellByColumnAndRow(5, $i)->getValue();
                $sche_room = $sheet->getCellByColumnAndRow(6, $i)->getValue();
                if ($sche_date == null || $sche_slot == null || $sche_room == null) {
                    break;
                    $message .= 'line ' . $i . ' is null, Please check again!';
                    return $this->json([
                        "uploadStatus" => $uploadStatus,
                        "message" => $message,
                    ]);
                }
                //get slot
                $room = $this->em->getRepository(LabRoom::class)->findBy(['roomname' => $sche_room]);
                if ($room == null) {
                    $message .= 'Room ' . $sche_room . ' in line ' . $i . ' is not exist in FLS, Please check again!';
                    return $this->json([
                        "uploadStatus" => $uploadStatus,
                        "message" => $message,
                    ]);
                }
                //get room id
                $slot = $this->em->getRepository(Timeslot::class)->find($sche_slot);
                if ($slot == null) {
                    $message .= 'Slot ' . $slot . ' in line ' . $i . ' is not exist in FLS, Please check again!';
                    return $this->json([
                        "uploadStatus" => $uploadStatus,
                        "message" => $message,
                    ]);
                }
                //add new schedule
                // $sche_date = strtotime($sche_date);
                // $sche_date = date('Y-m-d', $sche_date);
                $sche_date = DateTime::createFromFormat('d/m/Y', $sche_date);
                if ($sche_date == false) {
                    $message = 'Something wrong, It appears that the Date column at line ' . $i . ' is not date, please check again!';
                    return $this->json([
                        "uploadStatus" => $uploadStatus,
                        "message" => $message,
                    ]);
                }
                $sche_tstamp = $sche_date->format('U');
                //validate schedule date in semester range
                if($sche_tstamp < $existClass[0]->getSemester()->getStartedTime() || $sche_tstamp > $existClass[0]->getSemester()->getFinishedTime()) {
                    $message = 'Schedule Date at line ' . $i . ' is not in Semester: '. $existClass[0]->getSemester()->getName() .' date range, please check again!';
                    return $this->json([
                        "uploadStatus" => $uploadStatus,
                        "message" => $message,
                    ]);
                }

                $validateDupSchedule = $this->validateSchedule($sche_date, $room[0], $slot, $existClass[0]);
                if($validateDupSchedule !== true){
                    if($validateDupSchedule == 'Slot have been exists!'){
                        $message = 'Something wrong, It appears that the Slot at line ' . $i . ' is duplidate, please check again!';
                    }else{
                        $message = 'Something wrong, It appears that the Slot at line ' . $i .' have been asign in other class, please check again!';
                    }
                    return $this->json([
                        "uploadStatus" => $uploadStatus,
                        "message" => $message,
                    ]);
                }
                $new_schedule = new ClassSchedule();
                $new_schedule->setCid($existClass[0]);
                $new_schedule->setRoomid($room[0]);
                $new_schedule->setCheDate($sche_date);
                $new_schedule->setSlotid($slot);
                $this->em->persist($new_schedule);
                $this->em->flush();
                $added_slot++;
            }
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $message = 'Something wrong, It appears that the Date column is not date, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
            throw $e;
        }

        $message = 'Added success: ' . $added_slot . ' slot schedule for class: ' . $existClass[0]->getName() . '!';
        $uploadStatus = true;
        return $this->json([
            "uploadStatus" => $uploadStatus,
            "message" => $message,
        ]);
    }
    /**
     * @Route("/generate-passwords", name="admin_generate_passwords")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generatePasswordsAction(Request $request)
    {
        $form = $this->createForm(GeneratePasswordsType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $groups = $form->get('group')->getData();

            $classes = $this->em->getRepository(classes::class)->findAll();

            $changes = [];
            foreach ($classes as $class) {
                $doit = false;
                $roles = $class->getRoleList();

                $isadmin = in_array('admin', $roles);
                $isadmin = in_array('admin', $roles);

                //  if ( in_array('team', $groups) || in_array('team_nopass', $groups) ) {
                //      if ( $class->getTeamid() && ! $isadmin && ! $isadmin ) {
                //          if ( in_array('team', $groups) || empty($class->getPassword()) ) {
                //              $doit = true;
                //              $role = 'team';
                //          }
                //      }
                //  }

                if ((in_array('judge', $groups) && $isadmin) || (in_array('admin', $groups) && $isadmin)
                ) {
                    $doit = true;
                    $role = in_array('admin', $groups) ? 'admin' : 'judge';
                }

                if ($doit) {
                    $newpass = Utils::generatePassword();
                    $class->setPlainPassword($newpass);
                    $this->dj->auditlog('class', $class->getclassid(), 'set password');
                    $changes[] = [
                        'type' => $role,
                        'id' => $class->getclassid(),
                        'fullname' => $class->getName(),
                        'classname' => $class->getclassname(),
                        'password' => $newpass,
                    ];
                }
            }
            $this->em->flush();
            $response = $this->render('@DOMJudge/admin/tsv/classdata.tsv.twig', [
                'data' => $changes,
            ]);
            $disposition = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'classdata.tsv'
            );
            $response->headers->set('Content-Disposition', $disposition);
            $response->headers->set('Content-Type', 'text/plain');
            return $response;
        }

        return $this->render('@DOMJudge/admin/class_generate_passwords.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
