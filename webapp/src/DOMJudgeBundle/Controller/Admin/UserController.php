<?php

declare(strict_types=1);

namespace DOMJudgeBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Role;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Form\Type\GeneratePasswordsType;
use DOMJudgeBundle\Form\Type\UserType;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Utils\Utils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @Route("/admin/users")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    private $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em              = $em;
        $this->dj              = $dj;
        $this->eventLogService = $eventLogService;
        $this->tokenStorage    = $tokenStorage;
    }

    /**
     * @Route("", name="admin_users")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction()
    {
        $data = [];
        /** @var User[] $users */
        $users = $this->em->createQueryBuilder()
            ->select('u', 'r')
            ->from('DOMJudgeBundle:User', 'u')
            ->leftJoin('u.roles', 'r')
            // ->leftJoin('u.team', 't')sadsadasdas
            //->orderBy('u.username', 'ASC')
            ->getQuery()->getResult();

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $users_table      = [];
        $timeFormat  = (string) $this->dj->dbconfig_get('time_format', '%H:%M');
        $listUsers = [];
        foreach ($users as $u => $user) {
            //  $userdata    = [];
            //    $useractions = [];
            // Get whatever fields we can from the user object itself
            $temp['uId'] = $user->getUserid();
            $temp['uUsername'] = $user->getUsername();
            $temp['uName'] = $user->getName();
            $temp['uEmail'] = $user->getEmail();
            $temp['uRole'] =  [
                'value' => implode(', ', array_map(function (Role $role) {
                    return $role->getDjRole();
                }, $user->getRoles()))
            ];
            if($user->getEnabled()) {
                $temp['uStatus'] = 'Enable';
            }else {
                $temp['uStatus'] = 'Disable';
            }
            $listUsers[] = $temp;
            unset($temp);
        }
        $data['listUsers'] = json_encode($listUsers);
        return $this->render('@DOMJudge/Fls/Admin/users.html.twig', $data);
    }


    /**
     * @Route("/detail/{userId}", name="admin_user_detail", requirements={"userId": "^$|\d+"})
     * @param Request $request
     * @param int     $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $request, int $userId)
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($userId);
        if (!$user) {
            throw new NotFoundHttpException(sprintf('User with ID %s not found', $userId));
        }

        return $this->render('@DOMJudge/Fls/Admin/user_detail.html.twig', ['user' => $user]);
    }

    /**
     * @Route("/{userId}/edit", name="admin_user_edit", requirements={"userId": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param int     $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, int $userId)
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($userId);
        if (!$user) {
            throw new NotFoundHttpException(sprintf('User with ID %s not found', $userId));
        }

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $studentRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'student']);
            $teacherRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'teacher']);
            $adminRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'admin']);
            $sadminRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'super_admin']);
            $judgehostRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'judgehost']);
            $userRoles = $user->getRoles();
            if(in_array($studentRole[0],$userRoles) && (
                in_array($teacherRole[0],$userRoles) ||
                in_array($adminRole[0],$userRoles) ||
                in_array($sadminRole[0],$userRoles)||
                in_array($judgehostRole[0],$userRoles)        
                )) {
                    $this->addFlash('danger', 'User roles wrong!');
                    return $this->redirectToRoute('admin_user_edit', ['userId' => $user->getUserid()]);
            }
            if(in_array($judgehostRole[0],$userRoles) && (
                in_array($studentRole[0],$userRoles) ||
                in_array($teacherRole[0],$userRoles) ||
                in_array($adminRole[0],$userRoles) ||
                in_array($sadminRole[0],$userRoles)                
                )) {
                    $this->addFlash('danger', 'User roles wrong!');
                    return $this->redirectToRoute('admin_user_edit', ['userId' => $user->getUserid()]);
            }
            $this->saveEntity(
                $this->em,
                $this->eventLogService,
                $this->dj,
                $user,
                $user->getUserid(),
                false
            );

            // If we save the currently logged in used, update the login token
            if ($user->getUserid() === $this->dj->getUser()->getUserid()) {
                $token = new UsernamePasswordToken(
                    $user,
                    null,
                    'main',
                    $user->getRoles()
                );

                $this->tokenStorage->setToken($token);
            }

            return $this->redirect($this->generateUrl(
                'admin_user_detail',
                ['userId' => $user->getUserid()]
            ));
        }

        return $this->render('@DOMJudge/Fls/Admin/user_edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{userId}/delete", name="admin_user_delete", requirements={"userId": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param int     $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function deleteAction(Request $request, int $userId)
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($userId);
        if (!$user) {
            throw new NotFoundHttpException(sprintf('User with ID %s not found', $userId));
        }

        //clear all leaderboard data of user
        $qb = $this->em->createQueryBuilder()
            ->delete('DOMJudgeBundle:Leaderboard', 'lb')
            ->where('lb.userid = :uid')
            ->setParameter('uid', $user->getUserid())
            ->getQuery()->execute();

        return $this->deleteEntity(
            $request,
            $this->em,
            $this->dj,
            $user,
            $user->getName(),
            $this->generateUrl('admin_users')
        );
    }

    /**
     * @Route("/add", name="admin_user_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addAction(Request $request)
    {
        $user = new User();
        // if ($request->query->has('team')) {
        //     $user->setTeam($this->em->getRepository(Team::class)->find($request->query->get('team')));
        // }

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        
        if ($form->isSubmitted() && $form->isValid()) {
            $studentRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'student']);
            $teacherRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'teacher']);
            $adminRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'admin']);
            $sadminRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'super_admin']);
            $judgehostRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'judgehost']);

            $userRoles = $user->getRoles();
            if(in_array($studentRole[0],$userRoles) && (
                in_array($teacherRole[0],$userRoles) ||
                in_array($adminRole[0],$userRoles) ||
                in_array($sadminRole[0],$userRoles)||
                in_array($judgehostRole[0],$userRoles)                
                )) {
                    $this->addFlash('danger', 'User roles Wrong!');
                    return $this->redirectToRoute('admin_user_add');
            }
            if(in_array($judgehostRole[0],$userRoles) && (
                in_array($studentRole[0],$userRoles) ||
                in_array($teacherRole[0],$userRoles) ||
                in_array($adminRole[0],$userRoles) ||
                in_array($sadminRole[0],$userRoles)                
                )) {
                    $this->addFlash('danger', 'User roles wrong!');
                    return $this->redirectToRoute('admin_user_add');
            }
            $this->em->persist($user);
            $this->saveEntity(
                $this->em,
                $this->eventLogService,
                $this->dj,
                $user,
                $user->getUserid(),
                true
            );
            return $this->redirect($this->generateUrl(
                'admin_user_detail',
                ['userId' => $user->getUserid()]
            ));
        }

        return $this->render('@DOMJudge/Fls/Admin/user_add.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/import-users", name="admin_user_import")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function ImportExcel(Request $request)
    {
        $uploadStatus = false;
        $message = '';
        $file_data = $_FILES['Filedata']['tmp_name'];
        $file_name = $_FILES['Filedata']['name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file_data);
        $sheet = $spreadsheet->getActiveSheet();
        $Totalrow = $sheet->getHighestRow();
        $LastColumn = $sheet->getHighestColumn();
        $TotalCol = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($LastColumn);

        //validate file column
        $RollNumberCol = $sheet->getCellByColumnAndRow(1, 1)->getValue();
        $MemberCodeCol = $sheet->getCellByColumnAndRow(2, 1)->getValue();
        $FullNameCol = $sheet->getCellByColumnAndRow(3, 1)->getValue();
        if (
            strcasecmp($RollNumberCol, 'RollNumber') != 0 || strcasecmp($MemberCodeCol, 'MemberCode') != 0 || strcasecmp($FullNameCol, 'FullName') != 0
        ) {
            $message .= 'your file column incorect, please check again!';

            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }

        //create class
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            
            //Add student into class
            $error = false;
            $exist_student = 0;
            $new_student = 0;
            $add_leaderboard = [];
            $role_student = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'student']);
            for ($i = 2; $i <= $Totalrow; $i++) {
                $MemberCode = $sheet->getCellByColumnAndRow(2, $i)->getValue();
                if ($MemberCode == null || $MemberCode == '') {
                    $message .= 'Membercode in line' . $i . ' is null, Please check again!';
                    return $this->json([
                        "uploadStatus" => $uploadStatus,
                        "message" => $message,
                    ]);
                }
                $FullName = $sheet->getCellByColumnAndRow(6, $i)->getValue();
                $Email = $MemberCode . '@fpt.edu.vn';
                $user = $this->em->createQueryBuilder()
                    ->select('u')
                    ->from('DOMJudgeBundle:User', 'u')
                    ->where('u.username=:username')
                    ->setParameter('username', $MemberCode)
                    ->getQuery()->getResult();
                if ($user == null) {
                    $newUser = new User();
                    $newUser->setUsername($MemberCode);
                    $newUser->setName($FullName);
                    $newUser->setEmail($Email);
                    //default user password = 1
                    $password  = $this->get('security.password_encoder')->encodePassword($newUser, "1");
                    $newUser->setPassword($password);
                    $newUser->setEnabled(true);
                    $newUser->addRole($role_student[0]);
                    $this->em->persist($newUser);
                    $class[0]->addUserid($newUser);
                    $new_student++;                    
                    $add_leaderboard[] = $newUser->getUserid();
                } else {
                    //user exist
                    $exist_student++;                
                }                
            }
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $message = 'Something wrong, please check again!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
            throw $e;
        }

        if($new_student != 0 && $exist_student != 0) {
            $uploadStatus = true;
            $message = 'Added: ' . $new_student . ' new students, skipped ' . $exist_student . ' students!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }else if($new_student != 0 && $exist_student == 0) {
            $uploadStatus = true;
            $message = 'Added: ' . $new_student . ' new students!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }else if($new_student == 0 && $exist_student != 0) {
            $uploadStatus = true;
            $message = 'Skiped: ' . $exist_student . ' students, 0 students added!';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }else {
            $uploadStatus = false;
            $message = 'nothing change';
            return $this->json([
                "uploadStatus" => $uploadStatus,
                "message" => $message,
            ]);
        }
    }

    /**
     * @Route("/generate-passwords", name="admin_generate_passwords")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generatePasswordsAction(Request $request)
    {
        $form = $this->createForm(GeneratePasswordsType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $groups = $form->get('group')->getData();

            $users = $this->em->getRepository(User::class)->findAll();

            $changes = [];
            foreach ($users as $user) {
                $doit = false;
                $roles = $user->getRoleList();

                $isadmin = in_array('admin', $roles);
                $isadmin = in_array('admin', $roles);

                //  if ( in_array('team', $groups) || in_array('team_nopass', $groups) ) {
                //      if ( $user->getTeamid() && ! $isadmin && ! $isadmin ) {
                //          if ( in_array('team', $groups) || empty($user->getPassword()) ) {
                //              $doit = true;
                //              $role = 'team';
                //          }
                //      }
                //  }

                if ((in_array('judge', $groups) && $isadmin) || (in_array('admin', $groups) && $isadmin)
                ) {
                    $doit = true;
                    $role = in_array('admin', $groups) ? 'admin' : 'judge';
                }

                if ($doit) {
                    $newpass = Utils::generatePassword();
                    $user->setPlainPassword($newpass);
                    $this->dj->auditlog('user', $user->getUserid(), 'set password');
                    $changes[] = [
                        'type' => $role,
                        'id' => $user->getUserid(),
                        'fullname' => $user->getName(),
                        'username' => $user->getUsername(),
                        'password' => $newpass,
                    ];
                }
            }
            $this->em->flush();
            $response = $this->render('@DOMJudge/admin/tsv/userdata.tsv.twig', [
                'data' => $changes,
            ]);
            $disposition = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'userdata.tsv'
            );
            $response->headers->set('Content-Disposition', $disposition);
            $response->headers->set('Content-Type', 'text/plain');
            return $response;
        }

        return $this->render('@DOMJudge/admin/user_generate_passwords.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
