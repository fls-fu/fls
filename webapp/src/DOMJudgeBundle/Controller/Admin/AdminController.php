<?php declare (strict_types = 1);

namespace DOMJudgeBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Contest;
use DOMJudgeBundle\Entity\ContestProblem;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\Semester;
use DOMJudgeBundle\Entity\LabRoom;
use DOMJudgeBundle\Entity\Submission;
use DOMJudgeBundle\Entity\SubmissionFileWithSourceCode;
use DOMJudgeBundle\Entity\Testcase;
use DOMJudgeBundle\Entity\TestcaseWithContent;
use DOMJudgeBundle\Entity\Roomip;
use DOMJudgeBundle\Form\Type\ProblemType;
use DOMJudgeBundle\Form\Type\ProblemUploadType;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Service\ImportProblemService;
use DOMJudgeBundle\Service\SubmissionService;
use DOMJudgeBundle\Utils\Utils;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use ZipArchive;
use \Datetime;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    private $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * @var SubmissionService
     */
    protected $submissionService;

    /**
     * @var ImportProblemService
     */
    protected $importProblemService;

    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService,
        SubmissionService $submissionService,
        ImportProblemService $importProblemService
    ) {
        $this->em = $em;
        $this->dj = $dj;
        $this->eventLogService = $eventLogService;
        $this->submissionService = $submissionService;
        $this->importProblemService = $importProblemService;
    }

    /**
     * @Route("", name="admin_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function indexAction(Request $request)
    {

        return $this->render('@DOMJudge/Fls/Admin/index.html.twig');
    }

    /**
     * @Route("/{problemId}/export", name="jury_export_problem", requirements={"problemId": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param int $problemId
     * @return StreamedResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function exportAction(int $problemId)
    {
        // This might take a while
        ini_set('max_execution_time', '300');
        /** @var Problem $problem */
        $problem = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Problem', 'p')
            ->leftJoin('p.contest_problems', 'cp', Join::WITH, 'cp.contest = :contest')
            ->select('p', 'cp')
            ->andWhere('p.probid = :problemId')
            ->setParameter(':problemId', $problemId)
            ->setParameter(':contest', $this->dj->getCurrentContest())
            ->getQuery()
            ->getOneOrNullResult();

        /** @var ContestProblem|null $contestProblem */
        $contestProblem = $problem->getContestProblems()->first();

        // Build up INI
        $iniData = [
            'probid' => $contestProblem ? $contestProblem->getShortname() : null,
            'timelimit' => $problem->getTimelimit(),
            'special_run' => $problem->getSpecialRun(),
            'special_compare' => $problem->getSpecialCompare(),
            'color' => $contestProblem ? $contestProblem->getColor() : null,
        ];

        $iniString = "";
        foreach ($iniData as $key => $value) {
            if (!empty($value)) {
                $iniString .= $key . "='" . $value . "'\n";
            }
        }

        // Build up YAML
        $yaml = ['name' => $problem->getName()];
        if (!empty($problem->getSpecialCompare())) {
            $yaml['validation'] = 'custom';
        }
        if (!empty($problem->getSpecialCompareArgs())) {
            $yaml['validator_flags'] = $problem->getSpecialCompareArgs();
        }
        if (!empty($problem->getMemlimit())) {
            $yaml['limits']['memory'] = (int) round($problem->getMemlimit() / 1024);
        }
        if (!empty($problem->getOutputlimit())) {
            $yaml['limits']['output'] = (int) round($problem->getOutputlimit() / 1024);
        }

        $yamlString = '# Problem exported by DOMjudge on ' . date('c') . "\n" . Yaml::dump($yaml);

        $zip = new ZipArchive();
        if (!($tempFilename = tempnam($this->dj->getDomjudgeTmpDir(), "export-"))) {
            throw new ServiceUnavailableHttpException(null, 'Could not create temporary file.');
        }

        $res = $zip->open($tempFilename, ZipArchive::OVERWRITE);
        if ($res !== true) {
            throw new ServiceUnavailableHttpException(null, 'Could not create temporary zip file.');
        }
        $zip->addFromString('domjudge-problem.ini', $iniString);
        $zip->addFromString('problem.yaml', $yamlString);

        if (!empty($problem->getProblemtext())) {
            $zip->addFromString('problem.' . $problem->getProblemtextType(),
                stream_get_contents($problem->getProblemtext()));
        }

        /** @var TestcaseWithContent[] $testcases */
        $testcases = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:TestcaseWithContent', 't')
            ->select('t')
            ->andWhere('t.problem = :problem')
            ->setParameter(':problem', $problem)
            ->orderBy('t.rank')
            ->getQuery()
            ->getResult();

        foreach ($testcases as $testcase) {
            $filename = sprintf('data/%s/%d', $testcase->getSample() ? 'sample' : 'secret', $testcase->getRank());
            $zip->addFromString($filename . '.in', $testcase->getInput());
            $zip->addFromString($filename . '.ans', $testcase->getOutput());

            if (!empty($testcase->getDescription(true))) {
                $description = $testcase->getDescription(true);
                if (strstr($description, "\n") === false) {
                    $description .= "\n";
                }
                $zip->addFromString($filename . '.desc', $description);
            }

            if (!empty($testcase->getImageType())) {
                $zip->addFromString($filename . '.' . $testcase->getImageType(), $testcase->getImage());
            }
        }

        /** @var Submission[] $solutions */
        $solutions = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Submission', 's')
            ->select('s')
            ->andWhere('s.problem = :problem')
            ->andWhere('s.contest = :contest')
            ->andWhere('s.expected_results IS NOT NULL')
            ->setParameter(':problem', $problem)
            ->setParameter(':contest', $this->dj->getCurrentContest())
            ->getQuery()
            ->getResult();

        foreach ($solutions as $solution) {
            $results = $solution->getExpectedResults();
            // Only support single outcome solutions
            if (count($results) !== 1) {
                continue;
            }

            $result = reset($results);

            $problemResult = null;

            foreach (SubmissionService::PROBLEM_RESULT_REMAP as $key => $val) {
                if (trim(mb_strtoupper($result)) == $val) {
                    $problemResult = mb_strtolower($key);
                }
            }

            if ($problemResult === null) {
                // unsupported result
                continue;
            }

            // NOTE: we store *all* submissions inside a subdirectory, also
            // single-file submissions. This is to prevent filename clashes
            // since we can't change the filename to something unique, since
            // that could break e.g. Java sources, even if _we_ support this
            // by default.
            $directory = sprintf('submissions/%s/s%d/', $problemResult, $solution->getSubmitid());
            /** @var SubmissionFileWithSourceCode $source */
            foreach ($solution->getFilesWithSourceCode() as $source) {
                $zip->addFromString($directory . $source->getFilename(), $source->getSourcecode());
            }
        }

        $zip->close();

        if ($contestProblem && $contestProblem->getShortname()) {
            $zipFilename = sprintf('p%d-%s.zip', $problem->getProbid(), $contestProblem->getShortname());
        } else {
            $zipFilename = sprintf('p%d.zip', $problem->getProbid());
        }

        $response = new StreamedResponse();
        $response->setCallback(function () use ($tempFilename) {
            $fp = fopen($tempFilename, 'rb');
            fpassthru($fp);
            unlink($tempFilename);
        });
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $zipFilename . '"');
        $response->headers->set('Content-Length', filesize($tempFilename));
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Connection', 'Keep-Alive');
        $response->headers->set('Accept-Ranges', 'bytes');

        return $response;
    }

    /**
     * @Route("/{probId}", name="admin_problem", requirements={"probId": "\d+"})
     * @param Request           $request
     * @param SubmissionService $submissionService
     * @param int               $probId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws Exception
     */
    public function viewAction(Request $request, SubmissionService $submissionService, int $probId)
    {
        /** @var Problem $problem */
        $problem = $this->em->getRepository(Problem::class)->find($probId);
        if (!$problem) {
            throw new NotFoundHttpException(sprintf('Problem with ID %s not found', $probId));
        }

        $restrictions = ['probid' => $problem->getProbid()];
        /** @var Submission[] $submissions */
        list($submissions, $submissionCounts) = $submissionService->getSubmissionList(
            $this->dj->getCurrentContests(),
            $restrictions
        );

        $data = [
            'problem' => $problem,
            'submissions' => $submissions,
            'submissionCounts' => $submissionCounts,
            'defaultMemoryLimit' => (int) $this->dj->dbconfig_get('memory_limit'),
            'defaultOutputLimit' => (int) $this->dj->dbconfig_get('output_limit'),
            'defaultRunExecutable' => (string) $this->dj->dbconfig_get('default_run'),
            'defaultCompareExecutable' => (string) $this->dj->dbconfig_get('default_compare'),
            'showContest' => count($this->dj->getCurrentContests()) > 1,
            'refresh' => [
                'after' => 15,
                'url' => $this->generateUrl('jury_problem', ['probId' => $problem->getProbid()]),
                'ajax' => true,
            ],
        ];

        // For ajax requests, only return the submission list partial
        if ($request->isXmlHttpRequest()) {
            $data['showTestcases'] = false;
            return $this->render('@DOMJudge/jury/partials/submission_list.html.twig', $data);
        }

        return $this->render('@DOMJudge/jury/problem.html.twig', $data);
    }

    /**
     * @Route("/{probId}/text", name="jury_problem_text", requirements={"probId": "\d+"})
     * @param int $probId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewTextAction(int $probId)
    {
        /** @var Problem $problem */
        $problem = $this->em->getRepository(Problem::class)->find($probId);
        if (!$problem) {
            throw new NotFoundHttpException(sprintf('Problem with ID %s not found', $probId));
        }

        switch ($problem->getProblemtextType()) {
            case 'pdf':
                $mimetype = 'application/pdf';
                break;
            case 'html':
                $mimetype = 'text/html';
                break;
            case 'txt':
                $mimetype = 'text/plain';
                break;
            default:
                throw new BadRequestHttpException(sprintf('Problem p%d text has unknown type', $probId));
        }

        $filename = sprintf('prob-%s.%s', $problem->getName(), $problem->getProblemtextType());
        $problemText = stream_get_contents($problem->getProblemtext());

        $response = new StreamedResponse();
        $response->setCallback(function () use ($problemText) {
            echo $problemText;
        });
        $response->headers->set('Content-Type', sprintf('%s; name="%s', $mimetype, $filename));
        $response->headers->set('Content-Disposition', sprintf('inline; filename="%s"', $filename));
        $response->headers->set('Content-Length', strlen($problemText));

        return $response;
    }

    /**
     * @Route("/{probId}/testcases", name="jury_problem_testcases", requirements={"probId": "\d+"})
     * @param Request $request
     * @param int     $probId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function testcasesAction(Request $request, int $probId)
    {
        /** @var Problem $problem */
        $problem = $this->em->getRepository(Problem::class)->find($probId);
        if (!$problem) {
            throw new NotFoundHttpException(sprintf('Problem with ID %s not found', $probId));
        }

        $testcaseData = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Testcase', 'tc', 'tc.rank')
            ->join('tc.testcase_content', 'content')
            ->select('tc', 'LENGTH(content.input) AS input_size', 'LENGTH(content.output) AS output_size',
                'LENGTH(content.image) AS image_size', 'tc.image_type')
            ->andWhere('tc.problem = :problem')
            ->setParameter(':problem', $problem)
            ->orderBy('tc.rank')
            ->getQuery()
            ->getResult();

        /** @var Testcase[] $testcases */
        $testcases = array_map(function ($data) {
            return $data[0];
        }, $testcaseData);

        if ($request->isMethod('POST')) {
            $messages = [];
            $maxrank = 0;
            $outputLimit = $this->dj->dbconfig_get('output_limit');
            $thumbnailSize = $this->dj->dbconfig_get('thumbnail_size', 128);
            foreach ($testcases as $rank => $testcase) {
                $newSample = isset($request->request->get('sample')[$rank]);
                if ($newSample !== $testcase->getSample()) {
                    $testcase->setSample($newSample);
                    $messages[] = sprintf('Set testcase %d to %sbe a sample testcase', $rank, $newSample ? '' : 'not ');
                }

                $newDescription = $request->request->get('description')[$rank];
                if ($newDescription !== $testcase->getDescription(true)) {
                    $testcase->setDescription($newDescription);
                    $messages[] = sprintf('Updated description of testcase %d ', $rank);
                }

                foreach (['input', 'output', 'image'] as $type) {
                    /** @var UploadedFile $file */
                    if ($file = $request->files->get('update_' . $type)[$rank]) {
                        $content = file_get_contents($file->getRealPath());
                        if ($type === 'image') {
                            $imageType = Utils::getImageType($content, $error);
                            if ($imageType === false) {
                                $this->addFlash('danger', sprintf('image: %s', $error));
                                return $this->redirectToRoute('jury_problem_testcases', ['probId' => $probId]);
                            }
                            $thumb = Utils::getImageThumb($content, $thumbnailSize,
                                $this->dj->getDomjudgeTmpDir(), $error);
                            if ($thumb === false) {
                                $thumb = null;
                                $this->addFlash('danger', sprintf('image: %s', $error));
                                return $this->redirectToRoute('jury_problem_testcases', ['probId' => $probId]);
                            }

                            $testcase->getTestcaseContent()
                                ->setImageThumb($thumb)
                                ->setimage($content)
                                ->setImageType($imageType);
                        } else {
                            $contentMethod = sprintf('set%s', ucfirst($type));
                            $md5Method = sprintf('setMd5sum%s', ucfirst($type));
                            $testcase->getTestcaseContent()->{$contentMethod}($content);
                            $testcase->getTestcaseContent()->{$md5Method}(md5($content));
                        }

                        $this->dj->auditlog('testcase', $probId, 'updated',
                            sprintf('%s rank %d', $type, $rank));

                        $message = sprintf('Updated %s for testcase %d with file %s (%s)', $type, $rank,
                            $file->getClientOriginalName(), Utils::printsize($file->getSize()));

                        if ($type == 'output' && $file->getSize() > $outputLimit * 1024) {
                            $message .= sprintf('<br><b>Warning: file size exceeds <code>output_limit</code> of %s kB. This will always result in wrong answers!</b>',
                                $outputLimit);
                        }

                        $messages[] = $message;
                    }
                }

                if ($rank > $maxrank) {
                    $maxrank = $rank;
                }
            }

            $maxrank++;

            $allOk = true;
            foreach (['input', 'output'] as $type) {
                if (!$request->files->get('add_' . $type)) {
                    $messages[] = sprintf('<b>Warning: new %s file was not selected, not adding new testcase</b>',
                        $type);
                    $allOk = false;
                }
            }

            if ($allOk) {
                $newTestcase = new TestcaseWithContent();
                $newTestcase
                    ->setRank($maxrank)
                    ->setProblem($problem)
                    ->setDescription($request->request->get('add_desc'))
                    ->setSample($request->request->has('add_sample'));
                foreach (['input', 'output'] as $type) {
                    $file = $request->files->get('add_' . $type);
                    $content = file_get_contents($file->getRealPath());
                    $contentMethod = sprintf('set%s', ucfirst($type));
                    $md5Method = sprintf('setMd5sum%s', ucfirst($type));
                    $newTestcase->{$contentMethod}($content);
                    $newTestcase->{$md5Method}(md5($content));
                }

                if ($imageFile = $request->files->get('add_image')) {
                    $content = file_get_contents($imageFile->getRealPath());
                    $imageType = Utils::getImageType($content, $error);
                    if ($imageType === false) {
                        $this->addFlash('danger', sprintf('image: %s', $error));
                        return $this->redirectToRoute('jury_problem_testcases', ['probId' => $probId]);
                    }
                    $thumb = Utils::getImageThumb($content, $thumbnailSize,
                        $this->dj->getDomjudgeTmpDir(), $error);
                    if ($thumb === false) {
                        $thumb = null;
                        $this->addFlash('danger', sprintf('image: %s', $error));
                        return $this->redirectToRoute('jury_problem_testcases', ['probId' => $probId]);
                    }

                    $newTestcase
                        ->setImageThumb($thumb)
                        ->setimage($content)
                        ->setImageType($imageType);
                }

                $this->em->persist($newTestcase);
                $this->dj->auditlog('testcase', $probId, 'added', sprintf("rank %d", $maxrank));

                $inFile = $request->files->get('add_input');
                $outFile = $request->files->get('add_output');
                $message = sprintf('Added new testcase %d from files %s (%s) and %s (%s)', $maxrank,
                    $inFile->getClientOriginalName(), Utils::printsize($inFile->getSize()),
                    $outFile->getClientOriginalName(), Utils::printsize($outFile->getSize()));

                if ($newTestcase->getOutput() > $outputLimit * 1024) {
                    $message .= sprintf('<br><b>Warning: file size exceeds <code>output_limit</code> of %s kB. This will always result in wrong answers!</b>',
                        $outputLimit);
                }

                if (empty($newTestcase->getInput()) || empty($newTestcase->getOutput())) {
                    $message .= '<br /><b>Warning: empty testcase file(s)!</b>';
                }

                $messages[] = $message;
            }

            $this->em->flush();

            if (!empty($messages)) {
                $message = '<ul>' . implode('', array_map(function (string $message) {
                    return sprintf('<li>%s</li>', $message);
                }, $messages)) . '</ul>';

                $this->addFlash('info', $message);
            }
            return $this->redirectToRoute('jury_problem_testcases', ['probId' => $probId]);
        }

        $data = [
            'problem' => $problem,
            'testcases' => $testcases,
            'testcaseData' => $testcaseData,
        ];

        return $this->render('@DOMJudge/jury/problem_testcases.html.twig', $data);
    }

    /**
     * @Route(
     *     "/{probId}/testcases/{rank}/move/{direction}",
     *     name="jury_problem_testcase_move",
     *     requirements={"probId": "\d+", "rank": "\d+", "direction": "up|down"}
     *     )
     * @param int    $probId
     * @param int    $rank
     * @param string $direction
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function moveTestcaseAction(int $probId, int $rank, string $direction)
    {
        /** @var Problem $problem */
        $problem = $this->em->getRepository(Problem::class)->find($probId);
        if (!$problem) {
            throw new NotFoundHttpException(sprintf('Problem with ID %s not found', $probId));
        }

        /** @var Testcase[] $testcases */
        $testcases = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Testcase', 'tc', 'tc.rank')
            ->select('tc')
            ->andWhere('tc.problem = :problem')
            ->setParameter(':problem', $problem)
            ->orderBy('tc.rank')
            ->getQuery()
            ->getResult();

        // First find testcase to switch with
        /** @var Testcase|null $last */
        $last = null;
        /** @var Testcase|null $other */
        $other = null;
        /** @var Testcase|null $current */
        $current = null;

        foreach ($testcases as $testcaseRank => $testcase) {
            if ($testcaseRank == $rank) {
                $current = $testcase;
            }
            if ($testcaseRank == $rank && $direction == 'up') {
                $other = $last;
                break;
            }
            if ($last !== null && $rank == $last->getRank() && $direction == 'down') {
                $other = $testcase;
                break;
            }
            $last = $testcase;
        }

        if ($current !== null && $other !== null) {
            // (probid, rank) is a unique key, so we must switch via a temporary rank, and use a transaction.
            $this->em->transactional(function () use ($current, $other) {
                $otherRank = $other->getRank();
                $currentRank = $current->getRank();
                $other->setRank(-1);
                $current->setRank(-2);
                $this->em->flush();
                $current->setRank($otherRank);
                $other->setRank($currentRank);
            });

            $this->dj->auditlog('testcase', $probId, 'switch rank',
                sprintf("%d <=> %d", $current->getRank(), $other->getRank()));
        }

        return $this->redirectToRoute('jury_problem_testcases', ['probId' => $probId]);
    }

    /**
     * @Route(
     *     "/{probId}/testcases/{rank}/fetch/{type}",
     *     name="jury_problem_testcase_fetch",
     *     requirements={"probId": "\d+", "rank": "\d+", "type": "input|output|image"}
     *     )
     * @param int    $probId
     * @param int    $rank
     * @param string $type
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function fetchTestcaseAction(int $probId, int $rank, string $type)
    {
        /** @var TestcaseWithContent $testcase */
        $testcase = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:TestcaseWithContent', 'tc')
            ->select('tc')
            ->andWhere('tc.probid = :problem')
            ->andWhere('tc.rank = :rank')
            ->setParameter(':problem', $probId)
            ->setParameter(':rank', $rank)
            ->getQuery()
            ->getOneOrNullResult();
        if (!$testcase) {
            throw new NotFoundHttpException(sprintf('Testcase with rank %d for problem %d not found', $rank, $probId));
        }

        if ($type === 'image') {
            $extension = $testcase->getImageType();
            $mimetype = sprintf('image/%s', $extension);
        } else {
            $extension = substr($type, 0, -3);
            $mimetype = 'text/plain';
        }

        $filename = sprintf('p%d.t%d.%s', $probId, $rank, $extension);
        $content = null;

        switch ($type) {
            case 'input':
                $content = $testcase->getInput();
                break;
            case 'output':
                $content = $testcase->getOutput();
                break;
            case 'image':
                $content = $testcase->getImage();
                break;
        }

        $response = new StreamedResponse();
        $response->setCallback(function () use ($content) {
            echo $content;
        });
        $response->headers->set('Content-Type', sprintf('%s; name="%s', $mimetype, $filename));
        $response->headers->set('Content-Disposition', sprintf('inline; filename="%s"', $filename));
        $response->headers->set('Content-Length', strlen($content));

        return $response;
    }

    /**
     * @Route("/{probId}/edit", name="jury_problem_edit", requirements={"probId": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param int     $probId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function editAction(Request $request, int $probId)
    {
        /** @var Problem $problem */
        $problem = $this->em->getRepository(Problem::class)->find($probId);
        if (!$problem) {
            throw new NotFoundHttpException(sprintf('Problem with ID %s not found', $probId));
        }

        $form = $this->createForm(ProblemType::class, $problem);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveEntity($this->em, $this->eventLogService, $this->dj, $problem,
                $problem->getProbid(), false);
            return $this->redirectToRoute('jury_problem', ['probId' => $problem->getProbid()]);
        }

        $data = [];
        $uploadForm = $this->createForm(ProblemUploadType::class, $data);
        $uploadForm->handleRequest($request);

        if ($uploadForm->isSubmitted() && $uploadForm->isValid()) {
            $data = $uploadForm->getData();
            /** @var UploadedFile $archive */
            $archive = $data['archive'];
            $messages = [];

            /** @var Contest|null $contest */
            $contest = $data['contest'] ?? null;
            /** @var ContestProblem $contestProblem */
            foreach ($problem->getContestProblems() as $contestProblem) {
                if (($currentContest = $this->dj->getCurrentContest()) !== null &&
                    $contestProblem->getCid() === $currentContest->getCid()) {
                    $contest = $currentContest;
                    break;
                }
            }
            try {
                $zip = $this->dj->openZipFile($archive->getRealPath());
                $clientName = $archive->getClientOriginalName();
                if ($this->importProblemService->importZippedProblem($zip, $clientName, $problem,
                    $contest, $messages)) {
                    $this->dj->auditlog('problem', $problem->getProbid(), 'upload zip', $clientName);
                } else {
                    $message = '<ul>' . implode('', array_map(function (string $message) {
                        return sprintf('<li>%s</li>', $message);
                    }, $messages)) . '</ul>';
                    $this->addFlash('danger', $message);
                    return $this->redirectToRoute('jury_problem', ['probId' => $problem->getProbid()]);
                }
            } catch (Exception $e) {
                $messages[] = $e->getMessage();
            } finally {
                if (isset($zip)) {
                    $zip->close();
                }
            }

            if (!empty($messages)) {
                $message = '<ul>' . implode('', array_map(function (string $message) {
                    return sprintf('<li>%s</li>', $message);
                }, $messages)) . '</ul>';

                $this->addFlash('info', $message);
            }

            return $this->redirectToRoute('jury_problem', ['probId' => $problem->getProbid()]);
        }

        return $this->render('@DOMJudge/jury/problem_edit.html.twig', [
            'problem' => $problem,
            'form' => $form->createView(),
            'uploadForm' => $uploadForm->createView(),
        ]);
    }

    /**
     * @Route("/{probId}/delete", name="jury_problem_delete", requirements={"probId": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param int     $probId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function deleteAction(Request $request, int $probId)
    {
        /** @var Problem $problem */
        $problem = $this->em->getRepository(Problem::class)->find($probId);
        if (!$problem) {
            throw new NotFoundHttpException(sprintf('Problem with ID %s not found', $probId));
        }

        return $this->deleteEntity($request, $this->em, $this->dj, $problem,
            $problem->getName(), $this->generateUrl('jury_problems'));
    }

    /**
     * @Route("/semester", name="admin_semester")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function semester(Request $request)
    {
        $data = [];
        $user = $this->dj->getUser();
        $currentSemester = $this->dj->getCurrentSemester();
        $currentTime = Utils::now();

        $semesters = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Semester', 's')
            ->select('s.semid', 's.name', 's.startedTime', 's.finishedTime', 's.description')
            ->orderBy('s.startedTime', 'DESC')
            ->getQuery()
            ->getResult();        

        if (count($semesters) > 0) {
            $data['listSemester'] = json_encode($semesters);            
        }else {
            $data['message'] = "No semester created!";
        }
        return $this->render('@DOMJudge/Fls/Admin/semester.html.twig', $data);
    }

    /**
     * @Route("/semester-add", name="admin_add_semester")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addSemester(Request $request)
    {
        $semName = $request->request->get('semName');
        $startDate = $request->request->get('startDate');
        $endDate = $request->request->get('endDate');

        if($semName == '') {
            return $this->json([
                "status" => false,
                "field" => 'name',
                "message" => 'Please enter semester name!',
            ]);
        }
        //check exist semester name
        $exist_semester = $this->em->getRepository(Semester::class)->findBy(['name' => $semName]);
        if($exist_semester != null) {
            return $this->json([
                "status" => false,
                "field" => 'name',
                "message" => 'Semester ' . $semName . ' already exist, please check again!',
            ]);
        }
        if($this->validateDate($startDate) != 'valid') {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'Start date '. $this->validateDate($startDate) . ', please check again!',
            ]);
        }
        if($this->validateDate($endDate) != 'valid') {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'End date '. $this->validateDate($endDate) . ', please check again!',
            ]);
        }

        $startDate = (DateTime::createFromFormat('d/m/Y', $startDate))->getTimestamp();
        $endDate = (DateTime::createFromFormat('d/m/Y', $endDate))->getTimestamp();

        //validate date
        if($startDate >= $endDate) {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'Start date must be < End date, please check again!',
            ]);
        }
        $duplicateStartTime = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Semester', 's')
            ->select('s')
            ->where('s.startedTime <= :startTime')
            ->andWhere('s.finishedTime >= :startTime')
            ->setParameter(':startTime', $startDate)
            ->orderBy('s.startedTime', 'DESC')
            ->getQuery()
            ->getResult();

        $duplicateEndTime = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Semester', 's')
            ->select('s')
            ->where('s.startedTime <= :endTime')
            ->andWhere('s.finishedTime >= :endTime')
            ->setParameter('endTime', $endDate)
            ->orderBy('s.startedTime', 'DESC')
            ->getQuery()
            ->getResult();    

        if($duplicateStartTime != null) {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'Start date is duplicate with semester: '. $duplicateStartTime[0]->getName() .', please check again!',
            ]);
        }
        if($duplicateEndTime != null) {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'End date is duplicate with semester: '. $duplicateEndTime[0]->getName() .', please check again!',
            ]);
        }

        $semester = new semester();
        $semester->setName($semName);
        $semester->setStartedTime($startDate);
        $semester->setFinishedTime($endDate);

        $this->em->persist($semester);
        $this->em->flush();

        return $this->json([
            "status" => true,
            "message" => 'Add Semester Successfully!',
        ]);
    }

    /**
     * @Route("/semester-edit", name="admin_edit_semester")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editSemester(Request $request)
    {
        $semId = $request->request->get('semId');
        $semName = $request->request->get('semName');
        $startDate = $request->request->get('startDate');
        $endDate = $request->request->get('endDate');

        if($semName == '') {
            return $this->json([
                "status" => false,
                "field" => 'name',
                "message" => 'Please enter semester name!',
            ]);
        }
        //check exist semester name
        $ori_semester = $this->em->getRepository(Semester::class)->findBy(['semid' => $semId]);
        if($ori_semester[0]->getName() != $semName) {
            $exist_semester = $this->em->getRepository(Semester::class)->findBy(['name' => $semName]);
            if($exist_semester != null) {
                return $this->json([
                    "status" => false,
                    "field" => 'name',
                    "message" => 'Semester ' . $semName . ' already exist, please check again!',
                ]);
            }
        }
        
        if($this->validateDate($startDate) != 'valid') {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'Start date '. $this->validateDate($startDate) . ', please check again!',
            ]);
        }
        if($this->validateDate($endDate) != 'valid') {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'Start date '. $this->validateDate($endDate) . ', please check again!',
            ]);
        }

        $startDate = (DateTime::createFromFormat('d/m/Y', $startDate))->getTimestamp();
        $endDate = (DateTime::createFromFormat('d/m/Y', $endDate))->getTimestamp();

        //validate date
        if($startDate >= $endDate) {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'Start date must be < End date, please check again!',
            ]);
        }
        $changing_semester = $this->em->getRepository(Semester::class)->find($semId);

        $duplicateStartTime = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Semester', 's')
            ->select('s')
            ->where('s.startedTime <= :startTime')
            ->andWhere('s.finishedTime >= :startTime')
            ->andWhere('s.name != :name')
            ->setParameter(':startTime', $startDate)
            ->setParameter('name', $changing_semester->getName())
            ->orderBy('s.startedTime', 'DESC')
            ->getQuery()
            ->getResult();

        $duplicateEndTime = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Semester', 's')
            ->select('s')
            ->where('s.startedTime <= :endTime')
            ->andWhere('s.finishedTime >= :endTime')
            ->andWhere('s.name != :name')
            ->setParameter('endTime', $endDate)
            ->setParameter('name', $changing_semester->getName())
            ->orderBy('s.startedTime', 'DESC')
            ->getQuery()
            ->getResult();    

        if($duplicateStartTime != null) {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'Start date is duplicate with semester: '. $duplicateStartTime[0]->getName() .', please check again!',
            ]);
        }
        if($duplicateEndTime != null) {
            return $this->json([
                "status" => false,
                "field" => 'date',
                "message" => 'End date is duplicate with semester: '. $duplicateEndTime[0]->getName() .', please check again!',
            ]);
        }

        $changing_semester->setName($semName);
        $changing_semester->setStartedTime($startDate);
        $changing_semester->setFinishedTime($endDate);

        $this->em->merge($changing_semester);
        $this->em->flush();

        return $this->json([
            "status" => true,
            "message" => 'Update Semester Successfully!',
        ]);
    }

    //validate date dd/mm/yyyy
    public function validateDate($date) {
        if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/",$date)) {
            $split_date = explode("/", $date);
            if(count($split_date) != 3) {
                return 'wrong format';
            }
            if(checkdate((int) $split_date[1],(int) $split_date[0], (int) $split_date[2])) {
                return 'valid';
            }else {
                return 'invalid';
            }
        } else {
            return 'wrong format';
        }
        
    }

     /**
     * @Route("/lab-room", name="admin_rooms")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function labRoom(Request $request)
    {
        $data = [];
        $user = $this->dj->getUser();
        $currentTime = Utils::now();

        $labrooms = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:LabRoom', 'lr')
            ->select('lr.roomid', 'lr.roomname')
            ->orderBy('lr.roomname')
            ->getQuery()
            ->getResult();        

        if (count($labrooms) > 0) {
            $data['listLabRooms'] = json_encode($labrooms);            
        }else {
            $data['message'] = "No Laboratory Room created!";
        }
        return $this->render('@DOMJudge/Fls/Admin/lab_rooms.html.twig', $data);
    }

     /**
     * @Route("/rooms-add", name="admin_add_room")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addRoom(Request $request)
    {
        $roomName = $request->request->get('roomName');

        if($roomName == '') {
            return $this->json([
                "status" => false,
                "field" => 'name',
                "message" => 'Please enter semester name!',
            ]);
        }
        //check exist semester name
        $exist_room = $this->em->getRepository(LabRoom::class)->findBy(['roomname' => $roomName]);
        if($exist_room != null) {
            return $this->json([
                "status" => false,
                "field" => 'name',
                "message" => 'Room ' . $roomName . ' already exist, please check again!',
            ]);
        }   

        $room = new LabRoom();
        $room->setRoomname($roomName);

        $this->em->persist($room);
        $this->em->flush();

        return $this->json([
            "status" => true,
            "message" => 'Add Room Successfully!',
        ]);
    }

     /**
     * @Route("/rooms-edit", name="admin_edit_room")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editRoom(Request $request)
    {
        $roomName = $request->request->get('roomName');
        $roomId = $request->request->get('roomId');

        if($roomName == '') {
            return $this->json([
                "status" => false,
                "field" => 'name',
                "message" => 'Please enter semester name!',
            ]);
        }
        //check exist room name
        $exist_room = $this->em->getRepository(LabRoom::class)->findBy(['roomname' => $roomName]);
        if($exist_room != null) {
            return $this->json([
                "status" => false,
                "field" => 'name',
                "message" => 'Room ' . $roomName . ' already exist, please check again!',
            ]);
        }
        $changing_room = $this->em->getRepository(LabRoom::class)->find($roomId);

        $changing_room->setRoomname($roomName);

        $this->em->persist($changing_room);
        $this->em->flush();

        return $this->json([
            "status" => true,
            "message" => 'Edit Room Successfully!',
        ]);
    }

    /**
     * @Route("/room-ip/{roomId}", name="room_ip", requirements={"roomId": "^$|\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function roomIp(Request $request, $roomId)
    {
        $data = [];
        // $dataRequest = json_decode($request->getContent());
        $labrooms = $this->em->getRepository(LabRoom::class)->find($roomId);
        if (!empty($labrooms)) {
            $data['room'] = $labrooms;
            $listRoomIp = $this->em->getRepository(Roomip::class)->createQueryBuilder('rip')
                ->where('rip.roomid = :roomid')
                ->setParameter('roomid', $roomId)
                ->getQuery()
                ->getResult();
            if(!empty($listRoomIp)){
                $list = [];
                foreach ($listRoomIp as $value) {
                    $temp['ipid'] = $value->getIpid();
                    $temp['ipAddress'] = $value->getIpAddress();
                    $temp['roomid'] = $value->getRoomid();
                    $temp['pccode'] = $value->getPccode();
                    $list[] = $temp;
                    unset($temp);
                }
                $data['listRoomIp'] = json_encode($list);
                unset($list);
            }else{
                $data['message'] = "No possition created!";
            }
        }else{
            $data['message'] = "Room not found!";
        }
        
        return $this->render('@DOMJudge/Fls/Admin/room_ip.html.twig', $data);
    }

    /**
     * @Route("/room-ip-add", name="admin_add_room_ip")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addRoomIp(Request $request)
    {

        $roomid = $request->request->get('roomid');
        
        if($roomid == '') {
            return $this->json([
                "status" => false,
                "field" => 'roomid',
                "message" => 'room not exit!',
            ]);
        }else{
            $roomid = $this->em->getRepository(LabRoom::class)->find($roomid);
            if(empty($roomid)){
                return $this->json([
                    "status" => false,
                    "field" => 'roomid',
                    "message" => 'room not exit!',
                ]);
            }
        }

        $ipAddress = $request->request->get('ipAddress');        
        if($ipAddress == '') {
            return $this->json([
                "status" => false,
                "field" => 'ipAddress',
                "message" => 'Please enter ip address!',
            ]);
        }

        $pcCode = $request->request->get('pcCode');
        if($pcCode == '') {
            return $this->json([
                "status" => false,
                "field" => 'pcCode',
                "message" => 'Please enter pc code!',
            ]);
        }
        //check exist ip
        $exist_ip = $this->em->getRepository(Roomip::class)->findBy(['ipAddress' => $ipAddress]);
        if($exist_ip != null) {
            return $this->json([
                "status" => false,
                "field" => 'ipAddress',
                "message" => 'Ip ' . $ipAddress . ' already exist, please check again!',
            ]);
        }   

        //check exist pc code
        $exist_pc = $this->em->getRepository(Roomip::class)->findBy(['pccode' => $pcCode]);
        if($exist_pc != null) {
            return $this->json([
                "status" => false,
                "field" => 'pcCode',
                "message" => 'PC-code ' . $pcCode . ' already exist, please check again!',
            ]);
        }   

        $Roomip = new Roomip();
        $Roomip->setIpAddress($ipAddress);
        $Roomip->setPccode($pcCode);
        $Roomip->setRoomid($roomid);

        $this->em->persist($Roomip);
        $this->em->flush();

        return $this->json([
            "status" => true,
            "message" => 'Add positions Successfully!',
        ]);
    }

     /**
     * @Route("/room-ip-edit", name="admin_edit_room_ip")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editRoomIp(Request $request)
    {
        $roomid = $request->request->get('roomid');
        
        if($roomid == '') {
            return $this->json([
                "status" => false,
                "field" => 'roomid',
                "message" => 'room not exit!',
            ]);
        }else{
            $roomid = $this->em->getRepository(LabRoom::class)->find($roomid);
            if(empty($roomid)){
                return $this->json([
                    "status" => false,
                    "field" => 'roomid',
                    "message" => 'room not exit!',
                ]);
            }
        }

        $ipid = $request->request->get('ipid');        
        $roomIp = $this->em->getRepository(Roomip::class)->find($ipid);
        if(empty($roomIp)){
            return $this->json([
                "status" => false,
                "field" => 'roomid',
                "message" => 'Positions not exit!',
            ]);
        }

        $ipAddress = $request->request->get('ipAddress');
        if($ipAddress == '') {
            return $this->json([
                "status" => false,
                "field" => 'ipAddress',
                "message" => 'Please enter ip address!',
            ]);
        }

        $pcCode = $request->request->get('pcCode');
        if($pcCode == '') {
            return $this->json([
                "status" => false,
                "field" => 'pcCode',
                "message" => 'Please enter pc code!',
            ]);
        }

        if($roomIp->getIpAddress() != $ipAddress){
            //check exist ip
            $exist_ip = $this->em->getRepository(Roomip::class)->findBy(['ipAddress' => $ipAddress]);
            if($exist_ip != null) {
                return $this->json([
                    "status" => false,
                    "field" => 'ipAddress',
                    "message" => 'Ip ' . $ipAddress . ' already exist, please check again!',
                ]);
            }
            $roomIp->setIpAddress($ipAddress);
        }

        if($roomIp->getPccode() != $pcCode){
            //check exist pc code
            $exist_pc = $this->em->getRepository(Roomip::class)->findBy(['pccode' => $pcCode]);
            if($exist_pc != null) {
                return $this->json([
                    "status" => false,
                    "field" => 'pcCode',
                    "message" => 'PC-code ' . $pcCode . ' already exist, please check again!',
                ]);
            } 
            $Roomip->setPccode($pcCode);
        }

        $this->em->persist($roomIp);
        $this->em->flush();

        return $this->json([
            "status" => true,
            "message" => 'Edit positions Successfully!',
        ]);
    }

}
