<?php declare (strict_types = 1);

namespace DOMJudgeBundle\Controller\Student;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\Judging;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\Submission;
use DOMJudgeBundle\Entity\Testcase;
use DOMJudgeBundle\Entity\Userproblem;
use DOMJudgeBundle\Entity\Clarification;
use DOMJudgeBundle\Entity\ClassSchedule;
use DOMJudgeBundle\Form\Fls\SubmitProblemType;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Service\ImportProblemService;
use DOMJudgeBundle\Service\SubmissionService;
use DOMJudgeBundle\Utils\FlsConstant;
use DOMJudgeBundle\Utils\Utils;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/student")
 * @Security("has_role('ROLE_STUDENT')")
 */
class StudentController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    private $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * @var SubmissionService
     */
    protected $submissionService;

    /**
     * @var ImportProblemService
     */
    protected $importProblemService;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService,
        SubmissionService $submissionService,
        ImportProblemService $importProblemService,
        FormFactoryInterface $formFactory
    ) {
        $this->em = $em;
        $this->dj = $dj;
        $this->eventLogService = $eventLogService;
        $this->submissionService = $submissionService;
        $this->importProblemService = $importProblemService;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("", name="student_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function indexAction(Request $request)
    {
        $data = [];
        // get list Problem
        $user = $this->dj->getUser();
        // check flag menu
        $moreClass = false;
        if (count($user->getCid()) > 1) {
            $data['moreClass'] = true;
        } else {
            $data['moreClass'] = false;
        }

        $currentClass = $this->dj->getCurrentClass($user->getUserid());
        if ($currentClass == null) {
            $class = $user->getCid();
            $list = [];
            foreach ($class as $key => $c) {
                $class_show['id'] = $c->getCid();
                $class_show['name'] = $c->getName();
                $class_show['language'] = $c->getLangId()->getName();
                // get status class is active
                $class_show['status'] = $c->getStarttimeEnabled();
                $list[] = $class_show;
            }
            $data['listClass'] = json_encode($list);
            $data['moreClass'] = false;
            $data['hideScore'] = true;
            return $this->render('@DOMJudge/Fls/Student/selectClass.html.twig', $data);
        }
        if ($this->dj->isActiveCLass($currentClass->getCid())) {
            $data['class_enabled'] = true;
        } else {
            $data['class_enabled'] = false;
            $this->addFlash('danger', 'Class ' . $currentClass->getName() . ' is not enabled! Please contact with mentor!');
            return $this->render('@DOMJudge/Fls/Student/home.html.twig', $data);
        }
        $userid = $user->getUserid();

        // get list problem
        $problems = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Problem', 'p')
            ->leftJoin(
                'DOMJudgeBundle:Userproblem',
                'up',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'p.probid = up.probid'
            )
            ->where('up.userid = :userid')
            ->setParameter(':userid', $userid)
            ->andwhere('up.cid = :Cid')
            ->setParameter(':Cid', $currentClass->getCid())
            ->select('p.probid as id,
                p.shortname as name,
                p.name as content,
                p.points as point,
                up.status as result,
                up.changed as status')
            ->orderBy('p.points', 'ASC')
            ->getQuery()->getResult();
        $data['problems'] = json_encode($problems);

        // get data chart
        $charts = [];
        $arrayResult = array_column($problems, 'result');
        $totalSize = sizeof($arrayResult);
        $totalNull = $totalSize - count(array_filter($arrayResult));
        $arrayResult = array_count_values(array_filter($arrayResult));
        $charts['labels'] = [FlsConstant::ProblemStatus[0]];
        $charts['data'] = [$totalNull];
        $charts['color'] = ['#343a40'];
        foreach ($arrayResult as $key => $value) {
            if ($key != -1) {
                array_push($charts['labels'], FlsConstant::ProblemStatus[$key]);
                array_push($charts['data'], $value);
                switch ($key) {
                    case 1:
                        array_push($charts['color'], '#ffc107');
                        break;
                    case 2:
                        array_push($charts['color'], '#007bff');
                        break;
                    case 3:
                        array_push($charts['color'], '#17a2b8');
                        break;
                    case 4:
                        array_push($charts['color'], '#28a745');
                        break;
                    case 5:
                        array_push($charts['color'], '#dc3545');
                        break;
                    default:
                        break;
                }
            }
        }
        $data['charts'] = json_encode($charts);

        // ->leftJoin('DOMJudgeBundle:JudgingRun', 'judr', \Doctrine\ORM\Query\Expr\Join::WITH,
        // 'judr.judgingid = jud.judgingid')
        //get list submitted
        $submitted = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Submission', 'sub')
            ->leftJoin(
                'DOMJudgeBundle:Judging',
                'jud',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'sub.submitid = jud.submitid'
            )
            ->where('sub.userid = :userid')
            ->setParameter(':userid', $userid)
            ->andwhere('sub.cid = :cid')
            ->setParameter(':cid', $currentClass->getCid())
            ->select('sub,jud.result as result')
            ->orderBy('sub.submittime', 'DESC')
            ->getQuery()->getResult();

        $listSubmitted = [];
        foreach ($submitted as $key => $sub) {
            $temp['id'] = $sub[0]->getSubmitid();
            $temp['time'] = strftime('%H:%M:%S (%d-%m-%Y)', (int) $sub[0]->getSubmittime());
            $temp['problem'] = $sub[0]->getProblem()->getName();
            $temp['shortname'] = $sub[0]->getProblem()->getShortname();
            $temp['result'] = $sub['result'];

            $listSubmitted[] = $temp;
            unset($temp);
        }
        $data['submitted'] = json_encode($listSubmitted);
        return $this->render('@DOMJudge/Fls/Student/home.html.twig', $data);
    }

    /**
     * @Route("/class/{className}", methods={"GET"}, name="selected_class", requirements={"className": "^$|\d+"})
     * @Security("has_role('ROLE_STUDENT')")
     * @param string $className
     * @throws Exception
     */
    public function setSelectedClass(string $className, Request $request)
    {
        $check_ip = $this->dj->dbconfig_get('check_student_ip_posittion');
        
        $classes = $this->em->getRepository(Classes::class)->find($className);
        if($check_ip == 1) {
            $result = $this->dj->checkStudentPosition($this->dj->getUser()->getUserid(), $classes->getCid());
            if($result === true) {
                $response = $this->redirectToRoute('student_index');
                $key = $this->dj->getUser()->getUserid() . 'classes_id';
                $this->dj->setCookie(
                    $key,
                    (string) $classes->getCid(),
                    strtotime('now + 90 minutes'),
                    null,
                    '',
                    false,
                    false,
                    $response
                );
                return $response;
            }else if($result === false) {
                $this->addFlash('danger', 'You are not assign to any PC in Class: ' . $classes->getName());
                return $this->render('@DOMJudge/Fls/Student/home.html.twig');
            }else {
                $this->addFlash('danger', 'Wrong PC. You have been assigned to PC: ' . $result . ' in Class: ' . $classes->getName());
                return $this->render('@DOMJudge/Fls/Student/home.html.twig');
            }
        }else {
            $response = $this->redirectToRoute('student_index');
            $key = $this->dj->getUser()->getUserid() . 'classes_id';
            $this->dj->setCookie(
                $key,
                (string) $classes->getCid(),
                strtotime('now + 90 minutes'),
                null,
                '',
                false,
                false,
                $response
            );
            return $response;
        }       
    }

    /**
     * @Route("/", methods={"GET"}, name="exit_class")
     * @Security("has_role('ROLE_STUDENT')")
     * @param string $className
     * @throws Exception
     */
    public function exitClass(Request $request)
    {
        $response = $this->redirectToRoute('student_index');
        $key = $this->dj->getUser()->getUserid() . 'classes_id';
        $this->dj->clearCookie($key, null, '', false, false, $response);
        return $response;
    }

    /**
     * @Route("/problem/detail/{problemId}", name="detail_problem", requirements={"problemId": "^$|\d+"})
     * @Security("has_role('ROLE_STUDENT')")
     * @throws Exception
     */
    public function detailProblem(string $problemId, Request $request)
    {
        $data = [];
        $data['allowSubmit'] = true;
        $user = $this->dj->getUser();
        // get all class of user
        $class = $this->dj->getCurrentClass($user->getUserid());

        if ($class == null) {
            return $this->redirectToRoute('student_index');
        }

        $userproblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
            ->where('up.userid = :userid')
            ->setParameter('userid', $user->getUserid())
            ->andwhere('up.probid = :probid')
            ->setParameter('probid', $problemId)
            ->andwhere('up.cid = :cid')
            ->setParameter('cid', $class->getCid())
            ->select('up')
            ->getQuery()->getOneOrNullResult();
        if ($userproblem) {
            $userproblemStatus = $userproblem->getStatus();
            if ($userproblemStatus == 0) {
                $userproblem->setStatus(1);
                $userproblem->setStartedTime(Utils::now());
                $this->em->flush();
            } else if ($userproblemStatus == 4 || $userproblemStatus == 5) {
                $data['allowSubmit'] = false;
                if($userproblemStatus == 4){
                    $data['allowDowload'] = true;
                }
            } else {
                $data['allowDowload'] = true;
                $data['allowSubmit'] = true;
            }
        } else {
            $this->addFlash('danger', 'Problem of stduent not found');
            return $this->render('@DOMJudge/Fls/Student/detailProblem.html.twig');
        }
        if ($userproblem && $userproblem->getChanged() == 2) {
            return $this->redirectToRoute('student_index');
        }
        $problem = $this->em->getRepository(Problem::class)->find($problemId);
        $data['problem'] = $problem;
        $data['changeStatus'] = ($userproblem->getChanged()) != null ? FlsConstant::ChangeStatus[$userproblem->getChanged()] : null;
        $data['UrlProblemText'] = $this->generateUrl('problem_text', ['probId' => $problem->getProbid()]);
        $langid = $problem->getLangid();
        $data['language'] = $langid->getName();
        // check flag menu
        if (count($user->getCid()) > 1) {
            $data['moreClass'] = true;
        } else {
            $data['moreClass'] = false;
        }

        // submission form
        $form = $this->formFactory
            ->createBuilder(SubmitProblemType::class)
            ->setAction($this->generateUrl('detail_problem', ['problemId' => $problem->getProbid()]))
            ->getForm();

        $form->handleRequest($request);
        $data['formSubmit'] = $form->createView();

        if ($this->dj->isActiveCLass($class->getCid())) {
            $data['class_enabled'] = true;
        } else {
            $data['class_enabled'] = false;
            $this->addFlash('danger', 'Class ' . $class->getName() . ' is not enabled! Please contact with mentor!');
            return $this->render('@DOMJudge/Fls/Student/detailProblem.html.twig', $data);
        }

        // get requiment of class
        $conn = $this->em->getConnection();
        $sql = "SELECT cid, probid, add_requirement as addRequirement FROM classproblem where cid = :cid and probid = :probid LIMIT 1";

        $statement = $conn->prepare($sql);
        $statement->bindValue('cid', $class->getCid());
        $statement->bindValue('probid', $userproblem->getProbid()->getProbid());
        $statement->execute();
        $classPro = $statement->fetchAll();
        if(!empty($classPro )){
            $data['requirementClass'] = $classPro[0]['addRequirement'];
        }
        // get requiment of student
        if(!empty($userproblem) && $userproblem->getAddRequirement() != null){
            $data['requirementStudent'] = $userproblem->getAddRequirement();
        }        

        // get last submission
        $lastSubmission = $this->em->getRepository(Submission::class)->createQueryBuilder('s')
            ->select('s')
            ->andwhere('s.user = :userid')
            ->andwhere('s.classes = :cid')
            ->andwhere('s.problem = :probid')
            ->setParameter('userid', $user->getUserid())
            ->setParameter('cid', $class->getCid())
            ->setParameter('probid', $problem->getProbid())
            ->orderBy('s.submittime', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        $data['submission'] = $lastSubmission;

        if ($lastSubmission) {
            // get last file submitted
            $fileName = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:SubmissionFileWithSourceCode', 'file')
                ->select('file.filename')
                ->andWhere('file.submission = :submission')
                ->setParameter(':submission', $lastSubmission->getSubmitid())
                ->getQuery()
                ->getOneOrNullResult();

            // get last juding
            $lastJudging = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Judging', 'j')
                ->select('j')
                ->andWhere('j.submission = :submission')
                ->andWhere('j.valid = 1')
                ->setParameter(':submission', $lastSubmission)
                ->orderBy('j.judgingid', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
            if ($lastJudging != null) {
                $listTest = $this->getListTestCase($problem, $lastJudging,1);
                $listAllTest = $this->getListTestCase($problem, $lastJudging);
            } else {
                $listTest = $this->getListTestCase($problem,null,1);
                $listAllTest = $this->getListTestCase($problem);
            }
        } else {
            $fileName = null;
            $listTest = $this->getListTestCase($problem,null,1);
            $listAllTest = $this->getListTestCase($problem);
        }
        $data['fileName'] = $fileName['filename'];
        $data['listTest'] = json_encode($listTest);
        // count number of test done
         
        $testDone = 0;
        $testDo = 0;
        foreach ($listAllTest as $object) {
            // count  total test done
            if ($object['result'] == 'correct') {
                $testDone++;
            }
            // count  total test do
            if ($object['result'] != null) {
                $testDo++;
            }
        }
        $info['done'] = $testDone;
        $info['do'] = $testDo;
        $info['total'] = $listAllTest != null ? count($listAllTest) : 0;
        $data['testInfo'] = $info;

        $data['asks'] =  $this->getAskData($userproblem->getUserproblemid());
        $data['curUsername'] = $user->getUsername();
        $data['timeGetAsks'] = Utils::now();
        $data['userProblemID'] = $userproblem->getUserproblemid();

        // handle event submit form
        if ($form->isSubmitted() && $form->isValid()) {
            if($userproblem->getStatus() != 5){
                /** @var UploadedFile[] $files */
                $files = $form->get('fileInput')->getData();
                if (!is_array($files)) {
                    $files = [$files];
                }
                if(!empty($files)){
                    $ClientOriginalName = $files[0]->getClientOriginalName();
                    $extension = $files[0]->getClientOriginalExtension();
                    $listExtension = $langid->getExtensions();
                    if(!in_array($extension,$listExtension)){
                        $this->addFlash('danger', 'Extension of file submit is wrong! </br>'.$langid->getName().' not support extension is ".'.$extension.'", It just support extension is ".'.implode("\", \".",$listExtension).'"');
                        return $this->redirectToRoute('detail_problem', ['problemId' => $problem->getProbid()]); 
                    }
                    if(strpos($ClientOriginalName,$userproblem->getProbid()->getShortname()) !== 0){
                        $this->addFlash('danger', 'File name must contains shortname of problem! </br> Shortname of this problem is "'.$userproblem->getProbid()->getShortname().'"');
                        return $this->redirectToRoute('detail_problem', ['problemId' => $problem->getProbid()]);
                    }
                }                

                $submission = $this->submissionService->submitSolution($class, $problem, $langid, $files, null, null, null, null, null, $message, $userproblem);

                if ($submission) {
                    $this->dj->auditlog(
                        'submission',
                        $submission->getSubmitid(),
                        'added',
                        'via teampage',
                        null,
                        $class->getCid()
                    );
                    $this->addFlash('success', $message);
                    // update user problem status
                    $oldUserProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
                        ->where('up.userid = :userid')
                        ->setParameter('userid', $user->getUserid())
                        ->andwhere('up.probid = :probid')
                        ->setParameter('probid', $problemId)
                        ->andwhere('up.cid = :cid')
                        ->setParameter('cid', $class->getCid())
                        ->select('up')
                        ->getQuery()->getOneOrNullResult();
                    // $oldUserProblem = $this->em->getRepository(Userproblem::class)->find($oldUserProblem->getUserproblemid());
                    $oldUserProblem->setStatus(2);
                    $this->em->flush();
                } else {
                    $this->addFlash('danger', $message);
                }
                return $this->redirectToRoute('detail_problem', ['problemId' => $problem->getProbid()]);
            }else{
                $this->addFlash('danger', 'The problem have been rejected!');
                return $this->redirectToRoute('detail_problem', ['problemId' => $problem->getProbid()]);
            }
        }
        return $this->render('@DOMJudge/Fls/Student/detailProblem.html.twig', $data);
    }

    /**
     * request change problem
     * @Route("/problems/change", name="request_change_problem")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function requestChangeProblem(Request $request)
    {
        $dataRequest = json_decode($request->getContent());
        $probid = $dataRequest->probid;
        $user = $this->dj->getUser();
        $currentClass = $this->dj->getCurrentClass($user->getUserid());
        if ($currentClass == null || !$this->dj->isActiveCLass($currentClass->getCid())) {
            return $this->json(['result' => false]);
        } else {
            $userproblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
                ->where('up.userid = :userid')
                ->setParameter('userid', $user->getUserid())
                ->andwhere('up.probid = :probid')
                ->setParameter('probid', $probid)
                ->andwhere('up.cid = :cid')
                ->setParameter('cid', $currentClass->getCid())
                ->select('up')
                ->getQuery()->getOneOrNullResult();
            if (!$userproblem
                || $userproblem->getStatus() == 4
                || $userproblem->getStatus() == 5
                || $userproblem->getChanged() != 0
            ) {
                return $this->json(['result' => false]);
            } else {
                $userproblem->setChanged(1);
                $this->em->flush();
                return $this->json(['result' => true]);
            }
        }
    }

    protected function getListTestCase($problem, $lastJudging = null, $sample = null)
    {
        $listTest = [];
        // get last test case info list
        $query = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Testcase', 't');
        if ($lastJudging != null) {
            $query->leftJoin('t.judging_runs', 'jr', Join::WITH, 'jr.judging = :judging')
                ->setParameter(':judging', $lastJudging)
                ->select('t', 'jr');
        } else {
            $query->select('t');
        }
        if ($sample != null) {
            $query->andWhere('t.sample = :sample')
                ->setParameter(':sample', $sample);
        }
        
        $query->andWhere('t.problem = :problem')
            ->setParameter(':problem', $problem)
            ->orderBy('t.rank');

        $lastTest = $query->getQuery()->getResult();

        foreach ($lastTest as $test) {
            $temp['id'] = $test->getTestcaseid();
            $temp['rank'] = $test->getRank();
            if ($lastJudging != null && $test->getJudgingRuns() != null && count($test->getJudgingRuns()) > 0) {
                $temp['runid'] = $lastJudging->getJudgingid();
                $temp['result'] = $test->getJudgingRuns()->first()->getRunresult();
                $temp['runTime'] = $test->getJudgingRuns()->first()->getRuntime();
            } else {
                $temp['runTime'] = null;
                $temp['runid'] = null;
                $temp['result'] = null;
            }
            $listTest[] = $temp;
            unset($temp);
        }
        return $listTest;
    }

    /**
     * @Route("/problems/{probId}/text", name="problem_text", requirements={"probId": "\d+"})
     * @param int $probId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function problemTextAction(int $probId)
    {
        $user = $this->dj->getUser();
        $currentClass = $this->dj->getCurrentClass($user->getUserid());
        if ($currentClass == null || !$this->dj->isActiveCLass($currentClass->getCid())) {
            return $this->redirectToRoute('student_index');
        } else {
            $problem = $this->em->getRepository(Problem::class)->find($probId);

            switch ($problem->getProblemtextType()) {
                case 'pdf':
                    $mimetype = 'application/pdf';
                    break;
                case 'html':
                    $mimetype = 'text/html';
                    break;
                case 'txt':
                    $mimetype = 'text/plain';
                    break;
                default:
                    $this->addFlash('danger', sprintf('Problem p%d text has unknown type', $probId));
                    return $this->redirectToRoute('detail_problem', ['problemId' => $problem->getProbid()]);
            }

            $filename = sprintf('prob-%s.%s', $problem->getName(), $problem->getProblemtextType());
            $problemText = stream_get_contents($problem->getProblemtext());

            $response = new StreamedResponse();
            $response->setCallback(function () use ($problemText) {
                echo $problemText;
            });
            // $response->headers->set('X-Frame-Options', 'sameorigin');
            $response->headers->set('Content-Type', sprintf('%s; name="%s', $mimetype, $filename));
            $response->headers->set('Content-Disposition', sprintf('inline; filename="%s"', $filename));
            $response->headers->set('Content-Length', strlen($problemText));

            return $response;
        }
    }

    /**
     * @Route("/problem/detail/{submitId}/download", name="download_submission", requirements={"submitId": "^$|\d+"})
     * @Security("has_role('ROLE_STUDENT')")
     * @throws Exception
     */
    public function DowloadSubmission(Request $request, $submitId)
    {
        /** @var SubmissionFileWithSourceCode $file */
        $file = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:SubmissionFileWithSourceCode', 'file')
            ->select('file')
            ->andWhere('file.submission = :submission')
            ->setParameter(':submission', $submitId)
            ->getQuery()
            ->getOneOrNullResult();
        if (!$file) {
            throw new NotFoundHttpException(sprintf('No submission file found'));
        }
        // Download requested
        $response = new Response();
        $response->headers->set(
            'Content-Type',
            sprintf('text/plain; name="%s"; charset="utf-8"', $file->getFilename())
        );
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $file->getFilename()));
        $response->headers->set('Content-Length', (string) strlen($file->getSourcecode()));
        $response->setContent($file->getSourcecode());

        return $response;
    }

    /**
     * @Route("/submission/{submitId}", name="detail_submission", requirements={"submitId": "^$|\d+"})
     * @param Request $request
     * @param int     $submitId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function viewSubmission(Request $request, int $submitId)
    {
        $verificationRequired = (bool) $this->dj->dbconfig_get('verification_required', false);
        $showCompile = $this->dj->dbconfig_get('show_compile', 2);
        $showSampleOutput = $this->dj->dbconfig_get('show_sample_output', 0);
        $user = $this->dj->getUser();
        $class = $this->dj->getCurrentClass($user->getUserid());
        if ($class == null) {
            return $this->redirectToRoute('student_index');
        }

        /** @var Judging $judging */
        $judging = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Judging', 'j')
            ->join('j.submission', 's')
            ->join('s.classes', 'c')
            ->join('s.problem', 'p')
            ->join('p.langid', 'l')
            ->select('j', 's', 'c', 'p', 'l')
            ->andWhere('j.submitid = :submitId')
            ->andWhere('j.valid = 1')
            ->setParameter(':submitId', $submitId)
            ->getQuery()
            ->getOneOrNullResult();

        // Update seen status when viewing submission
        if ($judging) {
            $judging->setSeen(true);
            $this->em->flush();
        }

        /** @var Testcase[] $runs */
        $runs = [];
        if ($showSampleOutput && $judging && $judging->getResult() !== 'compiler-error') {
            $runs = $this->getAllRunTest($judging);
        }

        $data = [
            'judging' => $judging,
            'verificationRequired' => $verificationRequired,
            'showCompile' => $showCompile,
            'showSampleOutput' => $showSampleOutput,
            'runs' => $runs,
        ];

        $template = $this->render('@DOMJudge/Fls/Student/submissionDetail.html.twig', $data)->getContent();

        $json = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
        // return $this->render();
    }

    /**
     * @Route("/test/{judgingId}/{testId}", name="detail_test", requirements={"judgingId": "^$|\d+","testId": "^$|\d+"})
     * @param Request $request
     * @param int     $judgingId,$testId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function viewTestcase(Request $request, int $judgingId, int $testId)
    {
        $verificationRequired = (bool) $this->dj->dbconfig_get('verification_required', false);
        $showCompile = $this->dj->dbconfig_get('show_compile', 2);
        $showSampleOutput = $this->dj->dbconfig_get('show_sample_output', 0);
        $judging = $this->em->getRepository(Judging::class)->find($judgingId);
        if ($judging) {
            $listTest = $this->getRunTest($judging, $testId);
            $data['showCompile'] = $showCompile;
            $data['verificationRequired'] = $verificationRequired;
            $data['showSampleOutput'] = $showSampleOutput;
            // $test = $this->em->getRepository(Testcase::class)->find($testId);

            $data['testDeatil'] = $listTest[0];
        } else {
            $data = [];
        }

        $template = $this->render('@DOMJudge/Fls/Student/testDetail.html.twig', $data)->getContent();

        $json = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     *
     */
    public function getAllRunTest($judging)
    {
        $query = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Testcase', 't')
            ->join('t.testcase_content', 'tc')
            ->join('t.judging_runs', 'jr', Join::WITH, 'jr.judging = :judging')
            ->join('jr.judging_run_output', 'jro')
            ->select('t', 'jr', 'tc', 'jro')
            ->andWhere('t.problem = :problem')
            ->andWhere('t.sample = 1')
            ->setParameter(':judging', $judging)
            ->setParameter(':problem', $judging->getSubmission()->getProblem())
            ->orderBy('t.rank');

        return $query->getQuery()->getResult();
    }

     /**
     *
     */
    public function getRunTest($judging, $testId)
    {
        $query = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Testcase', 't')
            ->join('t.testcase_content', 'tc')
            ->join('t.judging_runs', 'jr', Join::WITH, 'jr.judging = :judging')
            ->join('jr.judging_run_output', 'jro')
            ->select('t', 'jr', 'tc', 'jro')
            ->andWhere('t.problem = :problem')
            ->andWhere('t.sample = 1')
            ->andWhere('jr.testcaseid = :testId')
            ->setParameter(':judging', $judging)
            ->setParameter(':problem', $judging->getSubmission()->getProblem())
            ->setParameter(':testId', $testId)
            ->orderBy('t.rank');

        return $query->getQuery()->getResult();
    }

    /**
     * @Route("/time/", name="get_time_slot")
     * @param int $probId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTimeSlot()
    {
        $data = [];
        $user = $this->dj->getUser();
        $currentTime = Utils::printtime(Utils::now(), '%H:%M');

        $data['active'] = false;
        $currentClass = $this->dj->getCurrentClass($user->getUserid());
        if (!empty($currentClass) && $this->dj->isActiveCLass($currentClass->getCid())) {
            $data['active'] = true;
            $free_time = true;
            if($currentClass->getStarttime() != null && $currentClass->getEndtime() != null){
                $end_time = date("U.u",(int)$currentClass->getEndtime());
                $data['end_time'] = $end_time;
                $free_time = false;
            }
        }else{
            $free_time = false;
        }
        $data['freetime'] = $free_time;
        return $this->json($data);
    }

    public function getAskData($userproblemid) {
        $ask = $this->em->createQueryBuilder()
            ->select('clar')
            ->from('DOMJudgeBundle:Clarification', 'clar')
            ->where('clar.userproblemid = :userproblemid')
            ->setParameter('userproblemid', $userproblemid)
            ->getQuery()->getResult();
        
        return $ask;
    }

    /**
     * @Route("/send", methods={"POST"}, name="student_ask_send")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function sendAsk(Request $request)
    {
        $curUser = $this->getUser();
        $curClass = $this->dj->getCurrentClass($curUser->getUserid());
        if($curClass == null) {
            return $this->redirectToRoute('student_index');
        }

        $clarification = new Clarification();

        $clarification->setClasses($curClass);
        $userproblemid = $request->request->get('userProblemId');
        if($userproblemid != null) {
            $userproblem = $this->em->getRepository(Userproblem::class)->find($userproblemid);
            $clarification->setUserproblem($userproblem);
        }        
       

        //get original ask
        $oriAsk = $this->em->getRepository(Clarification::class)->findOneBy(array('userproblemid' => $userproblemid));
        if($oriAsk != null) {
            $respclar = $this->em->getRepository(Clarification::class)->find($oriAsk);
            $clarification->setInReplyTo($respclar);
            $respclar->setAnswered(false);
            $clarification->setAnswered(true);
            $this->em->merge($respclar);
        }else{
            $clarification->setAnswered(false);
        }

        $teacher = $this->dj->getteacherByClassId($curClass->getCid());
        $clarification->setSender($curUser->getUsername());
        $clarification->setReceiver($teacher[0]['username']);

        
        $clarification->setBody($request->request->get('askContent'));
        $clarification->setSubmittime(Utils::now());

        $this->em->persist($clarification);
        $this->em->flush();

        return $this->json(['result' => 'OK']);
    }

    /**
     * @Route("/updates", methods={"GET"}, name="student_ajax_updates")
     * @Security("has_role('ROLE_STUDENT')")
     */
    public function updatesAction(Request $request)
    {
        if($request->query->has('target') && $request->query->get('target') == 'home'){
            $user = $this->dj->getUser();
            $currentClass = $this->dj->getCurrentClass($user->getUserid());
            if(empty($currentClass)){
                return $this->json(['result' => false]);
            }
            return $this->json($this->getAjaxHomeData($user,$currentClass));
        }else{
            if($request->query->has('userProblemID')) {
                $userproblemid = $request->query->get('userProblemID');
            }
            if($request->query->has('lastTime')) {
                $lastTime = $request->query->get('lastTime');
            }
            
            return $this->json($this->getAjaxDetailData($userproblemid, $lastTime));
        }        
    }

    protected function getAjaxHomeData($user,$currentClass)
    {
        $userid = $user->getUserid();

        // get list problem
        $problems = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Problem', 'p')
            ->leftJoin(
                'DOMJudgeBundle:Userproblem',
                'up',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'p.probid = up.probid'
            )
            ->where('up.userid = :userid')
            ->setParameter(':userid', $userid)
            ->andwhere('up.cid = :Cid')
            ->setParameter(':Cid', $currentClass->getCid())
            ->select('p.probid as id,
                p.shortname as name,
                p.name as content,
                p.points as point,
                up.status as result,
                up.changed as status')
            ->orderBy('p.points', 'ASC')
            ->getQuery()->getResult();
        $data['problems'] = $problems;

        // get data chart
        $charts = [];
        $arrayResult = array_column($problems, 'result');
        $totalSize = sizeof($arrayResult);
        $totalNull = $totalSize - count(array_filter($arrayResult));
        $arrayResult = array_count_values(array_filter($arrayResult));
        $charts['labels'] = [FlsConstant::ProblemStatus[0]];
        $charts['data'] = [$totalNull];
        $charts['color'] = ['#343a40'];
        foreach ($arrayResult as $key => $value) {
            if ($key != -1) {
                array_push($charts['labels'], FlsConstant::ProblemStatus[$key]);
                array_push($charts['data'], $value);
                switch ($key) {
                    case 1:
                        array_push($charts['color'], '#ffc107');
                        break;
                    case 2:
                        array_push($charts['color'], '#007bff');
                        break;
                    case 3:
                        array_push($charts['color'], '#17a2b8');
                        break;
                    case 4:
                        array_push($charts['color'], '#28a745');
                        break;
                    case 5:
                        array_push($charts['color'], '#dc3545');
                        break;
                    default:
                        break;
                }
            }
        }
        $data['charts'] = $charts;

        // ->leftJoin('DOMJudgeBundle:JudgingRun', 'judr', \Doctrine\ORM\Query\Expr\Join::WITH,
        // 'judr.judgingid = jud.judgingid')
        //get list submitted
        $submitted = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Submission', 'sub')
            ->leftJoin(
                'DOMJudgeBundle:Judging',
                'jud',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'sub.submitid = jud.submitid'
            )
            ->where('sub.userid = :userid')
            ->setParameter(':userid', $userid)
            ->andwhere('sub.cid = :cid')
            ->setParameter(':cid', $currentClass->getCid())
            ->select('sub,jud.result as result')
            ->orderBy('sub.submittime', 'DESC')
            ->getQuery()->getResult();

        $listSubmitted = [];
        foreach ($submitted as $key => $sub) {
            $temp['id'] = $sub[0]->getSubmitid();
            $temp['time'] = strftime('%H:%M:%S (%d-%m-%Y)', (int) $sub[0]->getSubmittime());
            $temp['problem'] = $sub[0]->getProblem()->getName();
            $temp['shortname'] = $sub[0]->getProblem()->getShortname();
            $temp['result'] = $sub['result'];

            $listSubmitted[] = $temp;
            unset($temp);
        }
        $data['submitted'] = $listSubmitted;
        return $data;
    }

    protected function getAjaxDetailData($userproblemid, $lastTime ): array
    {
        $data = [];
        $currentUser = $this->getUser();
        $classes = $this->dj->getCurrentClass($currentUser->getUserid());
        if($userproblemid != null) {
            $ask = $this->em->createQueryBuilder()
            ->select('clar.body', 'clar.submittime', 'p.probid')
            ->from('DOMJudgeBundle:Clarification', 'clar')
            ->leftJoin('clar.userproblem', 'up')
            ->leftJoin('up.probid', 'p')
            ->where('clar.submittime > :lastTime')
            ->andWhere('clar.userproblemid = :userproblemid')
            ->andWhere('clar.sender != :username')
            ->setParameter('lastTime', $lastTime)
            ->setParameter('userproblemid', $userproblemid)
            ->setParameter('username', $currentUser->getUsername())
            ->orderBy('clar.submittime')
            ->getQuery()->getResult();
        }
        $userproblem = $this->em->getRepository(Userproblem::class)->find($userproblemid);
        if(!empty($userproblem )){
            if ($this->dj->isActiveCLass($userproblem->getCid()->getCid())) {
                
                $problem = $userproblem->getProbid();
                $class = $userproblem->getCid();
                $userproblemStatus = $userproblem->getStatus();
                if ($userproblemStatus == 4 || $userproblemStatus == 5) {
                    $data['allowSubmit'] = false;
                    if($userproblemStatus == 4){
                        $data['allowDowload'] = true;
                    }
                } else {
                    $data['allowDowload'] = true;
                    $data['allowSubmit'] = true;
                }
                $data['class_enabled'] = true;
                $data['changeStatus'] = $userproblem->getChanged();
                // get last submission
                    $lastSubmission = $this->em->getRepository(Submission::class)->createQueryBuilder('s')
                    ->select('s')
                    ->andwhere('s.user = :userid')
                    ->andwhere('s.classes = :cid')
                    ->andwhere('s.problem = :probid')
                    ->setParameter('userid', $userproblem->getUserid())
                    ->setParameter('cid', $class->getCid())
                    ->setParameter('probid', $problem->getProbid())
                    ->orderBy('s.submittime', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                if ($lastSubmission) {
                    // get last juding
                    $lastJudging = $this->em->createQueryBuilder()
                        ->from('DOMJudgeBundle:Judging', 'j')
                        ->select('j')
                        ->andWhere('j.submission = :submission')
                        ->andWhere('j.valid = 1')
                        ->setParameter(':submission', $lastSubmission)
                        ->orderBy('j.judgingid', 'DESC')
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult();
                    if ($lastJudging != null) {
                        $listTest = $this->getListTestCase($problem, $lastJudging, 1);
                        $listAllTest = $this->getListTestCase($problem, $lastJudging);
                    } else {
                        $listTest = $this->getListTestCase($problem, null, 1);
                        $listAllTest = $this->getListTestCase($problem);
                    }
                } else {
                    $fileName = null;
                    $listTest = $this->getListTestCase($problem, null, 1);
                    $listAllTest = $this->getListTestCase($problem);
                }
                $data['listTest'] = $listTest;
                // count number of test done
                $testDone = 0;
                $testDo = 0;
                foreach ($listAllTest as $object) {
                    // count  total test done
                    if ($object['result'] == 'correct') {
                        $testDone++;
                    }
                    // count  total test do
                    if ($object['result'] != null) {
                        $testDo++;
                    }
                }
                $info['done'] = $testDone;
                $info['do'] = $testDo;
                $info['total'] = $listAllTest != null ? count($listAllTest) : 0;
                $data['testInfo'] = $info;
            } else {
                $data['class_enabled'] = false;
            }            
        }
        $data += [
            'ask' => $ask,
            'updateTime' => Utils::now(),
        ];
        
        return $data;
    }
}
