<?php declare (strict_types = 1);

namespace DOMJudgeBundle\Controller\Student;

use Doctrine\ORM\EntityManagerInterface;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\Submission;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Entity\Userproblem;
use DOMJudgeBundle\Entity\Leaderboard;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Service\ImportProblemService;
use DOMJudgeBundle\Service\SubmissionService;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DOMJudgeBundle\Utils\FlsConstant;

/**
 * @Route("/student")
 * @Security("has_role('ROLE_STUDENT')")
 */
class ScoreboardController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    private $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * @var SubmissionService
     */
    protected $submissionService;

    /**
     * @var ImportProblemService
     */
    protected $importProblemService;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService,
        SubmissionService $submissionService,
        ImportProblemService $importProblemService,
        FormFactoryInterface $formFactory
    ) {
        $this->em = $em;
        $this->dj = $dj;
        $this->eventLogService = $eventLogService;
        $this->submissionService = $submissionService;
        $this->importProblemService = $importProblemService;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/scoreboard", name="student_scoreboard")
     * @Security("has_role('ROLE_STUDENT')")
     * @throws Exception
     */
    public function scoreboard()
    {
        $user = $this->dj->getUser();
        $data = [];
        // flag menu
        if (count($class = $user->getCid()) > 1) {
            $data['moreClass'] = true;
        } else {
            $data['moreClass'] = false;
        }
        $currentClass = $this->dj->getCurrentClass($user->getUserid());
        $currentSemester = $this->dj->getCurrentSemester();
        if ($currentClass == null) {
            return $this->redirectToRoute('student_index');
        } else {
            $listClass = $this->em->getRepository(Classes::class)->createQueryBuilder('c')
                ->where('c.semid = :semid')
                ->setParameter(':semid', $currentSemester[0]->getSemid())
                ->andwhere('c.langid = :langid')
                ->setParameter(':langid', $currentClass->getLangid())
                ->select('c.cid as id, c.name as name')
                ->orderBy('c.name', 'ASC')
                ->getQuery()
                ->getResult();
            $data['listClass'] = $listClass;
            $data['currentCid'] = $currentClass->getCid($currentClass->getLangid());
            $listScore = $this->getListScore($currentClass->getCid());
            $data['listScore'] = json_encode($listScore);
            $data['subChart'] = $this->getDataChartSubmission($currentClass->getCid(),$user->getUserid());
            return $this->render('@DOMJudge/Fls/Student/scoreboard.html.twig', $data);
        }
    }

    /**
     * @Route("/scoreboard/detail", name="scoreboard_detail")
     * @Security("has_role('ROLE_STUDENT')")
     * @throws Exception
     */
    public function scoreboardDetail(Request $request)
    {
        $dataRequest = json_decode($request->getContent());
        $userid = $dataRequest->userid;
        $cid = $dataRequest->cid;
        $user = $this->dj->getUser();
        $data = [];
        $currentClass = $this->dj->getCurrentClass($user->getUserid());
        $currentSemester = $this->dj->getCurrentSemester();
        if ($currentClass == null) {
            return $this->json(['result' => 'reload']);
        } else {
            $data['student'] = $this->em->getRepository(User::class)->find($userid);
            $totalProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
                ->where('up.userid = :userid')
                ->andwhere('up.cid = :cid')
                ->setParameter(':userid', $userid)
                ->setParameter(':cid', $currentClass->getCid())
                ->select('up')
                ->getQuery()
                ->getResult();
            // fill problem pass
            $pass = array_filter($totalProblem, function($v, $k) {
                    return $v->getStatus() == 4;
                }, ARRAY_FILTER_USE_BOTH);
            // fill problem reject
            $reject = array_filter($totalProblem, function($v, $k) {
                return $v->getStatus() == 5;
                }, ARRAY_FILTER_USE_BOTH);
            // fill problem changed
            $changed = array_filter($totalProblem, function($v, $k) {
                return $v->getChanged() == 2;
                }, ARRAY_FILTER_USE_BOTH);

            $data['info'] = [
                'total' => count($totalProblem),
                'pass' => count($pass),
                'reject' => count($reject),
                'changed' => count($changed),
                ];
            $template = $this->render('@DOMJudge/Fls/Student/scoreboardDetail.html.twig', $data)->getContent();

            $json = json_encode($template);
            $response = new Response($json, 200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    /**
     * get chart data
     * 
     * @Route("/scoreboard/chart", name="chart_submit")
     * @Security("has_role('ROLE_STUDENT')")
     * @throws Exception
     */
    public function ajaxGetChartSubmission( Request $request)
    {
        $data = [];
        //get data from request
        $dataRequest = json_decode($request->getContent());
        // check data student ID
        if(isset($dataRequest->studentId) && !empty($dataRequest->studentId)){
            $studentId = $dataRequest->studentId;
        }else{
            $studentId = $this->dj->getUser()->getUserid();
        }
        // check data class ID
        if(isset($dataRequest->Cid) && !empty($dataRequest->Cid)){
            $classId = $dataRequest->Cid;
        }else{
            $currentClass = $this->dj->getCurrentClass($studentId);
            if($currentClass == null){
                $data['error'] = true;
            }else{
                $classId = $currentClass->getCid();
                $data['subChar'] = $this->getDataChartSubmission($classId,$studentId);
            }
        }
        return $this->json(['result' => $data]);
    }

    // get chart submission data
    public function getDataChartSubmission($classID, $studentId)
    {
        $data = [];
        $listDoneProb = $this->em->getRepository(Userproblem::class)
            ->createQueryBuilder('up')
            ->select('up')
            ->where('up.finishedTime IS NOT NULL')
            ->andwhere('up.changed != 2') // not get problem is changed
            ->andwhere('up.status = 4')
            ->andwhere('up.userid = :userid')
            ->andwhere('up.cid = :cid')
            ->setParameter(':cid', $classID)
            ->setParameter(':userid', $studentId)
            ->orderBy('up.finishedTime','ASC')
            ->getQuery()
            ->getResult();
        $class = $this->em->getRepository(Classes::class)->find($classID);
        if($listDoneProb){
            $totalLoc = 0;
            $data['time'][] = strftime('%d-%m-%Y', (int) $class->getSemester()->getStartedTime());
            $data['loc'][] = 0;
            $data['color'][] = "red";
            foreach ($listDoneProb as $key => $value) {
                array_push($data['time'],strftime('%d-%m-%Y', (int) $value->getFinishedTime()));
                $totalLoc += (int) $value->getProbid()->getPoints();
                array_push($data['loc'], $totalLoc);
                if($totalLoc >= $this->dj->getRequireLoc($class->getLangid()->getLangid())){
                    $data['color'] = "green";
                }
            }
            array_push($data['time'],strftime('%d-%m-%Y', (int) $class->getSemester()->getFinishedTime()));
            array_push($data['loc'], $totalLoc);
            array_push($data['loc'], $totalLoc+100);
        }
        return $data;
    }

    /**
     * @Route("/scoreboard/{classID}", name="fill_scoreboard", requirements={"classID": "^$|\d+"})
     * @Security("has_role('ROLE_STUDENT')")
     * @throws Exception
     */
    public function fillScoreboard(string $classID, Request $request)
    {
        $user = $this->dj->getUser();
        $currentClass = $this->dj->getCurrentClass($user->getUserid());
        if($currentClass == null){
            return $this->json(['result' => 'reload']);
        }
        $listScore = $this->getListScore($classID);
        return $this->json(['result' => $listScore]);
    }

    /**
     * get list user by class id
     */
    public function getListScore($classID)
    {
        $listScore = [];
        // get Leaderboard
        $listUser = $this->em->getRepository(Leaderboard::class)->createQueryBuilder('lb')
            ->select('lb')
            ->where('lb.cid = :cid')
            ->setParameter(':cid', $classID)
            ->orderBy('lb.pointsRestricted', 'DESC')
            ->getQuery()
            ->getResult();

        $class = $this->em->getRepository(Classes::class)->find($classID);

        if ($class == null) {
            return null;
        } else {
            // get require loc for language
            $requireLoc = $this->dj->getRequireLoc($class->getLangid()->getLangid());
            foreach ($listUser as $key => $value) {
                $temp['cid'] = $value->getCid();
                $temp['loc'] = $value->getPointsRestricted();
                $temp['requireLoc'] = $requireLoc;
                $temp['time'] = $value->getTotaltimeRestricted();
                $temp['userid'] = $value->getUserid();
                $student = $this->em->getRepository(User::class)->find($value->getUserid());
                $temp['name'] = $student->getName();
                $class = $this->em->getRepository(Classes::class)->find($value->getCid());
                $langId = $class->getLangid()->getLangid();;
                $temp['status'] = ($value->getPointsRestricted() >= $this->dj->getRequireLoc($langId)) ? '1':'0';
                $listScore[] = $temp;
                unset($temp);
            }
            return $listScore;
        }
    }

}
