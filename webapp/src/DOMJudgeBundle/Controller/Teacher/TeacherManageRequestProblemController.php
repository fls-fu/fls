<?php declare(strict_types=1);

namespace DOMJudgeBundle\Controller\Teacher;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\Userproblem;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Utils\Utils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TeacherManageRequestProblemController
 *
 * @Route("/teacher")
 *
 * @package DOMJudgeBundle\Controller\Teacher
 */
class TeacherManageRequestProblemController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    protected $dj;

    /**
     * GeneralInfoController constructor.
     * @param EntityManagerInterface $entityManager
     * @param DOMJudgeService        $dj
     */
    public function __construct(EntityManagerInterface $entityManager, DOMJudgeService $dj)
    {
        $this->em = $entityManager;
        $this->dj = $dj;
    }

    /**
     * @Route("/request/{cid}", name="teacher_manage_request_problem", requirements={"className": "^$|\d+"})
     * @Security("has_role('ROLE_TEACHER')")
     */
    public function indexAction(Request $request, $cid)
    {
        $data = [];
        $tempData = [];
        $listResult = [];
        $changed = 1;
        $currentClass = $this->em->getRepository(Classes::class)->find($cid);
        $listRequestChangeProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
            ->select('up')
            ->where('up.cid = :cid')
            ->setParameter(':cid', $currentClass->getCid())
            ->andWhere('up.changed = :changed')
            ->setParameter(':changed', $changed)
            ->getQuery()
            ->getResult();
        
        foreach ($listRequestChangeProblem as $key => $value) {
            $tempData['upID'] = $value->getUserproblemid();
            $tempData['cID'] = $currentClass->getCid();
            $tempData['uID'] = $value->getUserid()->getUserid();
            $tempData['uName'] = $value->getUserid()->getName();
            $tempData['pID'] = $value->getProbid()->getProbid();
            $tempData['pName'] = $value->getProbid()->getName();
            $tempData['pShortName'] = $value->getProbid()->getShortname();
            array_push($listResult, $tempData);
        }
        if(!empty($cid)){
            $data += ['cid' => $cid,'moreClass' => true];
            $data += ['classMenu' => $currentClass];
        }else{
            $data += ['moreClass' => false];
        }  
        $data += [
            'listProblem' => json_encode($listResult),
            'countRequest' => count($listResult),
        ];

        return $this->render('@DOMJudge/Fls/Teacher/teacher_manage_request_problem.html.twig', $data);
    }

    /**
     * @Route("/request/accept/", name="teacher_accept_change")
     * @Security("has_role('ROLE_TEACHER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function acceptRequest(Request $request)
    {
        $changed = 2;
        $userProblem = [];
        $dataRequest = json_decode($request->getContent());
        if( !empty($dataRequest) ) {
            $userProblemID = $dataRequest->upId;
            $userProblem = $this->em->getRepository(Userproblem::class)->find($userProblemID);
        }else{
            return $this->json(['result' => false]);
        }
        if($userProblem){
            $userProblem->setChanged($changed);
            $this->em->flush();
            $listData = $this->dj->genarateProblemForStudent($userProblem->getCid(), $userProblem->getUserid());
            $listDataEncode = [];
            foreach ($listData as $value) {
                $temp['student'] = $value['student']->getUserid();
                $temp['problem'] = $value['problem']->getProbid();
                $temp['review'] = $value['review'];
                $listDataEncode[] = $temp;
                unset($temp);
            }
            $data['listDataEncode'] = json_encode($listDataEncode);
            $data['listData'] = $listData;
            $data['redirect'] = $userProblemID;
            $userProblem->setChanged(1);
            $this->em->flush();
            $template = $this->render('@DOMJudge/Fls/Teacher/partials/modal_random_problem.html.twig', $data)->getContent();
            
            $json = json_encode($template);
            $response = new Response($json, 200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }else{
            return $this->json(['result' => false]);
        }
    }

    /**
     * @Route("/confirm/change/", name="confirm_chage")
     * @Security("has_role('ROLE_TEACHER')")
     * @throws Exception
     */
    public function confirmChange(Request $request)
    {
        $dataRequest = json_decode($request->getContent());
        $userProblemID = $dataRequest->upId;
        $userProblemChange = $this->em->getRepository(Userproblem::class)->find($userProblemID);

        if($dataRequest->confirm == false){
            $userProblemChange->setChanged(1);
            $this->em->flush();
            return $this->json(['result' => true]);
        }
        $userProblemChange->setChanged(2);
        $this->em->flush();
        
        $class = $userProblemChange->getCid();
        $listData = $dataRequest->listData;
        foreach ($listData as $key => $value) {
            $value->student = $this->em->getRepository(User::class)->find($value->student);
            $value->problem = $this->em->getRepository(Problem::class)->find($value->problem);
            $listData[$key] = $value;
        }
        // insert data to db
        foreach ($listData as $value) {
            $userProblem = new Userproblem();
            $userProblem->setUserid($value->student)
                ->setCid($class)
                ->setProbid($value->problem)
                ->setReviewStatus($value->review);
            $userProblem->setChanged(4);
            $userProblem->setStatus(0);
            $this->em->persist($userProblem);
        }
        // push data
        $this->em->transactional(function () {
            $this->em->flush();
        });

        return $this->json(['result' => true]);
    }

    /**
     * @Route("/request/reject/", name="teacher_reject_change")
     * @Security("has_role('ROLE_TEACHER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function rejectRequest(Request $request)
    {
        $dataRequest = json_decode($request->getContent());
        $userProblemID = $dataRequest->upId;
        $userProblem = $this->em->getRepository(Userproblem::class)->find($userProblemID);
        
        $userProblem->setChanged(3);
        $this->em->flush();
        return $this->json(['result' => true]);
    }
}
