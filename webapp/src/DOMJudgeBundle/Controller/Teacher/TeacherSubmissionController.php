<?php declare(strict_types=1);

namespace DOMJudgeBundle\Controller\Teacher;

use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\Judgehost;
use DOMJudgeBundle\Entity\Judging;
use DOMJudgeBundle\Entity\JudgingRun;
use DOMJudgeBundle\Entity\Language;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\Submission;
use DOMJudgeBundle\Entity\SubmissionFile;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Entity\Testcase;
use DOMJudgeBundle\Entity\Userproblem;
use DOMJudgeBundle\Entity\Leaderboard;
use DOMJudgeBundle\Utils\Utils;
use DOMJudgeBundle\Service\BalloonService;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Service\ScoreboardService;
use DOMJudgeBundle\Service\SubmissionService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/teacher")
 * @IsGranted("ROLE_TEACHER")
 */
class TeacherSubmissionController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    protected $dj;

    /**
     * @var SubmissionService
     */
    protected $submissionService;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * SubmissionController constructor.
     * @param EntityManagerInterface $em
     * @param DOMJudgeService        $dj
     * @param SubmissionService      $submissionService
     * @param RouterInterface        $router
     */
    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        SubmissionService $submissionService,
        RouterInterface $router
    ) {
        $this->em                = $em;
        $this->dj                = $dj;
        $this->submissionService = $submissionService;
        $this->router            = $router;
    }

    /**
     * @Route("/submissions/class/{cid}", name="teacher_submissions")
     */
    public function indexAction(Request $request, $cid = null)
    {
        $data = [];
        $class = null;
        $viewTypes = [0 => 'review', 1 => 'unverified', 2 => 'judging', 3 => 'accepted', 4 => 'rejected'];
        $view      = 0;
        // if (($submissionViewCookie = $this->dj->getCookie('domjudge_submissionview')) &&
        //     isset($viewTypes[$submissionViewCookie])) {
        //     $view = $submissionViewCookie;
        // }

        if ($request->query->has('view')) {
            $index = array_search($request->query->get('view'), $viewTypes);
            if ($index !== false) {
                $view = $index;
            }
        }

        if ($request->query->has('class')) {
            $class = $this->em->getRepository(Classes::class)->find($request->query->get('class'));
            $data += ['class' => $class];
        }

        // $response = $this->dj->setCookie('domjudge_submissionview', (string)$view);

        $refreshUrl = $this->generateUrl('teacher_submissions', ['view' => $viewTypes[$view],'cid' => !empty($cid)?$cid:'','class' => !empty($cid)?$cid:'']);
        $refresh = [
            'after' => 15,
            'url' => $refreshUrl,
            'ajax' => true,
        ];

        $restrictions = [];
        if ($viewTypes[$view] == 'unverified') {
            $restrictions['verified'] = 0;
        }
        if ($viewTypes[$view] == 'judging') {
            $restrictions['judged'] = 0;
        }

        //Start-Modify by SonNM
        // $contests = $this->dj->getCurrentContests();
        // if ($contest = $this->dj->getCurrentContest()) {
        //     $contests = [$contest->getCid() => $contest];
        // }
        //END-Modify

        
        $limit = $viewTypes[$view] == 'review' ? 50 : 0;

        if ($viewTypes[$view] == 'review') {
            $limit = 1;
        } else if ($viewTypes[$view] == 'accepted') {
            $limit = 2;
        } else if ($viewTypes[$view] == 'rejected') {
            $limit = 3;
        }

        /** @var Submission[] $submissions */
        //Start-Modify by SonNM
        // list($submissions, $submissionCounts) = $this->submissionService->getSubmissionList($contests, $restrictions,
                                                                                            // $limit);
        if($class != null){
            list($submissions, $submissionCounts) = $this->getSubmissionList($restrictions,$limit,$class->getCid());
        }else{
            list($submissions, $submissionCounts) = $this->getSubmissionList($restrictions,$limit);
        }
        
        //End-Modify
                                                                                                   
        // Load preselected filters
        // $filters          = $this->dj->jsonDecode((string)$this->dj->getCookie('domjudge_submissionsfilter') ?: '[]');
        $filters  = null;
        $filteredProblems = $filteredLanguages = $filteredStudents = [];
        if (isset($filters['problem-id'])) {
            /** @var Problem[] $filteredProblems */
            $filteredProblems = $this->em->createQueryBuilder()
                ->from(Problem::class, 'p')
                ->select('p')
                ->where('p.probid IN (:problemIds)')
                ->setParameter(':problemIds', $filters['problem-id'])
                ->getQuery()
                ->getResult();
        }
        //Start-Modify SonNM
        // if (isset($filters['team-id'])) {
        //     /** @var Team[] $filteredTeams */
        //     $filteredTeams = $this->em->createQueryBuilder()
        //         ->from(Team::class, 't')
        //         ->select('t')
        //         ->where('t.teamid IN (:teamIds)')
        //         ->setParameter(':teamIds', $filters['team-id'])
        //         ->getQuery()
        //         ->getResult();
        // }
        if (isset($filters['user-id'])) {
            /** @var User[] $filteredUser */
            $filteredStudents = $this->em->createQueryBuilder()
                ->from(User::class, 'u')
                ->select('u')
                ->where('u.userid IN (:userIds)')
                ->setParameter(':userIds', $filters['user-id'])
                ->getQuery()
                ->getResult();
        }
        //End-Modify

        $data += [
            'refresh' => $refresh,
            'viewTypes' => $viewTypes,
            'view' => $view,
            'submissions' => $submissions,
            'submissionCounts' => $submissionCounts,
            // 'showContest' => !empty($classID),
            'hasFilters' => !empty($filters),
            'filteredProblems' => $filteredProblems,
            'filteredLanguages' => $filteredLanguages,
            'filteredStudents' => $filteredStudents,
            // 'showExternalResult' => $this->dj->dbconfig_get('data_source', DOMJudgeService::DATA_SOURCE_LOCAL) ==
            //     DOMJudgeService::DATA_SOURCE_CONFIGURATION_AND_LIVE_EXTERNAL,
        ];
        if(!empty($cid)){
            $data += ['cid' => $cid,'moreClass' => true];
            $classes = $this->em->getRepository(Classes::class)->find($cid);
            $data += ['classMenu' => $classes];
        }else{
            $data += ['moreClass' => false];
        }  
        // For ajax requests, only return the submission list partial
        if ($request->isXmlHttpRequest()) {
            $data['showTestcases'] = true;
            return $this->render('@DOMJudge/Fls/Teacher/partials/submission_list.html.twig', $data);
        }

        return $this->render('@DOMJudge/Fls/Teacher/teacher_submissions.html.twig', $data);
    }

    /**
     * @Route("/submissions/{submitId}", name="teacher_submission", requirements={"submitId": "\d+"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function viewAction(Request $request, int $submitId)
    {
        $data = [];
        $judgingId   = $request->query->get('jid');
        $rejudgingId = $request->query->get('rejudgingid');
        $cid = $request->query->get('cid');
        if(!empty($cid)){
            $data += ['cid' => $cid,'moreClass' => true];
            $classes = $this->em->getRepository(Classes::class)->find($cid);
            $data += ['classMenu' => $classes];
        }else{
            $data += ['moreClass' => false];
        }
        if (isset($judgingId) && isset($rejudgingId)) {
            throw new BadRequestHttpException("You cannot specify jid and rejudgingid at the same time.");
        }

        // If judging ID is not set but rejudging ID is, try to deduce the judging ID from the database.
        if (!isset($judgingId) && isset($rejudgingId)) {
            $judging = $this->em->getRepository(Judging::class)
                ->findOneBy([
                                'submitid' => $submitId,
                                'rejudgingid' => $rejudgingId
                            ]);
            if ($judging) {
                $judgingId = $judging->getJudgingid();
            }
        }

        /** @var Submission|null $submission */
        //Start-modify SonNM
        // $submission = $this->em->createQueryBuilder()
        //     ->from(Submission::class, 's')
        //     ->join('s.team', 't')
        //     ->join('s.problem', 'p')
        //     ->join('s.language', 'l')
        //     ->join('s.contest', 'c')
        //     ->leftJoin('s.external_judgements', 'ej', Join::WITH, 'ej.valid = 1')
        //     ->leftJoin('s.contest_problem', 'cp')
        //     ->select('s', 't', 'p', 'l', 'c', 'cp', 'ej')
        //     ->andWhere('s.submitid = :submitid')
        //     ->setParameter(':submitid', $submitId)
        //     ->getQuery()
        //     ->getOneOrNullResult();
        $submission = $this->em->createQueryBuilder()
            ->from(Submission::class, 's')
            ->join('s.user', 'u')
            ->join('s.problem', 'p')
            ->join('s.language', 'l')
            ->join('s.classes', 'c')
            ->select('s', 'u', 'p', 'l', 'c')
            ->andWhere('s.submitid = :submitid')
            ->setParameter(':submitid', $submitId)
            ->getQuery()
            ->getOneOrNullResult();
        //End-Modify
        $checkReject = $this->em->createQueryBuilder()
            ->from(Submission::class, 's')
            ->join('s.userproblem', 'up')
            ->andwhere('up.status = 5')
            ->select('s')
            ->andWhere('s.submitid = :submitid')
            ->setParameter(':submitid', $submitId)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$submission) {
            throw new NotFoundHttpException(sprintf('No submission found with ID %d', $submitId));
        }

        $judgingData = $this->em->createQueryBuilder()
            ->from(Judging::class, 'j', 'j.judgingid')
            ->leftJoin('j.runs', 'jr')
            ->leftJoin('j.rejudging', 'r')
            ->select('j', 'r', 'MAX(jr.runtime) AS max_runtime')
            ->andWhere('j.classes = :classes')
            ->andWhere('j.submission = :submission')
            ->setParameter(':classes', $submission->getClasses())
            ->setParameter(':submission', $submission)
            ->groupBy('j.judgingid')
            ->orderBy('j.starttime')
            ->addOrderBy('j.judgingid')
            ->getQuery()
            ->getResult();

        /** @var Judging[] $judgings */
        $judgings    = array_map(function ($data) {
            return $data[0];
        }, $judgingData);
        $maxRunTimes = array_map(function ($data) {
            return $data['max_runtime'];
        }, $judgingData);

        $selectedJudging = null;
        // Find the selected judging
        if ($judgingId !== null) {
            $selectedJudging = $judgings[$judgingId] ?? null;
        } else {
            foreach ($judgings as $judging) {
                if ($judging->getValid()) {
                    $selectedJudging = $judging;
                }
            }
        }

        $claimWarning = null;

        if ($request->get('claim') || $request->get('unclaim')) {
            $user   = $this->dj->getUser();
            $action = $request->get('claim') ? 'claim' : 'unclaim';

            if ($selectedJudging === null) {
                $claimWarning = sprintf('Cannot %s this submission: no valid judging found.', $action);
            } elseif ($selectedJudging->getVerified()) {
                $claimWarning = sprintf('Cannot %s this submission: judging already verified.', $action);
            } elseif (!$user && $action === 'claim') {
                $claimWarning = 'Cannot claim this submission: no jury member specified.';
            } else {
                if (!empty($selectedJudging->getJuryMember()) && $action === 'claim' &&
                    $user->getUsername() !== $selectedJudging->getJuryMember() &&
                    !$request->request->has('forceclaim')) {
                    $claimWarning = sprintf('Submission has been claimed by %s. Claim again on this page to force an update.',
                                            $selectedJudging->getJuryMember());
                } else {
                    $selectedJudging->setJuryMember($action === 'claim' ? $user->getUsername() : null);
                    $this->em->flush();
                    $this->dj->auditlog('judging', $selectedJudging->getJudgingid(), $action . 'ed');

                    if ($action === 'claim') {
                        return $this->redirectToRoute('teacher_submission', ['submitId' => $submission->getSubmitid(), 'cid' => !isset($cid)? $cid:'']);
                    } else {
                        return $this->redirectToRoute('teacher_submissions', ['cid' => !isset($cid)? $cid:'']);
                    }
                }
            }
        }

        $unjudgableReasons = [];
        if ($selectedJudging === null) {
            // Determine if this submission is unjudgable

            // First, check if there is an active judgehost that can judge this submission.
            /** @var Judgehost[] $judgehosts */
            $judgehosts  = $this->em->createQueryBuilder()
                ->from(Judgehost::class, 'j')
                ->leftJoin('j.restriction', 'r')
                ->select('j', 'r')
                ->andWhere('j.active = 1')
                ->getQuery()
                ->getResult();
            $canBeJudged = false;
            foreach ($judgehosts as $judgehost) {
                if (!$judgehost->getRestriction()) {
                    $canBeJudged = true;
                    break;
                }

                //Start-Modify SonNM
                // $queryBuilder = $this->em->createQueryBuilder()
                //     ->from(Submission::class, 's')
                //     ->select('s')
                //     ->join('s.language', 'lang')
                //     ->join('s.contest_problem', 'cp')
                //     ->andWhere('s.submitid = :submitid')
                //     ->andWhere('s.judgehost IS NULL')
                //     ->andWhere('lang.allowJudge = 1')
                //     ->andWhere('cp.allowJudge = 1')
                //     ->andWhere('s.valid = 1')
                //     ->setParameter(':submitid', $submission->getSubmitid())
                //     ->setMaxResults(1);
                $queryBuilder = $this->em->createQueryBuilder()
                    ->from(Submission::class, 's')
                    ->select('s')
                    ->join('s.language', 'lang')
                    ->andWhere('s.submitid = :submitid')
                    ->andWhere('s.judgehost IS NULL')
                    ->andWhere('lang.allowJudge = 1')
                    ->andWhere('s.valid = 1')
                    ->setParameter(':submitid', $submission->getSubmitid())
                    ->setMaxResults(1);
                //End-Modify

                $restrictions = $judgehost->getRestriction()->getRestrictions();
                if (isset($restrictions['classes'])) {
                    $queryBuilder
                        ->andWhere('s.cid = :cid')
                        ->setParameter(':cid', $restrictions['classes']);
                }
                if (isset($restrictions['problem'])) {
                    $queryBuilder
                        ->leftJoin('s.problem', 'p')
                        ->andWhere('p.probid IN (:problems)')
                        ->setParameter(':problems', $restrictions['problem']);
                }
                if (isset($restrictions['language'])) {
                    $queryBuilder
                        ->andWhere('s.langid IN (:languages)')
                        ->setParameter(':languages', $restrictions['language']);
                }

                if ($queryBuilder->getQuery()->getOneOrNullResult()) {
                    $canBeJudged = true;
                }
            }

            if (!$canBeJudged) {
                $unjudgableReasons[] = 'No active judgehost can judge this submission. Edit judgehost restrictions!';
            }

            if (!$submission->getLanguage()->getAllowJudge()) {
                $unjudgableReasons[] = 'Submission language is currently not allowed to be judged!';
            }

            if (!$submission->getProblem()->getAllowJudge()) {
                $unjudgableReasons[] = 'Problem is currently not allowed to be judged!';
            }
        }

        $outputDisplayLimit    = (int)$this->dj->dbconfig_get('output_display_limit', 2000);
        $outputTruncateMessage = sprintf("\n[output display truncated after %d B]\n", $outputDisplayLimit);

        $externalRuns = [];
        //Start-Delete SonNM
        // $externalRuns = [];
        // if ($externalJudgement = $submission->getExternalJudgements()->first()) {
        //     $queryBuilder = $this->em->createQueryBuilder()
        //         ->from(Testcase::class, 't')
        //         ->leftJoin('t.external_runs', 'er', Join::WITH, 'er.external_judgement = :judging')
        //         ->select('t', 'er')
        //         ->andWhere('t.problem = :problem')
        //         ->setParameter(':judging', $externalJudgement)
        //         ->setParameter(':problem', $submission->getProblem())
        //         ->orderBy('t.rank');

        //     $externalRunResults = $queryBuilder
        //         ->getQuery()
        //         ->getResult();

        //     foreach ($externalRunResults as $externalRunResult) {
        //         $externalRuns[] = $externalRunResult;
        //     }
        // }
        //End-Delete

        $runs       = [];
        $runsOutput = [];
        if ($selectedJudging) {
            //Start-Modify SonNM
            // $queryBuilder = $this->em->createQueryBuilder()
            //     ->from('DOMJudgeBundle:Testcase', 't')
            //     ->join('t.testcase_content', 'tc')
            //     ->leftJoin('t.judging_runs', 'jr', Join::WITH, 'jr.judging = :judging')
            //     ->leftJoin('jr.judging_run_output', 'jro')
            //     ->select('t', 'jr', 'tc.image_thumb AS image_thumb', 'jro.metadata')
            //     ->andWhere('t.problem = :problem')
            //     ->setParameter(':judging', $selectedJudging)
            //     ->setParameter(':problem', $submission->getProblem())
            //     ->orderBy('t.rank');
            $queryBuilder = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Testcase', 't')
                ->join('t.testcase_content', 'tc')
                ->leftJoin('t.judging_runs', 'jr', Join::WITH, 'jr.judging = :judging')
                ->leftJoin('jr.judging_run_output', 'jro')
                ->select('t', 'jr', 'tc.image_thumb AS image_thumb')
                ->andWhere('t.problem = :problem')
                ->setParameter(':judging', $selectedJudging)
                ->setParameter(':problem', $submission->getProblem())
                ->orderBy('t.rank');
            //End-Modify
            //Start-delete SonNM
            if ($outputDisplayLimit < 0) {
                $queryBuilder
                    ->addSelect('tc.output AS output_reference')
                    ->addSelect('jro.output_run AS output_run')
                    ->addSelect('jro.output_diff AS output_diff')
                    ->addSelect('jro.output_error AS output_error')
                    ->addSelect('jro.output_system AS output_system');
            } else {
                $queryBuilder
                    ->addSelect('TRUNCATE(tc.output, :outputDisplayLimit, :outputTruncateMessage) AS output_reference')
                    ->addSelect('TRUNCATE(jro.output_run, :outputDisplayLimit, :outputTruncateMessage) AS output_run')
                    ->addSelect('TRUNCATE(jro.output_diff, :outputDisplayLimit, :outputTruncateMessage) AS output_diff')
                    ->addSelect('TRUNCATE(jro.output_error, :outputDisplayLimit, :outputTruncateMessage) AS output_error')
                    ->addSelect('TRUNCATE(jro.output_system, :outputDisplayLimit, :outputTruncateMessage) AS output_system')
                    ->setParameter(':outputDisplayLimit', $outputDisplayLimit)
                    ->setParameter(':outputTruncateMessage', $outputTruncateMessage);
            }
            //End-Delete

            $runResults = $queryBuilder
                ->getQuery()
                ->getResult();
            
            //Start-Delete SonNM
            foreach ($runResults as $runResult) {
                $runs[] = $runResult[0];
                unset($runResult[0]);
                $runResult['terminated'] = preg_match('/timelimit exceeded.*hard (wall|cpu) time/',
                                                      (string)$runResult['output_system']);
                $runsOutput[]            = $runResult;
            }
            //End-Delete
        }

        if ($submission->getOrigsubmitid()) {
            $lastSubmission = $this->em->getRepository(Submission::class)->find($submission->getOrigsubmitid());
        } else {
            /** @var Submission|null $lastSubmission */
            $lastSubmission = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('s')
                ->andWhere('s.user = :user')
                ->andWhere('s.problem = :problem')
                ->andWhere('s.submittime < :submittime')
                ->setParameter(':user', $submission->getUser())
                ->setParameter(':problem', $submission->getProblem())
                ->setParameter(':submittime', $submission->getSubmittime())
                ->orderBy('s.submittime', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        }

        /** @var Judging|null $lastJudging */
        $lastJudging = null;
        /** @var Testcase[] $lastRuns */
        $lastRuns = [];
        if ($lastSubmission !== null) {
            $lastJudging = $this->em->createQueryBuilder()
                ->from(Judging::class, 'j')
                ->select('j')
                ->andWhere('j.submission = :submission')
                ->andWhere('j.valid = 1')
                ->setParameter(':submission', $lastSubmission)
                ->orderBy('j.judgingid', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

            if ($lastJudging !== null) {
                // Clear the testcases, otherwise Doctrine will use the previous data
                $this->em->clear(Testcase::class);
                $lastRuns = $this->em->createQueryBuilder()
                    ->from(Testcase::class, 't')
                    ->leftJoin('t.judging_runs', 'jr', Join::WITH, 'jr.judging = :judging')
                    ->select('t', 'jr')
                    ->andWhere('t.problem = :problem')
                    ->setParameter(':judging', $lastJudging)
                    ->setParameter(':problem', $submission->getProblem())
                    ->orderBy('t.rank')
                    ->getQuery()
                    ->getResult();
            }
        }

        $data += [
            'submission' => $submission,
            'lastSubmission' => $lastSubmission,
            'rejected' => !empty($checkReject),
            'judgings' => $judgings,
            'maxRunTimes' => $maxRunTimes,
            'selectedJudging' => $selectedJudging,
            'lastJudging' => $lastJudging,
            'runs' => $runs,
            'externalRuns' => $externalRuns,
            'runsOutput' => $runsOutput,
            'lastRuns' => $lastRuns,
            'unjudgableReasons' => $unjudgableReasons,
            'verificationRequired' => (bool)$this->dj->dbconfig_get('verification_required', false),
            'claimWarning' => $claimWarning,
            'combinedRunCompare' => $submission->getProblem()->getCombinedRunCompare(),
        ];

        if ($selectedJudging === null) {
            // Automatically refresh page while we wait for judging data.
            $data['refresh'] = [
                'after' => 15,
                'url' => $this->generateUrl('teacher_submission', ['submitId' => $submission->getSubmitid(), 'cid' => !isset($cid)? $cid:'']),
            ];
        }

        return $this->render('@DOMJudge/Fls/Teacher/teacher_submission.html.twig', $data);
    }

    /**
     * @Route("/by-judging-id/{jid}", name="teacher_submission_by_judging")
     */
    public function viewForJudgingAction(Judging $jid)
    {
        $cid = $request->query->get('cid');
        if(!empty($cid)){
            $redirect = $this->redirectToRoute('teacher_submission', [
                            'submitId' => $jid->getSubmitid(),
                            'jid' => $jid->getJudgingid(),
                            'cid' => $cid,
                        ]);
        }else{
            $redirect = $this->redirectToRoute('teacher_submission', [
                'submitId' => $jid->getSubmitid(),
                'jid' => $jid->getJudgingid(),
            ]);
        }
        return $redirect;
    }

    /**
     * @Route("/by-external-id/{externalId}", name="jury_submission_by_external_id")
     */
    public function viewForExternalIdAction(string $externalId)
    {
        if (!$this->dj->getCurrentContest()) {
            throw new BadRequestHttpException("Cannot determine submission from external ID without selecting a contest.");
        }

        $submission = $this->em->getRepository(Submission::class)
            ->findOneBy([
                            'cid' => $this->dj->getCurrentContest()->getCid(),
                            'externalid' => $externalId
                        ]);

        if (!$submission) {
            throw new NotFoundHttpException(sprintf('No submission found with external ID %s', $externalId));
        }

        return $this->redirectToRoute('teacher_submission', [
            'submitId' => $submission->getSubmitid(),
        ]);
    }

    /**
     * @Route("/{submitid}/source", name="teacher_submission_source")
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function sourceAction(Request $request, Submission $submission)
    {
        /** @var SubmissionFileWithSourceCode $file */
        $file = $this->em->createQueryBuilder()
        ->from('DOMJudgeBundle:SubmissionFileWithSourceCode', 'file')
        ->select('file')
        ->andWhere('file.submission = :submission')
        ->setParameter(':submission', $submission)
        ->getQuery()
        ->getOneOrNullResult();
        if (!$file) {
            throw new NotFoundHttpException(sprintf('No submission file found'));
        }
        // Download requested
        $response = new Response();
        $response->headers->set('Content-Type',
                                sprintf('text/plain; name="%s"; charset="utf-8"', $file->getFilename()));
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $file->getFilename()));
        $response->headers->set('Content-Length', (string)strlen($file->getSourcecode()));
        $response->setContent($file->getSourcecode());

        return $response;
    }

    /**
     * @Route("/{submitId<\d+>}/update-status", name="jury_submission_update_status", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     * @param EventLogService   $eventLogService
     * @param ScoreboardService $scoreboardService
     * @param Request           $request
     * @param int               $submitId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function updateStatusAction(
        EventLogService $eventLogService,
        ScoreboardService $scoreboardService,
        Request $request,
        int $submitId
    ) {
        $submission = $this->em->getRepository(Submission::class)->find($submitId);
        $valid      = $request->request->getBoolean('valid');
        $submission->setValid($valid);
        $this->em->flush();

        // KLUDGE: We can't log an "undelete", so we re-"create".
        // FIXME: We should also delete/recreate any dependent judging(runs).
        $eventLogService->log('submission', $submission->getSubmitid(), ($valid ? 'create' : 'delete'),
                              $submission->getCid(), null, null, $valid);
        $this->dj->auditlog('submission', $submission->getSubmitid(),
                                         'marked ' . ($valid ? 'valid' : 'invalid'));
        $contest = $this->em->getRepository(Contest::class)->find($submission->getCid());
        $team    = $this->em->getRepository(Team::class)->find($submission->getTeamid());
        $problem = $this->em->getRepository(Problem::class)->find($submission->getProbid());
        $scoreboardService->calculateScoreRow($contest, $team, $problem);

        return $this->redirectToRoute('teacher_submission', ['submitId' => $submission->getSubmitid()]);
    }

    /**
     * @Route("/{judgingId}/verify", name="teacher_judging_verify", methods={"POST"}, requirements={"judgingId": "\d+"})
     * @param EventLogService   $eventLogService
     * @param ScoreboardService $scoreboardService
     * @param BalloonService    $balloonService
     * @param Request           $request
     * @param int               $judgingId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function verifyAction(
        EventLogService $eventLogService,
        ScoreboardService $scoreboardService,
        BalloonService $balloonService,
        Request $request,
        int $judgingId
    ) {
        $cid = $request->query->get('cid');
        $this->em->transactional(function () use ($eventLogService, $request, $judgingId) {
            /** @var Judging $judging */
            $judging  = $this->em->getRepository(Judging::class)->find($judgingId);           
            $verified = $request->request->getBoolean('verified');
            $out = $request->request->getBoolean('out');
            if($out){
                $verified = false;
            }
            $judging
                ->setVerified($verified)
                ->setJuryMember($verified ? $this->dj->getUser()->getUsername() : $this->dj->getUser()->getUsername());

            $this->em->flush();
            $this->em->clear();

            $this->dj->auditlog('judging', $judging->getJudgingid(),
                                             $verified ? 'set verified' : 'set unverified');
            
            $submission  = $this->em->getRepository(Submission::class)->find($judging->getSubmitid());
            // $problem = $this->em->getRepository(Problem::class)->find($submission->getProblem()->getProbid());
            $leaderBoard = $this->em->getRepository(Leaderboard::class)->createQueryBuilder('lb')
                ->select('lb')
                ->where('lb.cid = :cid')
                ->setParameter(':cid', $submission->getCid())
                ->andWhere('lb.userid = :userid')
                ->setParameter(':userid', $submission->getUserid())
                ->getQuery()
                ->getOneOrNullResult();
            $userProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
                ->select('up')
                ->where('up.cid = :cid')
                ->setParameter(':cid', $submission->getCid())
                ->andWhere('up.userid = :userid')
                ->setParameter(':userid', $submission->getUserid())
                ->andWhere('up.probid = :probid')
                ->setParameter(':probid', $submission->getProblem()->getProbid())
                ->getQuery()
                ->getOneOrNullResult();
            if($leaderBoard != null ){
                $totalPoint = $submission->getProblem()->getPoints() + $leaderBoard->getPointsRestricted();
                $leaderBoard->setPointsRestricted($totalPoint);
            }
            if($userProblem != null ) {
                $userProblem->setStatus('4');
                $userProblem->setFinishedTime(Utils::now());
            }                

            $this->em->flush();
            
            if ((bool)$this->dj->dbconfig_get('verification_required', false)) {
                // Log to event table (case of no verification required is handled
                // in the REST API API/JudgehostController::addJudgingRunAction
                $eventLogService->log('judging', $judging->getJudgingid(), 'update', $judging->getCid());
            }
        }); 

        // if ((bool)$this->dj->dbconfig_get('verification_required', false)) {
        //     $this->em->clear();
        //     /** @var Judging $judging */
        //     $judging = $this->em->getRepository(Judging::class)->find($judgingId);
        //     $scoreboardService->calculateScoreRow($judging->getContest(), $judging->getSubmission()->getTeam(),
        //                                           $judging->getSubmission()->getProblem());
        //     $balloonService->updateBalloons($judging->getContest(), $judging->getSubmission(), $judging);
        // }

        // Redirect to referrer page after verification or back to submission page when unverifying.
        if ($request->request->getBoolean('verified')) {
            $redirect = $request->request->get('redirect', $this->generateUrl('teacher_submissions',['cid'=>!empty($cid)?$cid:'','class' => !empty($cid)?$cid:'']));
        } else {
            $redirect = $this->generateUrl('teacher_submission_by_judging', ['jid' => $judgingId,'cid'=>!empty($cid)?$cid:'']);
        }

        return $this->redirect($redirect);
    }

    /**
     * @Route("/{judgingId}/reject", name="teacher_judging_reject", methods={"POST"}, requirements={"judgingId": "\d+"})
     * @param EventLogService   $eventLogService
     * @param ScoreboardService $scoreboardService
     * @param BalloonService    $balloonService
     * @param Request           $request
     * @param int               $judgingId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function rejectAction(
        EventLogService $eventLogService,
        ScoreboardService $scoreboardService,
        BalloonService $balloonService,
        Request $request,
        int $judgingId
    ) {
        $cid = $request->query->get('cid');
            $this->em->transactional(function () use ($eventLogService, $request, $judgingId) {
            /** @var Judging $judging */
            $judging  = $this->em->getRepository(Judging::class)->find($judgingId);           
            $verified = $request->request->get('reject');
            $judging
                ->setVerified($verified)
                ->setJuryMember($verified ? $this->dj->getUser()->getUsername() : null);

            $this->em->flush();

            
            if ($verified) {
                $submission  = $this->em->getRepository(Submission::class)->find($judging->getSubmitid());
                $problem = $this->em->getRepository(Problem::class)->find($submission->getProblem()->getProbid());
                $userProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
                    ->select('up')
                    ->where('up.cid = :cid')
                    ->setParameter(':cid', $submission->getCid())
                    ->andWhere('up.userid = :userid')
                    ->setParameter(':userid', $submission->getUserid())
                    ->andWhere('up.probid = :probid')
                    ->setParameter(':probid', $problem->getProbid())
                    ->getQuery()
                    ->getResult();
                $userProblem[0]->setStatus('5');

                $this->em->flush();
                // random problem for stduent
                $listProblem = $this->dj->genarateProblemForStudent($userProblem[0]->getCid(), $userProblem[0]->getUserid());
                // insert data to db
                foreach ($listProblem as $value) {
                    $tempUserProblem = new Userproblem();
                    $tempUserProblem->setUserid($value['student'])
                        ->setCid($userProblem[0]->getCid())
                        ->setProbid($value['problem'])
                        ->setReviewStatus($value['review'])
                        ->setChanged(0)
                        ->setStatus(0);
                    $this->em->persist($tempUserProblem);
                }
                // push data
                $this->em->transactional(function () {
                    $this->em->flush();
                });
                $msg = count($listProblem).' Problems random for student '.$userProblem[0]->getUserid()->getName();
                $this->addFlash('success',$msg);
            }
            
            if ((bool)$this->dj->dbconfig_get('verification_required', false)) {
                // Log to event table (case of no verification required is handled
                // in the REST API API/JudgehostController::addJudgingRunAction
                $eventLogService->log('judging', $judging->getJudgingid(), 'update', $judging->getCid());
            }
        }); 

        // if ((bool)$this->dj->dbconfig_get('verification_required', false)) {
        //     $this->em->clear();
        //     /** @var Judging $judging */
        //     $judging = $this->em->getRepository(Judging::class)->find($judgingId);
        //     $scoreboardService->calculateScoreRow($judging->getContest(), $judging->getSubmission()->getTeam(),
        //                                           $judging->getSubmission()->getProblem());
        //     $balloonService->updateBalloons($judging->getContest(), $judging->getSubmission(), $judging);
        // }

        // Redirect to referrer page after verification or back to submission page when unverifying.
        if ($request->request->get('reject') == 2) {
            $redirect = $request->request->get('redirect', $this->generateUrl('teacher_submissions',['cid'=>!empty($cid)?$cid:'','class' => !empty($cid)?$cid:'']));
        } else {
            $redirect = $this->generateUrl('teacher_submission_by_judging', ['jid' => $judgingId,'cid'=>!empty($cid)?$cid:'']);
        }

        return $this->redirect($redirect);
    }

    /**
     * @param SubmissionFile[] $filesteacher_submission_team_output
     * @param SubmissionFile[] $oldFiles
     * @return array
     */
    protected function determineFileChanged(array $files, array $oldFiles)
    {
        $result = [
            'added' => [],
            'removed' => [],
            'changed' => [],
            'changedfiles' => [], // These will be shown, so we will add pairs of files here
            'unchanged' => [],
        ];

        $newFilenames = [];
        $oldFilenames = [];
        foreach ($files as $newfile) {
            $oldFilenames = [];
            foreach ($oldFiles as $oldFile) {
                if ($newfile->getFilename() === $oldFile->getFilename()) {
                    if ($oldFile->getSubmissionFileSourceCode() === $newfile->getSubmissionFileSourceCode()) {
                        $result['unchanged'][] = $newfile->getFilename();
                    } else {
                        $result['changed'][]      = $newfile->getFilename();
                        $result['changedfiles'][] = [$newfile, $oldFile];
                    }
                }
                $oldFilenames[] = $oldFile->getFilename();
            }
            $newFilenames[] = $newfile->getFilename();
        }

        $result['added']   = array_diff($newFilenames, $oldFilenames);
        $result['removed'] = array_diff($oldFilenames, $newFilenames);

        // Special case: if we have exactly one file now and before but the filename is different, use that for diffing
        if (count($result['added']) === 1 && count($result['removed']) === 1 && empty($result['changed'])) {
            $result['added']        = [];
            $result['removed']      = [];
            $result['changed']      = [$files[0]->getFilename()];
            $result['changedfiles'] = [[$files[0], $oldFiles[0]]];
        }

        return $result;
    }

    /**
     * Get a list of submissions that can be displayed in the interface using
     * the submission_list partial.
     *
     * Restrictions can contain the following keys;
     * - rejudgingid: ID of a rejudging to filter on
     * - verified: If true, only return verified submissions.
     *             If false, only return unverified or unjudged submissions.
     * - judged: If true, only return judged submissions.
     *           If false, only return unjudged submissions.
     * - rejudgingdiff: If true, only return judgings that differ from their
     *                  original result in final verdict. Vice versa if false.
     * - teamid: ID of a team to filter on
     * - categoryid: ID of a team category to filter on
     * - probid: ID of a problem to filter on
     * - langid: ID of a language to filter on
     * - judgehost: hostname of a judgehost to filter on
     * - old_result: result of old judging to filter on
     * - result: result of current judging to filter on
     *
     * @param array $restrictions
     * @param int   $limit
     * @return array An array with two elements: the first one is the list of
     *               submissions and the second one is an array with counts.
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSubmissionList(array $restrictions, int $limit, $cid = null)
    {

        //Start-Modify SonNM
        // $queryBuilder = $this->em->createQueryBuilder()
        //     ->from(Submission::class, 's')
        //     ->select('s', 'j')
        //     ->join('s.team', 't')
        //     ->join('s.contest_problem', 'cp')
        //     ->andWhere('s.cid IN (:contests)')
        //     ->setParameter(':contests', array_keys($contests))
        //     ->orderBy('s.submittime', 'DESC')
        //     ->addOrderBy('s.submitid', 'DESC');
        $submissions = [];
        if($cid != null){
            $userproblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
                ->andwhere('up.cid = :cid')
                ->setParameter('cid', $cid)
                ->select('count(up.userproblemid)')
                ->getQuery()->getOneOrNullResult();
            if(isset($userproblem[1]) && $userproblem[1] == 0){
                $counts = [];
                return [$submissions, $counts];
            }
        }
        if ($limit == 1) {
            // review
            $reviewStatus = 1;
            $status = 3;
            $result = 'correct';
            $expr = $this->em->getExpressionBuilder();
                
            $sub = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('max(s.submitid) as submitid');
                if($cid != null){
                    $sub->where('s.cid = :cid')
                    ->setParameter(':cid', $cid);
                }
            $sub->groupBy('s.userproblemid');
            $listid = array_column($sub->getQuery()->getResult(),'submitid');
            if(empty($listid)){
                $counts = [];
                return [$submissions, $counts];
            }
            
            $queryBuilder = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('s','j');
                if($cid != null){
                    $queryBuilder->where('s.cid = :cid')
                        ->setParameter(':cid', $cid);
                }
            $queryBuilder->andwhere($expr->in('s.submitid',$listid))
                ->join('s.userproblem', 'up')
                ->andwhere('up.status = :status')
                ->setParameter(':status', $status)
                ->andWhere('j.result = :result')
                ->setParameter(':result', $result)
                ->andWhere('up.reviewStatus = :reviewStatus')
                ->setParameter(':reviewStatus', $reviewStatus);
            // $submissions = $this->getSubmission($queryBuilder,$restrictions);

        } else if ($limit == 2) {
            // accept
            $status = 4;
            $result = 'correct';
            $expr = $this->em->getExpressionBuilder();
                
            $sub = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('max(s.submitid) as submitid');
                if($cid != null){
                    $sub->where('s.cid = :cid')
                    ->setParameter(':cid', $cid); 
                }
            $sub->groupBy('s.userproblemid');
            $listid = array_column($sub->getQuery()->getResult(),'submitid');
            if(empty($listid)){
                $counts = [];
                return [$submissions, $counts];
            }
            $queryBuilder = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('s','j');
                if($cid != null){
                    $queryBuilder->where('s.cid = :cid')
                        ->setParameter(':cid', $cid);
                }
            $queryBuilder->andwhere($expr->in('s.submitid',$listid))
                ->join('s.userproblem', 'up')
                ->andwhere('up.status = :status')
                ->setParameter(':status', $status);
                // ->andWhere('j.result = :result')
                // ->setParameter(':result', $result);
            // $submissions = $this->getSubmission($queryBuilder,$restrictions);
            
        } else if ($limit == 3) {
            // reject
            $status = 5;
            $expr = $this->em->getExpressionBuilder();
                
            $sub = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('max(s.submitid) as submitid');
                if($cid != null){
                    $sub->where('s.cid = :cid')
                    ->setParameter(':cid', $cid); 
                }
            $sub->groupBy('s.userproblemid');
            $listid = array_column($sub->getQuery()->getResult(),'submitid');
            if(empty($listid)){
                $counts = [];
                return [$submissions, $counts];
            }

            $queryBuilder = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('s','j');
                if($cid != null){
                    $queryBuilder->where('s.cid = :cid')
                        ->setParameter(':cid', $cid);
                }
            $queryBuilder->andwhere($expr->in('s.submitid',$listid))
                ->join('s.userproblem', 'up')
                ->andwhere('up.status = :status')
                ->setParameter(':status', $status);
            // $submissions = $this->getSubmission($queryBuilder,$restrictions);
        } else {
            $reviewStatus = 0;
            $status = 3;
            $result = 'correct';
            $expr = $this->em->getExpressionBuilder();
                
            $sub = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('max(s.submitid) as submitid');
                if($cid != null){
                    $sub->where('s.cid = :cid')
                    ->setParameter(':cid', $cid); 
                }
            $sub->groupBy('s.userproblemid');
            $listid = array_column($sub->getQuery()->getResult(),'submitid');
            if(empty($listid)){
                $counts = [];
                return [$submissions, $counts];
            }

            $queryBuilder = $this->em->createQueryBuilder()
                ->from(Submission::class, 's')
                ->select('s','j');
                if($cid != null){
                    $queryBuilder->where('s.cid = :cid')
                        ->setParameter(':cid', $cid);
                }
            $queryBuilder->andwhere($expr->in('s.submitid',$listid))
                ->join('s.userproblem', 'up');
            // Submission correct and review is 0
            if (isset($restrictions['verified'])) {
                if ($restrictions['verified'] == 0) {
                    $queryBuilder->andwhere('up.status = :status')
                        ->setParameter(':status', $status)
                        ->andWhere('up.reviewStatus = :reviewStatus')
                        ->setParameter(':reviewStatus', $reviewStatus)
                        ->andWhere('j.result = :result')
                        ->setParameter(':result', $result);
                } 
            }
            if (isset($restrictions['judged'])) {
                if ($restrictions['judged'] == 0) {
                    $queryBuilder->andwhere('up.status != 5 and up.status != 4 ')
                        ->andWhere('j.result != :result OR j.endtime IS NULL')
                        ->setParameter(':result', $result);
                }
            }
        }
        //End-Modify 
        // congnt
        // if (isset($restrictions['rejudgingid'])) {
        //     $queryBuilder
        //         ->leftJoin('s.judgings', 'j', Join::WITH, 'j.rejudgingid = :rejudgingid')
        //         ->leftJoin(Judging::class, 'jold', Join::WITH,
        //                    'j.prevjudgingid IS NULL AND s.submitid = jold.submitid AND jold.valid = 1 OR j.prevjudgingid = jold.judgingid')
        //         ->addSelect('jold.result AS oldresult')
        //         ->andWhere('s.rejudgingid = :rejudgingid OR j.rejudgingid = :rejudgingid')
        //         ->setParameter(':rejudgingid', $restrictions['rejudgingid']);

        //     if (isset($restrictions['rejudgingdiff'])) {
        //         if ($restrictions['rejudgingdiff']) {
        //             $queryBuilder->andWhere('j.result != jold.result');
        //         } else {
        //             $queryBuilder->andWhere('j.result = jold.result');
        //         }
        //     }

        //     if (isset($restrictions['old_result'])) {
        //         $queryBuilder
        //             ->andWhere('jold.result = :oldresult')
        //             ->setParameter(':oldresult', $restrictions['old_result']);
        //     }
        // } else {
        //     $queryBuilder->leftJoin('s.judgings', 'j', Join::WITH, 'j.valid = 1');
        // }
        // congnt
        $queryBuilder->leftJoin('s.judgings', 'j', Join::WITH, 'j.valid = 1');
        // $queryBuilder->leftJoin('j.rejudging', 'r');
        // end

        // if (isset($restrictions['verified'])) {
        //     if ($restrictions['verified']) {
        //         $queryBuilder->andWhere('j.verified = 1');
        //     } else {
        //         $queryBuilder->andWhere('j.verified = 0 OR (j.verified IS NULL AND s.judgehost IS NULL)');
        //     }
        // }

        // if (isset($restrictions['judged'])) {
        //     if ($restrictions['judged']) {
        //         $queryBuilder->andWhere('j.result IS NOT NULL');
        //     } else {
        //         $queryBuilder->andWhere('j.result IS NULL OR j.endtime IS NULL');
        //     }
        // }

        if (isset($restrictions['externally_judged'])) {
            if ($restrictions['externally_judged']) {
                $queryBuilder->andWhere('ej.result IS NOT NULL');
            } else {
                $queryBuilder->andWhere('ej.result IS NULL OR ej.endtime IS NULL');
            }
        }

        if (isset($restrictions['external_diff'])) {
            if ($restrictions['external_diff']) {
                $queryBuilder->andWhere('j.result != ej.result');
            } else {
                $queryBuilder->andWhere('j.result = ej.result');
            }
        }

        if (isset($restrictions['external_result'])) {
            if ($restrictions['external_result'] === 'judging') {
                $queryBuilder->andWhere('ej.result IS NULL or ej.endtime IS NULL');
            } else {
                $queryBuilder
                    ->andWhere('ej.result = :externalresult')
                    ->setParameter(':externalresult', $restrictions['external_result']);
            }
        }

        if (isset($restrictions['userid'])) {
            $queryBuilder
                ->andWhere('s.userid = :userid')
                ->setParameter(':userid', $restrictions['userid']);
        }
        //End-Modify

        if (isset($restrictions['categoryid'])) {
            $queryBuilder
                ->andWhere('t.categoryid = :categoryid')
                ->setParameter(':categoryid', $restrictions['categoryid']);
        }

        if (isset($restrictions['probid'])) {
            $queryBuilder
                ->andWhere('s.probid = :probid')
                ->setParameter(':probid', $restrictions['probid']);
        }

        if (isset($restrictions['langid'])) {
            $queryBuilder
                ->andWhere('s.langid = :langid')
                ->setParameter(':langid', $restrictions['langid']);
        }

        if (isset($restrictions['judgehost'])) {
            $queryBuilder
                ->andWhere('s.judgehost = :judgehost')
                ->setParameter(':judgehost', $restrictions['judgehost']);
        }

        if (isset($restrictions['result'])) {
            if ($restrictions['result'] === 'judging') {
                $queryBuilder->andWhere('j.result IS NULL OR j.endtime IS NULL');
            } else {
                $queryBuilder
                    ->andWhere('j.result = :result')
                    ->setParameter(':result', $restrictions['result']);
            }
        }

        if ($this->dj->dbconfig_get('data_source', DOMJudgeService::DATA_SOURCE_LOCAL) ==
            DOMJudgeService::DATA_SOURCE_CONFIGURATION_AND_LIVE_EXTERNAL) {
            // When we are shadow, also load the external results
            $queryBuilder
                ->leftJoin('s.external_judgements', 'ej', Join::WITH, 'ej.valid = 1')
                ->addSelect('ej');
        }
        $submissions = $queryBuilder->getQuery()->getResult();
        //congnt
        // if (isset($restrictions['rejudgingid'])) {
        //     // Doctrine will return an array for each item. At index '0' will be the submission and at
        //     // index 'oldresult' will be the old result. Remap this
        //     $submissions = array_map(function ($submissionData) {
        //         /** @var Submission $submission */
        //         $submission = $submissionData[0];
        //         $submission->setOldResult($submissionData['oldresult']);
        //         return $submission;
        //     }, $submissions);
        // }

        $counts = [];
        $countQueryExtras = [
            'total' => '',
            'correct' => 'j.result LIKE \'correct\'',
            'ignored' => 's.valid = 0',
            // 'unverified' => 'j.verified = 0 AND j.result IS NOT NULL',
            'queued' => 'j.result IS NULL'
        ];
        foreach ($countQueryExtras as $count => $countQueryExtra) {
            $countQueryBuilder = (clone $queryBuilder)->select('COUNT(s.submitid) AS cnt');
            if (!empty($countQueryExtra)) {
                $countQueryBuilder->andWhere($countQueryExtra);
            }
            $counts[$count] = (int)$countQueryBuilder
                ->getQuery()
                ->getSingleScalarResult();
        }

        return [$submissions, $counts];
    }
}