<?php declare (strict_types = 1);

namespace DOMJudgeBundle\Controller\Teacher;

use Doctrine\ORM\EntityManagerInterface;
use DOMJudgeBundle\Entity\Clarification;
use DOMJudgeBundle\Entity\Contest;
use DOMJudgeBundle\Entity\ContestProblem;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Entity\Role;
use DOMJudgeBundle\Entity\Userproblem;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Utils\Utils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/teacher/asks")
 * @Security("has_role('ROLE_TEACHER')")
 */
class AsksController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    protected $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * ClarificationController constructor.
     * @param EntityManagerInterface $em
     * @param DOMJudgeService        $dj
     * @param EventLogService        $eventLogService
     */
    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService
    ) {
        $this->em = $em;
        $this->dj = $dj;
        $this->eventLogService = $eventLogService;
    }

    /**
     * @Route("", name="teacher_ask")
     * @throws \Exception
     */
    public function indexAction(Request $request)
    {

        $currentUser = $this->dj->getUser();

        $queryBuilder = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Clarification', 'clar')
            ->leftJoin('clar.userproblem', 'up')
            ->select('clar', 'up')
            ->andWhere('clar.receiver = :curUser')
            ->andWhere('clar.respid IS NULL')
            ->setParameter('curUser', $currentUser->getUsername())
            ->orderBy('clar.submittime', 'DESC')
            ->addOrderBy('clar.clarid', 'DESC');

        /**
         * @var Clarification[] $newClarifications
         * @var Clarification[] $oldClarifications
         * @var Clarification[] $generalClarifications
         */
        $newClarifications = $oldClarifications = $generalClarifications = [];
        $wheres = [
            'new' => 'clar.answered = 0',
            'old' => 'clar.answered != 0',
            'general' => '(clar.respid IS NULL)',
        ];
        foreach ($wheres as $type => $where) {
            $clarifications = (clone $queryBuilder)
                ->andWhere($where)
                ->getQuery()
                ->getResult();

            switch ($type) {
                case 'new':
                    $newClarifications = $clarifications;
                    break;
                case 'old':
                    $oldClarifications = $clarifications;
                    break;
                case 'general':
                    $generalClarifications = $clarifications;
                    break;
            }
        }

        return $this->render('@DOMJudge/Fls/Teacher/clarifications.html.twig', [
            'newClarifications' => $newClarifications,
            'oldClarifications' => $oldClarifications,
            'generalClarifications' => $generalClarifications,
            'showExternalId' => $this->eventLogService->externalIdFieldForEntity(Clarification::class),
            'currentQueue' => 'all',
        ]);
    }

    /**
     * @Route("/{id}", name="teacher_clarification", requirements={"id": "\d+"})
     * @throws \Exception
     */
    public function viewAction(Request $request, int $id)
    {
        /** @var Clarification $clarification */
        $clarification = $this->em->getRepository(Clarification::class)->find($id);
        if (!$clarification) {
            throw new NotFoundHttpException(sprintf('Clarification with ID %s not found', $id));
        }

        $clardata = ['list' => []];
        $clardata['clarform'] = $this->getClarificationFormData($clarification);
        $clardata['showExternalId'] = $this->eventLogService->externalIdFieldForEntity(Clarification::class);

        $categories = $clardata['clarform']['subjects'];
        $queues = $this->dj->dbconfig_get('clar_queues');
        $clar_answers = $this->dj->dbconfig_get('clar_answers', []);

        if ($irt = $clarification->getInReplyTo()) {
            $clarlist = [$irt];
            $replies = $irt->getReplies() ?? [];
            foreach ($replies as $clar_reply) {$clarlist[] = $clar_reply;}
        } else {
            $clarlist = [$clarification];
            $replies = $clarification->getReplies() ?? [];
            foreach ($replies as $clar_reply) {$clarlist[] = $clar_reply;}
        }

        $concernsteam = null;
        foreach ($clarlist as $k => $clar) {
            $data = ['clarid' => $clar->getClarid(), 'externalid' => $clar->getExternalid()];
            $data['time'] = $clar->getSubmittime();

            $jurymember = $clar->getReceiver();
            if (!empty($jurymember)) {
                $juryuser = $this->em->getRepository(User::class)->findBy(['username' => $jurymember]);
                $data['from_jurymember'] = $juryuser[0]->getName();
                $data['jurymember_is_me'] = $juryuser[0] == $this->getUser();
            }

            // if ( $fromuser = $clar->getUserproblem() ) {
            //     $data['from_username'] = $fromuser->getUserid()->getUsername();
            //     $data['from_userid'] = $fromuser->getUserid()->getUserid();
            //     $concernsteam = $fromuser->getUserid()->getUserid();
            // }
            // if ( $toteam = $clar->getRecipient() ) {
            //     $data['to_teamname'] = $toteam->getName();
            //     $data['to_teamid'] = $toteam->getTeamid();
            // }
            if ($clar->getReceiver() != null) {
                $data['from_username'] = $clar->getUserproblem()->getUserid()->getUsername();
                $data['from_userid'] = $clar->getUserproblem()->getUserid()->getUserid();
                $concernsteam = $clar->getUserproblem()->getUserid()->getUserid();

                $juryuser = $this->em->getRepository(User::class)->findBy(['username' => $clar->getReceiver()]);
                $data['to_teamname'] = $juryuser[0]->getName();
                $data['to_teamid'] = $juryuser[0] == $this->getUser();
            } else {
                $data['to_teamname'] = $clar->getUserproblem()->getUserid()->getUsername();
                $data['to_teamid'] = $clar->getUserproblem()->getUserid()->getUserid();
            }

            $classes = $clar->getUserproblem()->getProbid();
            $data['contest'] = $classes;
            $clarcontest = $classes->getShortname();
            if ($clar->getUserproblem()->getProbid()) {
                $concernssubject = $clar->getUserproblem()->getProbid()->getName() . "-" . $clar->getUserproblem()->getProbid()->getName();
            } elseif ($clar->getCategory()) {
                $concernssubject = $clar->getUserproblem()->getProbid()->getName() . "-" . $clar->getUserproblem()->getProbid()->getName();
            } else {
                $concernssubject = "";
            }
            if ($concernssubject !== "") {
                $data['subject'] = $categories[$clarcontest][$concernssubject];
            } else {
                $data['subject'] = $clarcontest;
            }
            $data['categoryid'] = $concernssubject;
            $data['queue'] = $queues[$clar->getQueue()] ?? 'Unassigned issues';
            $data['queueid'] = $clar->getQueue() ?? '';

            $data['answered'] = $clar->getAnswered();

            $data['body'] = Utils::wrap_unquoted($clar->getBody(), 78);
            $clardata['list'][] = $data;
        }

        if ($concernsteam) {
            $clardata['clarform']['toteam'] = $concernsteam;
        }
        if ($concernssubject) {
            $clardata['clarform']['onsubject'] = $concernssubject;
        }

        $clardata['clarform']['quotedtext'] = "> " . str_replace("\n", "\n> ", Utils::wrap_unquoted($data['body'])) . "\n\n";
        $clardata['clarform']['queues'] = $queues;
        $clardata['clarform']['answers'] = $clar_answers;

        return $this->render('@DOMJudge/Fls/Teacher/clarification.html.twig',
            $clardata
        );
    }

    protected function getProblemShortName(int $probid, int $cid): string
    {
        $cp = $this->em->getRepository(ContestProblem::class)->findBy(['probid' => $probid, 'cid' => $cid]);
        if (isset($cp[0])) {
            return "problem " . $cp[0]->getShortName();
        }
        return "unknown problem";
    }

    protected function getClarificationFormData(Clarification $clar = null): array
    {

        $data = ['teams' => null];

        $data['user'] = $clar->getUserproblem()->getUserid();
        $data['userproblem'] = $clar->getUserproblem();

        $data['subjects'] = null;

        return $data;
    }

    /**
     * @Route("/sendrequest", methods={"GET"}, name="teacher_ask_new")
     * @throws \Exception
     */
    public function composeClarificationAction(Request $request)
    {
        // TODO: use proper Symfony form for this

        // $data = $this->getClarificationFormData();

        // if ( $toteam = $request->query->get('teamto') ) {
        //     $data['toteam'] = $toteam;
        // }
        $adminRole = $this->em->getRepository(Role::class)->findBy(['dj_role' => 'admin']);
        $listReceive = $this->em->createQueryBuilder()
            ->select('u', 'r')
            ->from('DOMJudgeBundle:User', 'u')
            ->leftJoin('u.roles', 'r')
            ->where('r.dj_role = :adminRole or r.dj_role = :sadminRole')
            ->setParameter('adminRole', "admin")
            ->setParameter('sadminRole', "super_admin")
            ->getQuery()->getResult();
        
        $data['sendTo'] = $listReceive;
        return $this->render('@DOMJudge/Fls/Teacher/clarification_new.html.twig', ['clarform' => $data]);
    }

    /**
     * @Route("/{clarId}/answer", name="teacher_ask_answer", requirements={"clarId": "\d+"})
     * @param Request $request
     * @param int     $clarId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleClaimAction(Request $request, int $clarId)
    {
        /** @var Clarification $clarification */
        $clarification = $this->em->getReference(Clarification::class, $clarId);
        if (!$clarification) {
            throw new NotFoundHttpException(sprintf('Clarification with ID %i not found', $clarId));
        }

        if ($request->request->getBoolean('claimed')) {
            // $clarification->setTeacher($this->getUser()->getUsername());
            // $this->em->flush();
            return $this->redirectToRoute('teacher_clarification', ['id' => $clarId]);
        } else {
            // $clarification->setJuryMember(null);
            // $this->em->flush();
            return $this->redirectToRoute('teacher_ask');
        }
    }

    /**
     * @Route("/{clarId}/set-answered", name="jury_clarification_set_answered", requirements={"clarId": "\d+"})
     * @param Request $request
     * @param int     $clarId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleAnsweredAction(Request $request, int $clarId)
    {
        /** @var Clarification $clarification */
        $clarification = $this->em->getReference(Clarification::class, $clarId);
        if (!$clarification) {
            throw new NotFoundHttpException(sprintf('Clarification with ID %i not found', $clarId));
        }

        $answered = $request->request->getBoolean('answered');
        $clarification->setAnswered($answered);
        $this->em->flush();

        if ($answered) {
            return $this->redirectToRoute('teacher_ask');
        } else {
            return $this->redirectToRoute('teacher_clarification', ['id' => $clarId]);
        }
    }

    /**
     * @Route("/{clarId}/change-subject", name="jury_clarification_change_subject", requirements={"clarId": "\d+"})
     * @param Request $request
     * @param int     $clarId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeSubjectAction(Request $request, int $clarId)
    {
        /** @var Clarification $clarification */
        $clarification = $this->em->getReference(Clarification::class, $clarId);
        if (!$clarification) {
            throw new NotFoundHttpException(sprintf('Clarification with ID %i not found', $clarId));
        }

        $subject = $request->request->get('subject');
        list($cid, $probid) = explode('-', $subject);

        $contest = $this->em->getReference(Contest::class, $cid);
        $clarification->setContest($contest);

        if (ctype_digit($probid)) {
            $problem = $this->em->getReference(Problem::class, $probid);
            $clarification->setProblem($problem);
            $clarification->setCategory(null);
        } else {
            $clarification->setProblem(null);
            $clarification->setCategory($probid);
        }

        $this->em->flush();

        return $this->redirectToRoute('jury_clarification', ['id' => $clarId]);
    }

    /**
     * @Route("/{clarId}/change-queue", name="jury_clarification_change_queue", requirements={"clarId": "\d+"})
     * @param Request $request
     * @param int     $clarId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeQueueAction(Request $request, int $clarId)
    {
        /** @var Clarification $clarification */
        $clarification = $this->em->getReference(Clarification::class, $clarId);
        if (!$clarification) {
            throw new NotFoundHttpException(sprintf('Clarification with ID %i not found', $clarId));
        }

        $queue = $request->request->get('queue');
        if ($queue === "") {
            $queue = null;
        }
        $clarification->setQueue($queue);
        $this->em->flush();

        if ($request->isXmlHttpRequest()) {
            return $this->json(true);
        }
        return $this->redirectToRoute('jury_clarification', ['id' => $clarId]);
    }

    /**
     * @Route("/send", methods={"POST"}, name="teacher_clarification_send")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function sendAction(Request $request)
    {
        $currentUser = $this->dj->getUser();
        $clarification = new Clarification();

        if ($respid = $request->request->get('id')) {
            $respclar = $this->em->getRepository(Clarification::class)->find($respid);
            $clarification->setInReplyTo($respclar);
        }

        $userproblemid = $request->request->get('userproblemid');
        $userproblem = $this->em->getRepository(Userproblem::class)->find($userproblemid);

        // $clarification->setJuryMember($this->getUser()->getUsername());
        $clarification->setUserproblem($userproblem);
        $clarification->setClasses($respclar->getClasses());
        $clarification->setSender($currentUser->getUsername());
        $clarification->setReceiver($respclar->getSender());
        $clarification->setAnswered(true);
        $clarification->setBody($request->request->get('bodytext'));
        $clarification->setSubmittime(Utils::now());

        $this->em->persist($clarification);
        if ($respid) {
            $respclar->setAnswered(true);
            $this->em->persist($respclar);
        }
        $this->em->flush();

        $clarId = $clarification->getClarId();
        $this->dj->auditlog('clarification', $clarId, 'reply', null, null, null);
        $clarification = $this->em->getRepository(Clarification::class)->find($clarId);
        $this->em->flush();

        return $this->redirectToRoute('teacher_clarification', ['id' => $clarId]);
    }
}
