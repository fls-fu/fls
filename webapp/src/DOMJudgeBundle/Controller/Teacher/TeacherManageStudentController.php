<?php declare (strict_types = 1);

namespace DOMJudgeBundle\Controller\Teacher;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Userproblem;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Entity\Userip;
use DOMJudgeBundle\Entity\Configuration;
use DOMJudgeBundle\Entity\Leaderboard;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\ClassSchedule;
use DOMJudgeBundle\Entity\LabRoom;
use DOMJudgeBundle\Entity\Roomip;
use DOMJudgeBundle\Form\Fls\SubmitProblemType;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Service\ImportProblemService;
use DOMJudgeBundle\Service\SubmissionService;
use DOMJudgeBundle\Utils\FlsConstant;
use DOMJudgeBundle\Utils\Utils;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/teacher")
 * @Security("has_role('ROLE_TEACHER')")
 */
class TeacherManageStudentController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    private $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * @var SubmissionService
     */
    protected $submissionService;

    /**
     * @var ImportProblemService
     */
    protected $importProblemService;
 
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService,
        SubmissionService $submissionService,
        ImportProblemService $importProblemService,
        FormFactoryInterface $formFactory
    ) {
        $this->em = $em;
        $this->dj = $dj;
        $this->eventLogService = $eventLogService;
        $this->submissionService = $submissionService;
        $this->importProblemService = $importProblemService;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/student/{cid}", name="teacher_manage_student", requirements={"cid": "\d+"} )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function indexAction(Request $request,$cid)
    {
        $data = [];
        $tempStudent = [];
        $currentClass = $this->em->getRepository(Classes::class)->find($cid);
        $listStudent = $this->getListStudentProblem($currentClass);
        $userProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
            ->select('count(up.userproblemid)')
            ->where('up.cid = :cid')
            ->setParameter(':cid', $currentClass->getCid())
            ->getQuery()
            ->getOneOrNullResult();

        $langid = $currentClass->getlangid()->getLangid();
        $data['listStudent'] = json_encode($listStudent);
        if (!empty($userProblem) && $userProblem[1] > 0) {
            $data['moreProblem'] = true;
        } else {
            $data['moreProblem'] = false;
        }
        
        if(!empty($cid)){
            $data += ['cid' => $cid,'moreClass' => true];
            $data += ['classMenu' => $currentClass];
        }else{
            $data += ['moreClass' => false];
        }   
        return $this->render('@DOMJudge/Fls/Teacher/teacher_manage_student.html.twig', $data);
    }

    public function getListStudentProblem($class)
    {
        $data = [];
        $listUser = $this->em->getRepository(Leaderboard::class)->createQueryBuilder('lb')
            ->select('lb')
            ->where('lb.cid = :cid')
            ->setParameter(':cid', $class->getCid())
            ->orderBy('lb.pointsRestricted', 'DESC')
            ->getQuery()
            ->getResult();
        $listStudent = [];
        $teacher = $this->dj->getteacherByClassId($class);
        $requireLoc = $this->dj->getRequireLoc($class->getLangid()->getLangid());
        foreach ($listUser as $value) {
            if(!isset($teacher[0]) || $value->getUserid() != $teacher[0]['userid']){
                $student = $this->em->getRepository(User::class)->find($value->getUserid());
                if(empty($student)){
                    continue;
                }
                $temp['cid'] = $value->getCid();
                $temp['loc'] = $value->getPointsRestricted();
                $temp['requireLoc'] = $requireLoc;
                // $temp['time'] = $value->getTotaltimeRestricted();
                $temp['userid'] = $value->getUserid();
                $temp['name'] = $student->getName();
                $temp['status'] = ($value->getPointsRestricted() >= $requireLoc) ? 'Pass':'Not pass';

                // get data position
                $position = $this->em->getRepository(Userip::class)->createQueryBuilder('uip')
                    ->where('uip.userid = :userid')
                    ->andwhere('uip.cid = :cid')
                    ->setParameter(':userid', $value->getUserid())
                    ->setParameter(':cid', $class->getCid())
                    ->select('uip')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                // to do get (pc name)
                if(!empty($position)){
                    $roomip = $position->getRoomip();
                    $temp['position'] = $roomip->getPccode().' ('.$roomip->getRoomid()->getRoomname().')';
                    $temp['ipAddress'] = $roomip->getIpAddress();
                }else{
                    $temp['position'] = '';
                    $temp['ipAddress'] = '';
                }                
                // get data problem
                $totalProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
                    ->where('up.userid = :userid')
                    ->andwhere('up.cid = :cid')
                    ->setParameter(':userid', $value->getUserid())
                    ->setParameter(':cid', $class->getCid())
                    ->select('up')
                    ->getQuery()
                    ->getResult();
                // fill problem pass
                $pass = array_filter($totalProblem, function($v, $k) {
                        return $v->getStatus() == 4;
                    }, ARRAY_FILTER_USE_BOTH);
                // fill problem reject
                $reject = array_filter($totalProblem, function($v, $k) {
                    return $v->getStatus() == 5;
                    }, ARRAY_FILTER_USE_BOTH);
                // fill problem changed
                $changed = array_filter($totalProblem, function($v, $k) {
                    return $v->getChanged() == 2;
                }, ARRAY_FILTER_USE_BOTH);
                $temp += [
                    'total' => count($totalProblem),
                    'pass' => count($pass),
                    'reject' => count($reject),
                    'changed' => count($changed),
                    ];
                $listStudent[] = $temp;
                unset($temp);
            }

        }
        return $listStudent;
    }

    /**
     * @Route("/student/detail/{studentID}/{cid}", name="detail_student", requirements={"studentID": "^$|\d+"})
     * @Security("has_role('ROLE_TEACHER')")
     * @param string $studentID
     * @throws Exception
     */
    public function detailStudentClass(string $studentID, Request $request, $cid = null)
    {
        $classes = null;
        $student = $this->em->getRepository(User::class)->find($studentID);
        if(!empty($cid)){
            $data = ['cid' => $cid,'moreClass' => true];
            $classes = $this->em->getRepository(Classes::class)->find($cid);
            $data += ['classMenu' => $classes];
        }else{
            $data = ['moreClass' => false];
        }
        if(!empty($student) && !empty($classes)){
            $problems = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Problem', 'p')
                ->leftJoin(
                    'DOMJudgeBundle:Userproblem',
                    'up',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'p.probid = up.probid'
                )
                ->where('up.userid = :userid')
                ->setParameter(':userid', $student->getUserid())
                ->andwhere('up.cid = :Cid')
                ->setParameter(':Cid', $classes->getCid())
                ->select('p.probid as id,
                    p.shortname as name,
                    p.name as content,
                    p.points as point,
                    up.status as result,
                    up.changed as status')
                ->orderBy('p.points', 'ASC')
                ->getQuery()->getResult();
            $data['problems'] = json_encode($problems);
            $data['student'] = $student;
        }else{
                $data['error'] = 'Student is not found!';
        }
        return $this->render('@DOMJudge/Fls/Teacher/student_problems.html.twig', $data);
    }

    /**
     * @Route("/student/requirement", name="view_requirement_student")
     * @Security("has_role('ROLE_TEACHER')")
     * @throws Exception
     */
    public function viewRequirementStudent(Request $request)
    {
        $data = [];
        $dataRequest = json_decode($request->getContent());

        $cid = $dataRequest->cid;
        $classes = $this->em->getRepository(Classes::class)->find($cid);
        $probid = $dataRequest->probid;
        $problem = $this->em->getRepository(Problem::class)->find($probid);
        $studentid = $dataRequest->userid;
        $student = $this->em->getRepository(User::class)->find($studentid);
        if(!empty($student)){
            $data['student'] = $student;
        }
        if(isset($dataRequest->mode)){
            $mode = $dataRequest->mode;
            if($mode == 'edit'){
                $data['mode'] = false;
            }else if($mode == 'view'){
                $data['mode'] = true;
            }
        }
        $data['problem'] = $problem;
        $data['classes'] = $classes;

        // $conn = $this->em->getConnection();
        // $sql = "SELECT cid, probid, add_requirement FROM classproblem as cp where cid = :cid and probid = :probid LIMIT 1";

        // $statement = $conn->prepare($sql);
        // $statement->bindValue('cid', $cid);
        // $statement->bindValue('probid', $probid);
        // $statement->execute();
        // $classPro = $statement->fetchAll();
        $userProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
            ->where('up.userid = :userid')
            ->andwhere('up.cid = :cid')
            ->andwhere('up.probid = :probid')
            ->setParameter(':userid', $studentid)
            ->setParameter(':cid', $cid)
            ->setParameter(':probid', $probid)
            ->select('up')
            ->getQuery()
            ->getOneOrNullResult();

        if(!empty($userProblem)){
            $data['dataPro'] = $userProblem;
        }
        $data['route'] = 'add_requirement_student';
        $template = $this->render('@DOMJudge/Fls/Teacher/partials/modal_add_requirement.html.twig', $data)->getContent();

        $json = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/student/requirement/add", name="add_requirement_student")
     * @Security("has_role('ROLE_TEACHER')")
     * @throws Exception
     */
    public function addRequirementClass(Request $request)
    {
        $dataRequest = json_decode($request->getContent());

        $cid = $dataRequest->cid;
        $classes = $this->em->getRepository(Classes::class)->find($cid);
        $probid = $dataRequest->probid;
        $problem = $this->em->getRepository(Problem::class)->find($probid);
        $studentid = $dataRequest->userid;
        $student = $this->em->getRepository(User::class)->find($studentid);
        $textRequire = $dataRequest->textRequire;
        
        if( !empty($classes) && !empty($problem) && !empty($student)){
            $userProblem = $this->em->getRepository(Userproblem::class)->createQueryBuilder('up')
                ->where('up.userid = :userid')
                ->andwhere('up.cid = :cid')
                ->andwhere('up.probid = :probid')
                ->setParameter(':userid', $studentid)
                ->setParameter(':cid', $cid)
                ->setParameter(':probid', $probid)
                ->select('up')
                ->getQuery()
                ->getOneOrNullResult();
            $result = false;
            $mode = '';
            if($userProblem->getAddRequirement() != ''){
                $userProblem->setAddRequirement($textRequire);
                $this->em->persist($userProblem);
                $this->em->flush();
                $result = true;
                $mode = 'update';
            }else{
                $userProblem->setAddRequirement($textRequire);
                $this->em->persist($userProblem);
                $this->em->flush();
                $result = true;
                $mode = 'new';
            }
            return $this->json(['mode' => $mode,'result' => $result]);
        }else{
            return $this->json(['error' => 'class, problem or student not found!']);
        }
    }

    /**
     * @Route("/student/positions/add", name="add_positions")
     * @Security("has_role('ROLE_TEACHER')")
     * @throws Exception
     */
    public function addPosition(Request $request)
    {
        $fill = false;
        $data = [];
        $dataRequest = json_decode($request->getContent());
        $mode = 'view';
        $cid = $dataRequest->cid;
        $classes = $this->em->getRepository(Classes::class)->find($cid);
        if(isset($dataRequest->mode)){
            $mode = $dataRequest->mode;
        }
        $studentid = $dataRequest->userid;
        $student = $this->em->getRepository(User::class)->find($studentid);
        
        if( !empty($classes) && !empty($mode) && !empty($student)){
            $data['student'] = $student;
            $data['classes'] = $classes;
            if($mode == 'view'){
                $listRoom = $this->em->getRepository(LabRoom::class)->findAll();
                $classSchedule = $this->em->getRepository(ClassSchedule::class)->createQueryBuilder('cs')
                    ->where('cs.cid = :cid')
                    ->setParameter(':cid', $classes->getCid())
                    ->select('cs')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();                
                if(!empty($classSchedule)){
                    $roomid = $classSchedule->getRoomid();
                }else{
                    $roomid = reset($listRoom);                    
                }
                $data['currentRoom'] = $roomid;
                $listAllIp = $this->em->getRepository(Roomip::class)->createQueryBuilder('r')
                        ->where('r.roomid = :roomid')
                        ->setParameter(':roomid', $roomid)
                        ->select('r')
                        ->getQuery()
                        ->getResult();
                    $listIpset = $this->em->getRepository(Roomip::class)->createQueryBuilder('r')
                        ->join('DOMJudgeBundle:Userip','uip','WITH','uip.ipid = r.ipid')
                        ->where('r.roomid = :roomid')
                        ->setParameter(':roomid', $roomid)
                        ->andwhere('uip.cid = :cid')
                        ->setParameter(':cid', $classes->getCid())
                        ->select('r')
                        ->getQuery()
                        ->getResult();
                
                $listRoomip = array_filter($listAllIp, function($v) use($listIpset){
                    $result = array_search($v,$listIpset);
                    if($result === false){
                        return $v;
                    }
                });

                $data['listRoomip'] = $listRoomip;
                $data['listRoom'] = $listRoom;

                // get data position
                $position = $this->em->getRepository(Userip::class)->createQueryBuilder('uip')
                    ->where('uip.userid = :userid')
                    ->andwhere('uip.cid = :cid')
                    ->setParameter(':userid', $student->getUserid())
                    ->setParameter(':cid', $classes->getCid())
                    ->select('uip')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                
                // to do get (pc name)
                if(!empty($position)){
                    $roomip = $position->getRoomip();
                    $data['currentPos'] = $roomip;
                }
                $data['fill'] = $fill;         

                $template = $this->render('@DOMJudge/Fls/Teacher/partials/modal_roomip.html.twig', $data)->getContent();                
                $json = json_encode($template);
                $response = new Response($json, 200);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }else if($mode == 'filter'){
                $fill = true;
                if(isset($dataRequest->roomid)){
                    $roomid = $dataRequest->roomid;
                    $listAllIp = $this->em->getRepository(Roomip::class)->createQueryBuilder('r')
                        ->where('r.roomid = :roomid')
                        ->setParameter(':roomid', $roomid)
                        ->select('r')
                        ->getQuery()
                        ->getResult();
                    $listIpset = $this->em->getRepository(Roomip::class)->createQueryBuilder('r')
                        ->join('DOMJudgeBundle:Userip','uip','WITH','uip.ipid = r.ipid')
                        ->where('r.roomid = :roomid')
                        ->setParameter(':roomid', $roomid)
                        ->andwhere('uip.cid = :cid')
                        ->setParameter(':cid', $classes->getCid())
                        ->select('r')
                        ->getQuery()
                        ->getResult();
                    
                    $listRoomip = array_filter($listAllIp, function($v) use($listIpset){
                        $result = array_search($v,$listIpset);
                        if($result === false){
                            return $v;
                        }
                    });
                    $data['currentRoom'] = $listRoom = $this->em->getRepository(LabRoom::class)->find($roomid);
                    $listRoom = $this->em->getRepository(LabRoom::class)->findAll();
                    $data['listRoom'] = $listRoom;
                    if(!empty($listRoomip)){
                        $data['listRoomip'] = $listRoomip;
                        // get data position
                        $position = $this->em->getRepository(Userip::class)->createQueryBuilder('uip')
                            ->where('uip.userid = :userid')
                            ->andwhere('uip.cid = :cid')
                            ->setParameter(':userid', $student->getUserid())
                            ->setParameter(':cid', $classes->getCid())
                            ->select('uip')
                            ->setMaxResults(1)
                            ->getQuery()
                            ->getOneOrNullResult();
                        
                        // to do get (pc name)
                        if(!empty($position)){
                            $roomip = $position->getRoomip();
                            $data['currentPos'] = $roomip;
                        }
                        $data['fill'] = $fill;

                        $template = $this->render('@DOMJudge/Fls/Teacher/partials/modal_roomip.html.twig', $data)->getContent();                
                        $json = json_encode($template);
                        $response = new Response($json, 200);
                        $response->headers->set('Content-Type', 'application/json');

                        return $response;
                    }else{
                        $data['error'] = 'Positions of room is null!';
                    }
                }else{
                    $data['error'] = 'Room id not found!';
                }
                return $this->json($data);
            }else if($mode == 'save'){
                if(isset($dataRequest->roomid) && isset($dataRequest->ipid)){
                    $roomid = $dataRequest->roomid;
                    $ipid = $dataRequest->ipid;
                    $roomip = $this->em->getRepository(Roomip::class)->find($ipid);

                    $userip = $this->em->getRepository(Userip::class)->createQueryBuilder('uip')
                        ->where('uip.userid = :userid')
                        ->andwhere('uip.cid = :cid')
                        ->setParameter(':userid', $student->getUserid())
                        ->setParameter(':cid', $classes->getCid())
                        ->select('uip')
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult();

                    if(!empty($userip)){
                        $userip->setRoomip($roomip);
                        $time = Utils::now();
                        $userip->setLastupdate($time);
                    }else{
                        $userip = new Userip();
                        $userip->setUser($student);
                        $userip->setClasses($classes);
                        $userip->setRoomip($roomip);
                        $time = Utils::now();
                        $userip->setLastupdate($time);
                    }
                    $this->em->persist($userip);
                    $result = $this->em->flush();

                    return $this->json(['result' => 'success']);
                }else{
                    return $this->json(['error' => 'Room or positions not found!']);
                }
            }
        }else{
            return $this->json(['error' => 'class or student not found!']);
        }
        
    }

    /**
     * @Route("/random/{classID}", name="teacher_random_problem", requirements={"classID": "^$|\d+"})
     * @Security("has_role('ROLE_TEACHER')")
     * @param string $classID
     * @throws Exception
     */
    public function randomProblem(string $classID, Request $request)
    {
        try{
            $listData = $this->dj->genarateProblemForClass($classID);
        }catch(\Exception $e){
            $this->addFlash('danger',$e->getMessage());
            $response = $this->redirectToRoute('teacher_manage_student',['cid'=>$classID]);
            return $response;
        }        
        $class = $this->em->getRepository(Classes::class)->find($classID);

        // insert data to db
        foreach ($listData as $value) {
            $userProblem = new Userproblem();
            $userProblem->setUserid($value['student'])
                ->setCid($class)
                ->setProbid($value['problem'])
                ->setReviewStatus($value['review']);
            // if($change){
            //     $userProblem->setChanged(4);
            // }else{
                $userProblem->setChanged(0);
            // }
            $userProblem->setStatus(0);
            $this->em->persist($userProblem);
        }
        // push data
        $this->em->transactional(function () {
            $this->em->flush();
        });
        $response = $this->redirectToRoute('teacher_manage_student',['cid'=>$classID]);
        return $response;
    }
}