<?php declare (strict_types = 1);

namespace DOMJudgeBundle\Controller\Teacher;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\ClassSchedule;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\Submission;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Service\ImportProblemService;
use DOMJudgeBundle\Service\SubmissionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TeacherMiscController
 *
 * @Route("/teacher")
 *
 * @package DOMJudgeBundle\Controller\Teacher
 */
class TeacherController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    protected $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * @var SubmissionService
     */
    protected $submissionService;

    /**
     * @var ImportProblemService
     */
    protected $importProblemService;

    /**
     * GeneralInfoController constructor.
     * @param EntityManagerInterface $entityManager
     * @param DOMJudgeService        $dj
     */
    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService,
        SubmissionService $submissionService,
        ImportProblemService $importProblemService) {
        $this->em = $em;
        $this->dj = $dj;
        $this->eventLogService = $eventLogService;
        $this->submissionService = $submissionService;
        $this->importProblemService = $importProblemService;
    }

    /**
     * @Route("/control/{cid}", name="teacher_interface", requirements={"cid": "\d+"})
     * @Security("has_role('ROLE_TEACHER')")
     */
    public function indexAction(Request $request, $cid)
    {
        return $this->redirectToRoute('teacher_manage_student', ['cid' => $cid]);
        // $classes = $this->em->getRepository(Classes::class)->find($cid);
        // if ($classes) {
        //     $data['moreClass'] = true;
        //     $data['classMenu'] = $classes;
        // }
        // $data['cid'] = $classes->getCid();


        // return $this->render('@DOMJudge/Fls/Teacher/teacher_interface.html.twig', $data);
    }

    /**
     * @Route("/class/problems/{cid}", name="problem_class", requirements={"cid": "^$|\d+"})
     * @Security("has_role('ROLE_TEACHER')")
     * @throws Exception
     */

    public function problemClass(Request $request, $cid = null)
    {
        $classes = $this->em->getRepository(Classes::class)->find($cid);

        $data['cid'] = $cid;
        if ($classes) {
            $data['moreClass'] = true;
            $data['classMenu'] = $classes;
            $problems = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Userproblem', 'up')
                ->leftJoin(
                    'DOMJudgeBundle:Problem',
                    'p',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'p.probid = up.probid'
                )
                ->where('up.cid = :cid')
                ->setParameter(':cid', $cid)
                ->select('p.probid as probid,
                p.shortname as shortname,
                p.name as name')
                ->distinct()
                ->getQuery()
                ->getResult();

            $data['problems'] = json_encode($problems);
        } else {
            $data['moreClass'] = false;
            $this->addFlash('error', 'Class have id ' . $cid . ' do not exists!');
        }

        return $this->render('@DOMJudge/Fls/Teacher/classProblems.html.twig', $data);
    }
    
    /**
     * @Route("/problems/requirement", name="view_add_requirement_class")
     * @Security("has_role('ROLE_TEACHER')")
     * @throws Exception
     */
    public function viewRequirementClass(Request $request)
    {
        $dataRequest = json_decode($request->getContent());

        $cid = $dataRequest->cid;
        $classes = $this->em->getRepository(Classes::class)->find($cid);
        $probid = $dataRequest->probid;
        $problem = $this->em->getRepository(Problem::class)->find($probid);
        
        $data = [];

        $data['problem'] = $problem;
        $data['classes'] = $classes;

        $conn = $this->em->getConnection();
        $sql = "SELECT cid, probid, add_requirement as addRequirement FROM classproblem as cp where cid = :cid and probid = :probid LIMIT 1";

        $statement = $conn->prepare($sql);
        $statement->bindValue('cid', $cid);
        $statement->bindValue('probid', $probid);
        $statement->execute();
        $classPro = $statement->fetchAll();

        if(!empty($classPro)){
            $data['dataPro'] = $classPro[0];
        }
        $data['route'] = 'add_requirement_class';
        $template = $this->render('@DOMJudge/Fls/Teacher/partials/modal_add_requirement.html.twig', $data)->getContent();

        $json = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/problems/requirement/add", name="add_requirement_class")
     * @Security("has_role('ROLE_TEACHER')")
     * @throws Exception
     */
    public function addRequirementClass(Request $request)
    {
        $data = [];
        $dataRequest = json_decode($request->getContent());

        $cid = $dataRequest->cid;
        $classes = $this->em->getRepository(Classes::class)->find($cid);
        $probid = $dataRequest->probid;
        $problem = $this->em->getRepository(Problem::class)->find($probid);
        $textRequire = $dataRequest->textRequire;
        
        if( !empty($classes) && !empty($problem)){
            $conn = $this->em->getConnection();
            $sql = "SELECT cid, probid, add_requirement FROM classproblem as cp where cid = :cid and probid = :probid";
            $statement = $conn->prepare($sql);
            $statement->bindValue('cid', $cid);
            $statement->bindValue('probid', $probid);
            $statement->execute();
            $classPro = $statement->fetchAll();;
            unset($statement);

            $result = false;
            $mode = '';
            if(!empty($classPro)){
                $sqlInsert = "UPDATE classproblem SET add_requirement = :req WHERE cid = :cid and probid = :probid";
                $statement = $conn->prepare($sqlInsert);
                $statement->bindValue('cid', $cid);
                $statement->bindValue('probid', $probid);
                $statement->bindValue('req', $textRequire);
                $result = $statement->execute();
                $mode = 'update';
            }else{
                $sqlInsert = "INSERT INTO classproblem (cid, probid, add_requirement) VALUES (:cid,:probid,:req)";
                $statement = $conn->prepare($sqlInsert);
                $statement->bindValue('cid', $cid);
                $statement->bindValue('probid', $probid);
                $statement->bindValue('req', $textRequire);
                $result = $statement->execute();
                $mode = 'new';
            }
            return $this->json(['mode' => $mode,'result' => $result]);
        }else{
            return $this->json(['error' => 'class or problem not found']);
        }
    }

    /**
     * @Route("/class/schedule", name="class_schedule")
     * @Security("is_granted('ROLE_TEACHER')")
     * @param Request $request
   
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function ClassSchedule(Request $request)
    {
        $data = [];
        if($request->query->has('cid')){
            $cid = $request->query->get('cid');
        }else{
            $cid = null;
        }

        if(!empty($cid)){
            $data += ['cid' => $cid,'moreClass' => true];
            $classes = $this->em->getRepository(Classes::class)->find( $cid);
            $data += ['classMenu' => $classes];
        }else{
            $data['listSchedules'] = "[]";
            $data += ['moreClass' => false];
            return $this->render('@DOMJudge/Fls/Admin/class_schedule.html.twig', $data);
        } 
        
        $schedules = $this->em->getRepository(ClassSchedule::class)
            ->createQueryBuilder('s')
            ->select('s', 'c', 'r', 'sl')
            ->leftjoin('s.cid', 'c')
            ->leftjoin('s.roomid', 'r')
            ->leftjoin('s.slotid', 'sl')
            ->where('c.cid = :cid')
            ->setParameter("cid", $cid)
            ->orderBy('s.cheDate', 'asc')
            ->addOrderBy('s.slotid', 'asc')
            ->getQuery()->getResult();

        $listSchedules = [];
        foreach ($schedules as $s => $schedule) {
            $temp['cId'] = "";
            $temp['id'] = $schedule->getCheid();
            $temp['room'] = $schedule->getRoomid()->getRoomname();
            $temp['date'] = $schedule->getCheDate()->format('Y/m/d');;
            $temp['slot'] = $schedule->getSlotid()->getSlotid();
            $temp['detail'] = '('.$schedule->getSlotid()->getStartHour().':'.$schedule->getSlotid()->getStartMinute().
                        ' - '.$schedule->getSlotid()->getEndHour().':'.$schedule->getSlotid()->getEndMinute().')';
            $listSchedules[] = $temp;
            unset($temp);
        }
        $data['listSchedules'] = json_encode($listSchedules);
        return $this->render('@DOMJudge/Fls/Admin/class_schedule.html.twig', $data);
    }
}
