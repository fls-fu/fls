<?php

declare(strict_types=1);

namespace DOMJudgeBundle\Controller\Teacher;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Judging;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\Submission;
use DOMJudgeBundle\Entity\Team;
use DOMJudgeBundle\Entity\ClassSchedule;
use DOMJudgeBundle\Utils\Utils;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\ScoreboardService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class TeacherMiscController
 *
 * @Route("/teacher")
 *
 * @package DOMJudgeBundle\Controller\Teacher
 */
class TeacherMiscController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    protected $dj;

    /**
     * GeneralInfoController constructor.
     * @param EntityManagerInterface $entityManager
     * @param DOMJudgeService        $dj
     */
    public function __construct(EntityManagerInterface $entityManager, DOMJudgeService $dj)
    {
        $this->em = $entityManager;
        $this->dj = $dj;
    }

    /**
     * @Route("/{semesterId}", name="teacher_index", requirements={"semesterId": "^(\s*|\d+)$"})
     * @Security("has_role('ROLE_TEACHER')")
     */
    public function indexAction(Request $request, int $semesterId = null)
    {
        $data = [];
        $user = $this->dj->getUser();
        $currentSemester = $this->dj->getCurrentSemester();
        $currentTime = Utils::now();
        $data['showFilder'] = false;
        $data['semFilter'] = null;

        if ( $this->dj->checkrole('admin') ) {
            $semesters = $this->em->createQueryBuilder()
            ->from('DOMJudgeBundle:Semester', 's')
            ->select('s.semid', 's.name')
            ->orderBy('s.startedTime', 'DESC')
            ->getQuery()
            ->getResult();        

            if (count($semesters) > 0) {
                $fullSemester = array("semid" => 0, "name" => "All Semester");
                array_unshift($semesters , $fullSemester);
                $data['listSemester'] = $semesters;
                $data['showFilder'] = true;      
            }

            if ($currentSemester == null) {                
                if($semesterId === null) {
                    $semesterId = 0;
                }
            }else {
                $data['currentSemester'] = $currentSemester[0]->getName();
            }
        }else{
            if ($currentSemester == null) {
                $data['message'] = "No Semester started in this time!";
                return $this->render('@DOMJudge/Fls/Teacher/index.html.twig', $data);
            }else {
                $data['currentSemester'] = $currentSemester[0]->getName();
            }
        }

        // if ($currentSemester == null) {
        //     $data['message'] = "No Semester started in this time!";
        //     // $semesterId = 0;
        //     return $this->render('@DOMJudge/Fls/Teacher/index.html.twig', $data);
        // }else {
        //     // $data['currentSemester'] = $currentSemester[0]->getName();
        // }
        if ($this->dj->checkrole('admin')) {
            if($semesterId === null) {
                $classes = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Classes', 'c')
                ->leftJoin('c.semester', 's')
                ->select('c')
                ->andWhere('s.semid = :semid')
                ->setParameter('semid', $currentSemester[0]->getSemid())
                ->getQuery()
                ->getResult();

                $data['semFilter'] = $currentSemester[0]->getSemid();
            }else if($semesterId == 0) {
                $classes = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Classes', 'c')
                ->select('c')
                ->getQuery()
                ->getResult();

                $data['semFilter'] = 0;
            }else {
                $classes = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Classes', 'c')
                ->leftJoin('c.semester', 's')
                ->select('c')
                ->andWhere('s.semid = :semid')
                ->setParameter('semid', $semesterId)
                ->getQuery()
                ->getResult();

                $data['semFilter'] = $semesterId;
            }
            
        } else {
            $classes = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Classes', 'c')
                ->leftJoin('c.semester', 's')
                ->select('c')
                ->andWhere(':userid MEMBER OF c.userid')
                ->andWhere('s.semid = :semid')
                ->setParameter('userid', $user)
                ->setParameter('semid', $currentSemester[0]->getSemid())
                ->getQuery()
                ->getResult();
        }

        if (count($classes) > 0) {
            $list = [];
            foreach ($classes as $key => $c) {
                $class_show['id'] = $c->getCid();
                $class_show['name'] = $c->getName();
                $class_show['language'] = $c->getLangId()->getName();
                $class_show['semester'] = $c->getSemester()->getName();
                if ($c->getStarttime() == null && $c->getStarttimeString() == null) { //case that class just create
                    // $class_show['cStartTime'] = '-';
                    // $class_show['cEndTime'] = '-';
                    $class_show['cActive'] = false;
                } else {
                    if ($c->getStarttime() == null) { //case class in free time
                        $class_show['cStartTime'] = $c->getStarttimeString();
                        $class_show['cEndTime'] = $c->getEndtimeString();
                        $class_show['cActive'] = $c->getStarttimeEnabled();
                    } else { //case that class in schedule
                        $class_show['cStartTime'] = $c->getStarttime();
                        $class_show['cEndTime'] = $c->getEndtime();
                        if ($class_show['cStartTime'] <= $currentTime && $currentTime <= $class_show['cEndTime']) {
                            $class_show['cActive'] = true;
                        } else {
                            $class_show['cActive'] = false;
                        }
                    }
                }
                // if class is inactive display next slot
                if ($class_show['cActive'] == false) {
                    $next_slot = $this->getNextSlot($c->getCid());
                    if ($next_slot != null) {
                        $class_show['cStartTime'] = $next_slot['starttime'];
                        $class_show['cEndTime'] = $next_slot['endtime'];
                    }else {
                        $class_show['cStartTime'] = '-';
                        $class_show['cEndTime'] = '-';
                    }
                }

                $list[] = $class_show;
            }
            $data['listClass'] = json_encode($list);
        }

        if (count($classes) > 0) {
            $data['message'] = "Please select a class to manage.";
        } else {
            if ($this->dj->checkrole('admin')) {
                $data['message'] = "There are not any class in this Semester!";
            } else {
                $data['message'] = "You have not assigned to any Class in this Semester!";
            }
        }
        return $this->render('@DOMJudge/Fls/Teacher/index.html.twig', $data);
    }

    /**
     * @Route("", methods={"GET"}, name="teacher_exit_class")
     * @Security("has_role('ROLE_TEACHER')")
     * @param string $className
     * @throws Exception
     */
    public function exitClass(Request $request)
    {
        $response = $this->redirectToRoute('teacher_index');

        $this->dj->clearCookie('classes_id', null, '', false, false, $response);
        return $response;
    }

    /**
     * @Route("/class/{className}", methods={"GET"}, name="teacher_selected_class", requirements={"className": "^$|\d+"})
     * @Security("has_role('ROLE_TEACHER')")
     * @param string $className
     * @throws Exception
     */
    public function setSelectedClass(string $className, Request $request)
    {
        $classes = $this->em->getRepository(Classes::class)->find($className);
        $response = $this->redirectToRoute('teacher_interface', ['cid' => $classes->getCid()]);
        return $response;
    }

    /**
     * @Route("/updates", methods={"GET"}, name="teacher_ajax_updates")
     * @Security("has_role('ROLE_SADMIN') or has_role('ROLE_TEACHER')")
     */
    public function updatesAction(Request $request)
    {
        return $this->json($this->dj->getUpdates());
    }

    /**
     * @Route("/ajax/{datatype}", methods={"GET"}, name="jury_ajax_data")
     * @param string $datatype
     * @Security("has_role('ROLE_TEACHER')")
     */
    public function ajaxDataAction(Request $request, string $datatype)
    {
        $q  = $request->query->get('q');
        $qb = $this->em->createQueryBuilder();

        if ($datatype === 'problems') {
            $problems = $qb->from('DOMJudgeBundle:Problem', 'p')
                ->select('p.probid', 'p.name')
                ->where($qb->expr()->like('p.name', '?1'))
                ->orWhere($qb->expr()->eq('p.probid', '?2'))
                ->orderBy('p.name', 'ASC')
                ->getQuery()->setParameter(1, '%' . $q . '%')
                ->setParameter(2, $q)
                ->getResult();

            $results = array_map(function (array $problem) {
                $displayname = $problem['name'] . " (p" . $problem['probid'] . ")";
                return [
                    'id' => $problem['probid'],
                    'text' => $displayname,
                ];
            }, $problems);
        } elseif ($datatype === 'users') {
            $users = $qb->from('DOMJudgeBundle:User', 'u')
                ->select('u.userid', 'u.name')
                ->where($qb->expr()->like('u.name', '?1'))
                ->orWhere($qb->expr()->eq('u.userid', '?2'))
                ->orderBy('u.name', 'ASC')
                ->getQuery()->setParameter(1, '%' . $q . '%')
                ->setParameter(2, $q)
                ->getResult();

            $results = array_map(function (array $users) {
                $displayname = $users['name'] . " (u" . $users['userid'] . ")";
                return [
                    'id' => $users['userid'],
                    'text' => $displayname,
                ];
            }, $users);
        } else {
            throw new NotFoundHttpException("Unknown AJAX data type: " . $datatype);
        }

        return $this->json(['results' => $results]);
    }

    /**
     * @Route("/refresh-cache", name="jury_refresh_cache")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request           $request
     * @param ScoreboardService $scoreboardService
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function refreshCacheAction(Request $request, ScoreboardService $scoreboardService)
    {
        // Note: we use a XMLHttpRequest here as Symfony does not support streaming Twig outpit

        $contests = $this->dj->getCurrentContests();
        if ($cid = $request->request->get('cid')) {
            if (!isset($contests[$cid])) {
                throw new BadRequestHttpException(sprintf('Contest %s not found', $cid));
            }
            $contests = [$cid => $contests[$cid]];
        } elseif ($request->cookies->has('domjudge_cid') && ($contest = $this->dj->getCurrentContest())) {
            $contests = [$contest->getCid() => $contest];
        }

        if ($request->isXmlHttpRequest() && $request->isMethod('POST')) {
            $progressReporter = function (string $data) {
                echo $data;
                ob_flush();
                flush();
            };
            $response         = new StreamedResponse();
            $response->headers->set('X-Accel-Buffering', 'no');
            $response->setCallback(function () use ($contests, $progressReporter, $scoreboardService) {
                $timeStart = microtime(true);

                $this->dj->auditlog('scoreboard', null, 'refresh cache');

                foreach ($contests as $contest) {
                    $queryBuilder = $this->em->createQueryBuilder()
                        ->from('DOMJudgeBundle:Team', 't')
                        ->select('t')
                        ->orderBy('t.teamid');
                    if (!$contest->getPublic()) {
                        $queryBuilder
                            ->join('t.contests', 'c')
                            ->andWhere('c.cid = :cid')
                            ->setParameter(':cid', $contest->getCid());
                    }
                    /** @var Team[] $teams */
                    $teams = $queryBuilder->getQuery()->getResult();
                    /** @var Problem[] $problems */
                    $problems = $this->em->createQueryBuilder()
                        ->from('DOMJudgeBundle:Problem', 'p')
                        ->join('p.contest_problems', 'cp')
                        ->select('p')
                        ->andWhere('cp.contest = :contest')
                        ->setParameter(':contest', $contest)
                        ->orderBy('cp.shortname')
                        ->getQuery()
                        ->getResult();

                    $message = sprintf(
                        '<p>Recalculating all values for the scoreboard cache for contest %d (%d teams, %d problems)...</p>',
                        $contest->getCid(),
                        count($teams),
                        count($problems)
                    );
                    $progressReporter($message);
                    $progressReporter('<pre>');

                    if (count($teams) == 0) {
                        $progressReporter('No teams defined, doing nothing.</pre>');
                        return;
                    }
                    if (count($problems) == 0) {
                        $progressReporter('No problems defined, doing nothing.</pre>');
                        return;
                    }

                    // for each team, fetch the status of each problem
                    foreach ($teams as $team) {
                        $progressReporter(sprintf('Team %d:', $team->getTeamid()));

                        // for each problem fetch the result
                        foreach ($problems as $problem) {
                            $progressReporter(sprintf(' p%d', $problem->getProbid()));
                            $scoreboardService->calculateScoreRow($contest, $team, $problem, false);
                        }

                        $progressReporter(" rankcache\n");
                        $scoreboardService->updateRankCache($contest, $team);
                    }

                    $progressReporter('</pre>');

                    $progressReporter('<p>Deleting irrelevant data...</p>');

                    // Drop all teams and problems that do not exist in the contest
                    if (!empty($problems)) {
                        $problemIds = array_map(function (Problem $problem) {
                            return $problem->getProbid();
                        }, $problems);
                    } else {
                        // problemId -1 will never happen, but otherwise the array is empty and that is not supported
                        $problemIds = [-1];
                    }

                    if (!empty($teams)) {
                        $teamIds = array_map(function (Team $team) {
                            return $team->getTeamid();
                        }, $teams);
                    } else {
                        // teamId -1 will never happen, but otherwise the array is empty and that is not supported
                        $teamIds = [-1];
                    }

                    $params = [
                        ':cid' => $contest->getCid(),
                        ':problemIds' => $problemIds,
                    ];
                    $types  = [
                        ':problemIds' => Connection::PARAM_INT_ARRAY,
                        ':teamIds' => Connection::PARAM_INT_ARRAY,
                    ];
                    $this->em->getConnection()->executeQuery(
                        'DELETE FROM scorecache WHERE cid = :cid AND probid NOT IN (:problemIds)',
                        $params,
                        $types
                    );

                    $params = [
                        ':cid' => $contest->getCid(),
                        ':teamIds' => $teamIds,
                    ];
                    $this->em->getConnection()->executeQuery(
                        'DELETE FROM scorecache WHERE cid = :cid AND teamid NOT IN (:teamIds)',
                        $params,
                        $types
                    );
                    $this->em->getConnection()->executeQuery(
                        'DELETE FROM rankcache WHERE cid = :cid AND teamid NOT IN (:teamIds)',
                        $params,
                        $types
                    );
                }

                $timeEnd = microtime(true);

                $progressReporter(sprintf(
                    '<p>Scoreboard cache refresh completed in %.2lf seconds.</p>',
                    $timeEnd - $timeStart
                ));
            });
            return $response;
        }

        return $this->render('@DOMJudge/jury/refresh_cache.html.twig', [
            'contests' => $contests,
            'contest' => count($contests) === 1 ? reset($contests) : null,
            'doRefresh' => $request->request->has('refresh'),
        ]);
    }

    /**
     * @Route("/judging-verifier", name="jury_judging_verifier")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function judgingVerifierAction(Request $request)
    {
        /** @var Submission[] $submissions */
        $submissions = [];
        if ($contests = $this->dj->getCurrentContests()) {
            $submissions = $this->em->createQueryBuilder()
                ->from('DOMJudgeBundle:Submission', 's')
                ->join('s.judgings', 'j', Join::WITH, 'j.valid = 1')
                ->select('s', 'j')
                ->andWhere('s.contest IN (:contests)')
                ->andWhere('j.result IS NOT NULL')
                ->setParameter(':contests', $contests)
                ->getQuery()
                ->getResult();
        }

        $numChecked   = 0;
        $numUnchecked = 0;

        $unexpected = [];
        $multiple   = [];
        $verified   = [];
        $nomatch    = [];
        $earlier    = [];

        $verifier = 'auto-verifier';

        $verifyMultiple = (bool) $request->get('verify_multiple', false);

        foreach ($submissions as $submission) {
            // As we only load the needed judging, this will automatically be the first one
            /** @var Judging $judging */
            $judging         = $submission->getJudgings()->first();
            $expectedResults = $submission->getExpectedResults();
            $submissionLink  = $this->generateUrl('jury_submission', ['submitId' => $submission->getSubmitid()]);
            $submissionId    = sprintf('s%d', $submission->getSubmitid());

            if (!empty($expectedResults) && !$judging->getVerified()) {
                $numChecked++;
                $result = mb_strtoupper($judging->getResult());
                if (!in_array($result, $expectedResults)) {
                    $unexpected[] = sprintf(
                        "<a href='%s'>%s</a> has unexpected result '%s', should be one of: %s",
                        $submissionLink,
                        $submissionId,
                        $result,
                        implode(', ', $expectedResults)
                    );
                } elseif (count($expectedResults) > 1) {
                    if ($verifyMultiple) {
                        // Judging result is as expected, set judging to verified
                        $judging
                            ->setVerified(true)
                            ->setJuryMember($verifier);
                        $multiple[] = sprintf(
                            "<a href='%s'>%s</a> verified as %s out of multiple possible outcomes (%s)",
                            $submissionLink,
                            $submissionId,
                            $result,
                            implode(', ', $expectedResults)
                        );
                    } else {
                        $multiple[] = sprintf(
                            "<a href='%s'>%s</a> is judged as %s but has multiple possible outcomes (%s)",
                            $submissionLink,
                            $submissionId,
                            $result,
                            implode(', ', $expectedResults)
                        );
                    }
                } else {
                    // Judging result is as expected, set judging to verified
                    $judging
                        ->setVerified(true)
                        ->setJuryMember($verifier);
                    $verified[] = sprintf(
                        "<a href='%s'>%s</a> verified as '%s'",
                        $submissionLink,
                        $submissionId,
                        $result
                    );
                }
            } else {
                $numUnchecked++;

                if (empty($expectedResults)) {
                    $nomatch[] = sprintf(
                        "expected results unknown in <a href='%s'>%s</a>, leaving submission unchecked",
                        $submissionLink,
                        $submissionId
                    );
                } else {
                    $earlier[] = sprintf(
                        "<a href='%s'>%s</a> already verified earlier",
                        $submissionLink,
                        $submissionId
                    );
                }
            }
        }

        $this->em->flush();

        return $this->render('@DOMJudge/jury/check_judgings.html.twig', [
            'numChecked' => $numChecked,
            'numUnchecked' => $numUnchecked,
            'unexpected' => $unexpected,
            'multiple' => $multiple,
            'verified' => $verified,
            'nomatch' => $nomatch,
            'earlier' => $earlier,
            'verifyMultiple' => $verifyMultiple,
        ]);
    }

    /**
     * @Route("/change-contest/{contestId}", name="jury_change_contest", requirements={"contestId": "-?\d+"})
     * @param Request         $request
     * @param RouterInterface $router
     * @param int             $contestId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeContestAction(Request $request, RouterInterface $router, int $contestId)
    {
        if ($this->isLocalReferrer($router, $request)) {
            $response = new RedirectResponse($request->headers->get('referer'));
        } else {
            $response = $this->redirectToRoute('jury_index');
        }
        return $this->dj->setCookie(
            'domjudge_cid',
            (string) $contestId,
            0,
            null,
            '',
            false,
            false,
            $response
        );
    }

    /**
     * @Route("/insert/starttime/", name="starttime")
     * @Security("has_role('ROLE_TEACHER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function InsertStartTime(Request $request)
    {
        $timeFormat  = (string) $this->dj->dbconfig_get('time_format', '%H:%M');
        $dateTimeFormat = "%Y-%m-%d";
        $data = json_decode($request->getContent());
        $start_time = Utils::now();
        $curentDate = Utils::printtime($start_time, $dateTimeFormat);
        $start_time_string = Utils::printtime($start_time, $timeFormat);
        $class = $this->em->getRepository(Classes::class)->Find($data->cid);
        // $class->setStarttime($start_time); //make sure the removeGroup method is defined in your User model. 
        // $class->setStarttimeString($start_time_string);
        // $class->setStarttimeEnabled(true);
        // $this->em->persist($class);
        // $this->em->flush(); //only call this after you've made all your data modifications

        $class_schedule = $this->em->getRepository(ClassSchedule::class)
            ->createQueryBuilder('cu')
            ->select('cu', 'c', 'tl')
            ->innerJoin('cu.cid', 'c')
            ->Join('cu.slotid', 'tl')
            ->where('c.cid = :cid')
            ->andwhere('cu.cheDate = :cheDate')
            ->setParameter("cid", $data->cid)
            ->setParameter("cheDate", $curentDate)
            ->addOrderBy('cu.slotid', 'ASC')
            ->getQuery()->getResult();
        // $start_time = Utils::printtime($start_time, $timeFormat);
        $in_slot = false;
        $end_time_string = "freetime";
        foreach ($class_schedule as $cs) {
            $time_slot_start  = strval($cs->getSlotid()->getStartHour() . ':' . $cs->getSlotid()->getStartMinute());
            $time_slot_end  = strval($cs->getSlotid()->getEndHour() . ':' . $cs->getSlotid()->getEndMinute());
            if (
                date('H:i', strtotime($start_time_string)) >= date('H:i', strtotime($time_slot_start))
                && date('H:i', strtotime($start_time_string)) <= date('H:i', strtotime($time_slot_end))
            ) {
                $now = date("Y-m-d") . " " . $time_slot_end;
                $end_time = date("U.u", strtotime($now));
                $end_time_string = Utils::printtime($end_time, $timeFormat);
                $in_slot = true;
                break;
            }
        }


        if ($in_slot) {
            $class->setStarttime($start_time);
            $class->setEndtime($end_time);
            $class->setStarttimeString(null);
            $class->setEndtimeString(null);
            $class->setStarttimeEnabled(false);
            $this->em->persist($class);
            $this->em->flush(); //only call this after you've made all your data modifications  
            return $this->json([
                'starttime' => $start_time,
                'freetime' => '-',
                'endtime' => $end_time,
                'active' => true
            ]);
        } else {
            $class->setStarttime(null);
            $class->setEndtime(null);
            $class->setStarttimeString($start_time);
            $class->setEndtimeString('freetime');
            $class->setStarttimeEnabled(true);
            $this->em->persist($class);
            $this->em->flush(); //only call this after you've made all your data modifications  
            return $this->json([
                'starttime' => $start_time,
                'freetime' => '-',
                'endtime' => 'freetime',
                'active' => true
            ]);
        }
    }
    /**
     * @Route("/insert/stoptime/", name="stoptime")
     * @Security("has_role('ROLE_TEACHER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function InsertStopTime(Request $request)
    {
        $timeFormat  = (string) $this->dj->dbconfig_get('time_format', '%H:%M');
        // $classID = $this->dj->getCurrentClass() -> getCid();
        $data = json_decode($request->getContent());
        $end_time = Utils::now();
        $end_time_string = Utils::printtime($end_time, $timeFormat);
        $class = $this->em->getRepository(Classes::class)->Find($data->cid);
        if ($class->getStarttime() != null && $class->getEndtime() != null) {
            if ($end_time >= $class->getStarttime() && $end_time <= $class->getEndtime()) {
                $class->setEndtime($end_time);
            }
        } else {
            if ($class->getStarttimeString() != null && $class->getStarttimeEnabled() == true) {
                $class->setStarttimeEnabled(false);
            }
        }
        // $class->setStarttimeEnabled(false);
        // $class->setEndtime($end_time); //make sure the removeGroup method is defined in your User model. 
        // $class->setEndtimeString($end_time_string);
        $this->em->persist($class);
        $this->em->flush(); //only call this after you've made all your data modifications    
        $next_slot = $this->getNextSlot($class->getCid());
        if ($next_slot != null) {
            return $this->json([
                'starttime' => $next_slot['starttime'],
                'endtime' => $next_slot['endtime'],
                'active' => false
            ]);
        } else {
            return $this->json([
                'starttime' => '-',
                'endtime' => '-',
                'active' => false
            ]);
        }
    }

    public function getNextSlot($classId)
    {
        $dateTimeFormat = "%Y-%m-%d";
        $timeFormat = "%H:%M";
        $current_time = Utils::now();
        $curentDate = Utils::printtime($current_time, $dateTimeFormat);
        $currentTime = Utils::printtime($current_time, $timeFormat);
        $next_slot = null;
        $td_class_schedule = $this->em->getRepository(ClassSchedule::class)
            ->createQueryBuilder('cu')
            ->select('cu', 'c', 'tl')
            ->innerJoin('cu.cid', 'c')
            ->Join('cu.slotid', 'tl')
            ->where('c.cid = :cid')
            ->andwhere('cu.cheDate = :cheDate')
            ->setParameter("cid", $classId)
            ->setParameter("cheDate", $curentDate)
            ->orderBy('cu.slotid', 'ASC')
            ->getQuery()->getResult();
        foreach ($td_class_schedule as $cs) {
            $time_slot_start  = strval($cs->getSlotid()->getStartHour() . ':' . $cs->getSlotid()->getStartMinute());
            $time_slot_end  = strval($cs->getSlotid()->getEndHour() . ':' . $cs->getSlotid()->getEndMinute());
            if (
                // date('H:i', strtotime($currentTime)) <= date('H:i', strtotime($time_slot_start)) &&
                date('H:i', strtotime($currentTime)) <= date('H:i', strtotime($time_slot_end))
            ) {
                $slot_end = date("Y-m-d") . " " . $time_slot_end;
                $end_time = date("U.u", strtotime($slot_end));
                $slot_start = date("Y-m-d") . " " . $time_slot_start;
                $start_time = date("U.u", strtotime($slot_start));
                $next_slot['starttime'] = $start_time;
                $next_slot['endtime'] = $end_time;
                break;
            }
        }
        if ($next_slot == null) {
            $nd_class_schedule = $this->em->getRepository(ClassSchedule::class)
                ->createQueryBuilder('cu')
                ->select('cu', 'c', 'tl')
                ->innerJoin('cu.cid', 'c')
                ->Join('cu.slotid', 'tl')
                ->where('c.cid = :cid')
                ->andwhere('cu.cheDate > :cheDate')
                ->setParameter("cid", $classId)
                ->setParameter("cheDate", $curentDate)
                ->orderBy('cu.cheDate', 'ASC')
                ->addOrderBy('cu.slotid', 'ASC')
                ->getQuery()->getResult();

            foreach ($nd_class_schedule as $cs) {
                $time_slot_start  = strval($cs->getSlotid()->getStartHour() . ':' . $cs->getSlotid()->getStartMinute());
                $time_slot_end  = strval($cs->getSlotid()->getEndHour() . ':' . $cs->getSlotid()->getEndMinute());
                $csDate = $cs->getCheDate()->format('Y-m-d');
                $slot_end = $csDate . " " . $time_slot_end;
                $end_time = date("U.u", strtotime($slot_end));
                $slot_start = $csDate . " " . $time_slot_start;
                $start_time = date("U.u", strtotime($slot_start));
                $next_slot['starttime'] = $start_time;
                $next_slot['endtime'] = $end_time;
                break;
            }
        }

        return $next_slot;
    }
}
