<?php

declare(strict_types=1);

namespace DOMJudgeBundle\Controller\Superadmin;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\Expr\Join;
use DOMJudgeBundle\Controller\BaseController;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\Problem;
use DOMJudgeBundle\Entity\Submission;
use DOMJudgeBundle\Form\Fls\SubmitProblemType;
use DOMJudgeBundle\Service\DOMJudgeService;
use DOMJudgeBundle\Service\EventLogService;
use DOMJudgeBundle\Service\ImportProblemService;
use DOMJudgeBundle\Service\SubmissionService;
use DOMJudgeBundle\Service\CheckConfigService;
use DOMJudgeBundle\Utils\FlsConstant;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sadmin")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class SadminController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DOMJudgeService
     */
    private $dj;

    /**
     * @var EventLogService
     */
    protected $eventLogService;

    /**
     * @var SubmissionService
     */
    protected $submissionService;

    /**
     * @var ImportProblemService
     */
    protected $importProblemService;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var CheckConfigService
     */
    protected $CheckConfigService;

    public function __construct(
        EntityManagerInterface $em,
        DOMJudgeService $dj,
        EventLogService $eventLogService,
        SubmissionService $submissionService,
        ImportProblemService $importProblemService,
        FormFactoryInterface $formFactory,
        CheckConfigService $checkConfigService
    ) {
        $this->em = $em;
        $this->dj = $dj;
        $this->eventLogService = $eventLogService;
        $this->submissionService = $submissionService;
        $this->importProblemService = $importProblemService;
        $this->formFactory = $formFactory;
        $this->checkConfigService = $checkConfigService;
    }

    /**
     * @Route("", name="sadmin_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function indexAction(Request $request)
    {

        // return $this->render('@DOMJudge/Fls/Superadmin/home.html.twig');
        return $this->render('@DOMJudge/Fls/Sadmin/home.html.twig');
    }

    /**
     * @Route("/configcheck", name="sadmin_config_check")
     */
    public function checkAction(Request $request)
    {
        $results = $this->checkConfigService->runAll();
        return $this->render('@DOMJudge/Fls/Sadmin/config_check.html.twig', [
            'results' => $results
        ]);
    }

    /**
     * @Route("/analysis", name="sadmin_analysis")
     */
    public function analysis(Request $request)
    {
        // $this->clean_folder('/home/luong/fls/output/testclean/');
        $data['dbname'] = $this->em->getConnection()->getDatabase();
        $conn = $this->em->getConnection();
        $sql = "SELECT table_schema AS 'dbname', ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) AS 'size' FROM information_schema.tables WHERE table_schema = :dbname GROUP BY table_schema";
        $statement = $conn->prepare($sql);
        $statement->bindValue('dbname', $data['dbname']);
        $statement->execute();
        $dbSize = $statement->fetchAll();
        $data['dbsize'] = $dbSize[0]['size'];

        //dir
        $submit_dir = $this->getParameter('domjudge.submitdir');
        $judging_dir = substr($this->getParameter('domjudge.submitdir'), 0, -11).'judgings';

        // var_dump($judging_dir);
        //total stogare of server        
        $data['total_disk'] = ceil(disk_total_space("/") / 1024 / 1024);
        //total free space in GB
        $data['free_disk'] = ceil(disk_free_space("/") / 1024 / 1024);

        //directory size in MB
        $data['submit_dir_size'] = ceil($this->folderSize($submit_dir) / 1024 / 1024);
        $data['judging_dir_size'] = ceil($this->folderSize($judging_dir) / 1024 / 1024);
        //other use include database
        $data['other_used'] = ceil($data['total_disk'] - ($data['free_disk'] + $data['submit_dir_size'] + $data['judging_dir_size']));
        //other use wuthout database
        $data['other_used_db'] = ceil($data['total_disk'] - ($dbSize[0]['size'] + $data['free_disk']));
        // var_dump($data);

        //storage chart data
        $charts = [];
        $charts['labels'] = ['Submission Directory', 'Judgings Directory', 'Other used', 'Free Disk' ];
        $charts['data'] = [$data['submit_dir_size'], $data['judging_dir_size'], $data['other_used'], $data['free_disk']];
        $charts['color'] = ['Yellow', 'Blue','Orange', 'Green'];
        $data['charts'] = json_encode($charts);

        //Database chart
        $db_chart = [];
        $db_chart['labels'] = [$data['dbname'], 'Other used', 'Free Disk'];
        $db_chart['data'] = [$data['dbsize'], $data['other_used_db'], $data['free_disk']];
        $db_chart['color'] = ['Yellow', 'Orange', 'Green'];
        $data['dbcharts'] = json_encode($db_chart);
        // var_dump($data);
        return $this->render('@DOMJudge/Fls/Sadmin/analysis.html.twig', $data);
    }
    function folderSize($dir)
    {
        $size = 0;
        foreach (glob(rtrim($dir, '/') . '/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : $this->folderSize($each);
        }
        return $size;
    }

    function clean_folder($target) {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
    
            foreach( $files as $file ){
                $this->clean_folder( $file );      
            }
        } elseif(is_file($target)) {
            unlink( $target );  
        }
    }

    function delete_directory($dirname) {
        if (is_dir($dirname))
          $dir_handle = opendir($dirname);
    if (!$dir_handle)
         return false;
    while($file = readdir($dir_handle)) {
          if ($file != "." && $file != "..") {
               if (!is_dir($dirname."/".$file))
                    unlink($dirname."/".$file);
               else
                    $this->delete_directory($dirname.'/'.$file);
          }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}
}
