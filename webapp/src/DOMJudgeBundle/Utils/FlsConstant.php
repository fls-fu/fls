<?php declare (strict_types = 1);

namespace DOMJudgeBundle\Utils;

class FlsConstant
{
    const ProblemStatus = [
        '0' => 'open',
        '1' => 'Doing',
        '2' => 'submitted',
        '3' => 'Testing done',
        '4' => 'Pass',
        '5' => 'Reject',
    ];

    const ChangeStatus = [
        '1' => 'Requesting',
        '2' => 'Changed',
        '3' => 'Rejected',
        '4' => 'New',
    ];

    const SubmittedStatus = [
        'correct' => 'Correct',
        'compiler-error' => 'Compiler-error',
        'no-output' => 'No-output',
        'output-limit' => 'Output-limit',
        'run-error' => 'Run-error',
        'timelimit' => 'Timelimit',
        'wrong-answer' => 'Wrong-answer',
        'pending' => 'Pending',
        'judging' => 'Judging',
    ];

    const status = [
        '0'=> 'Not pass',
        '1'=> 'Pass',
    ];

    const KeyLangConfig = '_Loc';

    // const KeyProblemLevelConfig = [
    //     'hard' => '-hard',
    //     'medium' => '-medium',
    //     'easy' => '-easy',
    // ];

    // const KeyProblemLevel = [
    //     1 => 'hard',
    //     2 => 'medium',
    //     3 => 'easy',
    // ];

}
