<?php

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TimeSlot
 *
 * @ORM\Table(name="time_slot", indexes={@ORM\Index(name="slotid", columns={"slotid"})})
 * @ORM\Entity
 */
class TimeSlot
{
    /**
     * @var integer
     *
     * @ORM\Column(name="slotid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $slotid;

    /**
     * @var integer
     *
     * @ORM\Column(name="start_hour", type="integer", nullable=false)
     */
    private $startHour;

    /**
     * @var integer
     *
     * @ORM\Column(name="start_minute", type="integer", nullable=false)
     */
    private $startMinute;

    /**
     * @var integer
     *
     * @ORM\Column(name="end_hour", type="integer", nullable=false)
     */
    private $endHour;

    /**
     * @var integer
     *
     * @ORM\Column(name="end_minute", type="integer", nullable=false)
     */
    private $endMinute;



    /**
     * Get slotid
     *
     * @return integer
     */
    public function getSlotid()
    {
        return $this->slotid;
    }

    /**
     * Set startHour
     *
     * @param integer $startHour
     *
     * @return TimeSlot
     */
    public function setStartHour($startHour)
    {
        $this->startHour = $startHour;

        return $this;
    }

    /**
     * Get startHour
     *
     * @return integer
     */
    public function getStartHour()
    {
        return $this->startHour;
    }

    /**
     * Set startMinute
     *
     * @param integer $startMinute
     *
     * @return TimeSlot
     */
    public function setStartMinute($startMinute)
    {
        $this->startMinute = $startMinute;

        return $this;
    }

    /**
     * Get startMinute
     *
     * @return integer
     */
    public function getStartMinute()
    {
        return $this->startMinute;
    }

    /**
     * Set endHour
     *
     * @param integer $endHour
     *
     * @return TimeSlot
     */
    public function setEndHour($endHour)
    {
        $this->endHour = $endHour;

        return $this;
    }

    /**
     * Get endHour
     *
     * @return integer
     */
    public function getEndHour()
    {
        return $this->endHour;
    }

    /**
     * Set endMinute
     *
     * @param integer $endMinute
     *
     * @return TimeSlot
     */
    public function setEndMinute($endMinute)
    {
        $this->endMinute = $endMinute;

        return $this;
    }

    /**
     * Get endMinute
     *
     * @return integer
     */
    public function getEndMinute()
    {
        return $this->endMinute;
    }
}
