<?php declare(strict_types=1);

namespace DOMJudgeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DOMJudgeBundle\Utils\Utils;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Submission
 *
 * @ORM\Table(name="submission", uniqueConstraints={@ORM\UniqueConstraint(name="externalid", columns={"cid", "externalid"})}, indexes={@ORM\Index(name="userid", columns={"cid", "userid"}), @ORM\Index(name="judgehost", columns={"cid", "judgehost"}), @ORM\Index(name="userid_2", columns={"userid"}), @ORM\Index(name="probid", columns={"probid"}), @ORM\Index(name="langid", columns={"langid"}), @ORM\Index(name="judgehost_2", columns={"judgehost"}), @ORM\Index(name="origsubmitid", columns={"origsubmitid"}), @ORM\Index(name="rejudgingid", columns={"rejudgingid"}), @ORM\Index(name="IDX_DB055AF34B30D9C4", columns={"cid"})})
 * @ORM\Entity
 */
class Submission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="submitid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $submitid;

    /**
     * @var string
     *
     * @ORM\Column(name="submittime", type="decimal", precision=32, scale=9, nullable=false)
     */
    private $submittime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valid", type="boolean", nullable=false)
     */
    private $valid = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="expected_results", type="string", length=255, nullable=true)
     */
    private $expectedResults;

    /**
     * @var string
     *
     * @ORM\Column(name="externalid", type="string", length=255, nullable=true)
     */
    private $externalid;

    /**
     * @var string
     *
     * @ORM\Column(name="externalresult", type="string", length=32, nullable=true)
     */
    private $externalresult;

    /**
     * @var string
     *
     * @ORM\Column(name="entry_point", type="string", length=255, nullable=true)
     */
    private $entryPoint;

    /**
     * @var integer
     * @ORM\Column(name="cid", type="integer")
     */
    private $cid;

    /**
     * @var integer
     * @ORM\Column(name="userproblemid", type="integer")
     */
    private $userproblemid;

    /**
     * @var \DOMJudgeBundle\Entity\User
     *
     * @ORM\Column(name="userid", type="integer")
     */
    private $userid;

    /**
     * @var \DOMJudgeBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="userid")
     * })
     */
    private $user;

    /**
     * @var \DOMJudgeBundle\Entity\Problem
     * @ORM\Column(name="probid", type="integer")
     */
    private $probid;

    /**
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Problem", inversedBy="submissions")
     * @ORM\JoinColumn(name="probid", referencedColumnName="probid", onDelete="CASCADE")
     * @Serializer\Exclude()
     */
    private $problem;

    /**
     * @var int
     *
     * @ORM\Column(type="string", name="langid", options={"comment"="Language ID"}, nullable=false)
     * @Serializer\Exclude()
     */
    private $langid;

    /**
     * @var \DOMJudgeBundle\Entity\Language
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="langid", referencedColumnName="langid")
     * })
     */
    private $language;

    /**
     * @var \DOMJudgeBundle\Entity\Judgehost
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Judgehost")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="judgehost", referencedColumnName="hostname")
     * })
     */
    private $judgehost;

    /**
     * @var \DOMJudgeBundle\Entity\Submission
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Submission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="origsubmitid", referencedColumnName="submitid")
     * })
     */
    private $origsubmitid;
    
    /**
     * @var integer
     * @ORM\Column(name="rejudgingid", type="integer")
     * 
     */
    private $rejudgingid;

    /**
     * @var \DOMJudgeBundle\Entity\Rejudging
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Rejudging")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rejudgingid", referencedColumnName="rejudgingid")
     * })
     */
    private $rejudging;
    
    /**
     * @var Submission|null
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Submission", inversedBy="resubmissions")
     * @ORM\JoinColumn(name="origsubmitid", referencedColumnName="submitid", onDelete="SET NULL")
     * @Serializer\Exclude()
     */
    private $originalSubmission;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="DOMJudgeBundle\Entity\Submission", mappedBy="originalSubmission")
     * @Serializer\Exclude()
     */
    private $resubmissions;

    /**
     * @ORM\OneToMany(targetEntity="Judging", mappedBy="submission")
     * @Serializer\Exclude()
     */
    private $judgings;

    /**
     * @var \DOMJudgeBundle\Entity\Classes
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Classes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cid", referencedColumnName="cid")
     * })
     */
    private $classes;

    /**
     * @var \DOMJudgeBundle\Entity\Classes
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Userproblem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userproblemid", referencedColumnName="userproblemid")
     * })
     */
    private $userproblem;

    /**
     * Set problem
     *
     * @param Problem $problem
     *
     * @return Submission
     */
    public function setProblem(Problem $problem = null)
    {
        $this->problem = $problem;

        return $this;
    }

    /**
     * Get problem
     *
     * @return Problem
     */
    public function getProblem()
    {
        return $this->problem;
    }

    /**
     * Get submitid
     *
     * @return integer
     */
    public function getSubmitid()
    {
        return $this->submitid;
    }

    /**
     * Set submittime
     *
     * @param string $submittime
     *
     * @return Submission
     */
    public function setSubmittime($submittime)
    {
        $this->submittime = $submittime;

        return $this;
    }

    /**
     * Get submittime
     *
     * @return string
     */
    public function getSubmittime()
    {
        return $this->submittime;
    }

    /**
     * Set valid
     *
     * @param boolean $valid
     *
     * @return Submission
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid
     *
     * @return boolean
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Set expectedResults
     *
     * @param string $expectedResults
     *
     * @return Submission
     */
    public function setExpectedResults($expectedResults)
    {
        $this->expectedResults = $expectedResults;

        return $this;
    }

    /**
     * Get expectedResults
     *
     * @return string
     */
    public function getExpectedResults()
    {
        return $this->expectedResults;
    }

    /**
     * Set externalid
     *
     * @param string $externalid
     *
     * @return Submission
     */
    public function setExternalid($externalid)
    {
        $this->externalid = $externalid;

        return $this;
    }

    /**
     * Get externalid
     *
     * @return string
     */
    public function getExternalid()
    {
        return $this->externalid;
    }

    /**
     * Set externalresult
     *
     * @param string $externalresult
     *
     * @return Submission
     */
    public function setExternalresult($externalresult)
    {
        $this->externalresult = $externalresult;

        return $this;
    }

    /**
     * Get externalresult
     *
     * @return string
     */
    public function getExternalresult()
    {
        return $this->externalresult;
    }

    /**
     * Set entryPoint
     *
     * @param string $entryPoint
     *
     * @return Submission
     */
    public function setEntryPoint($entryPoint)
    {
        $this->entryPoint = $entryPoint;

        return $this;
    }

    /**
     * Get entryPoint
     *
     * @return string
     */
    public function getEntryPoint()
    {
        return $this->entryPoint;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return Submission
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set classes
     *
     * @param \DOMJudgeBundle\Entity\Classes $classes
     *
     * @return Classes
     */
    public function setClasses(\DOMJudgeBundle\Entity\Classes $classes = null)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Get Classes
     *
     * @return \DOMJudgeBundle\Entity\Classes
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Set userproblemid
     *
     * @param integer $userproblemid
     *
     * @return Submission
     */
    public function setUserproblemid($userproblemid)
    {
        $this->userproblemid = $userproblemid;
        return $this;
    }

    /**
     * Get userproblemid
     *
     * @return integer
     */
    public function getUserproblemid()
    {
        return $this->userproblemid;
    }

    /**
     * Set Userproblem
     *
     * @param \DOMJudgeBundle\Entity\Userproblem $userproblem
     *
     * @return Userproblem
     */
    public function setUserproblem(\DOMJudgeBundle\Entity\Userproblem $userproblem)
    {
        $this->userproblem = $userproblem;
        return $this;
    }

    /**
     * Get Userproblem
     *
     * @return \DOMJudgeBundle\Entity\Userproblem
     */
    public function getUserproblem()
    {
        return $this->userproblem;
    }

    /**
     * Set userid
     *
     * @param integer
     *
     * @return Submission
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Set user
     *
     * @param \DOMJudgeBundle\Entity\User $user
     *
     * @return Submission
     */
    public function setUser(\DOMJudgeBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Get user
     *
     * @return \DOMJudgeBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set probid
     *
     * @param integer
     *
     * @return Submission
     */
    public function setProbid($probid)
    {
        $this->probid = $probid;

        return $this;
    }

    /**
     * Get probid
     *
     * @return integer
     */
    public function getProbid()
    {
        return $this->probid;
    }

    /**
     * Set langid
     *
     * @param string $langid
     *
     * @return Submission
     */
    public function setLangid($langid)
    {
        $this->langid = $langid;

        return $this;
    }

    /**
     * Get langid
     *
     * @return string
     */
    public function getLangid()
    {
        return $this->langid;
    }

    /**
     * Set language
     *
     * @param \DOMJudgeBundle\Entity\Language $language
     *
     * @return Submission
     */
    public function setLanguage(\DOMJudgeBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \DOMJudgeBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set judgehost
     *
     * @param \DOMJudgeBundle\Entity\Judgehost $judgehost
     *
     * @return Submission
     */
    public function setJudgehost(\DOMJudgeBundle\Entity\Judgehost $judgehost = null)
    {
        $this->judgehost = $judgehost;

        return $this;
    }

    /**
     * Get judgehost
     *
     * @return \DOMJudgeBundle\Entity\Judgehost
     */
    public function getJudgehost()
    {
        return $this->judgehost;
    }

    /**
     * Set origsubmitid
     *
     * @param \DOMJudgeBundle\Entity\Submission $origsubmitid
     *
     * @return Submission
     */
    public function setOrigsubmitid(\DOMJudgeBundle\Entity\Submission $origsubmitid = null)
    {
        $this->origsubmitid = $origsubmitid;

        return $this;
    }

    /**
     * Get origsubmitid
     *
     * @return \DOMJudgeBundle\Entity\Submission
     */
    public function getOrigsubmitid()
    {
        return $this->origsubmitid;
    }

    /**
     * Set rejudgingid
     *
     * @param integer $rejudgingid
     *
     * @return Submission
     */
    public function setRejudgingid($rejudgingid)
    {
        $this->rejudgingid = $rejudgingid;

        return $this;
    }

    /**
     * Set rejudging
     *
     * @param \DOMJudgeBundle\Entity\Rejudging $rejudging
     *
     * @return Submission
     */
    public function setRejudging(\DOMJudgeBundle\Entity\Rejudging $rejudging = null)
    {
        $this->rejudging = $rejudging;

        return $this;
    }

    /**
     * Get rejudgingid
     *
     * @return integer
     */
    public function getRejudgingid()
    {
        return $this->rejudgingid;
    }

    /**
     * Get rejudging
     *
     * @return \DOMJudgeBundle\Entity\Rejudging
     */
    public function getRejudging()
    {
        return $this->rejudging;
    }

    /**
     * Get judgings
     *
     * @return Collection
     */
    public function getJudgings()
    {
        return $this->judgings;
    }

    /**
     * Get original submission
     * @return Submission|null
     */
    public function getOriginalSubmission()
    {
        return $this->originalSubmission;
    }

    /**
     * Set original submission
     * @param Submission|null $originalSubmission
     * @return Submission
     */
    public function setOriginalSubmission($originalSubmission): Submission
    {
        $this->originalSubmission = $originalSubmission;
        return $this;
    }

    /**
     * Get resubmissions
     *
     * @return Collection|Submission[]
     */
    public function getResubmissions()
    {
        return $this->resubmissions;
    }

    /**
     * Check whether this submission is still busy while the final result is already known,
     * e.g. with non-lazy evaluation.
     * @return bool
     */
    public function isStillBusy()
    {
        /** @var Judging|null $judging */
        $judging = $this->getJudgings()->first();
        if (!$judging) {
            return false;
        }

        return !empty($judging->getResult()) && empty($judging->getEndtime()) && !$this->isAborted();
    }
}
