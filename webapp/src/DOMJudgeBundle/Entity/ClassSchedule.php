<?php

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassSchedule
 *
 * @ORM\Table(name="class_schedule", indexes={@ORM\Index(name="cheid", columns={"cheid"}), @ORM\Index(name="cid", columns={"cid"}), @ORM\Index(name="roomid", columns={"roomid"}), @ORM\Index(name="class_schedule_ibfk_2", columns={"slotid"})})
 * @ORM\Entity
 */
class ClassSchedule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cheid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cheid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="che_date", type="date", nullable=false)
     */
    private $cheDate;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DOMJudgeBundle\Entity\Classes
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Classes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cid", referencedColumnName="cid")
     * })
     */
    private $cid;

    /**
     * @var \DOMJudgeBundle\Entity\TimeSlot
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\TimeSlot")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="slotid", referencedColumnName="slotid")
     * })
     */
    private $slotid;

    /**
     * @var \DOMJudgeBundle\Entity\LabRoom
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\LabRoom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roomid", referencedColumnName="roomid")
     * })
     */
    private $roomid;



    /**
     * Get cheid
     *
     * @return integer
     */
    public function getCheid()
    {
        return $this->cheid;
    }

    /**
     * Set cheDate
     *
     * @param \DateTime $cheDate
     *
     * @return ClassSchedule
     */
    public function setCheDate($cheDate)
    {
        $this->cheDate = $cheDate;

        return $this;
    }

    /**
     * Get cheDate
     *
     * @return \DateTime
     */
    public function getCheDate()
    {
        return $this->cheDate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ClassSchedule
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cid
     *
     * @param \DOMJudgeBundle\Entity\Classes $cid
     *
     * @return ClassSchedule
     */
    public function setCid(\DOMJudgeBundle\Entity\Classes $cid = null)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return \DOMJudgeBundle\Entity\Classes
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set slotid
     *
     * @param \DOMJudgeBundle\Entity\TimeSlot $slotid
     *
     * @return ClassSchedule
     */
    public function setSlotid(\DOMJudgeBundle\Entity\TimeSlot $slotid = null)
    {
        $this->slotid = $slotid;

        return $this;
    }

    /**
     * Get slotid
     *
     * @return \DOMJudgeBundle\Entity\TimeSlot
     */
    public function getSlotid()
    {
        return $this->slotid;
    }

    /**
     * Set roomid
     *
     * @param \DOMJudgeBundle\Entity\LabRoom $roomid
     *
     * @return ClassSchedule
     */
    public function setRoomid(\DOMJudgeBundle\Entity\LabRoom $roomid = null)
    {
        $this->roomid = $roomid;

        return $this;
    }

    /**
     * Get roomid
     *
     * @return \DOMJudgeBundle\Entity\LabRoom
     */
    public function getRoomid()
    {
        return $this->roomid;
    }
}
