<?php declare(strict_types=1);

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classes
 *
 * @ORM\Table(name="classes", uniqueConstraints={@ORM\UniqueConstraint(name="shortname", columns={"shortname"}), @ORM\UniqueConstraint(name="externalid", columns={"externalid"})}, indexes={@ORM\Index(name="cid", columns={"cid"}), @ORM\Index(name="class_ibfk_1", columns={"langid"})})
 * @ORM\Entity
 */
class Classes extends BaseApiEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cid;

    /**
     * @var string
     *
     * @ORM\Column(name="externalid", type="string", length=255, nullable=true)
     */
    private $externalid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="semid", type="integer", nullable=false)
     */
    private $semid;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=false)
     */
    private $shortname;

    /**
     * @var string
     *
     * @ORM\Column(name="starttime", type="decimal", precision=32, scale=9, nullable=false)
     */
    private $starttime;

    /**
     * @var string
     *
     * @ORM\Column(name="endtime", type="decimal", precision=32, scale=9, nullable=false)
     */
    private $endtime;

    /**
     * @var string
     *
     * @ORM\Column(name="starttime_string", type="string", length=64, nullable=false)
     */
    private $starttimeString;

    /**
     * @var string
     *
     * @ORM\Column(name="endtime_string", type="string", length=64, nullable=false)
     */
    private $endtimeString;

    /**
     * @var boolean
     *
     * @ORM\Column(name="starttime_enabled", type="boolean", nullable=false)
     */
    private $starttimeEnabled = '1';

    /**
     * @var \DOMJudgeBundle\Entity\Language
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="langid", referencedColumnName="langid")
     * })
     */
    private $langid;

    /**
     * @var \DOMJudgeBundle\Entity\Semester
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Semester")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="semid", referencedColumnName="semid")
     * })
     */
    private $semester;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="DOMJudgeBundle\Entity\Problem", inversedBy="cid")
     * @ORM\JoinTable(name="classproblem",
     *   joinColumns={
     *     @ORM\JoinColumn(name="cid", referencedColumnName="cid")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="probid", referencedColumnName="probid")
     *   }
     * )
     */
    private $probid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="DOMJudgeBundle\Entity\User", inversedBy="cid")
     * @ORM\JoinTable(name="classuser",
     *   joinColumns={
     *     @ORM\JoinColumn(name="cid", referencedColumnName="cid")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="userid", referencedColumnName="userid")
     *   }
     * )
     */
    private $userid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->probid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set externalid
     *
     * @param string $externalid
     *
     * @return Classes
     */
    public function setExternalid($externalid)
    {
        $this->externalid = $externalid;

        return $this;
    }

    /**
     * Get externalid
     *
     * @return string
     */
    public function getExternalid()
    {
        return $this->externalid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Classes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

        /**
     * Set semid
     *
     * @param integer $semid
     *
     * @return Classes
     */
    public function setSemid($semid)
    {
        $this->semid = $semid;

        return $this;
    }

    /**
     * Get semid
     *
     * @return integer
     */
    public function getSemid()
    {
        return $this->semid;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return Classes
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Set starttime
     *
     * @param string $starttime
     *
     * @return Classes
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Get starttime
     *
     * @return string
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set endtime
     *
     * @param string $endtime
     *
     * @return Classes
     */
    public function setEndtime($endtime)
    {
        $this->endtime = $endtime;

        return $this;
    }

    /**
     * Get endtime
     *
     * @return string
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Set starttimeString
     *
     * @param string $starttimeString
     *
     * @return Classes
     */
    public function setStarttimeString($starttimeString)
    {
        $this->starttimeString = $starttimeString;

        return $this;
    }

    /**
     * Get starttimeString
     *
     * @return string
     */
    public function getStarttimeString()
    {
        return $this->starttimeString;
    }

    /**
     * Set endtimeString
     *
     * @param string $endtimeString
     *
     * @return Classes
     */
    public function setEndtimeString($endtimeString)
    {
        $this->endtimeString = $endtimeString;

        return $this;
    }

    /**
     * Get endtimeString
     *
     * @return string
     */
    public function getEndtimeString()
    {
        return $this->endtimeString;
    }

    /**
     * Set starttimeEnabled
     *
     * @param boolean $starttimeEnabled
     *
     * @return Classes
     */
    public function setStarttimeEnabled($starttimeEnabled)
    {
        $this->starttimeEnabled = $starttimeEnabled;

        return $this;
    }

    /**
     * Get starttimeEnabled
     *
     * @return boolean
     */
    public function getStarttimeEnabled()
    {
        return $this->starttimeEnabled;
    }

    /**
     * Set langid
     *
     * @param \DOMJudgeBundle\Entity\Language $langid
     *
     * @return Classes
     */
    public function setLangid(\DOMJudgeBundle\Entity\Language $langid = null)
    {
        $this->langid = $langid;

        return $this;
    }

    /**
     * Get langid
     *
     * @return \DOMJudgeBundle\Entity\Language
     */
    public function getLangid()
    {
        return $this->langid;
    }

    /**
     * Set semester
     *
     * @param \DOMJudgeBundle\Entity\Semester $semester
     *
     * @return Semester
     */
    public function setSemester(\DOMJudgeBundle\Entity\Semester $semester = null)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return \DOMJudgeBundle\Entity\Semester
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Add probid
     *
     * @param \DOMJudgeBundle\Entity\Problem $probid
     *
     * @return Classes
     */
    public function addProbid(\DOMJudgeBundle\Entity\Problem $probid)
    {
        $this->probid[] = $probid;

        return $this;
    }

    /**
     * Remove probid
     *
     * @param \DOMJudgeBundle\Entity\Problem $probid
     */
    public function removeProbid(\DOMJudgeBundle\Entity\Problem $probid)
    {
        $this->probid->removeElement($probid);
    }

    /**
     * Get probid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProbid()
    {
        return $this->probid;
    }

    /**
     * Add userid
     *
     * @param \DOMJudgeBundle\Entity\User $userid
     *
     * @return Classes
     */
    public function addUserid(\DOMJudgeBundle\Entity\User $userid)
    {
        $this->userid[] = $userid;

        return $this;
    }

    /**
     * Remove userid
     *
     * @param \DOMJudgeBundle\Entity\User $userid
     */
    public function removeUserid(\DOMJudgeBundle\Entity\User $userid)
    {
        $this->userid->removeElement($userid);
    }

    /**
     * Get userid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
