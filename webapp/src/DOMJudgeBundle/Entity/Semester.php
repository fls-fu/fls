<?php declare(strict_types=1);

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Semester
 *
 * @ORM\Table(name="semester")
 * @ORM\Entity
 */
class Semester
{
    /**
     * @var integer
     *
     * @ORM\Column(name="semid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $semid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="started_time", type="decimal", precision=32, scale=9, nullable=true)
     */
    private $startedTime;

    /**
     * @var string
     *
     * @ORM\Column(name="finished_time", type="decimal", precision=32, scale=9, nullable=true)
     */
    private $finishedTime;

    /**
     * Get semid
     *
     * @return integer
     */
    public function getSemid()
    {
        return $this->semid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Semester
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Semester
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set startedTime
     *
     * @param string $startedTime
     *
     * @return Semester
     */
    public function setStartedTime($startedTime)
    {
        $this->startedTime = $startedTime;

        return $this;
    }

    /**
     * Get startedTime
     *
     * @return string
     */
    public function getStartedTime()
    {
        return $this->startedTime;
    }

    /**
     * Set finishedTime
     *
     * @param string $finishedTime
     *
     * @return Semester
     */
    public function setFinishedTime($finishedTime)
    {
        $this->finishedTime = $finishedTime;

        return $this;
    }

    /**
     * Get finishedTime
     *
     * @return string
     */
    public function getFinishedTime()
    {
        return $this->finishedTime;
    }
}
