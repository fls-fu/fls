<?php

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event", indexes={@ORM\Index(name="eventtime", columns={"cid", "eventtime"}), @ORM\Index(name="cid", columns={"cid"})})
 * @ORM\Entity
 */
class Event
{
    /**
     * @var integer
     *
     * @ORM\Column(name="eventid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eventid;

    /**
     * @var string
     *
     * @ORM\Column(name="eventtime", type="decimal", precision=32, scale=9, nullable=false)
     */
    private $eventtime;

    /**
     * @var string
     *
     * @ORM\Column(name="endpointtype", type="string", length=32, nullable=false)
     */
    private $endpointtype;

    /**
     * @var string
     *
     * @ORM\Column(name="endpointid", type="string", length=64, nullable=false)
     */
    private $endpointid;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=32, nullable=false)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="blob", nullable=false)
     */
    private $content;

    /**
     * @var \DOMJudgeBundle\Entity\Classes
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Classes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cid", referencedColumnName="cid")
     * })
     */
    private $cid;



    /**
     * Get eventid
     *
     * @return integer
     */
    public function getEventid()
    {
        return $this->eventid;
    }

    /**
     * Set eventtime
     *
     * @param string $eventtime
     *
     * @return Event
     */
    public function setEventtime($eventtime)
    {
        $this->eventtime = $eventtime;

        return $this;
    }

    /**
     * Get eventtime
     *
     * @return string
     */
    public function getEventtime()
    {
        return $this->eventtime;
    }

    /**
     * Set endpointtype
     *
     * @param string $endpointtype
     *
     * @return Event
     */
    public function setEndpointtype($endpointtype)
    {
        $this->endpointtype = $endpointtype;

        return $this;
    }

    /**
     * Get endpointtype
     *
     * @return string
     */
    public function getEndpointtype()
    {
        return $this->endpointtype;
    }

    /**
     * Set endpointid
     *
     * @param string $endpointid
     *
     * @return Event
     */
    public function setEndpointid($endpointid)
    {
        $this->endpointid = $endpointid;

        return $this;
    }

    /**
     * Get endpointid
     *
     * @return string
     */
    public function getEndpointid()
    {
        return $this->endpointid;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return Event
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Event
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set cid
     *
     * @param \DOMJudgeBundle\Entity\Classes $cid
     *
     * @return Event
     */
    public function setCid(\DOMJudgeBundle\Entity\Classes $cid = null)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return \DOMJudgeBundle\Entity\Classes
     */
    public function getCid()
    {
        return $this->cid;
    }
}
