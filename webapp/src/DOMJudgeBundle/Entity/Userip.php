<?php declare(strict_types=1);

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Userip
 *
 * @ORM\Table(name="userip", uniqueConstraints={@ORM\UniqueConstraint(name="userid", columns={"userid", "cid", "ipid"})}, indexes={@ORM\Index(name="Userip_ibfk_2", columns={"cid"}), @ORM\Index(name="Userip_ibfk_3", columns={"ipid"}), @ORM\Index(name="IDX_4B863076F132696E", columns={"userid"})})
 * @ORM\Entity
 */
class Userip
{
    /**
     * @var integer
     *
     * @ORM\Column(name="uip_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uipid;
    
    /**
     * @var \DOMJudgeBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="userid")
     * })
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    private $userid;

    /**
     * @var \DOMJudgeBundle\Entity\Classes
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Classes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cid", referencedColumnName="cid")
     * })
     */
    private $classes;

    /**
     * @var integer
     *
     * @ORM\Column(name="cid", type="integer")
     */
    private $cid;

    /**
     * @var \DOMJudgeBundle\Entity\Roomip
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Roomip")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ipid", referencedColumnName="ipid")
     * })
     */
    private $roomip;

    /**
     * @var integer
     *
     * @ORM\Column(name="ipid", type="integer")
     */
    private $ipid;

    /**
     * @var string
     *
     * @ORM\Column(name="lastupdate", type="decimal", precision=32, scale=9, nullable=true)
     */
    private $lastupdate;    

    /**
     * Get Useripid
     *
     * @return integer
     */
    public function getUipid()
    {
        return $this->uipid;
    }

    /**
     * Set userid
     *
     * @param \DOMJudgeBundle\Entity\User $userid
     *
     * @return Userip
     */
    public function setUser(\DOMJudgeBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \DOMJudgeBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set Classes
     *
     * @param \DOMJudgeBundle\Entity\Classes $cid
     *
     * @return Userip
     */
    public function setClasses(\DOMJudgeBundle\Entity\Classes $classes = null)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Get Classes
     *
     * @return \DOMJudgeBundle\Entity\Classes
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set Roomip
     *
     * @param \DOMJudgeBundle\Entity\Roomip $roomip
     *
     * @return Userip
     */
    public function setRoomip(\DOMJudgeBundle\Entity\Roomip $roomip = null)
    {
        $this->roomip = $roomip;

        return $this;
    }

    /**
     * Get roomip
     *
     * @return \DOMJudgeBundle\Entity\Roomip
     */
    public function getRoomip()
    {
        return $this->roomip;
    }

    /**
     * Get ipid
     *
     * @return integer
     */
    public function getIpid()
    {
        return $this->ipid;
    }


    /**
     * Set lastupdate
     * @param integer $lastupdate
     * 
     * @return Userip
     */
    public function setLastupdate($lastupdate = null)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }
    
    /**
     * Get lastupdate
     *
     * @return integer
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }
}
