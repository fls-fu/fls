<?php  declare(strict_types=1);


namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LabRoom
 *
 * @ORM\Table(name="lab_room", indexes={@ORM\Index(name="roomid", columns={"roomid"})})
 * @ORM\Entity
 */
class LabRoom
{
    /**
     * @var integer
     *
     * @ORM\Column(name="roomid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $roomid;

    /**
     * @var string
     *
     * @ORM\Column(name="roomname", type="string", length=255, nullable=false)
     */
    private $roomname;



    /**
     * Get roomid
     *
     * @return integer
     */
    public function getRoomid()
    {
        return $this->roomid;
    }

    /**
     * Set roomname
     *
     * @param string $roomname
     *
     * @return LabRoom
     */
    public function setRoomname($roomname)
    {
        $this->roomname = $roomname;

        return $this;
    }

    /**
     * Get roomname
     *
     * @return string
     */
    public function getRoomname()
    {
        return $this->roomname;
    }
}
