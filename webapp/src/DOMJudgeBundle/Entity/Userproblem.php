<?php declare(strict_types=1);

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Userproblem
 *
 * @ORM\Table(name="userproblem", uniqueConstraints={@ORM\UniqueConstraint(name="userid", columns={"userid", "cid", "probid"})}, indexes={@ORM\Index(name="userproblem_ibfk_2", columns={"cid"}), @ORM\Index(name="userproblem_ibfk_3", columns={"probid"}), @ORM\Index(name="IDX_4B863076F132696E", columns={"userid"})})
 * @ORM\Entity
 */
class Userproblem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userproblemid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userproblemid;

    /**
     * @var string
     *
     * @ORM\Column(name="add_requirement", type="text", nullable=true)
     */
    private $addRequirement;

    /**
     * @var integer
     * @ORM\Column(name="changed", type="integer")
     * 
     */
    private $changed;

    /**
     * @var string
     * @ORM\Column(name="review_status", type="string")
     * 
     */
    private $reviewStatus;

    /**
     * @var string
     * @ORM\Column(name="status", type="string")
     * 
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="started_time", type="decimal", precision=32, scale=9, nullable=true)
     */
    private $startedTime;

    /**
     * @var string
     *
     * @ORM\Column(name="finished_time", type="decimal", precision=32, scale=9, nullable=true)
     */
    private $finishedTime;

    /**
     * @var \DOMJudgeBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="userid")
     * })
     */
    private $userid;

    /**
     * @var \DOMJudgeBundle\Entity\Classes
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Classes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cid", referencedColumnName="cid")
     * })
     */
    private $cid;

    /**
     * @var \DOMJudgeBundle\Entity\Problem
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Problem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="probid", referencedColumnName="probid")
     * })
     */
    private $probid;



    /**
     * Get userproblemid
     *
     * @return integer
     */
    public function getUserproblemid()
    {
        return $this->userproblemid;
    }

    /**
     * Set addRequirement
     *
     * @param string $addRequirement
     *
     * @return Userproblem
     */
    public function setAddRequirement($addRequirement)
    {
        $this->addRequirement = $addRequirement;

        return $this;
    }

    /**
     * Get addRequirement
     *
     * @return string
     */
    public function getAddRequirement()
    {
        return $this->addRequirement;
    }

    /**
     * Set changed
     *
     * @param integer $changed
     *
     * @return Userproblem
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;

        return $this;
    }

    /**
     * Get changed
     *
     * @return integer
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Userproblem
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set reviewStatus
     *
     * @param string $reviewStatus
     *
     * @return Userproblem
     */
    public function setReviewStatus($reviewStatus)
    {
        $this->reviewStatus = $reviewStatus;

        return $this;
    }

    /**
     * Get reviewStatus
     *
     * @return string
     */
    public function getReviewStatus()
    {
        return $this->reviewStatus;
    }

    /**
     * Set startedTime
     *
     * @param string $startedTime
     *
     * @return Userproblem
     */
    public function setStartedTime($startedTime)
    {
        $this->startedTime = $startedTime;

        return $this;
    }

    /**
     * Get startedTime
     *
     * @return string
     */
    public function getStartedTime()
    {
        return $this->startedTime;
    }

    /**
     * Set finishedTime
     *
     * @param string $finishedTime
     *
     * @return Userproblem
     */
    public function setFinishedTime($finishedTime)
    {
        $this->finishedTime = $finishedTime;

        return $this;
    }

    /**
     * Get finishedTime
     *
     * @return string
     */
    public function getFinishedTime()
    {
        return $this->finishedTime;
    }

    /**
     * Set userid
     *
     * @param \DOMJudgeBundle\Entity\User $userid
     *
     * @return Userproblem
     */
    public function setUserid(\DOMJudgeBundle\Entity\User $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \DOMJudgeBundle\Entity\User
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set cid
     *
     * @param \DOMJudgeBundle\Entity\Classes $cid
     *
     * @return Userproblem
     */
    public function setCid(\DOMJudgeBundle\Entity\Classes $cid = null)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return \DOMJudgeBundle\Entity\Classes
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set probid
     *
     * @param \DOMJudgeBundle\Entity\Problem $probid
     *
     * @return Userproblem
     */
    public function setProbid(\DOMJudgeBundle\Entity\Problem $probid = null)
    {
        $this->probid = $probid;

        return $this;
    }

    /**
     * Get probid
     *
     * @return \DOMJudgeBundle\Entity\Problem
     */
    public function getProbid()
    {
        return $this->probid;
    }
}
