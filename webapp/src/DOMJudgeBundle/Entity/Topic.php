<?php

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Topic
 *
 * @ORM\Table(name="topic")
 * @ORM\Entity
 */
class Topic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="DOMJudgeBundle\Entity\Problem", inversedBy="tid")
     * @ORM\JoinTable(name="problemtopic",
     *   joinColumns={
     *     @ORM\JoinColumn(name="tid", referencedColumnName="tid")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="probid", referencedColumnName="probid")
     *   }
     * )
     */
    private $probid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->probid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get tid
     *
     * @return integer
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Topic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Topic
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add probid
     *
     * @param \DOMJudgeBundle\Entity\Problem $probid
     *
     * @return Topic
     */
    public function addProbid(\DOMJudgeBundle\Entity\Problem $probid)
    {
        $this->probid[] = $probid;

        return $this;
    }

    /**
     * Remove probid
     *
     * @param \DOMJudgeBundle\Entity\Problem $probid
     */
    public function removeProbid(\DOMJudgeBundle\Entity\Problem $probid)
    {
        $this->probid->removeElement($probid);
    }

    /**
     * Get probid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProbid()
    {
        return $this->probid;
    }
}
