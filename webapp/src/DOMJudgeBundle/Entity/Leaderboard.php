<?php declare(strict_types=1);

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Leaderboard
 *
 * @ORM\Table(name="leaderboard", indexes={@ORM\Index(name="order_restricted", columns={"cid", "points_restricted", "totaltime_restricted"})})
 * @ORM\Entity
 */
class Leaderboard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $cid;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userid;

    /**
     * @var integer
     *
     * @ORM\Column(name="points_restricted", type="integer", nullable=false)
     */
    private $pointsRestricted = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="totaltime_restricted", type="integer", nullable=false)
     */
    private $totaltimeRestricted = '0';



    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return Leaderboard
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return Leaderboard
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set pointsRestricted
     *
     * @param integer $pointsRestricted
     *
     * @return Leaderboard
     */
    public function setPointsRestricted($pointsRestricted)
    {
        $this->pointsRestricted = $pointsRestricted;

        return $this;
    }

    /**
     * Get pointsRestricted
     *
     * @return integer
     */
    public function getPointsRestricted()
    {
        return $this->pointsRestricted;
    }

    /**
     * Set totaltimeRestricted
     *
     * @param integer $totaltimeRestricted
     *
     * @return Leaderboard
     */
    public function setTotaltimeRestricted($totaltimeRestricted)
    {
        $this->totaltimeRestricted = $totaltimeRestricted;

        return $this;
    }

    /**
     * Get totaltimeRestricted
     *
     * @return integer
     */
    public function getTotaltimeRestricted()
    {
        return $this->totaltimeRestricted;
    }
}
