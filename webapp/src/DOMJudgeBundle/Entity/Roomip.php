<?php declare(strict_types=1);

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roomip
 *
 * @ORM\Table(name="roomip")
 * @ORM\Entity
 */
class Roomip
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ipid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ipid;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=255, nullable=false)
     */
    private $ipAddress;

    /**
     * @var \DOMJudgeBundle\Entity\LabRoom
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\LabRoom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roomid", referencedColumnName="roomid")
     * })
     */
    private $roomid;

    /**
     * @var string
     *
     * @ORM\Column(name="pc_code", type="string", length=255)
     */
    private $pccode;

    /**
     * Get ipid
     *
     * @return integer
     */
    public function getIpid()
    {
        return $this->ipid;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     *
     * @return Roomip
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set roomid
     *
     * @param \DOMJudgeBundle\Entity\LabRoom $roomid
     *
     * @return Roomip
     */
    public function setRoomid(\DOMJudgeBundle\Entity\LabRoom $roomid = null)
    {
        $this->roomid = $roomid;

        return $this;
    }

    /**
     * Get roomid
     *
     * @return \DOMJudgeBundle\Entity\LabRoom
     */
    public function getRoomid()
    {
        return $this->roomid;
    }

    /**
     * Set pccode
     *
     * @param string $pccode
     *
     * @return Roomip
     */
    public function setPccode($pccode)
    {
        $this->pccode = $pccode;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string
     */
    public function getPccode()
    {
        return $this->pccode;
    }
}
