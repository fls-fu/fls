<?php declare(strict_types=1);

namespace DOMJudgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DOMJudgeBundle\Utils\Utils;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Problem
 *
 * @ORM\Table(name="problem", uniqueConstraints={@ORM\UniqueConstraint(name="shortname", columns={"probid", "shortname"})}, indexes={@ORM\Index(name="externalid", columns={"externalid"}), @ORM\Index(name="problem_ibfk_1", columns={"langid"})})
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity
 */
class Problem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="probid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $probid;

    /**
     * @var string
     *
     * @ORM\Column(name="externalid", type="string", length=255, nullable=true)
     */
    private $externalid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=false)
     */
    private $shortname = 'problem';

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=false)
     */
    private $points = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="difficult", type="integer", nullable=true)
     */
    private $difficult = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_submit", type="boolean", nullable=false)
     */
    private $allowSubmit = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_judge", type="boolean", nullable=false)
     */
    private $allowJudge = true;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=32, nullable=true)
     */
    private $color;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lazy_eval_results", type="boolean", nullable=true)
     */
    private $lazyEvalResults;

    /**
     * @var double
     * @ORM\Column(type="float", name="timelimit", options={"comment"="Maximum run time (in seconds) for this problem"}, nullable=false)
     * @Serializer\Exclude()
     * @Assert\GreaterThan(0)
     */
    private $timelimit = 0;

    /**
     * @var double
     * @ORM\Column(type="float", name="timedolimit", options={"comment"="Maximum run time (in seconds) for doing this problem"}, nullable=false)
     */
    private $timedolimit = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", name="memlimit", options={"comment"="Maximum memory available (in kB) for this problem", "unsigned"=true}, nullable=true)
     * @Serializer\Exclude()
     * @Assert\GreaterThan(0)
     */
    private $memlimit;

    /**
     * @var int
     * @ORM\Column(type="integer", name="outputlimit", options={"comment"="Maximum output size (in kB) for this problem", "unsigned"=true}, nullable=true)
     * @Serializer\Exclude()
     * @Assert\GreaterThan(0)
     */
    private $outputlimit;

    /**
     * @var string
     * @ORM\Column(type="string", name="special_run", length=32, options={"comment"="Script to run submissions for this problem"}, nullable=true)
     * @Serializer\Exclude()
     */
    private $special_run;

    /**
     * @ORM\ManyToOne(targetEntity="Executable", inversedBy="problems_run")
     * @ORM\JoinColumn(name="special_run", referencedColumnName="execid")
     * @Serializer\Exclude()
     */
    private $run_executable;

    /**
     * @ORM\ManyToOne(targetEntity="Executable", inversedBy="problems_compare")
     * @ORM\JoinColumn(name="special_compare", referencedColumnName="execid")
     * @Serializer\Exclude()
     */
    private $compare_executable;

    /**
     * @var string
     * @ORM\Column(type="string", name="special_compare", length=32, options={"comment"="Script to compare problem and jury output for this problem"}, nullable=true)
     * @Serializer\Exclude()
     */
    private $special_compare;

    /**
     * @var string
     * @ORM\Column(type="string", name="special_compare_args", length=32, options={"comment"="Optional arguments to special_compare script"}, nullable=true)
     * @Serializer\Exclude()
     */
    private $special_compare_args;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="combined_run_compare", options={"comment"="Use the exit code of the run script to compute the verdict"}, nullable=false)
     * @Serializer\Exclude()
     */
    private $combinedRunCompare = FALSE ;

    /**
     * @var resource
     * @ORM\Column(type="blob", name="problemtext", options={"comment"="Problem text in HTML/PDF/ASCII"}, nullable=true)
     * @Serializer\Exclude()
     */
    private $problemtext;

    /**
     * @var string
     *
     * @ORM\Column(name="problemtext_type", type="string", length=4, nullable=true)
     */
    private $problemtextType;

    /**
     * @var UploadedFile|null
     * @Assert\File()
     * @Serializer\Exclude()
     */
    private $problemtextFile;

    /**
     * @var bool
     * @Serializer\Exclude()
     */
    private $clearProblemtext = false;

    /**
     * @var string
     *
     * @ORM\Column(name="created_date", type="decimal", precision=32, scale=9, nullable=false)
     */
    private $createdDate;

    /**
     * @var \DOMJudgeBundle\Entity\Language
     *
     * @ORM\ManyToOne(targetEntity="DOMJudgeBundle\Entity\Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="langid", referencedColumnName="langid")
     * })
     */
    private $langid ;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="DOMJudgeBundle\Entity\Classes", mappedBy="probid")
     */
    private $cid;

    /**
     * @ORM\ManyToMany(targetEntity="DOMJudgeBundle\Entity\Topic", inversedBy="probid")
     * @ORM\JoinTable(name="problemtopic",
     *                joinColumns={@ORM\JoinColumn(name="probid", referencedColumnName="probid")},
     *                inverseJoinColumns={@ORM\JoinColumn(name="tid", referencedColumnName="tid")}
     *               )
     * @Serializer\Exclude()
     */
    private $tid;

    /**
     * @ORM\OneToMany(targetEntity="Testcase", mappedBy="problem")
     * @Serializer\Exclude()
     */
    private $testcases;

    /**
     * @ORM\OneToMany(targetEntity="Submission", mappedBy="problem")
     * @Serializer\Exclude()
     */
    private $submissions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->testcases = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add submission
     *
     * @param \DOMJudgeBundle\Entity\Submission $submission
     *
     * @return Problem
     */
    public function addSubmission(\DOMJudgeBundle\Entity\Submission $submission)
    {
        $this->submissions[] = $submission;

        return $this;
    }

    /**
     * Remove submission
     *
     * @param \DOMJudgeBundle\Entity\Submission $submission
     */
    public function removeSubmission(\DOMJudgeBundle\Entity\Submission $submission)
    {
        $this->submissions->removeElement($submission);
    }

    /**
     * Get submissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubmissions()
    {
        return $this->submissions;
    }

    /**
     * Add testcase
     *
     * @param \DOMJudgeBundle\Entity\Testcase $testcase
     *
     * @return Problem
     */
    public function addTestcase(\DOMJudgeBundle\Entity\Testcase $testcase)
    {
        $this->testcases[] = $testcase;

        return $this;
    }

    /**
     * Remove testcase
     *
     * @param \DOMJudgeBundle\Entity\Testcase $testcase
     */
    public function removeTestcase(\DOMJudgeBundle\Entity\Testcase $testcase)
    {
        $this->testcases->removeElement($testcase);
    }

    /**
     * Get testcases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestcases()
    {
        return $this->testcases;
    }

    /**
     * Get probid
     *
     * @return integer
     */
    public function getProbid()
    {
        return $this->probid;
    }

    /**
     * Set externalid
     *
     * @param string $externalid
     *
     * @return Problem
     */
    public function setExternalid($externalid)
    {
        $this->externalid = $externalid;

        return $this;
    }

    /**
     * Get externalid
     *
     * @return string
     */
    public function getExternalid()
    {
        return $this->externalid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Problem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return Problem
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Set difficult
     *
     * @param integer $difficult
     *
     * @return Problem
     */
    public function setDifficult($difficult)
    {
        $this->difficult = $difficult;

        return $this;
    }

    /**
     * Get difficult
     *
     * @return integer
     */
    public function getDifficult()
    {
        return $this->difficult;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return Problem
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set allowSubmit
     *
     * @param boolean $allowSubmit
     *
     * @return Problem
     */
    public function setAllowSubmit($allowSubmit)
    {
        $this->allowSubmit = $allowSubmit;

        return $this;
    }

    /**
     * Get allowSubmit
     *
     * @return boolean
     */
    public function getAllowSubmit()
    {
        return $this->allowSubmit;
    }

    /**
     * Set allowJudge
     *
     * @param boolean $allowJudge
     *
     * @return Problem
     */
    public function setAllowJudge($allowJudge)
    {
        $this->allowJudge = $allowJudge;

        return $this;
    }

    /**
     * Get allowJudge
     *
     * @return boolean
     */
    public function getAllowJudge()
    {
        return $this->allowJudge;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Problem
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set lazyEvalResults
     *
     * @param boolean $lazyEvalResults
     *
     * @return Problem
     */
    public function setLazyEvalResults($lazyEvalResults)
    {
        $this->lazyEvalResults = $lazyEvalResults;

        return $this;
    }

    /**
     * Get lazyEvalResults
     *
     * @return boolean
     */
    public function getLazyEvalResults()
    {
        return $this->lazyEvalResults;
    }

    /**
     * Set timelimit
     *
     * @param float $timelimit
     *
     * @return Problem
     */
    public function setTimelimit($timelimit)
    {
        $this->timelimit = $timelimit;

        return $this;
    }

    /**
     * Get timelimit
     *
     * @return float
     */
    public function getTimelimit()
    {
        return $this->timelimit;
    }

    /**
     * Set timedolimit
     *
     * @param float $timedolimit
     *
     * @return Problem
     */
    public function setTimedolimit($timedolimit)
    {
        $this->timedolimit = $timedolimit;

        return $this;
    }

    /**
     * Get timedolimit
     *
     * @return float
     */
    public function getTimedolimit()
    {
        return $this->timedolimit;
    }

    /**
     * Set memlimit
     *
     * @param integer $memlimit
     *
     * @return Problem
     */
    public function setMemlimit($memlimit)
    {
        $this->memlimit = $memlimit;

        return $this;
    }

    /**
     * Get memlimit
     *
     * @return integer
     */
    public function getMemlimit()
    {
        return $this->memlimit;
    }

    /**
     * Set outputlimit
     *
     * @param integer $outputlimit
     *
     * @return Problem
     */
    public function setOutputlimit($outputlimit)
    {
        $this->outputlimit = $outputlimit;

        return $this;
    }

    /**
     * Get outputlimit
     *
     * @return integer
     */
    public function getOutputlimit()
    {
        return $this->outputlimit;
    }

    /**
     * Set specialRun
     *
     * @param string $specialRun
     *
     * @return Problem
     */
    public function setSpecialRun($specialRun)
    {
        $this->special_run = $specialRun;

        return $this;
    }

    /**
     * Get specialRun
     *
     * @return string
     */
    public function getSpecialRun()
    {
        return $this->special_run;
    }

    /**
     * Set specialCompare
     *
     * @param string $specialCompare
     *
     * @return Problem
     */
    public function setSpecialCompare($specialCompare)
    {
        $this->special_compare = $specialCompare;

        return $this;
    }

    /**
     * Get specialCompare
     *
     * @return string
     */
    public function getSpecialCompare()
    {
        return $this->special_compare;
    }

    /**
     * Set specialCompareArgs
     *
     * @param string $specialCompareArgs
     *
     * @return Problem
     */
    public function setSpecialCompareArgs($specialCompareArgs)
    {
        $this->special_compare_args = $specialCompareArgs;

        return $this;
    }

    /**
     * Get specialCompareArgs
     *
     * @return string
     */
    public function getSpecialCompareArgs()
    {
        return $this->special_compare_args;
    }

    /**
     * Set combinedRunCompare
     *
     * @param boolean $combinedRunCompare
     *
     * @return Problem
     */
    public function setCombinedRunCompare($combinedRunCompare)
    {
        $this->combinedRunCompare = $combinedRunCompare;

        return $this;
    }

    /**
     * Get combinedRunCompare
     *
     * @return boolean
     */
    public function getCombinedRunCompare()
    {
        return $this->combinedRunCompare;
    }

    /**
     * Set problemtext
     *
     * @param string $problemtext
     *
     * @return Problem
     */
    public function setProblemtext($problemtext)
    {
        $this->problemtext = $problemtext;

        return $this;
    }

    /**
     * Get problemtext
     *
     * @return resource|string
     */
    public function getProblemtext()
    {
        return $this->problemtext;
    }

    /**
     * Set problemtextType
     *
     * @param string $problemtextType
     *
     * @return Problem
     */
    public function setProblemtextType($problemtextType)
    {
        $this->problemtextType = $problemtextType;

        return $this;
    }

    /**
     * Get problemtextType
     *
     * @return string
     */
    public function getProblemtextType()
    {
        return $this->problemtextType;
    }

    /**
     * Set createdDate
     *
     * @param string $createdDate
     *
     * @return Problem
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
        // $this->createdDate = Utils::now();

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set langid
     *
     * @param \DOMJudgeBundle\Entity\Language $langid
     *
     * @return Problem
     */
    public function setLangid(\DOMJudgeBundle\Entity\Language $langid = null)
    {
        $this->langid = $langid;

        return $this;
    }

    /**
     * Get langid
     *
     * @return \DOMJudgeBundle\Entity\Language
     */
    public function getLangid()
    {
        return $this->langid;
    }

    /**
     * Add cid
     *
     * @param \DOMJudgeBundle\Entity\Classes $cid
     *
     * @return Problem
     */
    public function addCid(\DOMJudgeBundle\Entity\Classes $cid)
    {
        $this->cid[] = $cid;

        return $this;
    }

    /**
     * Remove cid
     *
     * @param \DOMJudgeBundle\Entity\Classes $cid
     */
    public function removeCid(\DOMJudgeBundle\Entity\Classes $cid)
    {
        $this->cid->removeElement($cid);
    }

    /**
     * Get cid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Add tid
     *
     * @param \DOMJudgeBundle\Entity\Topic $tid
     *
     * @return Problem
     */
    public function addTid(\DOMJudgeBundle\Entity\Topic $tid)
    {
        $this->tid[] = $tid;

        return $this;
    }

    /**
     * Remove tid
     *
     * @param \DOMJudgeBundle\Entity\Topic $tid
     */
    public function removeTid(\DOMJudgeBundle\Entity\Topic $tid)
    {
        $this->tid->removeElement($tid);
    }

    /**
     * Get tid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * Set runExecutable
     *
     * @param \DOMJudgeBundle\Entity\Executable $runExecutable
     *
     * @return Problem
     */
    public function setRunExecutable(\DOMJudgeBundle\Entity\Executable $runExecutable = null)
    {
        $this->run_executable = $runExecutable;

        return $this;
    }

    /**
     * Get runExecutable
     *
     * @return \DOMJudgeBundle\Entity\Executable
     */
    public function getRunExecutable()
    {
        return $this->run_executable;
    }

        /**
     * Set compareExecutable
     *
     * @param \DOMJudgeBundle\Entity\Executable $compareExecutable
     *
     * @return Problem
     */
    public function setCompareExecutable(\DOMJudgeBundle\Entity\Executable $compareExecutable = null)
    {
        $this->compare_executable = $compareExecutable;

        return $this;
    }

    /**
     * Get compareExecutable
     *
     * @return \DOMJudgeBundle\Entity\Executable
     */
    public function getCompareExecutable()
    {
        return $this->compare_executable;
    }

    /**
     * @param UploadedFile|null $problemtextFile
     * @return Problem
     */
    public function setProblemtextFile($problemtextFile)
    {
        $this->problemtextFile = $problemtextFile;
        // Clear the problem text to make sure the entity is modified
        $this->problemtext = '';

        return $this;
    }

    /**
     * @param bool $clearProblemtext
     * @return Problem
     */
    public function setClearProblemtext(bool $clearProblemtext)
    {
        $this->clearProblemtext = $clearProblemtext;
        $this->problemtext = null;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getProblemtextFile()
    {
        return $this->problemtextFile;
    }

    /**
     * @return bool
     */
    public function isClearProblemtext(): bool
    {
        return $this->clearProblemtext;
    }

    /**
     * Get whether this problem has a problem text
     * @return bool
     */
    public function hasProblemtext()
    {
        if (is_string($this->problemtext)) {
            return !empty($this->problemtext);
        } elseif (is_resource($this->problemtext)) {
            return fstat($this->problemtext)['size'] > 0;
        } else {
            return false;
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     * @throws \Exception
     */
    public function processProblemText()
    {
        if ($this->isClearProblemtext()) {
            $this
                ->setProblemtext(null)
                ->setProblemtextType(null);
        } elseif ($this->getProblemtextFile()) {
            $content         = file_get_contents($this->getProblemtextFile()->getRealPath());
            $clientName      = $this->getProblemtextFile()->getClientOriginalName();
            $problemTextType = null;

            if (strrpos($clientName, '.') !== false) {
                $ext = substr($clientName, strrpos($clientName, '.') + 1);
                if (in_array($ext, ['txt', 'html', 'pdf'])) {
                    $problemTextType = $ext;
                }
            }
            if (!isset($problemTextType)) {
                $finfo = finfo_open(FILEINFO_MIME);

                list($type) = explode('; ', finfo_file($finfo, $this->getProblemtextFile()->getRealPath()));

                finfo_close($finfo);

                switch ($type) {
                    case 'application/pdf':
                        $problemTextType = 'pdf';
                        break;
                    case 'text/html':
                        $problemTextType = 'html';
                        break;
                    case 'text/plain':
                        $problemTextType = 'txt';
                        break;
                }
            }

            if (!isset($problemTextType)) {
                throw new \Exception('Problem statement has unknown file type.');
            }

            $this
                ->setProblemtext($content)
                ->setProblemtextType($problemTextType);
        }
    }
}
