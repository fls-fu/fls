<?php declare(strict_types=1);

namespace DOMJudgeBundle\Form\Type\Flab;

use Doctrine\ORM\EntityRepository;
use DOMJudgeBundle\Entity\Role;
use DOMJudgeBundle\Entity\Team;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Entity\Language;
use DOMJudgeBundle\Entity\Classes;
use DOMJudgeBundle\Entity\ClassSchedule;
use DOMJudgeBundle\Entity\Semester;
use Proxies\__CG__\DOMJudgeBundle\Entity\Classes as ProxiesClasses;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'Class name',
        ]);
        // $builder->add('semester', EntityType::class, [
        //     'label' => 'Semester',
        //     'class' => Semester::class,
        //     'choice_label' => 'name',
        //     'choice_value' => 'semid',
        //     'required' => true,
        //     //'multiple' => true,
        //     'expanded' => true,
        // ]);

        $builder->add('semester', EntityType::class, [
            'label' => 'Semester',
            'class' => Semester::class,
            'required' => true,
            'placeholder' => 'Choose Semester for this class',
            'choice_label' => 'name',
            'query_builder' => function (EntityRepository $er) {
                return $er
                    ->createQueryBuilder('s')
                    ->orderBy('s.startedTime', 'DESC');
            },
        ]);


        $builder->add('langid', EntityType::class, [
            'label' => 'Language',
            'class' => Language::class,
            'required' => true,
            'placeholder' => 'Please choose Language for this class',
            'choice_label' => 'name',
            'query_builder' => function (EntityRepository $er) {
                return $er
                    ->createQueryBuilder('e')
                    // ->where('e.type = :run')
                    // ->setParameter(':run', 'run')
                    ->orderBy('e.name');
            },
        ]);
        // $builder->add('starttimeEnabled', ChoiceType::class, [
        //     'choices'  => [
        //         'Yes' => true,
        //         'No' => false,             
        //     ],
        //     'data' => false,
        // ]);
        $builder->add('save', SubmitType::class);
        // // Remove ID field when doing an edit
        // $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
        //     /** @var User|null $user */
        //     $user = $event->getData();
        //     $form = $event->getForm();

        //     if ($user && $user->getUserid() !== null) {
        //         $form->remove('username');
        //     }
        // });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Classes::class]);
    }

}
