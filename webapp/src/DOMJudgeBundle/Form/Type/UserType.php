<?php declare(strict_types=1);

namespace DOMJudgeBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use DOMJudgeBundle\Entity\Role;
use DOMJudgeBundle\Entity\Team;
use DOMJudgeBundle\Entity\User;
use DOMJudgeBundle\Service\DOMJudgeService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @var DOMJudgeService
     */
    protected $dj;

    /**
     * RootController constructor.
     * @param DOMJudgeService        $dj
     */
    public function __construct(DOMJudgeService $dj)
    {
        $this->dj = $dj;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class);
        $builder->add('name', TextType::class, [
            'label' => 'Full name',
        ]);
        $builder->add('email', EmailType::class, [
            'required' => true,
        ]);
        $builder->add('plainPassword', PasswordType::class, [
            'required' => false,
            'label' => 'Password',
        ]);
        $builder->add('enabled', ChoiceType::class, [
            'expanded' => true,
            'choices' => [
                'Yes' => true,
                'No' => false,
            ],
        ]);
        $builder->add('roles', EntityType::class, [
            'class' => Role::class,
            'query_builder' => function (EntityRepository $er) {
                if ( $this->dj->checkrole('super_admin',false)) {
                    return $er->createQueryBuilder('r')
                        ->where('r.dj_role IN (?1)')
                        ->orderBy('r.dj_role', 'ASC')
                        ->setParameter(1, ['student', 'teacher', 'admin', 'super_admin', 'judgehost']);
                }
                if ( $this->dj->checkrole('admin',false)) {
                    return $er->createQueryBuilder('r')
                        ->where('r.dj_role IN (?1)')
                        ->orderBy('r.dj_role', 'ASC')
                        ->setParameter(1, ['student', 'teacher']);
                }
            },
            'choice_label' => 'djRole',
            'required' => false,
            'multiple' => true,
            'expanded' => true,
        ]);
        $builder->add('save', SubmitType::class);

        // Remove ID field when doing an edit
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var User|null $user */
            $user = $event->getData();
            $form = $event->getForm();

            if ($user && $user->getUserid() !== null) {
                $form->remove('username');
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => User::class]);
    }
}
