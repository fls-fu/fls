$(document).ready(function() {
    $('#btn_reject').click(function(){
        swal({
            title: "Are you sure to reject?",
            text: "If approved, you will not be able to undo it!",
            icon: "warning",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
        }).then(function(e) {
            if(e){
                $('#teacher_judging_reject').submit();
            }
        });
    });
    $('#btn_accept').click(function(){
        let msg = ''
        if($(this).attr('data-btn') != 'out'){
            msg = 'If approved, you will not be able to undo it!'
        }else{
            msg = 'This problem when running is having an error!\n If approved, you will not be able to undo it!'
        }
        swal({
            title: "Are you sure to accept?",
            text: msg,
            icon: "warning",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
        }).then(function(e) {
            if(e){
                $('#teacher_judging_verify').submit();
            }
        });
    });
    
});