$(document).ready(function() {
    setTable();
    setRowData();
});

function setTable() {
    var columnDefs = [
        { headerName: "Problem ID", field: "probid", hide: true },
        { headerName: "No", valueGetter: 'node.id', width: 100, minWidth: 50, maxWidth: 100, suppressSizeToFit: true },
        {
            headerName: "Code",
            width: 150,
            minWidth: 100,
            maxWidth: 300,
            'cellRenderer': function(params) {
                let value = params.data;
                let router = $('#dom_data').attr('data-route-detail').slice(0, -1) + value.probid;
                router += '?cid=' + $('#dom_data').attr('data-classid');
                let link = '<a class="text-dark" href="' + router + '" >' + value.shortname + '</a>'
                return link;
            }

        },
        {
            headerName: "Problem",
            minWidth: 100,
            'cellRenderer': function(params) {
                let value = params.data;
                let router = $('#dom_data').attr('data-route-detail').slice(0, -1) + value.probid;
                router += '?cid=' + $('#dom_data').attr('data-classid');
                let link = '<a class="text-dark" href="' + router + '" >' + value.name + '</a>'
                return link;
            }

        },
        {
            headerName: "Action",
            width: 150,
            minWidth: 100,
            maxWidth: 300,
            'cellRenderer': function(params) {
                let value = params.data;
                let button = '<button type="button" data-id="' + value.probid + '" class="btn btn-success ml-2 btn_add" data-toggle="tooltip" data-placement="top" title="Add requirement" > <i class="far fa-edit"></i> </button>'
                return button;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        'columnDefs': columnDefs,
        'rowData': [],
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'icons': FlsConstant.iconAgGrid,
        'navigateToNextCell': function(params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function(event) {
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };
    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_class_problem');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    // gridOptions.api.addEventListener('rowDoubleClicked', onRowClick);
    gridOptions.api.addEventListener('cellMouseOver', onAddBtClick);
    setRowData();

    function onRowClick() {
        let router = $('#dom_data').attr('data-route-detail');
        window.location = router;
    }
}

function setRowData() {
    let rowdata = JSON.parse($('#dom_data').attr('data-problem'));
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    // gridOptions.api.selectionController.selectIndex(0, true);
    gridOptions.api.sizeColumnsToFit();
}

function onAddBtClick() {
    $('.btn_add').unbind('click');
    $('.btn_add').click(function() {
        let probid = $(this).attr('data-id');
        let cid = $('#dom_data').attr('data-classid');
        let router = $('#dom_data').attr('data-route-add-rq');
        let dataPass = JSON.stringify({ 'cid': cid, 'probid': probid });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            // add content
            $("#add_requirement_modal .modal-body").html(data);
            // open the other modal
            $("#add_requirement_modal").modal("show");
            handleEvent();
        });
    });
};

function handleEvent() {
    $("#btn_add_require").click(function() {
        swal({
            title: "Confirm!",
            text: "You want to save requirement?",
            icon: "warning",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
        }).then(function(e) {
            if (e) {
                let cid = $("#btn_add_require").attr('data-cid');
                let probid = $("#btn_add_require").attr('data-probid');
                let textRequire = $('#text_require').val();
                let router = $("#btn_add_require").attr('data-route');

                let dataPass = JSON.stringify({
                    'cid': cid,
                    'probid': probid,
                    'textRequire': textRequire
                });
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: router,
                    data: dataPass,
                }).done(function(data) {
                    showMes(data);
                });
            }
        });
    });
}

function showMes(data) {
    if (data.error) {
        swal({
            title: "ERROR",
            text: data.error,
            icon: "error",
            button: "OK",
        });
    } else {
        if (!data.result) {
            swal({
                title: "ERROR",
                text: 'Add requirement error!',
                icon: "error",
                button: "OK",
            });
        } else {
            if (data.mode == 'new') {
                $("#add_requirement_modal").modal("hide");
                swal({
                    title: "Successfull",
                    text: "Requirement of problem have added",
                    icon: "success",
                    button: "OK!",
                    timer: 3000,
                });
            }
            if (data.mode == 'update') {
                $("#add_requirement_modal").modal("hide");
                swal({
                    title: "Successfull",
                    text: "Requirement of problem have updated",
                    icon: "success",
                    button: "OK!",
                    timer: 3000,
                });
            }
        }

    }
}