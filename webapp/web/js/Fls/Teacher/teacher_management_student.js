$(document).ready(function() {
    setTableProblem();
    searchClass();
});

function searchClass() {
    $('#selectClass').change(function() {
        let selected = $(this).children("option:selected").val();
    });
}

function setTableProblem() {
    $('#export_excel').click(function() {
        onBtExport();
    });
    // table problem
    let columnDefs = [{
            headerName: "Student",
            minWidth: 300,
            children: [{
                    headerName: "student id",
                    width: 0,
                    minWidth: 0,
                    maxWidth: 0,
                    field: "userid" ,
                    hide:true
                },
                {
                    headerName: "class id",
                    width: 0,
                    minWidth: 0,
                    maxWidth: 0,
                    field: "cid" ,
                    hide:true
                },
                {
                    headerName: "Name",
                    // width: 250,
                    minWidth: 200,
                    field: "name" ,
                },
                {
                    headerName: "Loc",
                    // width: 250,
                    minWidth: 200,
                    sortable: false,
                    suppressMenu: true,
                    field: "loc" ,
                },
                {
                    headerName: "Status",
                    // width: 250,
                    minWidth: 200,
                    sortable: false,
                    suppressMenu: true,
                    field: "status",
                },
                {
                    headerName: "Positions",                    
                    minWidth: 200,
                    sortable: false,
                    suppressMenu: true,
                    'cellRenderer': function(params) {
                        let data = params.data;
                        if(data.position != ''){
                            return '<div class="btn_add_positions" data-cid="' + data.cid +'" data-userid="' + data.userid +'" title="'+ data.ipAddress+'">'+ data.position + '</div>';
                        }else{
                            button = '<button type="button" data-cid="' + data.cid +'" data-userid="' + data.userid +'" class="btn btn-success  ml-2 btn_add_positions" data-toggle="tooltip" data-placement="top" title="Add positions" > <i class="far fa-plus-square"></i> </button>'
                            return button;
                        }                        
                    }
                },
            ]
        },

        {
            headerName: "Problem info",
            minWidth: 300,
            children: [{
                headerName: "Total",
                width: 150,
                minWidth: 130,
                maxWidth: 200,
                sortable: false,
                suppressMenu: true,
                field: "total",
            }, {
                headerName: "Pass",
                width: 120,
                minWidth: 130,
                maxWidth: 200,
                sortable: false,
                suppressMenu: true,
                field: "pass",
            }, {
                headerName: "Reject",
                width: 150,
                minWidth: 130,
                maxWidth: 200,
                sortable: false,
                suppressMenu: true,
                field: "reject",
            }, {
                headerName: "Changed",
                width: 150,
                minWidth: 130,
                maxWidth: 200,
                sortable: false,
                suppressMenu: true,
                field: "changed",
            }]
        },
    ];

    // let the grid know which columns and what data to use
    var gridOptions = {
        columnDefs: columnDefs,
        rowSelection: 'single',
        'icons': FlsConstant.iconAgGrid,
        defaultColDef: {
            resizable: true,
            sortable: true,
            filter: true
        },
        onGridReady: function(event) {
            setRowData(event);
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
        pagination: true,
        paginationAutoPageSize: true
    };

    function setRowData(event) {
        let rowdata = JSON.parse($('#dom_data').attr('data-management-student'));
        event.api.setRowData(rowdata);
    }

    // event row click
    function onRowClicked() {
        let selectedRows = gridOptions.api.getSelectedRows();
        let cid = $('#dom_data').attr('data-classid');
        let router = $('#dom_data').attr('data-route') + selectedRows[0].userid + '/' + cid;
        window.location = router;
    }

    // lookup the container we want the Grid to use
    let eGridDiv = document.querySelector('#tb_managenement_student');
    showTable(eGridDiv, gridOptions);
    gridOptions.api.addEventListener('rowDoubleClicked', onRowClicked);
    gridOptions.api.addEventListener('cellMouseOver', onBtnCLicked);

    $('.ag-header-row').each(function() {
        if ($(this).attr('aria-rowindex') == 1) {
            $(this).css('font-size', '25px');
            $(this).css('color', 'black');
        }
        if ($(this).attr('aria-rowindex') == 2) {
            $(this).css('font-size', '20px');
        }
    });

    function onBtExport() {
        var params = {
            fileName: 'class',
            sheetName: 'class',
            exportMode: "xlsx",
            // allColumns: true,
            columnKeys: ['name','loc','status']
        };
    
        gridOptions.api.exportDataAsCsv(params);
        // console.log(gridOptions.api.getDataAsExcel(params));
    }

}

function showTable(eGridDiv, gridOptions) {
    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
}

function onBtnCLicked() {
    $(".btn_add_positions").mouseover(function() {
        $(this).css('cursor', 'pointer');
    });
    $(".btn_add_positions").unbind('click');
    $(".btn_add_positions").click(function() {
        let userid = $(this).attr('data-userid');
        let cid = $(this).attr('data-cid');
        let dataPass = JSON.stringify({ 'userid': userid, 'cid': cid });
        let router = $('#dom_data').attr('data-route-position');
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            // add content
            $("#add_positions .modal-body").html(data);
            // open the other modal
            $('#add_positions').modal({
                backdrop: 'static',
                keyboard: false
            })
            $("#add_positions").modal("show");
            handleEvent();
        });
    });
}

function handleEvent(){
    $('#btn_edit').click(function(){
        $(this).prop('hidden', true);
        $('#btn_save').prop('hidden', false);
        $('#btn_cancel').prop('hidden', false);
        $('#selectPositions_v').prop('hidden', false);
        $('#selectRoom_v').prop('hidden', false);
    });
    $('#btn_cancel').click(function(){
        $(this).prop('hidden', true);
        $('#btn_save').prop('hidden', true);
        $('#btn_edit').prop('hidden', false);
        $('#selectPositions_v').prop('hidden', true);
        $('#selectRoom_v').prop('hidden', true);
    });
    $('#btn_save').click(function(){
        let cid = $('#hide-data').attr('data-cid');
        let userid = $('#hide-data').attr('data-userid');
        let roomid = $('#selectRoom').children("option:selected").val();
        let ipid = $('#selectPositions').children("option:selected").val();
        let router = $('#hide-data').attr('data-route');
        let dataPass = JSON.stringify({ 'cid': cid, 'userid': userid, 'roomid': roomid, 'ipid': ipid, 'mode': 'save'});
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            if(data.error != null){
                swal({
                    title: "Error!",
                    text: data.error,
                    icon: "error",
                    button: "OK!",
                }).then(function(dismiss) {
                    location.reload();
                });
            }else{
                swal({
                    title: "Successfull!",
                    text: "The position has been saved",
                    icon: "success",
                    button: "OK!",
                    timer: 3000,
                }).then(function(dismiss) {
                    location.reload();
                });
            }
        });
    });
    $('#selectRoom').change(function(){
        let cid = $('#hide-data').attr('data-cid');
        let userid = $('#hide-data').attr('data-userid');
        let roomid = $(this).children("option:selected").val();
        let router = $('#hide-data').attr('data-route');
        let dataPass = JSON.stringify({ 'cid': cid, 'userid': userid, 'roomid': roomid, 'mode': 'filter'});
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            if(data.error != null){
                swal({
                    title: "Error!",
                    text: data.error,
                    icon: "error",
                    button: "OK!",
                });
            }else{
                // add content
                $("#add_positions .modal-body").html(data);
                handleEvent();
            }            
        });
    });
}
