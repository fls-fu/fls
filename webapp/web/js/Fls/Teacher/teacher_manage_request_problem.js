$(document).ready(function() {
    setTable();
    setRowData();
});

function setTable() {
    var columnDefs = [
        { headerName: "Class ID", field: "cID", hide: true },
        { headerName: "UserProblem ID", field: "upID", hide: true },
        { headerName: "Student ID", field: "uID", hide: true },
        { headerName: "Student Name", field: "uName" },
        { headerName: "Project ID", field: "pID", hide: true },
        { headerName: "Project Short Name", field: "pShortName" },
        { headerName: "Project Name", field: "pName" },
        {
            headerName: "Action",
            cellRenderer: function(params) {
                let data = params.data;
                let button = '<button id="bt_accept_' + data.upID + '" type="button" class="btn btn-success ml-2 btn_accept" data-toggle="tooltip" data-placement="top" title="Accept" > <i class="fas fa-check-double"></i> </button>' +
                    '<button id="bt_reject_' + data.upID + '" type="button" class="btn btn-danger ml-2 btn_reject" data-toggle="tooltip" data-placement="top" title="Reject"> <i class="fas fa-times"></i> </button>';
                let rowdata = '<div id="rowdata_hide" data-upId="' + data.upID + '" hidden></div>'
                return button + rowdata;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        'columnDefs': columnDefs,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'icons': FlsConstant.iconAgGrid,
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'navigateToNextCell': function(params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function(event) {
            onRowClick();
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_request_problem');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    setRowData();
}

function setRowData() {
    let rowdata = JSON.parse($('#dom_data').attr('data-list-problem'));
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    gridOptions.api.selectionController.selectIndex(0, true);
    gridOptions.api.sizeColumnsToFit();
    $("#rows").text(rowdata.length);
}

// event row click
function onRowClick() {
    $(".btn_accept").click(function() {
        var rowdata = $('#rowdata_hide').attr('data-upId');
        let router = $('#dom_data').attr('data-route-accept');
        var dataPass = JSON.stringify({ 'upId': rowdata });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            // add content
            $("#reRandom_modal .modal-body").html(data);
            // open the other modal
            $('#reRandom_modal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $("#reRandom_modal").modal("show");
            handleEvent();
        });
    });

    $(".btn_reject").click(function() {
        swal({
            title: "Confirm",
            text: "You want to reject request change problem?",
            icon: "warning",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
        }).then(function(dismiss) {
            if (dismiss) {
                var rowdata = $('#rowdata_hide').attr('data-upId');
                let router = $('#dom_data').attr('data-route-reject');
                var dataPass = JSON.stringify({ 'upId': rowdata });
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: router,
                    data: dataPass,
                }).done(function(data) {
                    if (data.result) {
                        swal({
                            title: "Successfull",
                            text: "Request change problem is rejected",
                            icon: "success",
                            timer: 3000
                        }).then(function(dismiss) {
                            location.reload();
                        });
                    }
                });
            }
        });
    });
}

function handleEvent() {
    $("#btn_confirm").click(function() {
        let listData = JSON.parse($('#hide-data').attr('data-list-update'));
        let upid = $('#hide-data').attr('data-redirect');
        let router = $('#hide-data').attr('data-route-confirm');

        let dataPass = JSON.stringify({ 'upId': upid, 'listData': listData, 'confirm': true });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            if (data.result) {
                $("#reRandom_modal").modal("hide");
                swal({
                    title: "Successfull",
                    text: "Request change problem is accepted",
                    icon: "success",
                    button: "OK!",
                    timer: 3000,
                }).then(function(dismiss) {
                    location.reload();
                });
            }
        });
    });
    // re random
    $("#btn_rerandom").click(function() {
        let listData = JSON.parse($('#hide-data').attr('data-list-update'));
        let upid = $('#hide-data').attr('data-redirect');
        let router = $('#hide-data').attr('data-route-rerandom');

        let dataPass = JSON.stringify({ 'upId': upid });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            // add content
            $("#reRandom_modal .modal-body").html(data);
            handleEvent();
        });
    });
}