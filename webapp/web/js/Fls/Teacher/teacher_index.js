$(document).ready(function () {
    if (document.getElementById("tb_class") != null) {
        setTable();
        setRowData();
        onRowClick();
    }
    //disable Upload button when do not browser file
    $("#btnClassUpload").attr("disabled", true);
    $("#btnScheduleUpload").attr("disabled", true);

    $("#btnClassImport").click(function () {
        $("#Classes_File").click();
        $("#Classes_File").on('change', function (e) {
            $('#class_filename').text($('#Classes_File').val().replace(/.*(\/|\\)/, ''));
            $("#btnClassUpload").attr("disabled", false);
        });

    })
    $("#btnClassUpload").click(function () {
        $('#btnClassUpload_span').text('Uploading...');
        $("#btnClassUpload").attr("disabled", true);
        $("#btnOk").attr("disabled", true);
        $("#btnCancel").attr("disabled", true);
        var formdata = new FormData($('#Classes_FileUploadForm').get(0));
        let router = $('#dom_data_import').attr('data-route-import');
        formdata.append('name', $('#Classes_File').val().replace(/.*(\/|\\)/, ''));
        // alert(formdata);
        $.ajax({
            url: router,
            type: "POST",
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json"
        }).done(function (data) {
            $('#btnClassUpload_span').text('Upload');
            $("#btnClassUpload").attr("disabled", false);
            $("#btnOk").attr("disabled", false);
            $("#btnCancel").attr("disabled", false);
            // location.reload();
            if (data.uploadStatus) {
                $('#class_success_status').text(data.message);
                $('#class_danger_status').text('');
            } else {
                $('#class_success_status').text('');
                $('#class_danger_status').text(data.message);
            }
        });
    })

    //import schedule
    $("#btnScheduleImport").click(function () {
        $("#Schedule_File").click();
        $("#Schedule_File").on('change', function (e) {
            $('#schedule_filename').text($('#Schedule_File').val().replace(/.*(\/|\\)/, ''));
            $("#btnScheduleUpload").attr("disabled", false);
        });

    })
    $("#btnScheduleUpload").click(function () {
        $('#btnScheduleUpload_span').text('Uploading...');
        $("#btnScheduleUpload").attr("disabled", true);
        $("#btnOk").attr("disabled", true);
        $("#btnCancel").attr("disabled", true);
        var formdata = new FormData($('#schedule_FileUploadForm').get(0));
        let router = $('#dom_data_import').attr('data-route-import-schedule');
        formdata.append('name', $('#Schedule_File').val().replace(/.*(\/|\\)/, ''));
        // alert(formdata);
        $.ajax({
            url: router,
            type: "POST",
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json"
        }).done(function (data) {
            $('#btnScheduleUpload_span').text('Upload');
            $("#btnScheduleUpload").attr("disabled", false);
            $("#btnOk").attr("disabled", false);
            $("#btnCancel").attr("disabled", false);
            // location.reload();
            if (data.uploadStatus) {
                $('#schedule_success_status').text(data.message);
                $('#schedule_danger_status').text('');
            } else {
                $('#schedule_success_status').text('');
                $('#schedule_danger_status').text(data.message);
            }
        });
    })

    //when click ok button, reload
    $("#btnOk").click(function () {
        location.reload();
    })
    //when click cancel button, reload
    $("#btnCancel").click(function () {
        location.reload();
    })
});

function setTable() {
    var columnDefs = [{
            headerName: "Class ID",
            field: "id",
            hide: true
        },
        {
            headerName: "Class Name",
            field: "name"
        },
        {
            headerName: "Semester",
            field: "semester"
        },
        {
            headerName: "Language",
            field: "language"
        },
        {
            headerName: "Start",
            field: "cStartTime",
            'cellRenderer': function (params) {
                let value = params.data.cStartTime;
                let classText = 'text-dark';
                let startTime = '-';
                let currentSemName = $('#currentSemName').attr('data-sem-name');
                let classSemName = params.data.semester;
                if(typeof currentSemName == 'undefined' || classSemName != currentSemName) {
                    return '<span class="' + classText + '"><b>' + startTime + '</b></span>';
                }
                if (value != '-') {
                    startTime = convertTimestamp(value);
                }
                return '<span class="' + classText + '"><b>' + startTime + '</b></span>';
            }
        },


        {
            headerName: "End",
            field: "cEndTime",
            'cellRenderer': function (params) {
                let value = params.data.cEndTime;
                let endTime = '-';
                let classText = '';

                let currentSemName = $('#currentSemName').attr('data-sem-name');
                let classSemName = params.data.semester;
                if(typeof currentSemName == 'undefined' || classSemName != currentSemName) {
                    classText = 'text-dark';
                    return '<span class="' + classText + '"><b>' + endTime + '</b></span>';
                }

                if (value == 'freetime') {
                    endTime = value;
                    classText = 'text-success';
                } else if (value != '-') {
                    endTime = convertTimestamp(value);
                    classText = 'text-dark';
                }
                
                return '<span class="' + classText + '"><b>' + endTime + '</b></span>';
            }
        },
        {
            headerName: "Status",
            field: "cActive",
            'cellRenderer': function (params) {
                let value = params.data.cActive;
                let classText = '';
                let vActive = '';
                //check diff semester
                let currentSemName = $('#currentSemName').attr('data-sem-name');
                let classSemName = params.data.semester;
                if(typeof currentSemName == 'undefined' || classSemName !== currentSemName) {
                    classText = "text-dark";
                    vActive = "-";
                    return '<span class="' + classText + '"><b>' + vActive + '</b></span>';
                }

                if (params.data.cStartTime < Date.now() / 1000 && params.data.cEndTime > Date.now() / 1000) {
                    if (value == false) {
                        classText = "text-danger";
                        vActive = "Disable (Slot Time)";
                    } else {
                        classText = "text-success";
                        vActive = "Enable (Slot Time)";
                    }

                } else {
                    if (value == true && params.data.cEndTime == 'freetime') {
                        classText = "text-success";
                        vActive = "Enable (Free Time)";
                    } else if (value == false) {
                        classText = "text-info";
                        vActive = "Disable"
                    } else {
                        value = -1;
                        classText = "text-dark";
                    }
                }

                return '<span class="' + classText + '"><b>' + vActive + '</b></span>';
            }
        },
        {
            headerName: "Action",
            cellRenderer: function (params) {
                let data = params.data;
                let button = '';
                //check diff semester
                let currentSemName = $('#currentSemName').attr('data-sem-name');
                let classSemName = params.data.semester;
                if(typeof currentSemName == 'undefined' || classSemName != currentSemName) {
                    return '<span class="text-dark"><b>-</b></span>';
                }
                if (!data.cActive) {
                    button = '<button data-id="' + data.id + '" type="button" class="btn btn-success ml-2 btn_start" data-toggle="tooltip" data-placement="top" title="Start" > <i class="fas fa-play"></i> </button>'
                } else {
                    button = '<button data-id="' + data.id + '" type="button" class="btn btn-danger ml-2 btn_stop" data-toggle="tooltip" data-placement="top" title="Stop"> <i class="fas fa-stop"></i> </button>';
                }
                return button;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        'columnDefs': columnDefs,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'icons': FlsConstant.iconAgGrid,
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'navigateToNextCell': function (params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function (event) {
            $(window).resize(function () {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_class');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    gridOptions.api.addEventListener('rowDoubleClicked', GridPress);
    gridOptions.api.addEventListener('cellMouseOver', onRowClick);
    setRowData();
}

function setRowData() {
    let rowdata = JSON.parse($('#dom_data').attr('data-class'));
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    // gridOptions.api.selectionController.selectIndex(0, true);
    gridOptions.api.sizeColumnsToFit();
    $("#rows").text(rowdata.length);
}

function GridPress(event) {
    let selectedRows = gridOptions.api.getSelectedRows();
    let router = $('#dom_data').attr('data-route') + selectedRows[0].id;
    window.location = router;
}

// event row click
function onRowClick() {
    $('.btn_start').unbind('click');
    $('.btn_start').click(function () {
        gridOptions.api.addEventListener('rowSelected', onRowClick);
        var rowdata = $(this).attr('data-id');
        let router = $('#dom_data').attr('data-route-starttime');
        var dataPass = JSON.stringify({
            'cid': rowdata
        });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function (data) {
            let selectedRows = gridOptions.api.getSelectedRows();
            selectedRows[0].cStartTime = data.starttime;
            selectedRows[0].cEndTime = data.endtime;
            selectedRows[0].cActive = data.active;
            gridOptions.api.rowDataChanged();
            // if (data.freetime == true) {
            //     selectedRows[0].cStartTime = data.starttime;
            //     selectedRows[0].cEndTime = data.endtime;
            //     selectedRows[0].cActive = data.active;
            //     gridOptions.api.rowDataChanged();
            // } else {
            //     selectedRows[0].cStartTime = data.starttime;
            //     selectedRows[0].cEndTime = data.endtime;
            //     selectedRows[0].cActive = data.active;
            //     gridOptions.api.rowDataChanged();
            // }
        });
    });
    $('.btn_stop').unbind('click');
    $('.btn_stop').click(function () {
        var rowdata = $(this).attr('data-id');
        let router = $('#dom_data').attr('data-route-stoptime');
        var dataPass = JSON.stringify({
            'cid': rowdata
        });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function (data) {
            let selectedRows = gridOptions.api.getSelectedRows();
            selectedRows[0].cStartTime = data.starttime;
            selectedRows[0].cEndTime = data.endtime;
            selectedRows[0].cActive = data.active;
            gridOptions.api.rowDataChanged();
        });
    });
};

function convertTimestamp(timestamp) {
    var date = new Date(timestamp * 1000);
    // Year part from the timestamp
    var year = date.getFullYear();
    // Month part from the timestamp
    var month = String(date.getMonth() + 1).padStart(2, '0');
    // Day part from the timestamp
    var day = String(date.getDate()).padStart(2, '0');
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    var totalDate = '';
    //check today
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    if (yyyy == year && mm == month && dd == day) {
        totalDate = ' (Today)'
    } else {
        totalDate = ' (' + day + '-' + month + '-' + year + ')';
    }

    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) + totalDate;
    return formattedTime;
}

 // onchange semester 
 function changeSemester() {
    let router = $('#dom_data_import').attr('data-route-index');
    let semester_id = $('#selectSemester').val();
    window.location = router +'/'+ semester_id;
 }
