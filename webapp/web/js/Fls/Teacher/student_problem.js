$(document).ready(function() {
    setTable();
    setRowData();
});

function setTable() {
    let columnDefs = [
        { headerName: "No", valueGetter: 'node.id', width: 100, minWidth: 50, maxWidth: 100, sortable: false, suppressMenu: true,},
        { headerName: "ID", field: "ID", width: 0, minWidth: 0, maxWidth: 0, hide: true },
        { headerName: "Code", field: "name", width: 100, minWidth: 80, maxWidth: 200 },
        { headerName: "Name", field: "content", minWidth: 160 },
        { headerName: "Loc", field: "point", width: 100, minWidth: 80, maxWidth: 200 },
        {
            headerName: "Result",
            // field: "result",
            width: 100,
            minWidth: 80,
            maxWidth: 200,
            sortable: false,
            suppressMenu: true,
            cellRenderer: function(params) {
                let result = params.data.result;
                let changeStatus = params.data.status;
                let classText = '';
                if (changeStatus != null && changeStatus == 2) {
                    classText = "text-success";
                    return '<span class="' + classText + '"><b>' + FlsConstant.changeStatus[changeStatus] + '</b></span>';
                } else {
                    if (result == 1) {
                        classText = "text-warning";
                    } else if (result == 2) {
                        classText = "text-primary";
                    } else if (result == 3) {
                        classText = "text-info";
                    } else if (result == 4) {
                        classText = "text-success";
                    } else if (result == 5) {
                        classText = "text-danger";
                    } else {
                        result = (result == null) ? 0 : result;
                        classText = "text-dark";
                    }
                    value = '<span class="' + classText + '"><b>' + FlsConstant.problemStatus[result] + '</b></span>';
                }
                return value;
            }
        },
        {
            headerName: "Change",
            // field: "result",
            width: 100,
            minWidth: 80,
            maxWidth: 200,
            cellRenderer: function(params) {
                let value = params.data.status;
                let classText = '';
                if (value == 0) {
                    classText = "text-info";
                } else if (value == 1) {
                    classText = "text-success";
                } else if (value == 2) {
                    classText = "text-danger";
                } else if (value == 3) {
                    classText = "text-primary";
                } else {
                    value = (value == null) ? -1 : value;
                    classText = "text-dark";
                }
                return '<span class="' + classText + '"><b>' + FlsConstant.changeStatus[value] + '</b></span>';

            },
            hide: true
        },
        {
            headerName: "Action",
            width: 150,
            minWidth: 100,
            maxWidth: 200,
            sortable: false,
            suppressMenu: true,
            'cellRenderer': function(params) {
                let data = params.data;
                let button = '';
                if (data.result != 4 && data.result != 5 && data.status != 2) {
                    button = '<button type="button" data-id="' + data.id + '" class="btn btn-success ml-2 btn_add" data-toggle="tooltip" data-placement="top" title="Add requirement" > <i class="far fa-edit"></i> </button>'
                } else {
                    button = '<button type="button" data-id="' + data.id + '" class="btn btn-success ml-2 btn_view" data-toggle="tooltip" data-placement="top" title="View requirement" > <i class="fas fa-eye"></i> </button>'
                }
                return button;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        defaultColDef: {
            resizable: true,
            sortable: true,
            filter: true
        },
        'columnDefs': columnDefs,
        'rowData': [],
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'icons': FlsConstant.iconAgGrid,
        'navigateToNextCell': function(params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function(event) {
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };
    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_student_problem');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    gridOptions.api.addEventListener('cellMouseOver', onBtnClick);
    setRowData();
}

function setRowData() {
    let rowdata = JSON.parse($('#dom_data').attr('data-problem'));
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    // gridOptions.api.selectionController.selectIndex(0, true);
    gridOptions.api.sizeColumnsToFit();
}

function onBtnClick() {
    $('.btn_add').unbind('click');
    $('.btn_add').click(function() {
        let probid = $(this).attr('data-id');
        let userid = $('#dom_data').attr('data-userid');
        let cid = $('#dom_data').attr('data-classid');
        let router = $('#dom_data').attr('data-route-add-rq');
        let dataPass = JSON.stringify({ 'cid': cid, 'probid': probid, 'userid': userid, 'mode': 'edit' });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            // add content
            $("#requirement_modal .modal-body").html(data);
            // open the other modal
            $("#requirement_modal").modal("show");
            handleEvent();
        });
    });
    $('.btn_view').unbind('click');
    $('.btn_view').click(function() {
        let probid = $(this).attr('data-id');
        let cid = $('#dom_data').attr('data-classid');
        let userid = $('#dom_data').attr('data-userid');
        let router = $('#dom_data').attr('data-route-add-rq');
        let dataPass = JSON.stringify({ 'cid': cid, 'probid': probid, 'userid': userid, 'mode': 'view' });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: router,
            data: dataPass,
        }).done(function(data) {
            // add content
            $("#requirement_modal .modal-body").html(data);
            // open the other modal
            $("#requirement_modal").modal("show");
        });
    });
};

function handleEvent() {
    $("#btn_add_require").click(function() {
        swal({
            title: "Confirm!",
            text: "You want to save requirement?",
            icon: "warning",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
        }).then(function(e) {
            if (e) {
                let cid = $("#btn_add_require").attr('data-cid');
                let probid = $("#btn_add_require").attr('data-probid');
                let userid = $('#btn_add_require').attr('data-userid');
                let textRequire = $('#text_require').val();
                let router = $("#btn_add_require").attr('data-route');

                let dataPass = JSON.stringify({
                    'cid': cid,
                    'probid': probid,
                    'userid': userid,
                    'textRequire': textRequire
                });
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: router,
                    data: dataPass,
                }).done(function(data) {
                    showMes(data);
                });
            }
        });
    });
}

function showMes(data) {
    if (data.error) {
        swal({
            title: "ERROR",
            text: data.error,
            icon: "error",
            button: "OK",
        });
    } else {
        if (!data.result) {
            swal({
                title: "ERROR",
                text: 'Add requirement error!',
                icon: "error",
                button: "OK",
            });
        } else {
            if (data.mode == 'new') {
                $("#requirement_modal").modal("hide");
                swal({
                    title: "Successfull",
                    text: "Requirement of problem have added",
                    icon: "success",
                    button: "OK!",
                    timer: 3000,
                });
            }
            if (data.mode == 'update') {
                $("#requirement_modal").modal("hide");
                swal({
                    title: "Successfull",
                    text: "Requirement of problem have updated",
                    icon: "success",
                    button: "OK!",
                    timer: 3000,
                });
            }
        }

    }
}