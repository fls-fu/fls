$(document).ready(function() {
    analysisChart();
    dbChart();
});
function analysisChart() {

    let data = JSON.parse($('#dom_data').attr('data-charts'));

    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: data['labels'],
            datasets: [{
                label: '',
                data: data['data'],
                backgroundColor: data['color']
            }],
            option: {
                responsive: true,
                maintainAspectRatio: true,
                onResize: resizable
            }
        }
    });

    function resizable() {
        myChart.canvas.parentNode.style.height = '100%';
        myChart.canvas.parentNode.style.width = '100%';
    }
}

function dbChart() {

    let data = JSON.parse($('#dom_data').attr('data-db-charts'));

    var ctx = document.getElementById('dbChart');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: data['labels'],
            datasets: [{
                label: '',
                data: data['data'],
                backgroundColor: data['color']
            }],
            option: {
                responsive: true,
                maintainAspectRatio: true,
                onResize: resizable
            }
        }
    });

    function resizable() {
        myChart.canvas.parentNode.style.height = '100%';
        myChart.canvas.parentNode.style.width = '100%';
    }
}