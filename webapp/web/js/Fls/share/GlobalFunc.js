function gf_column_width_set(arrColName, arrColWidth, strPgId, lngGridNo) {
    debugger;
    var lngLoop;
    var arrResult = [];

    for (lngLoop = 0; lngLoop < arrColName.length; lngLoop++) {
        arrResult.push(arrColWidth[lngLoop] * 1.2);
    }

    return arrResult;
}
function get_columnDefs(colLabel, colName, colWidth, colAlign, colEdit, colOption) {
    var _this = this;
    var aryRet = [];

    for (var i = 0; i < colName.length; i++) {
        if (colLabel[i] != '') {
          
            var objOneCol = {
                'headerName': colLabel[i]
                , 'field': colName[i]
                , 'width': colWidth[i]
                , 'pinned': false
                , 'suppressSorting': false
                , 'suppressSizeToFit': true
                , 'suppressMenu': true
                , 'onCellClicked': function (event) {
                 
                    event.api.deselectAll();
                    event.node.setSelected(!event.node.selected);
                }
                , 'cellStyle': { textAlign: colAlign[i] }
            };

          
            if (colEdit[i] == true) {
                objOneCol['onCellDoubleClicked'] = null;
                objOneCol['headerClass'] = 'editableCell';
                objOneCol['editable'] = true;
                objOneCol['onCellValueChanged'] = function (params) {
                    if (params.oldValue !== params.newValue) {
                      
                        params.data.EDIT_KBN = '1';
                    }
                };
            }

            if (colOption != undefined) {
                colOption.forEach(function (value) {
                    var key = Object.keys(value)[0];
                    if (key === colName[i]) {
                        $.extend(objOneCol, value[key]);
                    }
                });
            }

            aryRet[i] = objOneCol;
        }
    }

  
    var dummy = {
        'headerName': 'dummy'
        , 'field': 'dummy'
        , 'hide': true
        , 'checkboxSelection': true
    };
    aryRet.push(dummy);

    return aryRet;
}