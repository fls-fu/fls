$(document).ready(function() {
    showTimer();
});

function showTimer() {
    let router = $('#countdown_timer').attr('data-route');
    $.ajax({
        url: router
    }).done(function(data) {
        if (data.active) {
            if (data.freetime) {
                $('#freetime').removeClass('d-none');
                $("#freetime").show();
                $("#countdown_timer").hide();
            } else {
                if(data.end_time !== undefined){
                    $('#countdown_timer').removeClass('d-none');
                    $("#countdown_timer").show();
                    $("#freetime").hide();
                    countdownTimer($("#countdown_timer"), data.end_time);
                }else{
                    $("#countdown_timer").hide();
                }
            }
        }
    });
}

function countdownTimer(elememt, countDownDate) {
    let a = parseInt(countDownDate) + '';
    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();
        let no = now.toString().length;
        a = a.padEnd(no, "0");

        // Find the distance between now and the count down date
        var distance = a - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (hours == 0 && minutes == 5 && seconds == 0) {
            swal({
                title: "Class Warning!",
                text: "After 5 minutes the class will end! Please submit your problems before class end!",
                icon: "warning",
                button: "OK!",
            }).then(function(dismiss) {
                showTimer();
            });
            sendNotification('Class Warning!', {
                'body': 'After 5 minutes the class will end! Please submit your problems before class end!'
            });
        }
        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            swal({
                title: "Class was end!",
                text: "your class was end! please comeback later!",
                icon: "info",
                button: "OK!",
            }).then(function(dismiss) {
                window.location.href = domjudge_base_url;
            });
            sendNotification('Class was end!', {
                'body': 'your class was end! please comeback later!'
            });
        }
        // Output the result in an element with id="demo"
        if(hours > 0){
            elememt.html(hours + "h " + minutes + "m " + seconds + "s ");
        }else if(minutes > 0){
            elememt.html(minutes + "m " + seconds + "s ");
        }else{
            elememt.html(seconds + "s ");
        }
        
    }, 1000);
}