const problemStatus = {
    '0': 'open',
    '1': 'Doing',
    '2': 'Submitted',
    '3': 'Testing done',
    '4': 'Pass',
    '5': 'Reject',
}

const changeStatus = {
    '0': '-',
    '1': 'Requesting',
    '2': 'Changed',
    '3': 'Rejected',
    '4': 'New',
};

const SubmittedStatus = {
    'correct': 'Correct',
    'compiler-error': 'Compiler-error',
    'no-output': 'No-output',
    'output-limit': 'Output-limit',
    'run-error': 'Run-error',
    'timelimit': 'Timelimit',
    'wrong-answer': 'Wrong-answer',
    'pending': 'Pending',
    'judging': 'Judging',
}

const status = {
    '0': 'Not pass',
    '1': 'Pass',
}

const iconAgGrid = {
    sortAscending: '<i class="fa fa-sort-alpha-up"/>',
    sortDescending: '<i class="fa fa-sort-alpha-down"/>',
    first: '<i class="fas fa-fast-backward"/>',
    previous: '<i class="fas fa-angle-left" style="margin-right:10px;margin-left:5px"/>',
    next: '<i class="fas fa-angle-right" style="margin-right:5px";/> ',
    last: '<i class="fas fa-fast-forward"/>',
    sortUnSort: '<i class="fas fa-sort"></i>',
    filter: '<i class="fas fa-filter"></i>',
    groupContracted: '<i class="fas fa-chevron-right"></i>',
    menu: '<i class="fas fa-bars"></i>',
    smallLeft:'<i class="fas fa-chevron-left"></i>',
    smallRight: '<i class="fas fa-chevron-right"></i>',
    smallUp: '<i class="fas fa-chevron-up"></i>',
    smallDown: '<i class="fas fa-chevron-down"></i>',
    columnDrag: '<i class="fas fa-link"></i>',
    menuPin: '<i class="fas fa-link"></i>',
}

class FlsConstant {

    constructor() {}
        // get problem status
    static get problemStatus() { return problemStatus }
        // get submitted status
    static get SubmittedStatus() { return SubmittedStatus }
        // get status of student
    static get status() { return status }
    static get changeStatus() { return changeStatus }
    static get iconAgGrid() { return iconAgGrid }
}