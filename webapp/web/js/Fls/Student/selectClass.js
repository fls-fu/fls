$(document).ready(function() {

    var columnDefs = [
        { headerName: "ID", field: "id", width: 0, minWidth: 0, maxWidth: 0, hide: true },
        { headerName: "Name", field: "name", width: 100, minWidth: 80 },
        { headerName: "Language", field: "language" },
        {
            headerName: "Active",
            field: "status",
            width: 100,
            minWidth: 80,
            cellRenderer: function(params) {
                let value = params.data.status;
                if (value == 0) {
                    return '<span class="text-danger"><b>Disable</b></span>';
                } else if (value == 1) {
                    return '<span class="text-success"><b>Enable</b></span>';
                } else {
                    return "-";
                }
            },
        }
    ];

    // let the grid know which columns and what data to use
    var gridOptions = {
        columnDefs: columnDefs,
        rowSelection: 'single',
        'icons': FlsConstant.iconAgGrid,
        defaultColDef: {
            resizable: true
        },
        onGridReady: function(event) {
            setRowData(event);
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };

    function setRowData(event) {
        rowdata = JSON.parse($('#dom_data').attr('data-problem'));
        event.api.setRowData(rowdata);
    }

    // event row click
    function onSelectClass() {
        let selectedRows = gridOptions.api.getSelectedRows();
        let router = $('#dom_data').attr('data-route') + selectedRows[0].id;
        window.location = router;
    }

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_class');
    showTable(eGridDiv, gridOptions);
    gridOptions.api.addEventListener('rowClicked', onSelectClass);
});

function showTable(eGridDiv, gridOptions) {
    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
}