$(document).ready(function () {
    var updateDetail = setInterval(updateDetailProblemData, 2500);;
    var timeOutMousemove;
    document.onmousemove = function () {
        if (!updateDetail) {
            updateDetailProblemData();
            updateDetail = setInterval(updateDetailProblemData, 2500);
        }
        clearTimeout(timeOutMousemove);
        timeOutMousemove = setTimeout(function () {
            clearInterval(updateDetail);
            updateDetail = null;
        }, 300000);
    }

    $(".scrollbar").animate({
        scrollTop: $('.scrollbar').prop("scrollHeight")
    }, 10);
    $("#submit_problem_fileInput").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $("#bt_download").click(function () {
        $("#download_submission").click();
    });

    $('#btn_reuqest_change').click(function () {
        swal({
            title: "Confirm",
            text: "You want to request change this problem?",
            icon: "warning",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
        }).then(function (e) {
            if (e) {
                var rowdata = $('#dom_data').attr('data-problem-id');
                let router = $('#dom_data').attr('data-route-change-problem');
                var dataPass = JSON.stringify({
                    'probid': rowdata
                });
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: router,
                    data: dataPass,
                }).done(function (data) {
                    if (data.result) {
                        swal({
                            title: "Successfull",
                            text: "Request change problem is sent!",
                            icon: "success",
                            timer: 3000
                        }).then(function (dismiss) {
                            location.reload();
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "Class is not active!",
                            icon: "error",
                            timer: 3000
                        }).then(function (dismiss) {
                            location.reload();
                        });
                    }
                });
            }
        });
    });
    $('#askbox').keypress(function (e) {
        // Enter pressed?
        if (e.which == 10 || e.which == 13) {
            $('#sendAskButton').click();
        }
    });
});

let columnDefs = [{
        headerName: "Test Id",
        field: "id",
        width: 0,
        minWidth: 0,
        maxWidth: 0,
        hide: true
    },
    {
        headerName: "Status",
        width: 25,
        minWidth: 25,
        maxWidth: 25,
        cellRenderer: function (params) {
            let data = params.data;
            let icon = '';
            if (data.result == null) {
                icon = '<i class="far fa-question-circle"></i>';
            } else if (data.result != 'correct') {
                icon = '<i class="far fa-times-circle text-danger"></i>';
            } else {
                icon = '<i class="far fa-check-circle text-success"></i>';
            }
            return '<span>' + icon + '</span>'
        }
    },
    {
        headerName: "Name",
        field: "rank",
        width: 120,
        minWidth: 100,
        maxWidth: 300,
        cellRenderer: function (params) {
            return 'Test case ' + params.data.rank;
        }
    },
    {
        headerName: "Result",
        field: "result",
        width: 120,
        minWidth: 100,
        maxWidth: 300,
        cellClass: "grid-cell-centered",
        cellRenderer: function (params) {
            let val = params.data.result;
            if (val == null) {
                return '-';
            } else {
                if (val != 'correct') {
                    return '<span class="text-danger">' + val + '</span>';
                } else {
                    return '<span class="text-success">' + val + '</span>';
                }
            }
        }
    },
    {
        headerName: "Run Time",
        field: "runTime",
        width: 120,
        minWidth: 100,
        maxWidth: 300,
        cellClass: "grid-cell-centered",
        cellRenderer: function (params) {
            let val = params.data;
            if (val.runTime == null) {
                return '-';
            } else {
                if (val.result != 'correct') {
                    return '<span class="text-danger">' + val.runTime + '</span>';
                } else {
                    return '<span class="text-success">' + val.runTime + '</span>';
                }
            }
        }
    },
    {
        headerName: "Run Id",
        field: "runid",
        width: 0,
        minWidth: 0,
        maxWidth: 0,
        hide: true
    },
];

var dataDom = JSON.parse($('#dom_data').attr('data-test'));
dataDom = (dataDom != null) ? dataDom : [];
// let the grid know which columns and what data to use
var gridOptionsTetscase = {
    columnDefs: columnDefs,
    headerHeight: 0,
    rowData: dataDom,
    rowSelection: 'single',
    defaultColDef: {
        resizable: true
    },
    enableCellChangeFlash: true,
    onGridReady: function (event) {
        // setRowData(event);
        $(window).resize(function () {
            event.api.sizeColumnsToFit();
        });
        event.api.sizeColumnsToFit();
    },
    localeText: {
        noRowsToShow: 'No Test data'
    },
};

// event row click
function onTestcaseClick() {
    let selectedRows = gridOptionsTetscase.api.getSelectedRows();
    let judgingID = (selectedRows[0].runid != null) ? selectedRows[0].runid : 0;
    let testID = (selectedRows[0].id != null) ? selectedRows[0].id : '';
    let router = $('#dom_data').attr('data-route-detail-test').slice(0, -1) + judgingID + '/' + testID;
    // window.location = router;
    $.ajax({
        url: router
    }).done(function (data) {
        // add content
        $("#detail_test .modal-body").html(data);
        // open the other modal
        $("#detail_test").modal("show");
    });
}

// lookup the container we want the Grid to use
let eGridDiv = document.querySelector('#tb_test');

new agGrid.Grid(eGridDiv, gridOptionsTetscase);
gridOptionsTetscase.api.addEventListener('rowClicked', onTestcaseClick);

//ajax send new ask
function sendNewAsk() {
    var askContent = $('#askbox').val();
    var userproblemId = $('#dom_data').attr('data-userProblemID');
    if (askContent != "") {
        var newAsk = '<div class="m-1 message">' +
            ' <div class="message_bg_right float-right align-items-center">' +
            ' <div class="message_text"><pre>' + askContent + '</pre></div>' +
            '<span class="align-bottom message_time float-right">just now</span>' +
            '</div></div>';
        $("#ask_message_box").append(newAsk);
        $(".scrollbar").animate({
            scrollTop: $('.scrollbar').prop("scrollHeight")
        }, 1000);
        $('#askbox').val("");
        $.post($('#dom_data').attr('data-sendAskRoute'), {
                askContent: askContent,
                userProblemId: userproblemId
            },
            function (result) {});
        // swal({
        //     title: "Confirm send this request:",
        //     text: askContent,
        //     icon: "info",
        //     buttons: true,
        //     showCancelButton: true,
        //     dangerMode: true,
        //     confirmButtonText: "Yes!",
        //     cancelButtonText: "No!",
        // }).then(function (e) {
        //     if (e) {
        //         var newAsk = '<div class="m-1 message">' +
        //             ' <div class="message_bg_right float-right align-items-center">' +
        //             ' <div class="message_text">' + askContent + '</div>' +
        //             '<span class="align-bottom message_time float-right">just now</span>' +
        //             '</div></div>';
        //         $("#ask_message_box").append(newAsk);
        //         $(".scrollbar").animate({
        //             scrollTop: $('.scrollbar').prop("scrollHeight")
        //         }, 1000);
        //         $('#askbox').val("");
        //         $.post($('#dom_data').attr('data-sendAskRoute'), {
        //                 askContent: askContent,
        //                 userProblemId: userproblemId
        //             },
        //             function (result) {});
        //     }

        // })
    }

}

//ajax update data for student
function updateDetailProblemData() {
    var userproblemId = $('#dom_data').attr('data-userProblemID');
    var lastTime = $('#dom_data').attr('data-timeGetAsks');
    $.ajax({
        url: $('#studentAjaxRoute').data('update-url'),
        data: {
            lastTime: lastTime,
            userProblemID: userproblemId
        }
    }).done(function (json, status, jqXHR) {
        if (jqXHR.getResponseHeader('X-Login-Page')) {
            window.location = jqXHR.getResponseHeader('X-Login-Page');
        } else {
            // updateMenuClarifications(json.clarifications);
            updateChatBox(json.updateTime, json.ask);
            updateDataTest(json.listTest, json.testInfo);
            updateStatusChange(json.changeStatus, json.allowDowload, json.allowSubmit);
        }
    });
}

function updateDataTest(listTest, testInfo) {
    gridOptionsTetscase.api.setRowData(listTest);
    gridOptionsTetscase.api.sizeColumnsToFit();
    let width = ((testInfo.total != 0) ? testInfo.done / testInfo.total : 0) * 100;
    $('#progress_test').css('width', width + '%');
    let success = testInfo.done + '' + '/' + testInfo.total + '';
    $('#test_success').html(success);
    let execute = testInfo.do + '' + '/' + testInfo.total + '';
    $('#test_execute').html(execute);
}

function updateStatusChange(changeStatus, allowDowload, allowSubmit) {
    if (!allowDowload) {
        $('#dowload_submit_file').hide();
    }
    if (!allowSubmit) {
        $('#submit_file').hide();
    }

    if (changeStatus == 0 || changeStatus == null) {
        if (allowSubmit) {
            $('#btn_reuqest_change').show();
            $('#change_status').hide();
        }
    } else {
        if (changeStatus == 2) {
            $('#btn_reuqest_change').hide();
            $('#change_status').html('Change status: ' + FlsConstant.changeStatus[changeStatus]);
            $('#dowload_submit_file').hide();
            $('#submit_file').hide();
            $('#askbox').prop('disabled', true);
        }
        $('#btn_reuqest_change').hide();
        $('#change_status').html('Change status: ' + FlsConstant.changeStatus[changeStatus]);
    }
}

function updateChatBox(updateTime, data) {
    var num = data.length;
    if (num == 0) {} else {
        $('#dom_data').attr('data-timeGetAsks', updateTime);
        for (var i = 0; i < num; i++) {
            var newAsk = '<div class="m-1 message">' +
                ' <div class="message_bg_left float-left align-items-center">' +
                ' <div class="message_text"><pre>' + data[i].body + '</pre></div>' +
                '<span class="align-bottom message_time">' + convertTimestamp(data[i].submittime) + '</span>' +
                '</div></div>';
            $("#ask_message_box").append(newAsk);

            sendNotification('Your ask request has been reply.', {
                'tag': 'ask_' + data[i].probid,
                'link': domjudge_base_url + '/student/problem/detail/' + data[i].probid,
                'body': data[i].body
            });
        }
        $(".scrollbar").animate({
            scrollTop: $('.scrollbar').prop("scrollHeight")
        }, 1000);
    }
}

function convertTimestamp(timestamp) {
    var date = new Date(timestamp * 1000);
    // Year part from the timestamp
    var year = date.getFullYear();
    // Month part from the timestamp
    var month = date.getMonth() + 1;
    // Day part from the timestamp
    var day = date.getDate();
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    var formattedTime = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
}
