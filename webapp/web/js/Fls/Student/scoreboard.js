$(document).ready(function() {
    // fill class
    $('#selectClass').on('change', fillClass);
    let routeCharts = $('#dom_data').attr('data-route-charts');
    var dataChart = null;
    $.ajax({
        url: routeCharts
    }).done(function(data) {
        if (data.result != null) {
            if (data.result.error) {
                window.location.href = domjudge_base_url;
            } else {
                dataChart = data.result.subChar;
                if (dataChart.length == 0) {
                    $('#view_chart').attr('style', 'height: 80px !important');
                    $('.chart_title').attr('style', 'display: block !important');
                } else {
                    submissionCharts(dataChart);
                }
            }
        } else {
            location.reload();
        }
    });

});

function submissionCharts(dataChart) {
    let data = {
        labels: dataChart.time,
        datasets: [{
            data: dataChart.loc,
            label: "Loc",
            borderColor: dataChart.color,
            pointBackgroundColor: "#55bae7",
            fill: false,
            showLine: true,
            pointHoverBackgroundColor: "#000",
            pointHoverBorderColor: "#000",
            pointHoverBorderWidth: 8,
            steppedLine: 'before',
            pointBorderWidth: 6,
            // pointStyle: 'rectRot'
        }],
    }
    var ctx = document.getElementById('submitChart');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: true,
            onResize: resizable,
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                        return label;
                    }
                }
            },
        }
    });

    function resizable() {
        myLineChart.canvas.parentNode.style.height = '100%';
        myLineChart.canvas.parentNode.style.width = '100%';
    }
}

function fillClass() {
    let selected = $(this).children("option:selected").val();
    let route = $('#dom_data').attr('data-route-fill') + selected;
    $.ajax({
        url: route
    }).done(function(data) {
        if (data.result == 'reload') {
            location.reload();
        } else {
            if (data.result.length > 0) {
                setData(data.result);
            } else {
                setData([]);
            }
        }
    });
}

// table scoreboard
let columnDefs = [
    { headerName: "STT", valueGetter: 'node.id', width: 120, minWidth: 50, maxWidth: 300, suppressSizeToFit: true },
    { headerName: "ID", field: "userid", width: 0, minWidth: 0, maxWidth: 0, hide: true },
    { headerName: "Class ID", field: "cid", width: 0, minWidth: 0, maxWidth: 0, hide: true },
    { headerName: "Name", field: "name", width: 120, minWidth: 100 },
    {
        headerName: "Total loc",
        // field: "loc",
        width: 120,
        minWidth: 100,
        maxWidth: 300,
        cellRenderer: function(params) {
            let value = params.data;
            let width = (value.loc / value.requireLoc) * 100;
            return '<div class="progress mt-3">' +
                '<div class="progress-bar bg-success text-dark" role="progressbar" style="width: ' + width + '%" aria-valuenow="' + value.loc +
                '" aria-valuemin="0" aria-valuemax="' + value.requireLoc +
                '">' + value.loc + '/' + value.requireLoc + '</div></div>';
        }
    },
    { headerName: "Time", field: "time", width: 120, minWidth: 100, maxWidth: 300 },
    {
        headerName: "Status",
        // field: "result",
        width: 120,
        minWidth: 100,
        maxWidth: 300,
        cellRenderer: function(params) {
            let value = params.data.status;
            let classText = '';

            if (value == 1) {
                classText = "text-success";
            } else if (value == 0) {
                classText = "text-danger";
            } else {
                value = -1;
                classText = "text-dark";
            }
            return '<span class="' + classText + '"><b>' + FlsConstant.status[value] + '</b></span>';
        }
    },
];

var dataDom = JSON.parse($('#dom_data').attr('data-score'));
// let the grid know which columns and what data to use
var gridOptions = {
    columnDefs: columnDefs,
    rowData: dataDom,
    rowSelection: 'single',
    'icons': FlsConstant.iconAgGrid,
    defaultColDef: {
        resizable: true
    },
    enableCellChangeFlash: true,
    onGridReady: function(event) {
        $(window).resize(function() {
            event.api.sizeColumnsToFit();
        });
        event.api.sizeColumnsToFit();
    },
    localeText: { noRowsToShow: 'No data of scoreboard' },
};

function setData(rowdata) {
    gridOptions.api.setRowData(rowdata);
}

// event row click
function onRowClick() {
    let selectedRows = gridOptions.api.getSelectedRows();
    let router = $('#dom_data').attr('data-route-detail-score');
    let data = JSON.stringify({ 'userid': selectedRows[0].userid, 'cid': selectedRows[0].cid });
    // window.location = router;
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: router,
        data: data
    }).done(function(data) {
        // add content
        $("#detail_score .modal-body").html(data);
        // open the other modal
        $("#detail_score").modal("show");
    });
}

// lookup the container we want the Grid to use
let eGridDiv = document.querySelector('#tb_score_board');

showTable(eGridDiv, gridOptions);
gridOptions.api.addEventListener('rowClicked', onRowClick);

function showTable(eGridDiv, gridOptions) {
    // setup the grid after the page has finished loading
    var table = new agGrid.Grid(eGridDiv, gridOptions);
    return table;
}