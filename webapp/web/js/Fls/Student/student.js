$(document).ready(function() {

    var updateStudent = setInterval(updateHomeStudent, 2500);;
    var timeOutMousemove;
    document.onmousemove = function(){
        if(!updateStudent){
            updateHomeStudent();
            updateStudent = setInterval(updateHomeStudent, 2500);
        }
        clearTimeout(timeOutMousemove);
        timeOutMousemove = setTimeout( function(){
            clearInterval(updateStudent);
            updateStudent = null;
        }, 300000);
    }

    let data = JSON.parse($('#dom_data').attr('data-charts'));
    problemChart(data);
});

var columnDefs = [
    { headerName: "No", valueGetter: 'node.id', width: 100, minWidth: 50, maxWidth: 100, sortable: false, suppressMenu: true,},
    { headerName: "ID", field: "ID", width: 0, minWidth: 0, maxWidth: 0, hide: true },
    { headerName: "Code", field: "name", width: 100, minWidth: 80, maxWidth: 200,sortable: false, },
    { headerName: "Name", field: "content", minWidth: 160 ,unSortIcon: true},
    { headerName: "Loc", field: "point", width: 100, minWidth: 80, maxWidth: 200 ,unSortIcon: true, sort:'asc'},
    {
        headerName: "Result",
        // field: "result",
        width: 100,
        minWidth: 80,
        maxWidth: 200,
        sortable: false,
        suppressMenu: true,
        cellRenderer: function(params) {
            let result = params.data.result;
            let changeStatus = params.data.status;
            let classText = '';
            if (changeStatus != null && changeStatus == 2) {
                classText = "text-success";
                return '<span class="' + classText + '"><b>' + FlsConstant.changeStatus[changeStatus] + '</b></span>';
            } else {
                if (result == 1) {
                    classText = "text-warning";
                } else if (result == 2) {
                    classText = "text-primary";
                } else if (result == 3) {
                    classText = "text-info";
                } else if (result == 4) {
                    classText = "text-success";
                } else if (result == 5) {
                    classText = "text-danger";
                } else {
                    result = (result == null) ? 0 : result;
                    classText = "text-dark";
                }
                value = '<span class="' + classText + '"><b>' + FlsConstant.problemStatus[result] + '</b></span>';
            }
            return value;
        },
    },
    {
        headerName: "Change",
        // field: "result",
        width: 100,
        minWidth: 80,
        maxWidth: 200,
        cellRenderer: function(params) {
            let value = params.data.status;
            let classText = '';
            if (value == 0) {
                classText = "text-info";
            } else if (value == 1) {
                classText = "text-success";
            } else if (value == 2) {
                classText = "text-danger";
            } else if (value == 3) {
                classText = "text-primary";
            } else {
                value = (value == null) ? -1 : value;
                classText = "text-dark";
            }
            return '<span class="' + classText + '"><b>' + FlsConstant.changeStatus[value] + '</b></span>';

        },
        hide: true
    }
];

// let the grid know which columns and what data to use
var gridOptionsProblem = {
    columnDefs: columnDefs,
    rowSelection: 'single',
    defaultColDef: {
        resizable: true,
        sortable: true,
        filter: true
    },
    onGridReady: function(event) {
        setProblemData(event);
        $(window).resize(function() {
            event.api.sizeColumnsToFit();
        });
        event.api.sizeColumnsToFit();
    },
    localeText: { noRowsToShow: 'No problem' },
    'icons': FlsConstant.iconAgGrid,
};

function setProblemData(event) {
    let rowdata = JSON.parse($('#dom_data').attr('data-problem'));
    event.api.setRowData(rowdata);
}

// event row click
function onSelectionChanged() {
    let selectedRows = gridOptionsProblem.api.getSelectedRows();
    // check changed problem
    if (selectedRows[0].status == 2) {
        swal({
            title: "Error",
            text: "The problem is changed, Please choose other problem!",
            icon: "error",
            button: "OK!",
        });
    } else {
        let router = $('#dom_data').attr('data-route-detail') + selectedRows[0].id;
        window.location = router;
    }
}

// lookup the container we want the Grid to use
var eGridDiv = document.querySelector('#tb_problem');
showTable(eGridDiv, gridOptionsProblem);
gridOptionsProblem.api.addEventListener('rowClicked', onSelectionChanged);

var columnProblemDefs = [
    { headerName: "no", valueGetter: 'node.id', width: 100, minWidth: 80, maxWidth: 200, sortable: false, suppressMenu: true, },
    { headerName: "Code", field: "shortname", width: 100, minWidth: 80, maxWidth: 200, sortable: false },        
    { headerName: "ID", field: "id", width: 0, minWidth: 0, maxWidth: 0, hide: true },
    { headerName: "Problem", field: "problem", unSortIcon: true},
    { headerName: "Date", field: "time", width: 150, minWidth: 80, maxWidth: 300 ,sortable: true },
    {
        width: 150,
        minWidth: 100,
        maxWidth: 300,
        headerName: "result",
        suppressMenu: true,
        cellRenderer: function(params) {
            let value = params.data.result;
            value = (value != null) ? value : 'pending';
            let classText = '';
            if (value == 'correct') {
                classText = "text-success";
            } else {
                classText = "text-danger";
            }
            return '<span class="' + classText + '"><b>' + FlsConstant.SubmittedStatus[value] + '</b></span>';
        },
        sortable: false,
    },
];

// let the grid know which columns and what data to use
var submittedGridOptions = {
    columnDefs: columnProblemDefs,
    rowSelection: 'single',

    defaultColDef: {
        resizable: true,
        sortable: true,
        filter: true
    },
    onGridReady: function(event) {
        setSubmissionData(event);
        $(window).resize(function() {
            event.api.sizeColumnsToFit();
        });
        event.api.sizeColumnsToFit();
    },
    localeText: { noRowsToShow: 'No submitted' },
    animateRows: true,
    'icons': FlsConstant.iconAgGrid,
    'pagination': true,
    'paginationPageSize': 5,
};

function setSubmissionData(event) {
    let rowdata = JSON.parse($('#dom_data').attr('data-submitted'));
    event.api.setRowData(rowdata);
}

// event row click
function onSubmittedClick(params) {
    let selectedRows = params.api.getSelectedRows();
    let router = $('#dom_data').attr('data-route-submitted') + selectedRows[0].id;
    // window.location = router;
    $.ajax({
        url: router
    }).done(function(data) {
        // add content
        $("#detail_submit .modal-body").html(data);
        // open the other modal
        $("#detail_submit").modal("show");
    });
}

    // lookup the container we want the Grid to use
var GridDivTbsubmitted = document.querySelector('#tb_submitted');
showTable(GridDivTbsubmitted, submittedGridOptions);
submittedGridOptions.api.addEventListener('rowClicked', onSubmittedClick);

function showTable(eGridDiv, gridOptions) {
    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
}

function problemChart(data) {
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: data['labels'],
            datasets: [{
                label: '',
                data: data['data'],
                backgroundColor: data['color']
            }],
            option: {
                responsive: true,
                maintainAspectRatio: true,
                onResize: resizable
            }
        }
    });

    function resizable() {
        myChart.canvas.parentNode.style.height = '100%';
        myChart.canvas.parentNode.style.width = '100%';
    }
}

function updateHomeStudent() {
    $.ajax({
        url: $('#studentAjaxRoute').data('update-url'),
        data: {
            target: 'home'
        }
    }).done(function (json, status, jqXHR) {
        if (jqXHR.getResponseHeader('X-Login-Page')) {
            window.location = jqXHR.getResponseHeader('X-Login-Page');
        } else {
            if(json.result != null && !json.result){
                window.location.reload();
            }else{
                update(json);
            }            
        }
    });
}

function update(data) {
    let dataChart = JSON.parse($('#dom_data').attr('data-charts'));
    let dataSubmission = JSON.parse($('#dom_data').attr('data-submitted'));
    let dataProblem = JSON.parse($('#dom_data').attr('data-problem'));
    // update data chart if have change
    if(!(_.isEqual(dataChart, data.charts))){
        problemChart(data.charts);
        $('#dom_data').attr('data-charts',JSON.stringify(data.charts));
    }
    // update data submission if have change
    if(!(_.isEqual(dataSubmission, data.submitted))){
        submittedGridOptions.api.setRowData(data.submitted);
        submittedGridOptions.api.sizeColumnsToFit();
        $('#dom_data').attr('data-submitted', JSON.stringify(data.submitted));
    }
    // update data problem if have change
    if(!(_.isEqual(dataProblem, data.problems))){
        gridOptionsProblem.api.setRowData(data.problems);
        gridOptionsProblem.api.sizeColumnsToFit();
        $('#dom_data').attr('data-problem', JSON.stringify(data.problems));
    }
}