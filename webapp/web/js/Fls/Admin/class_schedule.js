$(document).ready(function() {
    setTable();
    setRowData();
    $('#show_add_scheduling').on('click', function() {
        addScheduling($(this), 'show');
    });
});

function addScheduling(param, mode) {
    let dataAjax;
    let router = $('#dom_data').attr('data-route-add-schedule');
    if (mode == 'show') {
        let cid = param.attr('data-cid');
        dataAjax = JSON.stringify({ 'cid': cid, 'mode': mode });
    } else {
        let cid = $('#hide_data').attr('data-cid');
        let date = $('#schedule_date').val();
        let roomid = $('#input_room').children("option:selected").val();
        let ipid = $('#input_slot').children("option:selected").val();
        dataAjax = JSON.stringify({ 'cid': cid, 'mode': mode ,'date':date,'roomid':roomid,'slotid':ipid});
    }

    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: router,
        data: dataAjax,
    }).done(function(data) {
        if(typeof data.error_date !== 'undefined' && data.error_date != ''){
            $('#date_error').removeClass('d-none');
            $('#date_error').html(data.error_date);
        }else{
            if($('#date_error').hasClass('d-none')){
                $('#date_error').addClass('d-none');
                $('#date_error').html('');
            }
            if (typeof data.success !== 'undefined' && data.success != '') {
                swal({
                    title: "Successfull!",
                    text: data.success,
                    icon: "success",
                    button: "OK!",
                }).then(function(dismiss) {
                    location.reload();
                });
            } else if (typeof data.error !== 'undefined' && data.error != '') {
                swal({
                    title: "Error!",
                    text: data.error,
                    icon: "error",
                    button: "OK!",
                });
            } else {
                // add content
                $("#add_scheduling .modal-body").html(data);
                // open the other modal
                $("#add_scheduling").modal("show");
                handleBtn();
            }
        }
    });
}

function handleBtn() {
    $( ".datepicker" ).datepicker({
        showButtonPanel: true,
        dateFormat: 'dd/mm/yy',
        beforeShow: function() {
            setTimeout(function(){
                $('.ui-datepicker').css('z-index', 9999);
            }, 0);
        }
    });
    $('#add_schedule').click(function () { 
        addScheduling($(this), 'add');
    });
}

function setTable() {
    var columnDefs = [
        { headerName: "ID", field: "id", hide: true },
        { headerName: "Date", field: "date", width: 230 },
        { headerName: "Room", field: "room", width: 230 },
        {
            headerName: "Slot",
            field: "slot",
            width: 230,
            sortable: false,
            suppressMenu: true,
            'cellRenderer': function(params) {
                let data = params.data;
                return params.data.slot + ' ' + params.data.detail;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        defaultColDef: {
            resizable: true,
            sortable: true,
            filter: true
        },
        'columnDefs': columnDefs,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'icons': FlsConstant.iconAgGrid,
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'navigateToNextCell': function(params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function(event) {
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
        'pagination': true,
        'paginationPageSize': 10,
    };

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_schedules');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    // gridOptions.api.addEventListener('cellDoubleClicked', GridPress);
    // gridOptions.api.addEventListener('cellMouseOver', RowSelection);
}

function setRowData() {
    var rowdata = JSON.parse($('#dom_data').attr('data-list-schedules'));
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    gridOptions.api.setFocusedCell(0, "cId");
    gridOptions.api.selectionController.selectIndex(0, true);
    gridOptions.api.sizeColumnsToFit();
}