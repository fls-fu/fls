$(document).ready(function () {
    if (document.getElementById("tb_room_ip") != null) {
        setTable();
        setRowData();
    }

    //when click ok button, reload
    // $(".btnOk").click(function () {
    //     window.location.reload(true);
    // })
    //when click cancel button, reload
    $(".btnCancel").click(function () {
        window.location.reload(true);
    });
    $("#btnAddPositions").click(function () {
        resetMessage();
        $('#ipAddress').val('');
        $('#pcCode').val('');
    });
    
    $("#btn_sendAjax").click(function () {
        resetMessage();
        let ipAddress = $('#ipAddress').val();
        let pcCode = $('#pcCode').val();
        let roomid = $('#roomid').val();
        let dataPass = JSON.stringify({
            'ipAddress': ipAddress,
            'pcCode': pcCode,
            'roomid': roomid
        });
        var router = $('#dom_data_route').attr('data-route-add-room-ip');
        $.ajax({
            contentType: 'application/json; charset=utf-8',            
            type: 'POST',
            url: router,
            data: dataPass,
            dataType: 'json'
        }).done(function (data) {
            if (data.status) {
                $('#add_success_span').text(data.message);            
            }else {
                if(data.field == 'roomid'){
                    window.location.reload();
                }else{
                    let field = '#'+data.field+"_error";
                    $(field).text(data.message);    
                } 
            }
        });
    })
    //change submit
    $("#btn_sendAjaxChange").click(function () {
        resetMessage();
        let ipAddress = $('#ipAddress1').val();
        let pcCode = $('#pcCode1').val();
        let roomid = $('#roomid1').val();
        let ipid = $('#ipid').val();
        let dataPass = JSON.stringify({
            'ipid': ipid,
            'ipAddress': ipAddress,
            'pcCode': pcCode,
            'roomid': roomid
        });
        var router = $('#dom_data_route').attr('data-route-edit-room-ip');
        $.ajax({
            contentType: 'application/json; charset=utf-8',            
            type: 'POST',
            url: router,
            data: dataPass,
            dataType: 'json'
        }).done(function (data) {
            if (data.status) {
                $('#change_success_span').text(data.message);            
            }else {
                if(data.field == 'roomid'){
                    window.location.reload();
                }else{
                    let field = '#'+data.field+"_error1";
                    $(field).text(data.message);    
                }                
            }
        });
    })
});

function resetMessage() {
    $('#ipAddress_error').text('');
    $('#pcCode_error').text('');
    $('#add_success_span').text('');
    $('#ipAddress_error1').text('');
    $('#pcCode_error1').text('');
    $('#change_success_span').text('');
}

function setTable() {
    var columnDefs = [{
            headerName: "Positions ID",
            field: "ipid",
            width: 150,
            maxWidth: 200
        },
        {
            headerName: "Ip Address",
            field: "ipAddress"
        },
        {
            headerName: "PC Code",
            field: "pccode"
        },
        {
            headerName: "Edit",
            cellRenderer: function (params) {
                let data = params.data;
                let button = '<button data-id="' + data.ipid 
                    + '" data-ip="' + data.ipAddress 
                    + '" data-pccode="' + data.pccode 
                    + '" type="button" class="btn btn-success ml-2 btn_edit" data-toggle="modal" data-target="#editLabroom" title="edit" > <i class="fas fa-edit"></i> </button>';
                // <button type="button" id="btnAddSemester" class="btn btn-success" data-toggle="modal" data-target="#addSemester" aria-expanded="false">
                return button;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        'columnDefs': columnDefs,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'icons': FlsConstant.iconAgGrid,
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'navigateToNextCell': function (params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function (event) {
            $(window).resize(function () {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_room_ip');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    // gridOptions.api.addEventListener('rowDoubleClicked', GridPress);
    gridOptions.api.addEventListener('cellMouseOver', onRowClick);
    setRowData();
}

function setRowData() {
    let rowdata = JSON.parse($('#dom_data').attr('data-roomip'));
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    // gridOptions.api.selectionController.selectIndex(0, true);
    gridOptions.api.sizeColumnsToFit();
    $("#rows").text(rowdata.length);
}

// event row click
function onRowClick() {
    $('.btn_edit').unbind('click');
    $('.btn_edit').click(function () {
        resetMessage();
        gridOptions.api.addEventListener('rowSelected', onRowClick);
        let ipAddress = $(this).attr('data-ip');
        $('#ipAddress1').val(ipAddress);        
        let room_id = $(this).attr('data-pccode');
        $('#pcCode1').val(room_id);
        let ipid = $(this).attr('data-id');
        $('#ipid').val(ipid);
        
    });
};

function convertTimestamp(timestamp) {
    var date = new Date(timestamp * 1000);
    // Year part from the timestamp
    var year = date.getFullYear();
    // Month part from the timestamp
    var month = String(date.getMonth() + 1).padStart(2, '0');
    // Day part from the timestamp
    var day = String(date.getDate()).padStart(2, '0');
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    var totalDate = day + '/' + month + '/' + year + '';
    return totalDate;
}
