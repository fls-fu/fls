$(document).ready(function () {
    setTable();
    setRowData();
    $("#import_users").click(function () {
        $("#Users_File").click();
        $("#Users_File").on('change', function (e) {
            var formdata = new FormData($('#Users_FileUploadForm').get(0));
            let router = $('#dom_data').attr('data-route-import');
            formdata.append('name', $('#Users_File').val().replace(/.*(\/|\\)/, ''));
            $.ajax({
                    url: router,
                    type: "POST",
                    data: formdata,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json'
                })
                .done(function (data) {
                    if (data.uploadStatus) {
                        // $('#add_success_span').text(data.message);
                        swal({
                            title: "Successfully",
                            text: data.message,
                            icon: "success",
                            button: "OK!",
                        }).then(function (dismiss) {
                            window.location.reload(true)
                        });                        
                    } else {
                        swal({
                            title: "Fail",
                            text: data.message,
                            icon: "error",
                            button: "OK!",
                        });

                    }
                });
        });
    })
});

function setTable() {
    var columnDefs = [{
            headerName: "ID",
            field: "uId"
        },
        {
            headerName: "Username",
            field: "uUsername",
            width: 230
        },
        {
            headerName: "Fullname",
            field: "uName",
            width: 230
        },
        {
            headerName: "Role",
            field: "uRole"
        },
        {
            headerName: "Status",
            field: "uStatus",
            'cellRenderer': function (params) {

                let value = params.data.uStatus;
                let classText = '';

                if (value == 'Enable') {
                    classText = "text-success";
                } else if (value == 'Disable') {
                    classText = "text-danger";
                } else {
                    value = -1;
                    classText = "text-dark";
                }
                return '<span class="' + classText + '"><b>' + value + '</b></span>';
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        'columnDefs': columnDefs,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'pagination': true,
        'paginationPageSize': 50,
        'paginationNumberFormatter': function (params) {
            return '[' + params.value.toLocaleString() + ']';
        },
        'icons': FlsConstant.iconAgGrid,
        'navigateToNextCell': function (params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function (event) {
            $(window).resize(function () {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_users');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    setRowData();
    gridOptions.api.addEventListener('cellDoubleClicked', GridPress);
    gridOptions.api.addEventListener('rowClicked', RowSelection);

}

function setRowData() {
    let rowdata = JSON.parse($('#dom_data').attr('data-list-users'));
    for (var i = 0; i < rowdata.length; i++) {
        rowdata[i]["uRole"] = rowdata[i].uRole["value"];
    }
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    gridOptions.api.setFocusedCell(0, "cId");
    gridOptions.api.selectionController.selectIndex(0, true);
}

function GridPress(event) {
    let selectedRows = event.data;
    let router = $('#dom_data').attr('data-route-detail') + selectedRows.uId;
    window.location = router;
}

function RowSelection(event) {

}

function onFilterTextBoxChanged() {
    gridOptions.api.setQuickFilter(document.getElementById('filter-text-box').value);
}

function onPrintQuickFilterTexts() {
    gridOptions.api.forEachNode(function (rowNode, index) {
        console.log('Row ' + index + ' quick filter text is ' + rowNode.quickFilterAggregateText);
    });
}

function onQuickFilterTypeChanged() {
    var rbCache = document.querySelector('#cbCache');
    var cacheActive = rbCache.checked;
    console.log('using cache = ' + cacheActive);
    gridOptions.cacheQuickFilter = cacheActive;
    // set row data again, so to clear out any cache that might of existed
    gridOptions.api.setRowData(createRowData());
}

function onPageSizeChanged(newPageSize) {
    var value = document.getElementById('page-size').value;
    gridOptions.api.paginationSetPageSize(Number(value));
}
