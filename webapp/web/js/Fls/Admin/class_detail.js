$(document).ready(function() {
    listStudents_Update = [];
    setTable();
    setTable2();
    setTableTeacher();
    setRowData();
    $("#btn_del").click(function() {
        swal({
                title: "Are you sure?",
                text: "Deleted data will not be recoverable",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    onRemoveSelected();
                    // swal("The student has been removed from class!", {
                    //   icon: "success",
                    // });
                } else {
                    // swal("You canceled the delete command!");
                }
            });
    });

    $("#btn_save").click(function() {
        swal({
                title: "Are you sure?",
                text: "Do you want to update the changes?",
                icon: "warning",
                buttons: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    saveData();
                    swal("Data has been updated!", {
                        icon: "success",
                    });
                } else {
                    swal("You canceled the delete command!");
                }
            });
    });
    //Add all student be choose
    $("#btnOk").click(function() {
        AddStudent();
    });
    // $("#btnAdd").click(function () {
    //     updateDuplicate();
    // });
    gridOptions2.api.setRowData([]);
    $('#search_student').keydown(function() {
        SearchStudent($(this));
    });
    $('#search_teacher').keydown(function() {
        SearchTeacher($(this));
    });

    $("#add_teacher").on('shown.bs.modal', function() {
        addTeacher(null, 'show');
    });
});

function SearchStudent(param) {
    let router = param.attr('data-route');
    let textSearch = param.val().trim();
    let dataAjax = JSON.stringify({ 'searchText': textSearch });
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: router,
        data: dataAjax,
    }).done(function(data) {
        if (data.result != null) {
            gridOptions2.api.setRowData(data.result);
            gridOptions2.api.sizeColumnsToFit();
        } else {
            gridOptions2.api.setRowData([]);
            gridOptions2.api.sizeColumnsToFit();
        }
    });
}

function addStudent(param) {
    let router = $('#hide_data').attr('data-route-add');
    let cid = $('#hide_data').attr('data-cid');
    let userid = param.attr('data-userid');
    let dataAjax = JSON.stringify({ 'userid': userid, 'cid': cid });
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: router,
        data: dataAjax,
    }).done(function(data) {
        if (typeof data.result.success !== 'undefined' && data.result.success != '') {
            swal({
                title: "Successfull!",
                text: data.result.success,
                icon: "success",
                button: "OK!",
            }).then(function(dismiss) {
                location.reload();
            });
        } else if (typeof data.result.error !== 'undefined' && data.result.error != '') {
            swal({
                title: "Error!",
                text: data.result.error,
                icon: "error",
                button: "OK!",
            });
        }
    });
}

function SearchTeacher(param) {
    let router = param.attr('data-route');
    let textSearch = param.val().trim();
    let dataAjax = JSON.stringify({ 'searchText': textSearch });
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: router,
        data: dataAjax,
    }).done(function(data) {
        if (data.result != null) {
            gridOptionsTeacher.api.setRowData(data.result);
            gridOptionsTeacher.api.sizeColumnsToFit();
        } else {
            gridOptionsTeacher.api.setRowData([]);
            gridOptionsTeacher.api.sizeColumnsToFit();
        }
    });
}

function addTeacher(param, mode) {
    let cid = $('#hide_data_teacher').attr('data-cid');
    let router = $('#hide_data_teacher').attr('data-route-add');
    let dataAjax;
    if (mode != 'show') {
        let tid = param.attr('data-userid');
        dataAjax = JSON.stringify({ 'tid': tid, 'cid': cid, 'mode': mode });
    } else {
        dataAjax = JSON.stringify({ 'cid': cid, 'mode': mode });
    }

    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: router,
        data: dataAjax,
    }).done(function(data) {
        if (typeof data.result.success !== 'undefined' && data.result.success != '') {
            swal({
                title: "Successfull!",
                text: data.result.success,
                icon: "success",
                button: "OK!",
            }).then(function(dismiss) {
                location.reload();
            });
        } else if (typeof data.result.teacher_old !== 'undefined' && data.result.teacher_old != '') {
            $('#teacher_old').html(data.result.teacher_old);
            $('#view_teacher_old').removeClass('d-none');
        } else if (typeof data.result.error !== 'undefined' && data.result.error != '') {
            swal({
                title: "Error!",
                text: data.result.error,
                icon: "error",
                button: "OK!",
            });
        }
    });
}

function hanndleBtnStudent() {
    $('.btn_add_student').unbind('click');
    $('.btn_add_student').click(function() {
        addStudent($(this));
    });
}

function hanndleBtnTeacher() {
    $('.btn_add_teacher').unbind('click');
    $('.btn_add_teacher').click(function() {
        addTeacher($(this), 'add');
    });
}

function setTable() {
    var columnDefs = [
        { headerName: "ID", field: "uId", hide: true },
        { headerName: "No", field: "no" },
        { headerName: "Username", field: "uUsername" },
        { headerName: "Name", field: "uName", width: 230 },
        { headerName: "Email", field: "uEmail", width: 350 },
        { headerName: "Role", field: "role", width: 350 },
    ];

    gridOptions = {
        'columnDefs': columnDefs,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'multiple',
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'icons': FlsConstant.iconAgGrid,
        'navigateToNextCell': function(params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            gridOptions.api.deselectAll();
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function(node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            gridOptions.api.deselectAll();
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function(event) {
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
        'pagination': true,
        'paginationPageSize': 15,
    };

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_class_detail');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
}
//table students
function setTable2() {
    var columnDefs2 = [
        { headerName: "ID", field: "id" },
        { headerName: "Username", field: "username", width: 230 },
        { headerName: "Fullname", field: "name", width: 230 },
        {
            headerName: "Action",
            minWidth: 200,
            sortable: false,
            suppressMenu: true,
            'cellRenderer': function(params) {
                let data = params.data;
                button = '<button type="button" data-userid="' + data.id + '" class="btn btn-success  ml-2 btn_add_student" data-toggle="tooltip" data-placement="top" title="Add to class" > <i class="far fa-plus-square"></i> </button>'
                return button;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions2 = {
        'columnDefs': columnDefs2,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'navigateToNextCell': function(params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions2.rowData.length - 1;
                    gridOptions2.api.forEachNode(function(node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            gridOptions2.api.deselectAll();
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions2.rowData.length - 1;
                    gridOptions2.api.forEachNode(function(node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            gridOptions2.api.deselectAll();
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function(event) {
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
        'icons': FlsConstant.iconAgGrid,
        'pagination': true,
        'paginationPageSize': 10,
    };

    // lookup the container we want the Grid to use
    var eGridDiv2 = document.querySelector('#tb_students');
    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv2, gridOptions2);
    gridOptions2.api.addEventListener('cellMouseOver', hanndleBtnStudent);
}

function setTableTeacher() {
    var columnDefsTeacher = [
        { headerName: "ID", field: "id" },
        { headerName: "Username", field: "username", width: 230 },
        { headerName: "Fullname", field: "name", width: 230 },
        {
            headerName: "Action",
            minWidth: 200,
            sortable: false,
            suppressMenu: true,
            'cellRenderer': function(params) {
                let data = params.data;
                button = '<button type="button" data-userid="' + data.id + '" class="btn btn-success  ml-2 btn_add_teacher" data-toggle="tooltip" data-placement="top" title="Add to class" > <i class="far fa-plus-square"></i> </button>'
                return button;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptionsTeacher = {
        'columnDefs': columnDefsTeacher,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'navigateToNextCell': function(params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions2.rowData.length - 1;
                    gridOptions2.api.forEachNode(function(node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            gridOptions2.api.deselectAll();
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions2.rowData.length - 1;
                    gridOptions2.api.forEachNode(function(node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            gridOptions2.api.deselectAll();
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function(event) {
            $(window).resize(function() {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
        'icons': FlsConstant.iconAgGrid,
        'pagination': true,
        'paginationPageSize': 10,
    };

    // lookup the container we want the Grid to use
    var eGridDivTeacher = document.querySelector('#tb_teacher');
    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDivTeacher, gridOptionsTeacher);
    gridOptionsTeacher.api.addEventListener('cellMouseOver', hanndleBtnTeacher);
}

function setRowData() {
    //Set data for tbl detail class
    let rowdata1 = JSON.parse($('#dom_data').attr('data-list-users'));
    //var no = 0;
    setNo(rowdata1);
    gridOptions.rowData = rowdata1;
    gridOptions.api.setRowData(rowdata1);
    gridOptions.api.setFocusedCell(0, "uId");
    gridOptions.api.selectionController.selectIndex(0, true);
}
//Remove selected data
function onRemoveSelected() {
    var selectedData = gridOptions.api.getSelectedRows();
    var listUsers = JSON.stringify(selectedData);
    var route = $('#dom_data').attr('data-route-del');
    gridOptions.api.updateRowData({ remove: selectedData });

    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: route,
        data: listUsers,
    }).done(function(data) {
        if (data.isDelete) {
            swal("The student has been removed from class!", {
                icon: "success",
            });
        } else {
            swal("You canceled the delete command!");
        }
    });
}

function onFilterTextBoxChanged() {
    gridOptions2.api.setQuickFilter(document.getElementById('filter-text-box').value);
}


function onQuickFilterTypeChanged() {
    var rbCache = document.querySelector('#cbCache');
    var cacheActive = rbCache.checked;
    console.log('using cache = ' + cacheActive);
    gridOptions2.cacheQuickFilter = cacheActive;
    // set row data again, so to clear out any cache that might of existed
    gridOptions2.api.setRowData(createRowData());
}

function AddStudent() {
    //Validate student exist or not
    var isExist = false;
    var lastIndex;
    var rowdata = gridOptions.rowData;
    var rowSelected = gridOptions2.api.getSelectedRows();
    //Validate student exist or not
    for (var i = 0; i < rowdata.length; i++) {
        if (rowSelected[0].uId == rowdata[i].uId) {
            swal({
                title: "Duplicate",
                text: "This student is exist in class!",
                icon: "warning",
                button: "OK!",
            });
            isExist = true;
        }
    }
    if (isExist == false) {
        for (var i = 0; i < rowSelected.length; i++) {
            rowdata.push(rowSelected[i]);
            lastIndex = rowdata.length;
            listStudents_Update.push(rowSelected[i]);
        }
        setNo(rowdata);
        gridOptions.api.setRowData(rowdata);

        gridOptions.api.setFocusedCell(lastIndex - 1, "uId");
        gridOptions.api.selectionController.selectIndex(lastIndex - 1, true);
    }
}

function setNo(data) {
    for (i = 0; i < data.length; i++) {
        data[i]["no"] = (i + 1);
    }
}