$(document).ready(function () {
    if (document.getElementById("tb_labroom") != null) {
        setTable();
        setRowData();
    }

    //when click cancel button, reload
    $(".btnCancel").click(function () {
        window.location.reload();
    });

    $("#btn_sendAjax").click(function () {
        $('#add_success_span').text('');
        $('#name_danger_span').text('');
        var roomName = $('#sem-name').val();
        var dataPass = JSON.stringify({
            'roomName': roomName
        });
        var router = $('#dom_data_route').attr('data-route-add-room');
        $.ajax({
            contentType: 'application/json; charset=utf-8',            
            type: 'POST',
            url: router,
            data: dataPass,
            dataType: 'json'
        }).done(function (data) {
            if (data.status) {
                $('#add_success_span').text(data.message);            
            }else {
                if(data.field == 'name') {
                    $('#name_danger_span').text(data.message);
                }             
            }
        });
    })
    //change submit
    $("#btn_sendAjaxChange").click(function () {
        $('#name_change_danger_span').text('');
        $('#date_change_danger_span').text('');
        $('#change_success_span').text('');    
        var roomId = $('#room-id').val();
        var roomName = $('#room-name-change').val();
        var dataPass = JSON.stringify({
            'roomId': roomId,
            'roomName': roomName
        });
        var router = $('#dom_data_route').attr('data-route-edit-room');
        $.ajax({
            contentType: 'application/json; charset=utf-8',            
            type: 'POST',
            url: router,
            data: dataPass,
            dataType: 'json'
        }).done(function (data) {
            if (data.status) {
                $('#change_success_span').text(data.message);            
            }else {
                if(data.field == 'name') {
                    $('#name_change_danger_span').text(data.message);
                }else if(data.field == 'date') {
                    $('#date_change_danger_span').text(data.message);
                }                
            }
        });
    })
});

function setTable() {
    var columnDefs = [{
            headerName: "Room ID",
            field: "roomid"
        },
        {
            headerName: "Room Name",
            field: "roomname"
        },
        {
            headerName: "Edit",
            cellRenderer: function (params) {
                let data = params.data;
                let button = '<button data-id="' + data.roomid + '" data-name="' + data.roomname + '" type="button" class="btn btn-success ml-2 btn_edit" data-toggle="modal" data-target="#editLabroom" title="edit" > <i class="fas fa-edit"></i> </button>';
                // <button type="button" id="btnAddSemester" class="btn btn-success" data-toggle="modal" data-target="#addSemester" aria-expanded="false">
                return button;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        'columnDefs': columnDefs,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'icons': FlsConstant.iconAgGrid,
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'navigateToNextCell': function (params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function (event) {
            $(window).resize(function () {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_labroom');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    gridOptions.api.addEventListener('rowDoubleClicked', GridPress);
    gridOptions.api.addEventListener('cellMouseOver', onRowClick);
    setRowData();
}

function setRowData() {
    let rowdata = JSON.parse($('#dom_data').attr('data-semester'));
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    // gridOptions.api.selectionController.selectIndex(0, true);
    gridOptions.api.sizeColumnsToFit();
    $("#rows").text(rowdata.length);
}

function GridPress(event) {
    let selectedRows = gridOptions.api.getSelectedRows();
    let router = $('#dom_data_route').attr('data-route-roomip') + selectedRows[0].roomid;
    window.location = router;
}

// event row click
function onRowClick() {
    $('.btn_edit').unbind('click');
    $('.btn_edit').click(function () {
        gridOptions.api.addEventListener('rowSelected', onRowClick);
        var room_id = $(this).attr('data-id');
        $('#room-id').val(room_id);
        var room_name = $(this).attr('data-name');
        $('#old_name_span').text(room_name);
        $('#room-name-change').val(room_name);
    });
};

function convertTimestamp(timestamp) {
    var date = new Date(timestamp * 1000);
    // Year part from the timestamp
    var year = date.getFullYear();
    // Month part from the timestamp
    var month = String(date.getMonth() + 1).padStart(2, '0');
    // Day part from the timestamp
    var day = String(date.getDate()).padStart(2, '0');
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    var totalDate = day + '/' + month + '/' + year + '';
    return totalDate;
}
