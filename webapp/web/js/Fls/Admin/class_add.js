$(document).ready(function () {
    $( ".datepicker" ).datepicker({
        showButtonPanel: true,
        dateFormat: 'dd/mm/yy'
    });

    //when click ok button, reload
    $(".btnOk").click(function () {
        window.location.reload(true);
    })
    //when click cancel button, reload
    $(".btnCancel").click(function () {
        window.location.reload(true)
    })
    $("#btn_sendAjax").click(function () {
        $('#add_success_span').text('');
        $('#name_danger_span').text('');
        $('#date_danger_span').text('');
        var semName = $('#sem-name').val();
        var startDate = $('#sem-start').val();
        var endDate = $('#sem-end').val();
        var dataPass = JSON.stringify({
            'semName': semName,
            'startDate': startDate,
            'endDate': endDate
        });
        var router = $('#dom_data_route').attr('data-route-add-semester');
        $.ajax({
            contentType: 'application/json; charset=utf-8',            
            type: 'POST',
            url: router,
            data: dataPass,
            dataType: 'json'
        }).done(function (data) {
            if (data.status) {
                $('#add_success_span').text(data.message);            
            }else {
                if(data.field == 'name') {
                    $('#name_danger_span').text(data.message);
                }else if(data.field == 'date') {
                    $('#date_danger_span').text(data.message);
                }                
            }
        });
    })
});


function convertTimestamp(timestamp) {
    var date = new Date(timestamp * 1000);
    // Year part from the timestamp
    var year = date.getFullYear();
    // Month part from the timestamp
    var month = String(date.getMonth() + 1).padStart(2, '0');
    // Day part from the timestamp
    var day = String(date.getDate()).padStart(2, '0');
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    var totalDate = day + '/' + month + '/' + year + '';
    return totalDate;
}
