$(document).ready(function () {
  setTable();
  setRowData();
  $("#btnImport").click(function () {
    $("#Classes_File").click();
    $("#Classes_File").on('change', function (e) {
      
      var formdata = new FormData($('#Classes_FileUploadForm').get(0));
      let router = $('#dom_data').attr('data-route-import');
      formdata.append('name', $('#Classes_File').val().replace(/.*(\/|\\)/, ''));
      $.ajax({
        url: router,
        type: "POST",
        data: formdata,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "html"
      })
        .done(function () {
          location.reload();
       //   arr_data = $.parseJSON(data);
          // if(arr_data.error){
          //   swal({
          //     title: "Warning",
          //     text: "Some student is not exist : "+arr_data.user_error["username"],
          //     icon: "warning",
          //     button: "OK!",
          //   });
          // }
          // 
          // rowdata = gridOptions.rowData;
          // rowdata.push($.parseJSON(data));
          // gridOptions.api.setRowData(rowdata);
        
        });
    });

  })
  $("#btnOk").click(function () {
    var cId = gridOptions.api.getSelectedRows()[0].cId;

    setTeacherForClass(cId);
  })

});
function setTable() {
  var columnDefs = [
    { headerName: "ID", field: "cId" },
    { headerName: "Name", field: "cName" },
    { headerName: "Start", field: "cStartTime" },
    { headerName: "End", field: "cEndTime" },
    { headerName: "Teacher", field: "cTeacher" },

    {
      headerName: "Status", field: "cActive",
      'cellRenderer': function (params) {
        let value = params.data.cActive;
        let classText = '';
        let vActive = '';
        if (value == true) {
          classText = "text-success";
          vActive = "Enable"
        } else if (value == false) {
          classText = "text-danger";
          vActive = "Disable"
        } else {
          value = -1;
          classText = "text-dark";
        }
        return '<span class="' + classText + '"><b>' + vActive + '</b></span>';
      }
    },
    {
      headerName: " ", field: "cSet",
      'cellRenderer': function (params) {
        let value = params.data.cId;
        return '<button class="btn btn-secondary" type="button"  data-toggle="modal" data-target="#addTeacher" aria-expanded="false" title="Insert teacher into class">'
          + '<i class="fas fa-chalkboard-teacher"></i> Set Teacher</span></button>';
      },
      
    },
    {
      headerName: "Schedule", field: "cSchedule",
      'cellRenderer': function (params) {
        let value = params.data.cId;
        return '<button class="btn btn-secondary btn_schedule" type="button"  aria-expanded="false" title="Detail schedule">'
          + '<i class="fas fa-chalkboard-teacher"></i><i class="fas fa-calendar-alt"></i></button>';
      },
    },
    {
      headerName: "Submit",
      cellRenderer: function(params) {
          let data = params.data;
          let button = '<button id="bt_start_' + data.cId + '" type="button" class="btn btn-success  btn_start" data-toggle="tooltip" data-placement="top" title="Start" > <i class="fas fa-play"></i> </button>' +
              '<button id="bt_stop_' + data.cId + '" type="button" class="btn btn-danger ml-2 btn_stop" data-toggle="tooltip" data-placement="top" title="Stop"> <i class="fas fa-stop"></i> </button>';
          let rowdata = '<div id="rowdata_hide" data-id="' + data.cId + '" hidden></div>'
          return button + rowdata;
      }
    }
  ];
  // specify the data

  // let the grid know which columns and what data to use
  gridOptions = {
    'columnDefs': columnDefs,
    'rowData': [],
    'singleClickEdit': true,

    'enableColResize': true,
    'enableSorting': true,
    'animateRows': true,
    'rowSelection': 'single',
    'stopEditingWhenGridLosesFocus': true,
    'suppressColumnVirtualisation': true,
    'icons': FlsConstant.iconAgGrid,
    'navigateToNextCell': function (params) {
      var previousCell = params.previousCellPosition;
      var suggestedNextCell = params.nextCellPosition;
      var KEY_UP = 38;
      var KEY_DOWN = 40;
      var KEY_LEFT = 37;
      var KEY_RIGHT = 39;
      switch (params.key) {
        case KEY_DOWN:
          previousCell = params.previousCellPosition;
          var lastRow = gridOptions.rowData.length - 1;
          gridOptions.api.forEachNode(function (node) {
            if (previousCell.rowIndex + 1 === node.rowIndex) {
              node.setSelected(true);
            }
          });
          return suggestedNextCell;
        case KEY_UP:
          previousCell = params.previousCellPosition;
          var lastRow = gridOptions.rowData.length - 1;
          gridOptions.api.forEachNode(function (node) {
            if (previousCell.rowIndex - 1 === node.rowIndex) {
              node.setSelected(true);
            }
          });
          return suggestedNextCell;
        case KEY_LEFT:
        case KEY_RIGHT:
          return suggestedNextCell;
        default:
      }
    },
    onGridReady: function(event) {
      // setRowData(event);
      $(window).resize(function() {
          event.api.sizeColumnsToFit();
      });
      event.api.sizeColumnsToFit();
  },
  };

  // lookup the container we want the Grid to use
  var eGridDiv = document.querySelector('#tb_classes');

  // create the grid passing in the div to use together with the columns & data we want to use
  new agGrid.Grid(eGridDiv, gridOptions);
  gridOptions.api.addEventListener('cellDoubleClicked', GridPress);
  gridOptions.api.addEventListener('cellMouseOver', RowSelection);


  var columnDefs2 = [
    { headerName: "cId", field: "cId", hide: true },
    { headerName: "ID", field: "tId" },
    { headerName: "Name", field: "tName" },
    { headerName: "Username", field: "tUsername" },
    { headerName: "Email", field: "tEmail" },
  ];
  // specify the data

  // let the grid know which columns and what data to use
  gridOptions2 = {
    'columnDefs': columnDefs2,
    'rowData': [],
    'singleClickEdit': true,
    
    'enableColResize': true,
    'enableSorting': true,
    'animateRows': true,
    'rowSelection': 'single',
    'stopEditingWhenGridLosesFocus': true,
    'suppressColumnVirtualisation': true,
    'navigateToNextCell': function (params) {
      var previousCell = params.previousCellPosition;
      var suggestedNextCell = params.nextCellPosition;
      var KEY_UP = 38;
      var KEY_DOWN = 40;
      var KEY_LEFT = 37;
      var KEY_RIGHT = 39;
      switch (params.key) {
        case KEY_DOWN:
          previousCell = params.previousCellPosition;
          var lastRow = gridOptions2.rowData.length - 1;
          gridOptions2.api.forEachNode(function (node) {
            if (previousCell.rowIndex + 1 === node.rowIndex) {
              node.setSelected(true);
            }
          });
          return suggestedNextCell;
        case KEY_UP:
          previousCell = params.previousCellPosition;
          var lastRow = gridOptions2.rowData.length - 1;
          gridOptions2.api.forEachNode(function (node) {
            if (previousCell.rowIndex - 1 === node.rowIndex) {
              node.setSelected(true);
            }
          });
          return suggestedNextCell;
        case KEY_LEFT:
        case KEY_RIGHT:
          return suggestedNextCell;
        default:
      }
    }
  };

  // lookup the container we want the Grid to use
  var eGridDiv = document.querySelector('#tb_teachers');

  // create the grid passing in the div to use together with the columns & data we want to use
  new agGrid.Grid(eGridDiv, gridOptions2);
  setRowData();
  gridOptions2.api.addEventListener('cellDoubleClicked', GridPress2);

}
function setRowData() {
  let rowdata = JSON.parse($('#dom_data').attr('data-list-classes'));
  gridOptions.rowData = rowdata;
  gridOptions.api.setRowData(rowdata);
  gridOptions.api.setFocusedCell(0, "cId");
  gridOptions.api.selectionController.selectIndex(0, true);
  gridOptions.api.sizeColumnsToFit();

  let rowdata2 = JSON.parse($('#dom_data').attr('data-list-teachers'));
  gridOptions2.rowData = rowdata2;
  gridOptions2.api.setRowData(rowdata2);
  gridOptions2.api.setFocusedCell(0, "tId");
  gridOptions2.api.selectionController.selectIndex(0, true);
  gridOptions2.api.sizeColumnsToFit();

  $("#rows").text(rowdata.length);
}
function GridPress(event) {
  let selectedRows = event.data;
  let router = $('#dom_data').attr('data-route-detail') + selectedRows.cId;
  window.location = router;
}
function GridPress2(event) {
  //Validate student exist or not
  //    var isExist = false;
  //  //  var lastIndex;
  //    var rowdata = gridOptions.rowData;
  //    var rowSelected = gridOptions2.api.getSelectedRows();
  //Validate student exist or not
  //  for (var i = 0; i < rowdata.length; i++) {
  //    if (rowSelected[0].uId == rowdata[i].uId) {
  //      swal({
  //        title: "Duplicate",
  //        text: "This student is exist in class!",
  //        icon: "warning",
  //        button: "OK!",
  //      });
  //      isExist = true;
  //    }
  //  }
  // if (isExist == false) {
  //  for (var i = 0; i < rowSelected.length; i++) {
  //    rowdata[i].cTeacher = rowSelected[i].cTeacher;
  //    listStudents_Update.push(rowSelected[i]);
  //  }
  //  gridOptions.api.setRowData(rowdata);
  //  gridOptions.api.setFocusedCell(0, "uId");
  //  gridOptions.api.selectionController.selectIndex(0, true);
  //}
  $("#btnOk").click();
}
function RowSelection(event) {
  
  rowdata = event.data;
  checkButton(rowdata);
  $('.btn_start').unbind('click');
  $('.btn_start').on('click', function () { 
    let router = $('#dom_data').attr('data-route-starttime');
    var dataPass = JSON.stringify(rowdata);
    $.ajax({
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      type: 'POST',
      url: router,
      data: dataPass,
    }).done(function (data) {
      $(".btn_start").prop("disabled", data);
      $(".btn_stop").prop("disabled", !data);
      if (data.freetime == true) {
        rowdata.cStartTime = data.starttime;
        rowdata.cEndTime = data.endtime;
        rowdata.cActive = data.active;
        gridOptions.api.rowDataChanged();
      } else {
        rowdata.cStartTime = data.starttime;
        rowdata.cEndTime = data.endtime;
        rowdata.cActive = data.active;
        gridOptions.api.rowDataChanged();
      }
    });
  })
  //Event button stop
  $('.btn_stop').unbind('click');
  $('.btn_stop').on('click', function () { 
    let router = $('#dom_data').attr('data-route-stoptime');
    var dataPass = JSON.stringify(rowdata);
    $.ajax({
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      type: 'POST',
      url: router,
      data: dataPass,
    }).done(function (data) {
      $("#btnStart").prop("disabled", data.active);
      $("#btnStop").prop("disabled", !data.active);
      rowdata.cEndTime = data.endtime;
      rowdata.cActive = data.active;
      gridOptions.api.rowDataChanged();
    });
  })
  //Event detail schedule
  $('.btn_schedule').unbind('click');
  $('.btn_schedule').on('click', function () {
    var rowIndex = gridOptions.api.getSelectedNodes()[0].rowIndex;
    var rowdata  = gridOptions.rowData[rowIndex];
    var router = $('#dom_data').attr('data-route-schedule');
    var dataPass = JSON.stringify(rowdata);
   $.ajax({
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    type: 'POST',
    url: router,
    data: dataPass,
  }).done(function () {
  });
  });
}
function setTeacherForClass(cId) {
  
  var rowSelected = gridOptions.api.getSelectedNodes()[0].rowIndex
  var rowSelected2 = gridOptions2.api.getSelectedRows();
  rowSelected2[0].cId = cId;
  gridOptions.rowData[rowSelected].cTeacher = rowSelected2[0].tUsername;
  gridOptions.api.rowDataChanged();

  var route = $('#dom_data').attr('data-route-add-teacher');
  // rowSelected = gridOptions2.api.getSelectedRows();
  var dataUpdate = JSON.stringify(rowSelected2[0]);
  $.ajax({
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    type: 'POST',
    url: route,
    data: dataUpdate,
  }).done(function (data) { 
    
    if (data.isSuccess == true) {
      swal({
        title: "Success",
        text: "The teacher has been added to the classroom!",
        icon: "success",
        button: "OK!",
      });
    }
  });
}
function checkButton(data) {
  var isActive = data.cActive;
  $("#btnStart").prop("disabled", isActive);
  $("#btnStop").prop("disabled", !isActive);
}
