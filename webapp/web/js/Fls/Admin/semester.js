$(document).ready(function () {
    $( ".datepicker" ).datepicker({
        showButtonPanel: true,
        dateFormat: 'dd/mm/yy'
    });
    if (document.getElementById("tb_semester") != null) {
        setTable();
        setRowData();
    }

    //when click ok button, reload
    $(".btnOk").click(function () {
        window.location.reload(true);
    })
    //when click cancel button, reload
    $(".btnCancel").click(function () {
        window.location.reload(true)
    })
    $("#btn_sendAjax").click(function () {
        $('#add_success_span').text('');
        $('#name_danger_span').text('');
        $('#date_danger_span').text('');
        var semName = $('#sem-name').val();
        var startDate = $('#sem-start').val();
        var endDate = $('#sem-end').val();
        var dataPass = JSON.stringify({
            'semName': semName,
            'startDate': startDate,
            'endDate': endDate
        });
        var router = $('#dom_data_route').attr('data-route-add-semester');
        $.ajax({
            contentType: 'application/json; charset=utf-8',            
            type: 'POST',
            url: router,
            data: dataPass,
            dataType: 'json'
        }).done(function (data) {
            if (data.status) {
                $('#add_success_span').text(data.message);            
            }else {
                if(data.field == 'name') {
                    $('#name_danger_span').text(data.message);
                }else if(data.field == 'date') {
                    $('#date_danger_span').text(data.message);
                }                
            }
        });
    })
    //change submit
    $("#btn_sendAjaxChange").click(function () {
        $('#name_change_danger_span').text('');
        $('#date_change_danger_span').text('');
        $('#change_success_span').text('');    
        var semId = $('#sem-id').val();
        var semName = $('#sem-name-change').val();
        var startDate = $('#sem-change-start').val();
        var endDate = $('#sem-change-end').val();
        var dataPass = JSON.stringify({
            'semId': semId,
            'semName': semName,
            'startDate': startDate,
            'endDate': endDate
        });
        var router = $('#dom_data_route').attr('data-route-edit-semester');
        $.ajax({
            contentType: 'application/json; charset=utf-8',            
            type: 'POST',
            url: router,
            data: dataPass,
            dataType: 'json'
        }).done(function (data) {
            if (data.status) {
                $('#change_success_span').text(data.message);            
            }else {
                if(data.field == 'name') {
                    $('#name_change_danger_span').text(data.message);
                }else if(data.field == 'date') {
                    $('#date_change_danger_span').text(data.message);
                }                
            }
        });
    })
});

function setTable() {
    var columnDefs = [{
            headerName: "Semester ID",
            field: "semid",
            hide: true
        },
        {
            headerName: "Semester Name",
            field: "name"
        },
        {
            headerName: "Start Date",
            field: "startedTime",
            'cellRenderer': function (params) {
                let value = params.data.startedTime;
                let classText = 'text-dark';
                let startTime = '-'
                if (value != '-') {
                    startTime = convertTimestamp(value);
                }
                return '<span class="' + classText + '"><b>' + startTime + '</b></span>';
            }
        },
        {
            headerName: "End Date",
            field: "finishedTime",
            'cellRenderer': function (params) {
                let value = params.data.finishedTime;
                let endTime = '-';
                let classText = '';
                if (value == 'freetime') {
                    endTime = value;
                    classText = 'text-success';
                } else if (value != '-') {
                    endTime = convertTimestamp(value);
                    classText = 'text-dark';
                }

                return '<span class="' + classText + '"><b>' + endTime + '</b></span>';
            }
        },
        {
            headerName: "Edit",
            cellRenderer: function (params) {
                let data = params.data;
                let button = '<button data-id="' + data.semid + '" data-name="' + data.name + '" data-start="' + data.startedTime + '" data-end="' + data.finishedTime + '" type="button" class="btn btn-success ml-2 btn_edit" data-toggle="modal" data-target="#editSemester" title="edit" > <i class="fas fa-edit"></i> </button>';
                // <button type="button" id="btnAddSemester" class="btn btn-success" data-toggle="modal" data-target="#addSemester" aria-expanded="false">
                return button;
            }
        },
    ];
    // specify the data

    // let the grid know which columns and what data to use
    gridOptions = {
        'columnDefs': columnDefs,
        'rowData': [],
        'singleClickEdit': true,
        'enableColResize': true,
        'enableSorting': true,
        'animateRows': true,
        'rowSelection': 'single',
        'icons': FlsConstant.iconAgGrid,
        'stopEditingWhenGridLosesFocus': true,
        'suppressColumnVirtualisation': true,
        'navigateToNextCell': function (params) {
            var previousCell = params.previousCellPosition;
            var suggestedNextCell = params.nextCellPosition;
            var KEY_UP = 38;
            var KEY_DOWN = 40;
            var KEY_LEFT = 37;
            var KEY_RIGHT = 39;
            switch (params.key) {
                case KEY_DOWN:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex + 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_UP:
                    previousCell = params.previousCellPosition;
                    var lastRow = gridOptions.rowData.length - 1;
                    gridOptions.api.forEachNode(function (node) {
                        if (previousCell.rowIndex - 1 === node.rowIndex) {
                            node.setSelected(true);
                        }
                    });
                    return suggestedNextCell;
                case KEY_LEFT:
                case KEY_RIGHT:
                    return suggestedNextCell;
                default:
            }
        },
        onGridReady: function (event) {
            $(window).resize(function () {
                event.api.sizeColumnsToFit();
            });
            event.api.sizeColumnsToFit();
        },
    };

    // lookup the container we want the Grid to use
    var eGridDiv = document.querySelector('#tb_semester');

    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridDiv, gridOptions);
    gridOptions.api.addEventListener('rowDoubleClicked', GridPress);
    gridOptions.api.addEventListener('cellMouseOver', onRowClick);
    setRowData();
}

function setRowData() {
    let rowdata = JSON.parse($('#dom_data').attr('data-semester'));
    gridOptions.rowData = rowdata;
    gridOptions.api.setRowData(rowdata);
    // gridOptions.api.selectionController.selectIndex(0, true);
    gridOptions.api.sizeColumnsToFit();
    $("#rows").text(rowdata.length);
}

function GridPress(event) {
    let selectedRows = gridOptions.api.getSelectedRows();
    // let router = $('#dom_data').attr('data-route') + selectedRows[0].semid;
    window.location = router;
}

// event row click
function onRowClick() {
    $('.btn_edit').unbind('click');
    $('.btn_edit').click(function () {
        gridOptions.api.addEventListener('rowSelected', onRowClick);
        var sem_id = $(this).attr('data-id');
        $('#sem-id').val(sem_id);
        var sem_name = $(this).attr('data-name');
        var sem_start = $(this).attr('data-start');
        var sem_end = $(this).attr('data-end');
        $('#old_name_span').text(sem_name);
        $('#old_start_span').text(convertTimestamp(sem_start)); 
        $('#old_end_span').text(convertTimestamp(sem_end));
        $('#sem-name-change').val(sem_name);
        $('#sem-change-start').val(convertTimestamp(sem_start)); 
        $('#sem-change-end').val(convertTimestamp(sem_end));
    });
};

function convertTimestamp(timestamp) {
    var date = new Date(timestamp * 1000);
    // Year part from the timestamp
    var year = date.getFullYear();
    // Month part from the timestamp
    var month = String(date.getMonth() + 1).padStart(2, '0');
    // Day part from the timestamp
    var day = String(date.getDate()).padStart(2, '0');
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    var totalDate = day + '/' + month + '/' + year + '';
    return totalDate;
}
